package com.wstuo.itsm.updateRequest.dto;

import com.wstuo.common.dto.BaseDTO;

@SuppressWarnings("serial")
public class UpdateRequestDTO extends BaseDTO{

	private Long id;
	private Long timeLong;
	private Boolean enable = false;
	private Integer day;
	private Integer hour;
	private Integer minute;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getTimeLong() {
		return timeLong;
	}
	public void setTimeLong(Long timeLong) {
		this.timeLong = timeLong;
	}
	public Boolean getEnable() {
		return enable;
	}
	public void setEnable(Boolean enable) {
		this.enable = enable;
	}
	public Integer getDay() {
		return day;
	}
	public void setDay(Integer day) {
		this.day = day;
	}
	public Integer getHour() {
		return hour;
	}
	public void setHour(Integer hour) {
		this.hour = hour;
	}
	public Integer getMinute() {
		return minute;
	}
	public void setMinute(Integer minute) {
		this.minute = minute;
	}
	
}
