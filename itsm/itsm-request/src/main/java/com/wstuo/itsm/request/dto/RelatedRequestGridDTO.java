package com.wstuo.itsm.request.dto;

import java.util.Date;

import com.wstuo.common.dto.BaseDTO;

/**
 * 关联请求Grid DTO
 * @author WSTUO_QXY
 *
 */
@SuppressWarnings("serial")
public class RelatedRequestGridDTO extends BaseDTO {
	/**
	 * 关联ID
	 */
	private Long relatedId;
	/**
	 * 请求Eno
	 */
	private Long eno;
	/**
	 * 请求分类名称
	 */
	private String ecategoryName;
	/**
	 * 请求标题
	 */
    private String etitle;
    /**
	 * 请求编号
	 */
    private String requestCode;
    /**
	 * 请求创建人
	 */
    private String createdByName;
    /**
	 * 请求技术员
	 */
    private String assigneeName;
    /**
	 * 请求优先级
	 */
    private String priorityName;
    /**
	 * 请求状态
	 */
    private String statusName;
    /**
	 * 请求创建时间
	 */
    private Date createdOn;
    /**
	 * 请求Sla状态
	 */
    private String slaState;
    /**
	 * 请求技术员全名
	 */
    private String assigneeFullName;
    
    
	
    
    
	public String getAssigneeFullName() {
		return assigneeFullName;
	}
	public void setAssigneeFullName(String assigneeFullName) {
		this.assigneeFullName = assigneeFullName;
	}
	public Long getRelatedId() {
		return relatedId;
	}
	public void setRelatedId(Long relatedId) {
		this.relatedId = relatedId;
	}
	public Long getEno() {
		return eno;
	}
	public void setEno(Long eno) {
		this.eno = eno;
	}
	public String getEcategoryName() {
		return ecategoryName;
	}
	public void setEcategoryName(String ecategoryName) {
		this.ecategoryName = ecategoryName;
	}
	public String getEtitle() {
		return etitle;
	}
	public void setEtitle(String etitle) {
		this.etitle = etitle;
	}
	public String getRequestCode() {
		return requestCode;
	}
	public void setRequestCode(String requestCode) {
		this.requestCode = requestCode;
	}
	public String getCreatedByName() {
		return createdByName;
	}
	public void setCreatedByName(String createdByName) {
		this.createdByName = createdByName;
	}
	public String getAssigneeName() {
		return assigneeName;
	}
	public void setAssigneeName(String assigneeName) {
		this.assigneeName = assigneeName;
	}
	public String getPriorityName() {
		return priorityName;
	}
	public void setPriorityName(String priorityName) {
		this.priorityName = priorityName;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public String getSlaState() {
		return slaState;
	}
	public void setSlaState(String slaState) {
		this.slaState = slaState;
	}
    
}
