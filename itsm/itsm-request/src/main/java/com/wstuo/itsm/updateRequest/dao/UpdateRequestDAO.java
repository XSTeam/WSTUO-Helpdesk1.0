package com.wstuo.itsm.updateRequest.dao;

import com.wstuo.itsm.updateRequest.entity.UpdateRequest;
import com.wstuo.common.dao.BaseDAOImplHibernate;

/**
 * 升级时间DAO class
 * 
 * @author Will
 * 
 */
public class UpdateRequestDAO extends BaseDAOImplHibernate<UpdateRequest> implements IUpdateRequestDAO {

}
