package com.wstuo.itsm.request.dto;

import java.io.Serializable;

/**
 * 流程任务变更DTO
 * @author WSTUO_QXY
 *
 */
@SuppressWarnings("serial")
public class JbpmTaskVariablesEventDTO  implements Serializable{
	private static long serialVersionUID =1;
	private Long eno;
	private String ecode;
	private String etitle;
	private Long assigneeNo;
    private String assigneeName;
    private Long assigneeGroupNo;
    private String assigneeGroupName;
    private Long approvalNo;
    private String approvalName;
    private String pid;
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
	public static void setSerialVersionUID(long serialVersionUID) {
		JbpmTaskVariablesEventDTO.serialVersionUID = serialVersionUID;
	}
	public Long getEno() {
		return eno;
	}
	public void setEno(Long eno) {
		this.eno = eno;
	}
	public String getEcode() {
		return ecode;
	}
	public void setEcode(String ecode) {
		this.ecode = ecode;
	}
	public String getEtitle() {
		return etitle;
	}
	public void setEtitle(String etitle) {
		this.etitle = etitle;
	}
	public Long getAssigneeNo() {
		return assigneeNo;
	}
	public void setAssigneeNo(Long assigneeNo) {
		this.assigneeNo = assigneeNo;
	}
	public String getAssigneeName() {
		return assigneeName;
	}
	public void setAssigneeName(String assigneeName) {
		this.assigneeName = assigneeName;
	}
	public Long getAssigneeGroupNo() {
		return assigneeGroupNo;
	}
	public void setAssigneeGroupNo(Long assigneeGroupNo) {
		this.assigneeGroupNo = assigneeGroupNo;
	}
	public String getAssigneeGroupName() {
		return assigneeGroupName;
	}
	public void setAssigneeGroupName(String assigneeGroupName) {
		this.assigneeGroupName = assigneeGroupName;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public Long getApprovalNo() {
		return approvalNo;
	}
	public void setApprovalNo(Long approvalNo) {
		this.approvalNo = approvalNo;
	}
	public String getApprovalName() {
		return approvalName;
	}
	public void setApprovalName(String approvalName) {
		this.approvalName = approvalName;
	}
    
}
