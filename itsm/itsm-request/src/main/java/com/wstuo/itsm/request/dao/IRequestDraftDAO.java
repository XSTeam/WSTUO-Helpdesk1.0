package com.wstuo.itsm.request.dao;

import com.wstuo.itsm.request.dto.RequestDraftQueryDTO;
import com.wstuo.itsm.request.entity.RequestDraft;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

/**
 * 事件统计DAO接口类
 * @author WSTUO_QXY
 *
 */
public interface IRequestDraftDAO extends IEntityDAO<RequestDraft> {
	
	PageDTO findPageRequestDraft(RequestDraftQueryDTO queryDTO);
}
