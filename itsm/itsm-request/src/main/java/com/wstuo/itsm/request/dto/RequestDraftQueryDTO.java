package com.wstuo.itsm.request.dto;


/**
 * 定期任务搜索DTO
 * @author WSTUO_QXY
 *
 */
public class RequestDraftQueryDTO{
	private Long requestDraftID;//ID
	private String keywork;
	private String sord;
    private String sidx;
    private int start;
    private int limit = 10;
    private Long companyNo;
    private String creator;
    private String requestDraftName;
    private String loginName;
	
    
    
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public Long getRequestDraftID() {
		return requestDraftID;
	}
	public void setRequestDraftID(Long requestDraftID) {
		this.requestDraftID = requestDraftID;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getRequestDraftName() {
		return requestDraftName;
	}
	public void setRequestDraftName(String requestDraftName) {
		this.requestDraftName = requestDraftName;
	}
	public String getKeywork() {
		return keywork;
	}
	public void setKeywork(String keywork) {
		this.keywork = keywork;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public Long getCompanyNo() {
		return companyNo;
	}
	public void setCompanyNo(Long companyNo) {
		this.companyNo = companyNo;
	}

    
}
