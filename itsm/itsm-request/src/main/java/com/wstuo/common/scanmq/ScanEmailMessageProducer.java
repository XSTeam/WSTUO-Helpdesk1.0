package com.wstuo.common.scanmq;

import javax.jms.Destination;

import org.springframework.jms.core.JmsTemplate;

import com.wstuo.common.tools.dto.ExportPageDTO;

/**
 * 发送消息
 * @author WSTUO
 *
 */
public class ScanEmailMessageProducer {
	private JmsTemplate template;

	private Destination destination;


	public void setTemplate(JmsTemplate template) {
	   this.template = template;
	}

	public void setDestination(Destination destination) {
	   this.destination = destination;
	}

	/**
	 * 发送
	 * @param order
	 */
	public void send(ExportPageDTO order) {
	  template.convertAndSend(this.destination, order);
	} 
}
