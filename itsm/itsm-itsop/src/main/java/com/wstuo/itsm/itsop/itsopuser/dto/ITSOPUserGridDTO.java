package com.wstuo.itsm.itsop.itsopuser.dto;
import com.wstuo.common.dto.BaseDTO;

/**
 * 外包客户管理列表数据传递类
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class ITSOPUserGridDTO extends BaseDTO{

	private Long orgNo;
	private String orgName;
	private String address;
	private String email;
	private String officePhone;
	private String officeFax;
	private String [] technicianNames;
	private String [] technicianNameFullName;//负责的技术员真实姓名
	
	private String corporate;//法人代表
	private String companySize;//公司规模
	private String regNumber;//工商注册号;
	private Long regCapital;//注册资金
	
	private Long typeNo;
	private String typeName;
	private String orgType;


	public String getCorporate() {
		return corporate;
	}
	public void setCorporate(String corporate) {
		this.corporate = corporate;
	}
	public String getCompanySize() {
		return companySize;
	}
	public void setCompanySize(String companySize) {
		this.companySize = companySize;
	}
	public String getRegNumber() {
		return regNumber;
	}
	public void setRegNumber(String regNumber) {
		this.regNumber = regNumber;
	}
	public Long getRegCapital() {
		return regCapital;
	}
	public void setRegCapital(Long regCapital) {
		this.regCapital = regCapital;
	}
	public String[] getTechnicianNames() {
		return technicianNames;
	}
	public void setTechnicianNames(String[] technicianNames) {
		this.technicianNames = technicianNames;
	}

	public Long getOrgNo() {
		return orgNo;
	}
	public void setOrgNo(Long orgNo) {
		this.orgNo = orgNo;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getOfficePhone() {
		return officePhone;
	}
	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}
	public String getOfficeFax() {
		return officeFax;
	}
	public void setOfficeFax(String officeFax) {
		this.officeFax = officeFax;
	}
	public Long getTypeNo() {
		return typeNo;
	}
	public void setTypeNo(Long typeNo) {
		this.typeNo = typeNo;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getOrgType() {
		return orgType;
	}
	public void setOrgType(String orgType) {
		this.orgType = orgType;
	}
	public String[] getTechnicianNameFullName() {
		return technicianNameFullName;
	}
	public void setTechnicianNameFullName(String[] technicianNameFullName) {
		this.technicianNameFullName = technicianNameFullName;
	}

	
}
