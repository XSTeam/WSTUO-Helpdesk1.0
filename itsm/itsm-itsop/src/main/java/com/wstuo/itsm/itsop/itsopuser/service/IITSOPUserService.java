package com.wstuo.itsm.itsop.itsopuser.service;

import java.io.InputStream;

import com.wstuo.itsm.itsop.itsopuser.dto.ITSOPUserDTO;
import com.wstuo.itsm.itsop.itsopuser.dto.ITSOPUserQueryDTO;
import com.wstuo.common.dto.PageDTO;

/**
 * 外包客户的业务处理类
 * @author QXY
 *
 */
public interface IITSOPUserService {
	/**
	 * 超过外包客户许可数量
	 */
	public static final String OVER_ITSOP_LICENSE_NUMBER = "over_itsop_license_number";
	
	/**
	 * 分页查找外包客户.
	 * @param qdto
	 * @return PageDTO
	 */
	PageDTO findITSOPUserPager(ITSOPUserQueryDTO qdto);

	/**
	 * 添加外包客户.
	 * @param dto
	 * @throws Exception 
	 */
	String addITSOPUser(ITSOPUserDTO dto);
	/**
	 * 修改外包客户.
	 * @param dto
	 */
	void updateITSOPUser(ITSOPUserDTO dto);
	/**
	 * 删除外包客户（多个）.
	 * @param ids
	 */
	void deleteITSOPUser(Long [] ids);
	
	/**
	 * 删除外包客户（单个）.
	 * @param id
	 */
	void deleteITSOPUser(Long id);
	/**
	 * 我所在的公司和我负责的客户
	 * @return Long[]
	 */
	Long[] findMyAllCustomer(String loginName);
	
	/**
	 * 分页查找在的公司和我负责外包客户.
	 * @param qdto
	 * @return PageDTO
	 */
	PageDTO findMySupportITSOPUserPager(ITSOPUserQueryDTO qdto);
	
    /**
     * 全文检索导出.
     */
	InputStream exportITSOPUser(ITSOPUserQueryDTO qdto);
	
    /**
     * 导入
     */
	String importITSOPUser(java.io.File file,int fix);
	/**
	 * 自动补全验证用户
	 * @param orgName
	 * @return Long[]
	 */
	Long[] autoVerification( String orgName );
	
	/**
	 * 根据登录账号获取相关的公司信息(负责的公司、所在的公司)
	 * @param loginName
	 * @return 公司编号Long[]
	 */
	Long[] getRelatedCompanyByLoginName(String loginName);
	/**
	 * 取外包总数
	 * @return
	 */
	int getitsopAlreadyUsed();
	
}
