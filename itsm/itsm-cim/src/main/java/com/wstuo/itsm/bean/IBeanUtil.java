package com.wstuo.itsm.bean;

import java.util.Map;
/**
 * 把beanutil独立一下
 * @author gs60
 *
 */
public interface IBeanUtil {
	/**
	 * Map转换成具体的bean
	 */
	void populate(final Object object,final Map<String,Object> lineData);
}
