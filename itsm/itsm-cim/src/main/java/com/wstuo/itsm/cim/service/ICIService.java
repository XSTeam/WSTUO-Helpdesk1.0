package com.wstuo.itsm.cim.service;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import com.wstuo.common.config.customfilter.dto.KeyTransferDTO;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.tools.dto.ExportPageDTO;
import com.wstuo.common.tools.dto.ExportQueryDTO;
import com.wstuo.itsm.cim.dto.BehaviorModificationDTO;
import com.wstuo.itsm.cim.dto.CIDTO;
import com.wstuo.itsm.cim.dto.CIDetailDTO;
import com.wstuo.itsm.cim.dto.CIGridDTO;
import com.wstuo.itsm.cim.dto.CIHistoryUpdateDTO;
import com.wstuo.itsm.cim.dto.CIQueryDTO;
import com.wstuo.itsm.cim.dto.CIScanDTO;
import com.wstuo.itsm.cim.dto.CiHardwareDTO;
import com.wstuo.itsm.cim.dto.CiSoftwareDTO;
import com.wstuo.itsm.cim.dto.HardwareDTO;
import com.wstuo.itsm.cim.entity.BehaviorModification;
import com.wstuo.itsm.itsop.itsopuser.dto.CustomerDataCountDTO;

/***
 * 配置项Service类接口
 * 
 * @author QXY
 */
public interface ICIService {
	public static final String ENTITYCLASS="com.wstuo.itsm.cim.entity.CI";
	public static final String FILTERCATEGORY="CI";
	/**
	 * 配置项分页查询
	 * 
	 * @param ciQueryDTO
	 *            :配置项查询条件属性DTO
	 * @param start
	 *            :第几页
	 * @param limit
	 *            ：每页数
	 * @return String PageDTO:分页数据集
	 */
	public PageDTO findPagerConfigureItem(final CIQueryDTO ciQueryDTO,
			int start, int limit, String sidx, String sord);

	/**
	 * 自定义分页查询配置项列表.
	 * 
	 * @return String
	 */
	PageDTO findConfigureItemPagerByCustomtFilter(CIQueryDTO ciQueryDTO,
			int start, int limit, String sidx, String sord);

	/**
	 * 根据ID查询配置项
	 * 
	 * @param ciId
	 *            :配置项ID
	 * @return String CIDTO
	 */
	public CIDetailDTO findConfigureItemById(Long ciId);

	/***
	 * 保存配置项
	 * 
	 * @param ciDTO
	 *            :要保存到数据库的DTO数据值
	 * @return String void
	 */

	/**
	 * 删除影响附件
	 * 
	 * @param aid
	 */
	void deleteAttachement(Long ciId, Long aid);
	/***
	 * 保存配置项
	 * 
	 * @param ciDTO
	 *要保存到数据库的DTO数据值
	 */
	public void addConfigureItem(CIDTO ciDTO,HardwareDTO hardwareDTO);

	/**
	 * 配置项批量删除
	 * 要删除的配置项ID数组
	 * @return boolean
	 */
	public boolean deleteConfigureItem(Long[] ciId);

	/***
	 * 更新配置项
	 * 要更新到数据库的DTO数据值
	 */
	public void updateConfigureItem(CIDTO ciDTO, HardwareDTO hardwareDTO);

	/**
	 * 根据CI ID判断是否存在
	 * 
	 * @param ciId
	 *            :查要判断的ID
	 * @return String boolean
	 */
	public boolean isConfigureItemExist(Long ciId);

	/**
	 * 判断配置项编号是否存在
	 * 
	 * @param ciNo
	 * @return String
	 */
	boolean isConfigureItemExist(String ciNo);

	/**
	 * 保存电脑配置项信息
	 * 
	 * @param ciId
	 * @param hardwareDTO
	 * @param softwareDTO
	 */
	void saveCiConfigInfo(Long ciId, CiHardwareDTO hardwareDTO,
			CiSoftwareDTO softwareDTO);

	/**
	 * 保存CI配置信息
	 * 
	 * @param hardwareDTO
	 */
	void saveHardware(CiHardwareDTO hardwareDTO);

	/**
	 * 获取硬件配置信息
	 * 
	 * @param ciId
	 * @return String
	 */
	CiHardwareDTO getCiHardware(Long ciId);

	/**
	 * 获取软件安装信息
	 * 
	 * @param ciId
	 * @return String PageDTO
	 */
	PageDTO getCiSoftware(Long ciId, int page, int limit);

	/**
	 * 定时删除导出文件
	 */
	void deleteExportFile();

	/**
	 * 导出到CSV.
	 * 
	 */
	void exportConfigureItems(CIQueryDTO ciQueryDTO, String sidx, String sord);

	/**
	 * 根据过滤器导出.
	 * 
	 * @param ciQueryDTO
	 */
	void exportConfigureItemsByFilter(CIQueryDTO ciQueryDTO, String sidx,
			String sord);

	/**
	 * 下载导出文件
	 * 
	 * @param fileName
	 * @return String
	 */
	InputStream exportDownLoad(String fileName);
	/**
	 * 从CSV文件进行导入 importFile 导入文件
	 */
	String importCI(File importFile, CIDTO ciDto);

	/**
	 * 负责客户的数据统计
	 * 
	 * @return String List<CustomerDataCountDTO>
	 */
	List<CustomerDataCountDTO> configureItemDataCountByCompanyNo(String loginName);

	/**
	 * 软件使用许可统计
	 * 
	 * @param softwareName
	 * @return String
	 */
	Integer softwareLicenseUseCount(String softwareName);

	/**
	 * 判断是否是软件
	 * 
	 * @return Integer
	 */
	Integer softwareLicenseTotal(Long ciId);

	/**
	 * 判断配置项生命周期与保修期是否已到期
	 * @return String
	 */
	String calcExpired(Long ciid);

	/**
	 * 根据用户查询所关联的配置项
	 * 
	 * @param ciQueryDTO
	 * @param start
	 * @param limit
	 * @param sidx
	 * @param sord
	 * @return String
	 */
	PageDTO findPageConfigureItemByUser(CIQueryDTO ciQueryDTO, int start,
			int limit, String sidx, String sord);
	/**
	 * 导入WMI文件
	 */
	String importWMIFile(File importFile, CIDTO ciDto);
	/**
	 * 获取配置信息
	 */
	HardwareDTO findConfigureItenHardware(Long ciId);

	/**
	 * 添加关联软件
	 * @param ciId
	 * @param softwareIds
	 */
	void addConfigureItemSoftware(final Long ciId, Long[] softwareIds);

	/**
	 * 删除关联软件
	 * @param ciId
	 * @param softwareIds
	 */
	void deleteConfigureItemSoftware(final Long ciId, final Long[] softwareIds);

	/**
	 * 文件下载信息
	 * 
	 * @param p
	 */
	void exportData(ExportPageDTO p);

	/**
	 * 根据ip更新硬件信息
	 */
	public void scanToolUpdate(String configJson);
	/**
	 * 根据编号更新硬件信息
	 */
	public void scanTooloneUpdate(String ciNo, String configJson);

	/**
	 * 配置项历史更新查询
	 * 
	 * @throws ClassNotFoundException
	 * */
	public List<CIHistoryUpdateDTO> findCiHistoryUpdate(Long ciid);

	/**
	 * 配置项历史更新查询ID
	 * 
	 * @throws ClassNotFoundException
	 * */
	public CIHistoryUpdateDTO findCiHistoryUpdateID(Long id);

	/**
	 * 根据条件查询导出数据
	 * 
	 * @param dto
	 */
	void commonExportCIData(ExportQueryDTO exportQueryDTO);

	/**
	 * 添加配置项附件
	 */
	void saveCIAttachment(Long ciId, String attachmentStr, String eventType,
			Long aid);

	/**
	 * 查询动态报表配置项列表
	 * @param rows
	 * @param start
	 * @param sidx
	 * @param sord
	 * @return String
	 */
	PageDTO findConfigureItemsByCrosstabCell(KeyTransferDTO ktd, int start, int rows, String sidx, String sord);
	/**
	 * 删除软件附件
	 * 
	 * @param aid
	 */
	void deleteSoftAttachement(Long ciId, Long aid);

	/**
	 * 更具配置项编号查询 cino,并返回二维码配置项信息
	 * 
	 * */
	public CIScanDTO findConfigureItemByCINO(String cino);

	/**
	 * 更具配置项id查询 ciid,并返回二维码配置项信息
	 * 
	 * */
	CIScanDTO findConfigureItemByCIID(Long ciid);
	/**
	 * ACL更新，已失效
	 * (non-Javadoc)
	 * @see com.wstuo.itsm.cim.service.ICIService#aclUpUpdate()
	 */
	void aclUpUpdate();

	/**
	 * 根据配置项硬件ip查询配置项信息
	 */
	public CIDetailDTO findByHardwareIp(String hardwareIP);

	/**
	 * 根据配置项eno查询配置项信息
	 */
	public CIDetailDTO findByCieno(String findCino);

	/**
	 * 根据数组ids查看详情
	 * 
	 * @param ids
	 * @return String
	 */
	public List<CIGridDTO> findByIds(Long[] ids);
	
	/**
	 * 模板值转换
	 * @param ciDto
	 * @return String
	 */
	public CIDTO convertCiTemplateValue(CIDTO ciDto);
	/**
	 * 查询修改行为
	 * */
    public List<BehaviorModificationDTO> findBehaviorModification(CIDTO cidto);
    
    /**
     * 根据ID查找修改行为；
     * @param beId
     * @return
     */
    public BehaviorModificationDTO findBehaviorModificationByid(Long beId);
    
    
    
    
    
    
}
