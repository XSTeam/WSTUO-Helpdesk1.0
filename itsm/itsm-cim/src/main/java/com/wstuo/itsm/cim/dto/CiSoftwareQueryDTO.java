package com.wstuo.itsm.cim.dto;

import java.util.Date;
import com.wstuo.common.dto.BaseDTO;
/**
 * 配置项安装的软件
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class CiSoftwareQueryDTO extends BaseDTO {
	private Long softwareId;//软件ID
	private String softwareName;//软件名
	private String softwareVersion;//软件版本
	private Long softwareTypeId;//软件类型
	private Long softwareTypeName;
    private Long softwareCategoryId; //软件分类
    private String softwareCategoryName;
    private Long softwareProviderId;//软件供应商
    private String softwareProviderName;
	private String installDate;//安装时间
	private String description;//描述
	private Date createTime;//创建时间
	private Date lastUpdateTime;//更新时间
	private Integer start;
    private Integer limit = 10;
    private String sidx;
    private String sord;
    
	public Long getSoftwareId() {
		return softwareId;
	}
	public void setSoftwareId(Long softwareId) {
		this.softwareId = softwareId;
	}
	public String getSoftwareName() {
		return softwareName;
	}
	public void setSoftwareName(String softwareName) {
		this.softwareName = softwareName;
	}
	public String getSoftwareVersion() {
		return softwareVersion;
	}
	public void setSoftwareVersion(String softwareVersion) {
		this.softwareVersion = softwareVersion;
	}

	public String getInstallDate() {
		return installDate;
	}
	public void setInstallDate(String installDate) {
		this.installDate = installDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	public Long getSoftwareTypeId() {
		return softwareTypeId;
	}
	public void setSoftwareTypeId(Long softwareTypeId) {
		this.softwareTypeId = softwareTypeId;
	}
	public Long getSoftwareTypeName() {
		return softwareTypeName;
	}
	public void setSoftwareTypeName(Long softwareTypeName) {
		this.softwareTypeName = softwareTypeName;
	}
	public Long getSoftwareCategoryId() {
		return softwareCategoryId;
	}
	public void setSoftwareCategoryId(Long softwareCategoryId) {
		this.softwareCategoryId = softwareCategoryId;
	}
	public String getSoftwareCategoryName() {
		return softwareCategoryName;
	}
	public void setSoftwareCategoryName(String softwareCategoryName) {
		this.softwareCategoryName = softwareCategoryName;
	}
	public Long getSoftwareProviderId() {
		return softwareProviderId;
	}
	public void setSoftwareProviderId(Long softwareProviderId) {
		this.softwareProviderId = softwareProviderId;
	}
	public String getSoftwareProviderName() {
		return softwareProviderName;
	}
	public void setSoftwareProviderName(String softwareProviderName) {
		this.softwareProviderName = softwareProviderName;
	}
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	
}
