package com.wstuo.itsm.cim.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.noticeRule.dto.NoticeInfoDTO;
import com.wstuo.common.noticeRule.service.INoticeRuleService;
import com.wstuo.common.security.dao.IUserDAO;
import com.wstuo.common.security.entity.User;
import com.wstuo.itsm.cim.dao.ICIDAO;
import com.wstuo.itsm.cim.dao.ICIHistoryNoticeDAO;
import com.wstuo.itsm.cim.dto.CIDTO;
import com.wstuo.itsm.cim.dto.CIHistoryNoticeQueryDTO;
import com.wstuo.itsm.cim.entity.CIHistoryNotice;
import com.wstuo.itsm.cim.entity.CI;

/**
 * 配置基生命周期过期提醒
 * 
 * @author WSTUO_QXY
 * 
 */
public class CiExpiredService implements ICiExpiredService{

	@Autowired
	private ICIDAO ciDAO;
	@Autowired
	private ICIHistoryNoticeDAO ciHistoryNoticeDAO;
	@Autowired
	private INoticeRuleService noticeRuleService;
	@Autowired
	private IUserDAO userDAO;

	private static final Long LIFE_CYCLE_NOTCE_TYPE = 1L;
	private static final Long ARRIVAL_DATE_NOTCE_TYPE = 2L;
	private static final Long WARNING_DATE_NOTCE_TYPE = 0L;
	
	/**
	 * 配置项检查是否到期通知
	 */
	@Transactional
	public void configureItemDetectExpiredNotice() {
		// 配置项生命周期到期通知
		ciLifeCycleExpiredNotice();
		// 配置项保修期到期通知
		ciWarrantyExpiredNotice();
		// 配置项预警时间到期通知
		ciWarningDateExpiredNotice();
	}
	
	/**
	 * set CIHistoryNoticeQueryDTO
	 * @param ci
	 * @param type
	 */
	public CIHistoryNoticeQueryDTO setCIHistoryNoticeQueryDTO(CI ci,Long type){
		CIHistoryNoticeQueryDTO chnDTO = new CIHistoryNoticeQueryDTO();
		chnDTO.setCiId(ci.getCiId());
		chnDTO.setArrivalDate(ci.getArrivalDate());
		chnDTO.setLifeCycle(ci.getLifeCycle());
		chnDTO.setWarranty(ci.getWarranty());
		chnDTO.setWarningDate(ci.getWarningDate());
		chnDTO.setType(type);
		return chnDTO;
	}
	/**
	 * save CIHistoryNotice
	 * @param ci
	 * @param type
	 */
	@Transactional
	public void saveCIHistoryNotice(CI ci,Long type){
		CIHistoryNotice entity = new CIHistoryNotice();
		entity.setCiId(ci.getCiId());
		entity.setArrivalDate(ci.getArrivalDate());
		entity.setType(type);
		entity.setLifeCycle(ci.getLifeCycle());
		entity.setWarranty(ci.getWarranty());
		entity.setWarningDate(ci.getWarningDate());
		ciHistoryNoticeDAO.save(entity);
	}
	/**
	 * 配置项生命周期到期通知
	 */
	public void ciLifeCycleExpiredNotice(){
		// 生命周期过期
		List<CI> expiredCis = ciDAO.calcCiLifeCycle();
		for (CI ci : expiredCis) {
			CIHistoryNoticeQueryDTO queryDTO = setCIHistoryNoticeQueryDTO(ci,LIFE_CYCLE_NOTCE_TYPE);
			Integer result = ciHistoryNoticeDAO
					.findPagerCiHistoryNotice(queryDTO);
			if (result <= 0) {
				// 配置项生命周期到期通知
				cimProcessNotice(INoticeRuleService.CIM_LIFE_CYCLE_EXPIRY_NOTICE, ci);
				saveCIHistoryNotice(ci,LIFE_CYCLE_NOTCE_TYPE);
			}
		}
	}
	/**
	 * 保修期到期通知
	 */
	public void ciWarrantyExpiredNotice(){
		// 保修期过期
		List<CI> expiredCis = ciDAO.calcCiWarranty();
		for (CI ci : expiredCis) {
			CIHistoryNoticeQueryDTO queryDTO = setCIHistoryNoticeQueryDTO(ci, ARRIVAL_DATE_NOTCE_TYPE);
			Integer result = ciHistoryNoticeDAO
					.findPagerCiHistoryNotice(queryDTO);
			if (result <= 0) {
				// 配置项保修期到期通知
				cimProcessNotice(INoticeRuleService.CIM_WARRANTY_EXPIRY_NOTICE,ci);
				saveCIHistoryNotice(ci, ARRIVAL_DATE_NOTCE_TYPE);
			}
		}
	}
	/**
	 * 预警时间到期通知
	 */
	public void ciWarningDateExpiredNotice(){
		// 预警时间过期
		List<CI> expiredCis = ciDAO.calcWarningDate();
		for (CI ci : expiredCis) {
			CIHistoryNoticeQueryDTO queryDTO = setCIHistoryNoticeQueryDTO(ci, WARNING_DATE_NOTCE_TYPE);
			Integer result = ciHistoryNoticeDAO
					.findPagerCiHistoryNotice(queryDTO);
			if (result <= 0) {
				// 配置项预警时间到期通知
				cimProcessNotice(INoticeRuleService.CIM_ALERT_CYCLE_EXPIRY_NOTICE,ci);
				saveCIHistoryNotice(ci, WARNING_DATE_NOTCE_TYPE);
			}
		}
	}

	/**
	 * 配置项通知规则
	 * 
	 * @param noticeRuleNo
	 *            通知规则编号
	 * @param loginNames
	 * @param ciOwner
	 * @param ciUse
	 * @param visitPath
	 */
	public void cimProcessNotice(String noticeRuleNo, CI ci) {
		NoticeInfoDTO noticeInfoDto = new NoticeInfoDTO();
		noticeInfoDto.setNoticeRuleNo(noticeRuleNo);
		User owner = null;
		if(ci.getOwnerId()!=null && ci.getOwnerId()>0){
			owner = userDAO.findById(ci.getOwnerId());
		}else{
			owner = userDAO.findUniqueBy("fullName", ci.getOwner());
		}
		noticeInfoDto.setCiOwner(owner);
		User user = null;
		if(ci.getUserNameId()!=null && ci.getUserNameId()>0){
			user = userDAO.findById(ci.getUserNameId());
		}else{
			user = userDAO.findUniqueBy("fullName", ci.getUserName());
		}
		noticeInfoDto.setCiUse(user);
		CIDTO variables = new CIDTO();
		variables.setCiname(ci.getCiname());//配置项名称
		variables.setCino(ci.getCino());//配置项编号
		variables.setCiId(ci.getCiId());
		noticeInfoDto.setVariables(variables );
		noticeRuleService.commonNotice(noticeInfoDto);
	}
}
