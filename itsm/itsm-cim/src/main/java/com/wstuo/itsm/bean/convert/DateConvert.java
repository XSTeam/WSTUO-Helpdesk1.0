package com.wstuo.itsm.bean.convert;
 
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.beanutils.Converter;
import org.apache.log4j.Logger;

public class DateConvert implements Converter {

	final static Logger LOGGER = Logger.getLogger(DateConvert.class);

	private static String str = "yyyy-MM-dd hh:mm";
	private static SimpleDateFormat dateFormat = new SimpleDateFormat(str);
	
	@Override
	public Object convert(Class class1, Object object) {
		if( object != null && object instanceof String ){
			Date date = null;
			try {
				String dateStr = dateFormatStr((String)object);
				date = dateFormat.parse(dateStr);
			} catch (ParseException e) {
				LOGGER.error(e);
			}
			return date;
		}
		return object;
	}
	/**
	 * 
	 * @param dateStr
	 * @return
	 */
	private String dateFormatStr(String dateStr){
		dateStr = dateStr.replace("/", "-").trim();
		/*StringBuffer buffer = new StringBuffer(dateStr);
		switch (dateStr.length()) {
		case 10://yyyy-MM-dd
			buffer.append(" 00:00");
			break;

		default:
			break;
		}
		return buffer.toString();*/
		return dateStr;
	}
}
