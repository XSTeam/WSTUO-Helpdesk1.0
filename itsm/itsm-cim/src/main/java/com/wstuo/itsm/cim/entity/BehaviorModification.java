package com.wstuo.itsm.cim.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

/***
 *配置项历史更新记录 
 * */

@Entity
@Table(name="behaviormodification")
public class BehaviorModification {
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
	private Long id;//ID
	@Column(nullable=true)
	@Lob
	private String actContent;//修改内容
	@Column(nullable=true)
	private String userName;//操作人
	@Column(nullable=true)
	private Date updateDate=new Date();//更新时间
	@Column(nullable=true)
	private Long ciId;//关联配置编号
	@Column(nullable=true)
	private String changeCode;//管理的变更编号;
	
	
	public String getChangeCode() {
		return changeCode;
	}
	public void setChangeCode(String changeCode) {
		this.changeCode = changeCode;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getActContent() {
		return actContent;
	}
	public void setActContent(String actContent) {
		this.actContent = actContent;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public Long getCiId() {
		return ciId;
	}
	public void setCiId(Long ciId) {
		this.ciId = ciId;
	}
}
