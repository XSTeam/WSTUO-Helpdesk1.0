package com.wstuo.itsm.cim.dto;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.wstuo.common.config.attachment.entity.Attachment;
import com.wstuo.common.dto.BaseDTO;

/**
 * 配置项DTO
 * @author WSTUO_QXY
 *
 */
@SuppressWarnings("serial")
public class CIGridDTO extends BaseDTO {
	/**
     * Configuration item sheet auto identity column
     */
    private Long ciId;

    /**
     * Configuration item NO.
     */
    private String cino;

    /**
     * Configuration item name
     */
    private String ciname;

    /**
     * Product Type
     */
    private String model;

    /**
     * serial number
     */
    private String serialNumber;

    /**
     * barcode
     */
    private String barcode;

    /**
     * buy time
     */
    private Date buyDate;

    /**
     * Arrival time
     */
    private Date arrivalDate;

    /**
     * Purchase Order No.
     */
    private String poNo;

    /**
     * Warning time
     */
    private Date warningDate;

    /**
     * provider id
     */
    private Long providerId;

    /**
     * providers name
     */
    private String providerName;

    /**
     * Life Cycle
     */
    private Integer lifeCycle;

    /**
     * Warranty
     */
    private Integer warranty;

    /**
     * User
     */
    private String userName;

    /**
     * Owner
     */
    private String owner;

    /**
     * status id
     */
    private Long statusId;

    /**
     * status
     */
    private String status;

    /**
     * Location id
     */
    private Long locId;

    /**
     * Location
     */
    private String loc;

    /**
     * brand id
     */
    private Long brandId;

    /**
     * brand Name
     */
    private String brandName;

    /**
     * category No.
     */
    private Long categoryNo;

    /***
     * category Name
     */
    private String categoryName;
    
    private Long eavNo;
    
    private Map<String,String> attrVals =new HashMap<String, String>(); 
    
    /**
     * 附件list
     */
    private List<Attachment> attachments;
    
    /**
     * 附件字符串
     */
    private String attachmentStr;
    
    //20110709-QXY
    //部门
    private String department;
    //CDI
    private String CDI;
    //Work Number
    private String workNumber;
    //Project
    private String project;
    //来源单位
    private String sourceUnits;
    //与财务对应
    private Boolean financeCorrespond=false;
    //资产原值
    private Double assetsOriginalValue; 
    //报废时间
    private Date wasteTime;
    //借出时间
    private Date borrowedTime;
    //原使用者
    private String originalUser;
    //回收时间
    private Date recoverTime;
    //预计回收时间
    private Date expectedRecoverTime;
    //使用权限
    private String usePermissions;
    private String companyName;//客户名称
    private Long companyNo;//客户No
    private Long ciServiceDirNo;
    private String ciServiceDirName;
    private Long systemPlatformId;
    private String systemPlatform;
    private Date createTime;
    private Date lastUpdateTime;
    
  //20130706 ciel
    private Integer depreciationIsZeroYears; //多少年折旧率为0
    private Double ciDepreciation ; //折旧率
    //背景色
    private String systemPlatformColor;//所属项目位置
    private String statusColor;//状态位置 	
    private String brandNameColor;//品牌位置 
    private String providerNameColor;//产品提供商位置 
    private String locColor;//位置背景色
    private String permissions;
    private String ipAddress;//IP地址
    
    
    
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	public String getSystemPlatformColor() {
		return systemPlatformColor;
	}
	public void setSystemPlatformColor(String systemPlatformColor) {
		this.systemPlatformColor = systemPlatformColor;
	}
	public String getStatusColor() {
		return statusColor;
	}
	public void setStatusColor(String statusColor) {
		this.statusColor = statusColor;
	}
	public String getBrandNameColor() {
		return brandNameColor;
	}
	public void setBrandNameColor(String brandNameColor) {
		this.brandNameColor = brandNameColor;
	}
	public String getProviderNameColor() {
		return providerNameColor;
	}
	public void setProviderNameColor(String providerNameColor) {
		this.providerNameColor = providerNameColor;
	}
	public String getLocColor() {
		return locColor;
	}
	public void setLocColor(String locColor) {
		this.locColor = locColor;
	}
	public Long getCiServiceDirNo() {
		return ciServiceDirNo;
	}
	public void setCiServiceDirNo(Long ciServiceDirNo) {
		this.ciServiceDirNo = ciServiceDirNo;
	}
	public String getCiServiceDirName() {
		return ciServiceDirName;
	}
	public void setCiServiceDirName(String ciServiceDirName) {
		this.ciServiceDirName = ciServiceDirName;
	}
	public Long getCiId() {
		return ciId;
	}
	public void setCiId(Long ciId) {
		this.ciId = ciId;
	}
	public String getCino() {
		return cino;
	}
	public void setCino(String cino) {
		this.cino = cino;
	}
	public String getCiname() {
		return ciname;
	}
	public void setCiname(String ciname) {
		this.ciname = ciname;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public Date getBuyDate() {
		return buyDate;
	}
	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}
	public Date getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public String getPoNo() {
		return poNo;
	}
	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}
	public Date getWarningDate() {
		return warningDate;
	}
	public void setWarningDate(Date warningDate) {
		this.warningDate = warningDate;
	}
	public Long getProviderId() {
		return providerId;
	}
	public void setProviderId(Long providerId) {
		this.providerId = providerId;
	}
	public String getProviderName() {
		return providerName;
	}
	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}
	public Integer getLifeCycle() {
		return lifeCycle;
	}
	public void setLifeCycle(Integer lifeCycle) {
		this.lifeCycle = lifeCycle;
	}
	public Integer getWarranty() {
		return warranty;
	}
	public void setWarranty(Integer warranty) {
		this.warranty = warranty;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public Long getStatusId() {
		return statusId;
	}
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getLocId() {
		return locId;
	}
	public void setLocId(Long locId) {
		this.locId = locId;
	}
	public String getLoc() {
		return loc;
	}
	public void setLoc(String loc) {
		this.loc = loc;
	}
	public Long getBrandId() {
		return brandId;
	}
	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public Long getCategoryNo() {
		return categoryNo;
	}
	public void setCategoryNo(Long categoryNo) {
		this.categoryNo = categoryNo;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public Long getEavNo() {
		return eavNo;
	}
	public void setEavNo(Long eavNo) {
		this.eavNo = eavNo;
	}
	public Map<String, String> getAttrVals() {
		return attrVals;
	}
	public void setAttrVals(Map<String, String> attrVals) {
		this.attrVals = attrVals;
	}
	public List<Attachment> getAttachments() {
		return attachments;
	}
	public void setAttachments(List<Attachment> attachments) {
		this.attachments = attachments;
	}
	public String getAttachmentStr() {
		return attachmentStr;
	}
	public void setAttachmentStr(String attachmentStr) {
		this.attachmentStr = attachmentStr;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getCDI() {
		return CDI;
	}
	public void setCDI(String cDI) {
		CDI = cDI;
	}
	public String getWorkNumber() {
		return workNumber;
	}
	public void setWorkNumber(String workNumber) {
		this.workNumber = workNumber;
	}
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public String getSourceUnits() {
		return sourceUnits;
	}
	public void setSourceUnits(String sourceUnits) {
		this.sourceUnits = sourceUnits;
	}
	public Boolean getFinanceCorrespond() {
		return financeCorrespond;
	}
	public void setFinanceCorrespond(Boolean financeCorrespond) {
		this.financeCorrespond = financeCorrespond;
	}
	public Double getAssetsOriginalValue() {
		return assetsOriginalValue;
	}
	public void setAssetsOriginalValue(Double assetsOriginalValue) {
		this.assetsOriginalValue = assetsOriginalValue;
	}
	public Date getWasteTime() {
		return wasteTime;
	}
	public void setWasteTime(Date wasteTime) {
		this.wasteTime = wasteTime;
	}
	public Date getBorrowedTime() {
		return borrowedTime;
	}
	public void setBorrowedTime(Date borrowedTime) {
		this.borrowedTime = borrowedTime;
	}
	public String getOriginalUser() {
		return originalUser;
	}
	public void setOriginalUser(String originalUser) {
		this.originalUser = originalUser;
	}
	public Date getRecoverTime() {
		return recoverTime;
	}
	public void setRecoverTime(Date recoverTime) {
		this.recoverTime = recoverTime;
	}
	public Date getExpectedRecoverTime() {
		return expectedRecoverTime;
	}
	public void setExpectedRecoverTime(Date expectedRecoverTime) {
		this.expectedRecoverTime = expectedRecoverTime;
	}
	public String getUsePermissions() {
		return usePermissions;
	}
	public void setUsePermissions(String usePermissions) {
		this.usePermissions = usePermissions;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Long getCompanyNo() {
		return companyNo;
	}
	public void setCompanyNo(Long companyNo) {
		this.companyNo = companyNo;
	}
	public Long getSystemPlatformId() {
		return systemPlatformId;
	}
	public void setSystemPlatformId(Long systemPlatformId) {
		this.systemPlatformId = systemPlatformId;
	}
	public String getSystemPlatform() {
		return systemPlatform;
	}
	public void setSystemPlatform(String systemPlatform) {
		this.systemPlatform = systemPlatform;
	}
	public Integer getDepreciationIsZeroYears() {
		return depreciationIsZeroYears;
	}
	public void setDepreciationIsZeroYears(Integer depreciationIsZeroYears) {
		this.depreciationIsZeroYears = depreciationIsZeroYears;
	}
	public Double getCiDepreciation() {
		return ciDepreciation;
	}
	public void setCiDepreciation(Double ciDepreciation) {
		this.ciDepreciation = ciDepreciation;
	}
	public String getPermissions() {
		return permissions;
	}
	public void setPermissions(String permissions) {
		this.permissions = permissions;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

    
}
