package com.wstuo.itsm.cim.dao;

import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.itsm.cim.dto.CIHistoryNoticeQueryDTO;
import com.wstuo.itsm.cim.entity.CIHistoryNotice;

/**
 * 配置项历史通知DAO CLASS
 * @author WSTUO_QXY
 *
 */
public interface ICIHistoryNoticeDAO extends IEntityDAO<CIHistoryNotice> {
	/**
	 * 分页查询
	 * @param queryDTO
	 * @return String
	 */
	Integer findPagerCiHistoryNotice(final CIHistoryNoticeQueryDTO queryDTO);
}
