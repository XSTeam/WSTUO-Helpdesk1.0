package com.wstuo.itsm.cim.dto;
import java.util.Date;

import com.wstuo.common.dto.BaseDTO;
/**
 * 配置项安装的软件
 * @author WSTUO_QXY
 *
 */
@SuppressWarnings("serial")
public class CiSoftwareDTO extends BaseDTO {
	
	private Long softwareId;//软件ID
	private String softwareName;//软件名
	private String softwareVersion;//软件版本
	private Long softwareTypeId;//软件类型
	private String softwareTypeName;
    private Long softwareCategoryId; //软件分类
    private String softwareCategoryName;
    private Long softwareProviderId;//软件供应商
    private String softwareProviderName;
	private String installDate;//安装时间
	private String description;//描述
	private Date createTime;//创建时间
	private Date lastUpdateTime;//更新时间
	private String softwareStr;
	private String installState;//安装状态
   	private String caption;//标题
   	private String identifyingNumber;//标识编号
   	private String productID;// 产品ID
   	private String regOwner;//登记人
   	private String packageCache;//软件包缓存
   	private String packageCode;//软件包编码
   	private String assignmentType;//分配类型
   	private String localPackage;//本地包
   	private String installSource;//安装来源
   	private String installLocation;//安装位置
   	private String installDate2;//安装日期
   	private String vendor;// 供应商
   	private String packageName;//软件包名
   	private String softwareLanguage;//语言
   	private String regCompany;//注册公司
   	private String skuNumber;//SKU编号
   	private String softwareTypeColor;//类型背景色
   	private String softwareProviderColor;//提供商背景色
    
	public String getSoftwareTypeColor() {
		return softwareTypeColor;
	}
	public void setSoftwareTypeColor(String softwareTypeColor) {
		this.softwareTypeColor = softwareTypeColor;
	}
	public String getSoftwareProviderColor() {
		return softwareProviderColor;
	}
	public void setSoftwareProviderColor(String softwareProviderColor) {
		this.softwareProviderColor = softwareProviderColor;
	}
	public String getInstallState() {
		return installState;
	}
	public void setInstallState(String installState) {
		this.installState = installState;
	}
	public String getCaption() {
		return caption;
	}
	public void setCaption(String caption) {
		this.caption = caption;
	}
	public String getIdentifyingNumber() {
		return identifyingNumber;
	}
	public void setIdentifyingNumber(String identifyingNumber) {
		this.identifyingNumber = identifyingNumber;
	}
	public String getProductID() {
		return productID;
	}
	public void setProductID(String productID) {
		this.productID = productID;
	}
	public String getRegOwner() {
		return regOwner;
	}
	public void setRegOwner(String regOwner) {
		this.regOwner = regOwner;
	}
	public String getPackageCache() {
		return packageCache;
	}
	public void setPackageCache(String packageCache) {
		this.packageCache = packageCache;
	}
	public String getPackageCode() {
		return packageCode;
	}
	public void setPackageCode(String packageCode) {
		this.packageCode = packageCode;
	}
	public String getAssignmentType() {
		return assignmentType;
	}
	public void setAssignmentType(String assignmentType) {
		this.assignmentType = assignmentType;
	}
	public String getLocalPackage() {
		return localPackage;
	}
	public void setLocalPackage(String localPackage) {
		this.localPackage = localPackage;
	}
	public String getInstallSource() {
		return installSource;
	}
	public void setInstallSource(String installSource) {
		this.installSource = installSource;
	}
	public String getInstallLocation() {
		return installLocation;
	}
	public void setInstallLocation(String installLocation) {
		this.installLocation = installLocation;
	}
	public String getInstallDate2() {
		return installDate2;
	}
	public void setInstallDate2(String installDate2) {
		this.installDate2 = installDate2;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	
    public String getSoftwareLanguage() {
        return softwareLanguage;
    }
    public void setSoftwareLanguage(String softwareLanguage) {
        this.softwareLanguage = softwareLanguage;
    }
    public String getRegCompany() {
		return regCompany;
	}
	public void setRegCompany(String regCompany) {
		this.regCompany = regCompany;
	}
	public String getSkuNumber() {
		return skuNumber;
	}
	public void setSkuNumber(String skuNumber) {
		this.skuNumber = skuNumber;
	}
	public Long getSoftwareId() {
		return softwareId;
	}
	public void setSoftwareId(Long softwareId) {
		this.softwareId = softwareId;
	}
	public String getSoftwareName() {
		return softwareName;
	}
	public void setSoftwareName(String softwareName) {
		this.softwareName = softwareName;
	}
	public String getInstallDate() {
		return installDate;
	}
	public void setInstallDate(String installDate) {
		this.installDate = installDate;
	}
	public String getSoftwareStr() {
		return softwareStr;
	}
	public void setSoftwareStr(String softwareStr) {
		this.softwareStr = softwareStr;
	}
	public CiSoftwareDTO(){
		
	}
	public String getSoftwareVersion() {
		return softwareVersion;
	}
	public void setSoftwareVersion(String softwareVersion) {
		this.softwareVersion = softwareVersion;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	
	public Long getSoftwareTypeId() {
		return softwareTypeId;
	}
	public void setSoftwareTypeId(Long softwareTypeId) {
		this.softwareTypeId = softwareTypeId;
	}
	public String getSoftwareTypeName() {
		return softwareTypeName;
	}
	public void setSoftwareTypeName(String softwareTypeName) {
		this.softwareTypeName = softwareTypeName;
	}
	public Long getSoftwareCategoryId() {
		return softwareCategoryId;
	}
	public void setSoftwareCategoryId(Long softwareCategoryId) {
		this.softwareCategoryId = softwareCategoryId;
	}
	public String getSoftwareCategoryName() {
		return softwareCategoryName;
	}
	public void setSoftwareCategoryName(String softwareCategoryName) {
		this.softwareCategoryName = softwareCategoryName;
	}
	public Long getSoftwareProviderId() {
		return softwareProviderId;
	}
	public void setSoftwareProviderId(Long softwareProviderId) {
		this.softwareProviderId = softwareProviderId;
	}
	public String getSoftwareProviderName() {
		return softwareProviderName;
	}
	public void setSoftwareProviderName(String softwareProviderName) {
		this.softwareProviderName = softwareProviderName;
	}
	public CiSoftwareDTO(Long softwareId, String softwareName,
			String installDate, String softwareStr) {
		super();
		this.softwareId = softwareId;
		this.softwareName = softwareName;
		this.installDate = installDate;
		this.softwareStr = softwareStr;
	}
	
	
	
	
	
}
