package com.wstuo.itsm.cim.dto;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.wstuo.common.dto.BaseDTO;
import com.wstuo.common.tools.entity.ExportInfo;

/***
 * 配置项查询条件属性DTO类
 * 
 * @author QXY
 */
@SuppressWarnings("serial")
public class CIQueryDTO extends BaseDTO {
	/**
	 * Configuration item sheet auto identity column
	 */
	private Long ciId;

	/**
	 * Configuration item NO.
	 */
	private String cino;

	/**
	 * Configuration item name
	 */
	private String ciname;

	/**
	 * Product Type
	 */
	private String model;

	/**
	 * serial number
	 */
	private String serialNumber;

	/**
	 * barcode
	 */
	private String barcode;

	/**
	 * buy time
	 */
	private Date buyDate;

	/**
	 * buy endTime
	 */
	private Date buyEndDate;

	/**
	 * Arrival time
	 */
	private Date arrivalDate;

	/**
	 * Arrival endTime
	 */
	private Date arrivalEndDate;

	/**
	 * Purchase Order No.
	 */
	private String poNo;

	/**
	 * Warning time
	 */
	private Date warningDate;

	/**
	 * Warning endTime
	 */
	private Date warningEndDate;

	/**
	 * provider id
	 */
	private Long providerId;

	/**
	 * provider name
	 */
	private String providerName;

	/**
	 * Life Cycle
	 */
	private Integer lifeCycle;

	/**
	 * Life EndCycle
	 */
	private Integer lifeEndCycle;

	/**
	 * Warranty
	 */
	private Integer warranty;

	/**
	 * EndWarranty
	 */
	private Integer endWarranty;

	/**
	 * User
	 */
	private String userName;

	/**
	 * Owner
	 */
	private String owner;

	/**
	 * status id
	 */
	private Long statusId;

	/**
	 * status
	 */
	private String status;

	/**
	 * Location id
	 */
	private Long locId;

	/**
	 * Location
	 */
	private String loc;

	/**
	 * brand id
	 */
	private Long brandId;

	/**
	 * brand Name
	 */
	private String brandName;

	/**
	 * category No.
	 */
	private Long categoryNo;

	/***
	 * category Name
	 */
	private String categoryName;

	/**
	 * Where to start
	 */
	private Integer start;

	/**
	 * limit
	 */
	private Integer limit;
	private String sord;
	private String sidx;

	private Long[] categoryNos;
	// 20110709-QXY
	// 部门
	private String department;
	// CDI
	private String CDI;
	// Work Number
	private String workNumber;
	// Project
	private String project;
	// 来源单位
	private String sourceUnits;
	// 与财务对应
	private Long financeCorrespond;
	// 资产原值
	private Double assetsOriginalValue;

	private Double assetsOriginalEndValue;
	// 报废时间
	private Date wasteTime;

	private Date wasteTimeEnd;
	// 借出时间
	private Date borrowedTime;

	private Date borrowedTimeEnd;
	// 原使用者
	private String originalUser;
	// 回收时间
	private Date recoverTime;

	private Date recoverTimeEnd;

	// 预计回收时间
	private Date expectedRecoverTime;

	private Date expectedRecoverTimeEnd;

	// 使用权限
	private String usePermissions;
	private String companyName;// 客户名称
	private Long companyNo;// 客户No
	private Long[] companyNos;
	// 分类权限
	private Long[] categoryRes;
	private String loginName;// 当前登录用户名
	private Map<String, String> attrVals = new HashMap<String, String>();
	private Long filterId;// 过滤器ID
	private String fileName;
	private String mqTitle;// MQ执行的主题
	private String mqContent;// MQ执行的内容
	private String mqCreator;// MQ创建者
	private ExportInfo exportInfo;
	private Long systemPlatformId;
	private String systemPlatform;
	private Date createTime;
	private Date createTimeEnd;
	private Date lastUpdateTime;
	private Date lastUpdateTimeEnd;

	// 20130706 ciel
	private Integer depreciationIsZeroYears; // 多少年折旧率为0
	private Double ciDepreciation; // 折旧率

	private String lang; // i18n导出国际化参数
	private Set<Long> ciIds;// 我能查看的CIID
	private String groupField;//分组统计字段
	private String label;
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getGroupField() {
		return groupField;
	}

	public void setGroupField(String groupField) {
		this.groupField = groupField;
	}

	public Date getCreateTimeEnd() {
		return createTimeEnd;
	}

	public void setCreateTimeEnd(Date createTimeEnd) {
		this.createTimeEnd = createTimeEnd;
	}

	public Date getLastUpdateTimeEnd() {
		return lastUpdateTimeEnd;
	}

	public void setLastUpdateTimeEnd(Date lastUpdateTimeEnd) {
		this.lastUpdateTimeEnd = lastUpdateTimeEnd;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public ExportInfo getExportInfo() {
		return exportInfo;
	}

	public void setExportInfo(ExportInfo exportInfo) {
		this.exportInfo = exportInfo;
	}

	public String getMqTitle() {
		return mqTitle;
	}

	public void setMqTitle(String mqTitle) {
		this.mqTitle = mqTitle;
	}

	public String getMqContent() {
		return mqContent;
	}

	public void setMqContent(String mqContent) {
		this.mqContent = mqContent;
	}

	public String getMqCreator() {
		return mqCreator;
	}

	public void setMqCreator(String mqCreator) {
		this.mqCreator = mqCreator;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public Long[] getCategoryNos() {
		return categoryNos;
	}

	public void setCategoryNos(Long[] categoryNos) {
		this.categoryNos = categoryNos;
	}

	public void setCiId(Long ciId) {
		this.ciId = ciId;
	}

	public String getCino() {
		return cino;
	}

	public void setCino(String cino) {
		this.cino = cino;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public Date getBuyDate() {
		return buyDate;
	}

	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public String getPoNo() {
		return poNo;
	}

	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}

	public Date getWarningDate() {
		return warningDate;
	}

	public void setWarningDate(Date warningDate) {
		this.warningDate = warningDate;
	}

	public Long getProviderId() {
		return providerId;
	}

	public void setProviderId(Long providerId) {
		this.providerId = providerId;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public Integer getLifeCycle() {
		return lifeCycle;
	}

	public void setLifeCycle(Integer lifeCycle) {
		this.lifeCycle = lifeCycle;
	}

	public Integer getWarranty() {
		return warranty;
	}

	public void setWarranty(Integer warranty) {
		this.warranty = warranty;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getLocId() {
		return locId;
	}

	public void setLocId(Long locId) {
		this.locId = locId;
	}

	public String getLoc() {
		return loc;
	}

	public void setLoc(String loc) {
		this.loc = loc;
	}

	public Long getBrandId() {
		return brandId;
	}

	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public Long getCiId() {
		return ciId;
	}

	public Long getCategoryNo() {
		return categoryNo;
	}

	public void setCategoryNo(Long categoryNo) {
		this.categoryNo = categoryNo;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCiname() {
		return ciname;
	}

	public void setCiname(String ciname) {
		this.ciname = ciname.replace("%", "\\%");
	}

	public Date getBuyEndDate() {
		return buyEndDate;
	}

	public void setBuyEndDate(Date buyEndDate) {
		this.buyEndDate = buyEndDate;
	}

	public Date getArrivalEndDate() {
		return arrivalEndDate;
	}

	public void setArrivalEndDate(Date arrivalEndDate) {
		this.arrivalEndDate = arrivalEndDate;
	}

	public Date getWarningEndDate() {
		return warningEndDate;
	}

	public void setWarningEndDate(Date warningEndDate) {
		this.warningEndDate = warningEndDate;
	}

	public Integer getLifeEndCycle() {
		return lifeEndCycle;
	}

	public void setLifeEndCycle(Integer lifeEndCycle) {
		this.lifeEndCycle = lifeEndCycle;
	}

	public Integer getEndWarranty() {
		return endWarranty;
	}

	public void setEndWarranty(Integer endWarranty) {
		this.endWarranty = endWarranty;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Long getCompanyNo() {
		return companyNo;
	}

	public void setCompanyNo(Long companyNo) {
		this.companyNo = companyNo;
	}

	public Long[] getCompanyNos() {
		return companyNos;
	}

	public void setCompanyNos(Long[] companyNos) {
		this.companyNos = companyNos;
	}

	public Long[] getCategoryRes() {
		return categoryRes;
	}

	public void setCategoryRes(Long[] categoryRes) {
		this.categoryRes = categoryRes;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getCDI() {
		return CDI;
	}

	public void setCDI(String cDI) {
		CDI = cDI;
	}

	public String getWorkNumber() {
		return workNumber;
	}

	public void setWorkNumber(String workNumber) {
		this.workNumber = workNumber;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getSourceUnits() {
		return sourceUnits;
	}

	public void setSourceUnits(String sourceUnits) {
		this.sourceUnits = sourceUnits;
	}

	public Long getFinanceCorrespond() {
		return financeCorrespond;
	}

	public void setFinanceCorrespond(Long financeCorrespond) {
		this.financeCorrespond = financeCorrespond;
	}

	public Double getAssetsOriginalValue() {
		return assetsOriginalValue;
	}

	public void setAssetsOriginalValue(Double assetsOriginalValue) {
		this.assetsOriginalValue = assetsOriginalValue;
	}

	public Double getAssetsOriginalEndValue() {
		return assetsOriginalEndValue;
	}

	public void setAssetsOriginalEndValue(Double assetsOriginalEndValue) {
		this.assetsOriginalEndValue = assetsOriginalEndValue;
	}

	public Date getWasteTime() {
		return wasteTime;
	}

	public void setWasteTime(Date wasteTime) {
		this.wasteTime = wasteTime;
	}

	public Date getWasteTimeEnd() {
		return wasteTimeEnd;
	}

	public void setWasteTimeEnd(Date wasteTimeEnd) {
		this.wasteTimeEnd = wasteTimeEnd;
	}

	public Date getBorrowedTime() {
		return borrowedTime;
	}

	public void setBorrowedTime(Date borrowedTime) {
		this.borrowedTime = borrowedTime;
	}

	public Date getBorrowedTimeEnd() {
		return borrowedTimeEnd;
	}

	public void setBorrowedTimeEnd(Date borrowedTimeEnd) {
		this.borrowedTimeEnd = borrowedTimeEnd;
	}

	public String getOriginalUser() {
		return originalUser;
	}

	public void setOriginalUser(String originalUser) {
		this.originalUser = originalUser;
	}

	public Date getRecoverTime() {
		return recoverTime;
	}

	public void setRecoverTime(Date recoverTime) {
		this.recoverTime = recoverTime;
	}

	public Date getRecoverTimeEnd() {
		return recoverTimeEnd;
	}

	public void setRecoverTimeEnd(Date recoverTimeEnd) {
		this.recoverTimeEnd = recoverTimeEnd;
	}

	public Date getExpectedRecoverTime() {
		return expectedRecoverTime;
	}

	public void setExpectedRecoverTime(Date expectedRecoverTime) {
		this.expectedRecoverTime = expectedRecoverTime;
	}

	public Date getExpectedRecoverTimeEnd() {
		return expectedRecoverTimeEnd;
	}

	public void setExpectedRecoverTimeEnd(Date expectedRecoverTimeEnd) {
		this.expectedRecoverTimeEnd = expectedRecoverTimeEnd;
	}

	public String getUsePermissions() {
		return usePermissions;
	}

	public void setUsePermissions(String usePermissions) {
		this.usePermissions = usePermissions;
	}

	public Map<String, String> getAttrVals() {
		return attrVals;
	}

	public void setAttrVals(Map<String, String> attrVals) {
		this.attrVals = attrVals;
	}

	public Long getFilterId() {
		return filterId;
	}

	public void setFilterId(Long filterId) {
		this.filterId = filterId;
	}

	public Long getSystemPlatformId() {
		return systemPlatformId;
	}

	public void setSystemPlatformId(Long systemPlatformId) {
		this.systemPlatformId = systemPlatformId;
	}

	public String getSystemPlatform() {
		return systemPlatform;
	}

	public void setSystemPlatform(String systemPlatform) {
		this.systemPlatform = systemPlatform;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getDepreciationIsZeroYears() {
		return depreciationIsZeroYears;
	}

	public void setDepreciationIsZeroYears(Integer depreciationIsZeroYears) {
		this.depreciationIsZeroYears = depreciationIsZeroYears;
	}

	public Double getCiDepreciation() {
		return ciDepreciation;
	}

	public void setCiDepreciation(Double ciDepreciation) {
		this.ciDepreciation = ciDepreciation;
	}

	public Set<Long> getCiIds() {
		return ciIds;
	}

	public void setCiIds(Set<Long> ciIds) {
		this.ciIds = ciIds;
	}

}
