package com.wstuo.itsm.cim.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

/***
 *配置项历史更新记录 
 * */

@Entity
@Table(name="cihistoryupdate")
public class CIHistoryUpdate{
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
	private Long id;       			//ID
	@Column(nullable=true)
	@Lob
	private String cihistoryInfo;   //更新内容
	@Column(nullable=true)
	private String userName;   //操作人
	@Column(nullable=true)
	private Date updateDate;   //更新时间
	@Column(nullable=true)
	private Long revision;  //版本
	@Column(nullable=true)
	private Long ciId;
	
	@Column(nullable=true)
	private Long beId;//修改行为；
	
	public Long getBeId() {
		return beId;
	}
	public void setBeId(Long beId) {
		this.beId = beId;
	}
	public Long getCiId() {
		return ciId;
	}
	public void setCiId(Long ciId) {
		this.ciId = ciId;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCihistoryInfo() {
		return cihistoryInfo;
	}
	public void setCihistoryInfo(String cihistoryInfo) {
		this.cihistoryInfo = cihistoryInfo;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getRevision() {
		return revision;
	}
	public void setRevision(Long revision) {
		this.revision = revision;
	}
}
