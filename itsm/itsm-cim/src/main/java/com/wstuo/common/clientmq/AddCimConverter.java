package com.wstuo.common.clientmq;


import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.jms.support.converter.MessageConverter;

import com.wstuo.common.activemq.service.IActiveMQService;
import com.wstuo.itsm.cim.dto.CIDTO;

/**
 * add cim Converter
 * @author wing
 *
 */
public class AddCimConverter implements MessageConverter {
	@Autowired
	private IActiveMQService activeMQService;
	public CIDTO fromMessage(Message msg) throws JMSException,
			MessageConversionException {
		System.err.println("cim111");
		return (CIDTO) activeMQService.fromMessage(msg);
	}

	/**
	 * 发送信息
	 */
    public Message toMessage(Object obj, Session session) throws JMSException,MessageConversionException {
    	System.err.println("cim");
    	return activeMQService.toMessage(obj,session);
	}

}
