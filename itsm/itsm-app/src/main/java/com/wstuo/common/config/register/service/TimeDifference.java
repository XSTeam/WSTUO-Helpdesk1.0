package com.wstuo.common.config.register.service;

import java.util.Date;

/**
 *  求时间差
 * @author tqp
 *
 */
public class TimeDifference {
	
	/**
	 *  计算已经使用天数
	 * @param startDate  开始使用时间
	 * @return int 已经使用天数
	 */
	public int makeUsedDay(Date startDate){
		int userDay;
		Date nowDay = new Date();
		long startTime = startDate.getTime();
		long nowTime = nowDay.getTime();
		if(startTime>=nowTime){
			userDay = 0;
		}else{
			long result = nowTime-startTime;
			userDay = (int)(result/1000/3600/24);
		}
		return userDay;
	}
	
	/**
	 *  计算剩余天数
	 * @param start 
	 * @param end 截至日期
	 * @return int 剩余天数
	 */
	public int makeSurplusDay(Date start,Date end){
//		int SurplusDay = 0;
//		long endTime = end.getTime()+24*3600*1000;				//截止日期加一天
//		long nowTime = new Date().getTime();
//		long startTime = start.getTime();
//		long result ;
//		if(endTime<nowTime){
//			return 0;
//		}else if(startTime>nowTime){
//			result = endTime-startTime;
//		}else{
//			result = endTime-nowTime;
//		}
//		SurplusDay = (int)(result/1000/3600/24);			//最后一天还是可以使用的，所以加一天的剩余天数
		
		
		return makeTotalDay(start,end)-makeUsedDay(start);
	}

	
	/**
	 *  计算总时间
	 * @param start 开始时间
	 * @param end 结束时间
	 * @return int 总天数
	 */
	public int makeTotalDay(Date start,Date end){
		long startTime = start.getTime();
		long endTime = end.getTime();
		return (int) ((endTime-startTime)/1000/3600/24);
	}
	
}
