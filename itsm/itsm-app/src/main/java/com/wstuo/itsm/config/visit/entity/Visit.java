package com.wstuo.itsm.config.visit.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.wstuo.common.entity.BaseEntity;

/**
 * 请求回访实体类
 * @author QXY
 *
 */
@SuppressWarnings({ "serial", "rawtypes" })
@Entity
public class Visit extends BaseEntity {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long visitNo;
	@Column(nullable=false)
	private String visitName;
	@Column(nullable=true)
	private String visitItemType;
	@ManyToMany
	private List<VisitItem> visitItems;
	private Boolean useStatus=false;
	
	private Long visitOrder;
	public Long getVisitNo() {
		return visitNo;
	}
	public void setVisitNo(Long visitNo) {
		this.visitNo = visitNo;
	}
	public String getVisitName() {
		return visitName;
	}
	public void setVisitName(String visitName) {
		this.visitName = visitName;
	}
	public String getVisitItemType() {
		return visitItemType;
	}
	public void setVisitItemType(String visitItemType) {
		this.visitItemType = visitItemType;
	}

	public List<VisitItem> getVisitItems() {
		return visitItems;
	}
	public void setVisitItems(List<VisitItem> visitItems) {
		this.visitItems = visitItems;
	}
	public Boolean getUseStatus() {
		return useStatus;
	}
	public void setUseStatus(Boolean useStatus) {
		this.useStatus = useStatus;
	}
	public Long getVisitOrder() {
		return visitOrder;
	}
	public void setVisitOrder(Long visitOrder) {
		this.visitOrder = visitOrder;
	}

	
}
