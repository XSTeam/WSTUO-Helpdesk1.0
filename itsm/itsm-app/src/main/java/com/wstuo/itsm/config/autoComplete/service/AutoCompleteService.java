package com.wstuo.itsm.config.autoComplete.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.wstuo.itsm.config.autoComplete.dao.IAutoCompleteDAO;
import com.wstuo.itsm.config.autoComplete.dto.AutoCompleteParamDTO;
import com.wstuo.common.dto.AutoCompleteDTO;

/**
 * 自动补全 业务层实现类
 * 
 */
public class AutoCompleteService implements IAutoCompleteService {
	@Autowired
	private IAutoCompleteDAO autoCompleteDAO;
	/**
	 * 自动补全数据查询方法
	 * @param paramDto 自定补全参数DTO
	 * @return List<AutoCompleteDTO>
	 */
	public List<AutoCompleteDTO> autoComplete(AutoCompleteParamDTO paramDto) {
		return autoCompleteDAO.autoComplete(paramDto);
	}

}
