package com.wstuo.itsm.domain.dao;

import com.wstuo.common.dao.IEntityDAO;
/**
 * 公共数据访问类
 * @author will
 *
 */
public interface IEventDAO extends IEntityDAO<Event>{

}
