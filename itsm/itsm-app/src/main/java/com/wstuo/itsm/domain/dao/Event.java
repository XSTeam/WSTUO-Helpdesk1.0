package com.wstuo.itsm.domain.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.wstuo.common.config.category.entity.EventCategory;
import com.wstuo.common.config.dictionary.entity.DataDictionaryItems;
import com.wstuo.common.entity.BaseEntity;
import com.wstuo.common.security.entity.User;

/**
 * 请求实体类
 * @author QXY
 *
 */
@SuppressWarnings( {"serial","rawtypes"} )
@MappedSuperclass
public class Event extends BaseEntity {

	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    private Long eno;
	@Column(nullable=false)
    private String etitle;
    @Lob
    private String edesc;
    @ManyToOne(fetch=FetchType.EAGER)
    private DataDictionaryItems effectRange;
    private String effectRemark;
    @ManyToOne(fetch=FetchType.EAGER)
    private DataDictionaryItems seriousness;
    @ManyToOne( fetch = FetchType.EAGER )
    private DataDictionaryItems priority;
    @ManyToOne( fetch = FetchType.EAGER)
    private DataDictionaryItems status;
    @ManyToOne(fetch = FetchType.LAZY)
    private EventCategory eventCategory; 
    //所在位置
    @ManyToOne
    @JoinColumn( name = "locDcode" )
    private EventCategory location;
    
    private String address;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn=new Date();
    @ManyToOne
    private User createdBy;
    @ManyToOne(fetch = FetchType.LAZY)
    private User technician;
    @Lob
    @Column(nullable=true)
    private String solutions;
    @Column(nullable=true)
    private String detailsOf;
    @Column(nullable=true)
    private String keyWord;
    @Column(nullable=true)
    private String eventCode;
    //关闭Code
    private String closeCode;
    //流程是否开启，false 为未开启 ，true为已开启
    private Boolean flowStart = false;
    private Boolean attendance = false;
    private String processKey;


    
    
	public EventCategory getLocation() {
		return location;
	}
	public void setLocation(EventCategory location) {
		this.location = location;
	}
	public String getProcessKey() {
		return processKey;
	}
	public void setProcessKey(String processKey) {
		this.processKey = processKey;
	}
	public Boolean getFlowStart() {
		return flowStart;
	}
	public void setFlowStart(Boolean flowStart) {
		this.flowStart = flowStart;
	}
	public Event() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getEno() {
		return eno;
	}
	public void setEno(Long eno) {
		this.eno = eno;
	}
	public String getEtitle() {
		return etitle;
	}
	public void setEtitle(String etitle) {
		this.etitle = etitle;
	}
	public String getEdesc() {
		return edesc;
	}
	public void setEdesc(String edesc) {
		this.edesc = edesc;
	}
	public DataDictionaryItems getEffectRange() {
		return effectRange;
	}
	public void setEffectRange(DataDictionaryItems effectRange) {
		this.effectRange = effectRange;
	}
	public String getEffectRemark() {
		return effectRemark;
	}
	public void setEffectRemark(String effectRemark) {
		this.effectRemark = effectRemark;
	}
	public DataDictionaryItems getSeriousness() {
		return seriousness;
	}
	public void setSeriousness(DataDictionaryItems seriousness) {
		this.seriousness = seriousness;
	}
	public DataDictionaryItems getPriority() {
		return priority;
	}
	public void setPriority(DataDictionaryItems priority) {
		this.priority = priority;
	}
	public DataDictionaryItems getStatus() {
		return status;
	}
	public void setStatus(DataDictionaryItems status) {
		this.status = status;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public User getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	public User getTechnician() {
		return technician;
	}
	public void setTechnician(User technician) {
		this.technician = technician;
	}
	public String getSolutions() {
		return solutions;
	}
	public void setSolutions(String solutions) {
		this.solutions = solutions;
	}
	public String getDetailsOf() {
		return detailsOf;
	}
	public void setDetailsOf(String detailsOf) {
		this.detailsOf = detailsOf;
	}
	public String getKeyWord() {
		return keyWord;
	}
	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}
	public String getEventCode() {
		return eventCode;
	}
	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}
	public EventCategory getEventCategory() {
		return eventCategory;
	}
	public void setEventCategory(EventCategory eventCategory) {
		this.eventCategory = eventCategory;
	}
	public String getCloseCode() {
		return closeCode;
	}
	public void setCloseCode(String closeCode) {
		this.closeCode = closeCode;
	}
	public Boolean getAttendance() {
		return attendance;
	}
	public void setAttendance(Boolean attendance) {
		this.attendance = attendance;
	}
}
