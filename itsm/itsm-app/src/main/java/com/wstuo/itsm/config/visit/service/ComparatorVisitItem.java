package com.wstuo.itsm.config.visit.service;

import java.util.Comparator;

import com.wstuo.itsm.config.visit.entity.VisitItem;

/**
 * 回访事项比较器
 * @author QXY
 *
 */
@SuppressWarnings("rawtypes")
public class ComparatorVisitItem implements Comparator {
	public int compare(Object arg0, Object arg1) {
		VisitItem vi1 = (VisitItem)arg0;
		VisitItem vi2 = (VisitItem)arg1;
		int flag=vi1.getVisitItemNo().compareTo(vi2.getVisitItemNo());
		if(flag==0){
			return vi1.getVisitItemName().compareTo(vi2.getVisitItemName());
		}else{
			return flag;
		}
	}
}
