package com.wstuo.itsm.config.visit.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import com.wstuo.itsm.config.visit.dao.IVisitDAO;
import com.wstuo.itsm.config.visit.dao.IVisitItemDAO;
import com.wstuo.itsm.config.visit.dto.VisitDTO;
import com.wstuo.itsm.config.visit.dto.VisitItemDTO;
import com.wstuo.itsm.config.visit.entity.Visit;
import com.wstuo.itsm.config.visit.entity.VisitItem;
import com.wstuo.common.dto.PageDTO;
/**
 * 请求回访事项服务层
 * @author QXY
 *
 */
public class VisitItemService implements IVisitItemService {
	
	@Autowired
	private IVisitItemDAO visitItemDAO;
	@Autowired
	private IVisitDAO visitDAO;
	

	/**
	 * 分页查询回访事项
	 * @param visitNo
	 * @param page1
	 * @param limit
	 * @param sord
	 * @param sidx
	 * @return PageDTO
	 */
	@Transactional
	public PageDTO findPagerVisitItems(Long visitNo,int page1, int limit,String sord, String sidx){
		
		PageDTO page=new PageDTO();
		if(visitNo!=null){
			Visit vis=visitDAO.findById(visitNo);
			List<VisitItem> visitItems=vis.getVisitItems();
			int totalSize=visitItems.size();
	        int start = (page1 - 1) * limit;
	        int end = limit*page1-1;
	        if (end >= totalSize) {end = totalSize-1;}
	        ComparatorVisitItem cvi = new ComparatorVisitItem();
	        Collections.sort(visitItems, cvi);
	        List<VisitItemDTO> dtos=new ArrayList<VisitItemDTO>();
	        for ( int i = start; i <=end; i++ )
	        {
	        	VisitItem visitItem=visitItems.get(i);
	        	VisitItemDTO visitItemDTO= new VisitItemDTO();
	        	VisitDTO.entity2dto(visitItem, visitItemDTO);
	        	dtos.add(visitItemDTO);
	        }
	        page.setTotalSize(totalSize);
			page.setData(dtos);
		}
		return page;
		
	};
	/**
	 * 回访事项保存
	 * @param visitItemDTO
	 */
	@Transactional
	public void visitItemSave(VisitItemDTO visitItemDTO){
		VisitItem visitItem = new VisitItem();
		VisitDTO.dto2entity(visitItemDTO, visitItem);
		visitItemDAO.save(visitItem);
		
		Visit visit = visitDAO.findById(visitItemDTO.getVisitNo());
		if(visitItem.getVisitItemNo()!=null){
			if(visit.getVisitItems()==null){
				List<VisitItem> visitItems=new ArrayList<VisitItem>();
				visit.setVisitItems(visitItems);
			}
			visit.getVisitItems().add(visitItem);
		}
		visitDAO.merge(visit);
		visitItemDTO.setVisitItemNo(visitItem.getVisitItemNo());
	};
	/**
	 * 回访事项修改
	 * @param visitItemDTO
	 */
	@Transactional
	public void visitItemUpdate(VisitItemDTO visitItemDTO){
		VisitItem visitItem = visitItemDAO.findById(visitItemDTO.getVisitItemNo());
		VisitDTO.dto2entity(visitItemDTO, visitItem);
		visitItemDAO.update(visitItem);
	};
	/**
	 * 回访事项删除
	 * @param visitNo
	 * @param visitItemNo
	 */
	@Transactional
	public void visitItemDelete(Long visitNo,Long[] visitItemNo){
		Visit visit=visitDAO.findById(visitNo);
		for(int i=0;i<visitItemNo.length;i++){
			VisitItem deleteItem=visitItemDAO.findById(visitItemNo[i]);
			if(visit.getVisitItems()!=null && visit.getVisitItems().contains(deleteItem)){
				visit.getVisitItems().remove(deleteItem);
			}
		}
		visitDAO.merge(visit);

	};
	
	/**
	 * 回访事项修改
	 * @param visitItemNo
	 */
	@Transactional
	public VisitItemDTO findById(Long visitItemNo){
		VisitItemDTO dto=new VisitItemDTO();
		VisitItem entity = visitItemDAO.findById(visitItemNo);
		VisitDTO.entity2dto(entity, dto);
		
		return dto;
	};
}
