<style>
    .table_border{width:100%}.table_border{ border-collapse:collapse; border:solid
    black; border-width:1px 0 0 1px;}.table_border caption {font-size:14px;font-weight:bolder;}.table_border
    th,.table_border td {border:solid black;border-width:0 1px 1px 0;padding-left:5px;}
</style>
<table width="550">
    <#escape x as x!>
        <tr>
            <td colspan="2">
                <#if (userName??)>
                    ${userName}，
                </#if>
                您好:
                </b>
                </font>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                標題為：${variables.etitle}，編號為：${variables.ecode}的變更由${variables.operator}
                指派給：${variables.technicianName}
                <#if (variables.technicianName??)>
                    <#if (variables.assigneeGroupName??)>
                        和
                    </#if>
                </#if>
                ${variables.assigneeGroupName}
                <#if (variables.assigneeGroupName??)>
                    (組)
                </#if>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br>
                謝謝！
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Wstuo Team
            </td>
        </tr>
    </#escape>
</table>
<br>
-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
<br>
這是一封係統的郵件，請勿直接迴複！
<br>
-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~