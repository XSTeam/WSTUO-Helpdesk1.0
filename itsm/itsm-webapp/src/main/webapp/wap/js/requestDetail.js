$import('wstuo.customForm.formControlImpl');
$import('wstuo.tools.xssUtil');
var requestDetail = function() 
{
	this.loadAutoUserName=true;
	this.loadAutoGroup=true;
	this.loadAutoUserName1=true;
	this.loadAutoGroup1=true;
	this.loadAutoUserName2=true;
	this.initDetailForm=function(){
		var htmlDivId = "detail_formField";
		var eno=$('#requestDetails_requestNo').val();
		$.post('request!findRequestById.action?requestQueryDTO.eno='+eno,function(data){
			if(data.formId){
				var _param = {"formCustomId" : data.formId};
				var eventEavVals=data.attrVals;
				$.post('formCustom!findFormCustomById.action',_param,function(res){
					var formCustomContents = wstuo.customForm.formControlImpl.editHtml(res.formCustomContents,htmlDivId,true);
					$("#"+htmlDivId).html(formCustomContents);
					$('#'+htmlDivId).find('.glyphicon-trash').remove();
					$("#"+htmlDivId).find(".required").remove();
					for(var o in eventEavVals){
						var value='';
						var attrName=o;
						var obj=$("#"+htmlDivId+" :input[name*='"+attrName+"']");
						if(eventEavVals && eval('eventEavVals.'+attrName)!==undefined && eval('eventEavVals.'+attrName)!==''){
							value = eval('eventEavVals.'+attrName);
						}
						if(value==undefined || value==null)value='';
						
						if(obj.attr("attrType")=="radio" || obj.attr("attrType")=="checkbox"){
							obj.siblings("."+obj.attr("attrType")+"-inline").remove();
							obj.replaceWith(value.substr(value.indexOf('~')+1));
						}else if(obj.attr("attrType")=="tree"){
							obj.siblings(".form-control").remove();
							obj.replaceWith(value.substr(value.indexOf('~')+1));
						}else if(obj.attr("attrType")=="select"){
							obj.hide();
							if(value.length>0)
								showDataDicInfo(value,"#"+htmlDivId,attrName);
							else obj.replaceWith(value);
						}else{
							value = wstuo.tools.xssUtil.html_code(value);
							obj.replaceWith(value);
						}
					}
				});
			}else{
				$("#detail_formField_row").hide();
			}
			
		});
	};
	this.showDataDicInfo=function(attrValNo,formId,attrName){
		$.post('dataDictionaryItems!findByDcode.action?groupNo='+attrValNo,function(res){
			$(formId+" :input[name*='"+attrName+"']").replaceWith(res.dname);
		});
	};
	this.tag='';
	 return{
		
		requestHistorytabClick:function(){
			var requestHistoryRecord=new historyRecord('requestHistoryRecord',$('#requestDetails_requestNo').val(),'itsm.request');
			requestHistoryRecord.showHistoryRecord();
		},
		saveSlutionsOnly:function(){
			tag="only";
			requestDetail.saveSlutions();
		},
		/**
		 * 保存解决方案（保存到知识库）
		 */
		saveSlutionsBoth:function(){
			tag="both";
			requestDetail.saveSlutions();
			
		},
		/**
		 * 保存解决方案method
		 */
		saveSlutions:function(){
			var content =editor.$txt.html();// $("#request_detail_solutions").val();
			content = content=content.replace(/&nbsp;/g,"").replace(/<p>/g,"").replace(/<\/p>/g,"");
			content=$.trim(content);
			$('#request_detail_solutions').val(content);
			if(content === ''){
				msgAlert(i18n.title_bpm_solutions_not_empty_validate,'info');
			}else{
					var frm = $('#requestSolutionsDiv form').serialize();
					var url = 'request!saveSolutions.action';
					startProcess();
					$.post(url,frm, function()
					{
						endProcess();
						if($("#requestDetails_offmode").val()!==''){
							$("#requestDetails_offmode").attr("disabled","true");
						}else{
							$("#requestDetails_offmode").attr("disabled","");
						}
						if(tag=="both"){
							requestDetail.saveToKnowledge();
						}else{
							msgAlert(i18n.msg_request_solutionsSaveSuccessful,'show');
						}
					});
			}
		},
		/**
		 * @description 解决方案转为知识
		 */
		saveToKnowledge:function(){
			var content=editor.$txt.html();//trim($("#request_detail_solutions").val());
			if(content === ''){
				msgAlert(i18n.title_bpm_solutions_not_empty_validate,'info');
			}else{
				location.href="request!jumpToAddKnowledgeWap.action?eno="+$('#requestDetails_requestNo').val();
			}
		},
		showEventAttachment:function(eventAttachmentTable,eno,eventType,delshow){
			if(eno!=null && eno!=""){
				$('#'+eventAttachmentTable+' table tbody').html('');
				var url = 'eventAttachment!findAllEventAttachment.action';
				$.post(url,'eventAttachmentDto.eno='+eno+'&eventAttachmentDto.eventType='+eventType, function(res){
					var arr=res;
					if(arr!=null && arr.length>0){
						for(var i=0;i<arr.length;i++){
							var file=arr[i].url;
							var fileName=file.substring(file.lastIndexOf("/")+1);
							var fileFix=fileName.substring(fileName.indexOf(".")).toLowerCase();
							var iconUrl="<img src='../images/attachicons/";
							if(fileFix==".rar"){
								iconUrl=iconUrl+"rar.gif'";
							}
							if(fileFix==".zip"){
								iconUrl=iconUrl+"zip.gif'";
							}
							if(fileFix==".gif"||fileFix==".jpg"||fileFix==".bmp"||fileFix==".png"){
								iconUrl=iconUrl+"image.gif'";
							}
							if(fileFix==".doc"||fileFix==".docx"||fileFix==".xls"||fileFix==".ppt"||fileFix==".pptx"||fileFix==".xlsx"){
								iconUrl=iconUrl+"msoffice.gif'";
							}
							if(fileFix==".pdf"){
								iconUrl=iconUrl+"pdf.gif'";
							}
							if(fileFix==".swf"){
								iconUrl=iconUrl+"flash.gif'";
							}
							if(fileFix==".txt"){
								iconUrl=iconUrl+"text.gif'";
							}
							else{
								iconUrl=iconUrl+"unknown.gif'";
							}
							iconUrl=iconUrl+" width='14px' height='14px'/>&nbsp;";
							var attachmetstr="<tr id={id}>" +
									"<td>{no}</td>" +
									"<td style='text-align: left;' >{name}</td>" +
									"<td>{download}</td>" +
									"</tr>";
							attachmetstr=attachmetstr.replace('{id}',eventAttachmentTable+'eventAttachment_ID_'+arr[i].aid)
													 .replace('{no}',i*1+1)
													 .replace('{name}',iconUrl+'<a href=attachment!download.action?downloadAttachmentId='+arr[i].aid+' target=_blank>'+arr[i].attachmentName.substring(arr[i].attachmentName.lastIndexOf("/")+1)+'</a>')
													 .replace('{url}','<a href=attachment!download.action?downloadAttachmentId='+arr[i].aid+' target=_blank>'+arr[i].url+'</a>')
							if(delshow){
								attachmetstr=attachmetstr.replace('{download}','<a href=attachment!download.action?downloadAttachmentId='+arr[i].aid+' target=_blank>['+i18n['common_download']+']</a>'+
										 '&nbsp;&nbsp;<a href=javascript:wstuo.tools.eventAttachment.deleteEventAttachment("'+eventAttachmentTable+'",'+arr[i].aid+','+eno+')>['+i18n['deletes']+']</a>')
							}else{
								attachmetstr=attachmetstr.replace('{download}','<a href=attachment!download.action?downloadAttachmentId='+arr[i].aid+' target=_blank>['+i18n['common_download']+']</a>')
							}
							//$('#'+eventAttachmentTable+' table thead').remove();
							$('#'+eventAttachmentTable+' table tbody').append(attachmetstr)
						}
					}else{
						$('#'+eventAttachmentTable+' table thead').remove();
						$('#'+eventAttachmentTable+' table tbody').append("<tr><td style='color:red;font-size:16px' colspan='4'>"+i18n['noData']+"</td></tr>");
					}
				});
			}else{
				$('#'+eventAttachmentTable+' table thead').remove();
				$('#'+eventAttachmentTable+' table tbody').append("<tr><td style='color:red;font-size:16px' colspan='4'>"+i18n['noData']+"</td></tr>");
			}
			
		},
		/**
		 * @description 选择指派组
		 * @param assigneeGroupNo 指派组No
		 * @param assigneeGroupName 指派组名称
		 */
		selectAssginGroup:function(assigneeGroupNo,assigneeGroupName){
			if(loadAutoGroup){
				//请求用户
				autocomplete.bindAutoComplete(assigneeGroupName,'com.wstuo.common.security.entity.Organization','orgName','orgName','orgNo','Long',assigneeGroupNo,companyNo,false);//所属客户
				loadAutoGroup=false;
			}
			
		},
		/**
		 * @description 选择指派信息
		 * @param userName 用户名
		 * @param userId 用户Id
		 */
		selectRequestUser_byOrgNo:function(userName,userId){
			if(loadAutoUserName){
				//请求用户
				autocomplete.bindAutoComplete(userName,'com.wstuo.common.security.entity.User','fullName','fullName','userId','Long',userId,null,false);
				loadAutoUserName=false;
			}
		},
		/**
		 * @description 选择指派组
		 * @param assigneeGroupNo 指派组No
		 * @param assigneeGroupName 指派组名称
		 */
		selectAssginGroup1:function(assigneeGroupNo,assigneeGroupName){
			if(loadAutoGroup1){
				//请求用户
				autocomplete.bindAutoComplete(assigneeGroupName,'com.wstuo.common.security.entity.Organization','orgName','orgName','orgNo','Long',assigneeGroupNo,companyNo,false);//所属客户
				loadAutoGroup1=false;
			}
			
		},
		/**
		 * @description 选择指派信息
		 * @param userName 用户名
		 * @param userId 用户Id
		 */
		selectRequestUser_byOrgNo1:function(userName,userId){
			if(loadAutoUserName1){
				//请求用户
				autocomplete.bindAutoComplete(userName,'com.wstuo.common.security.entity.User','fullName','fullName','userId','Long',userId,null,false);
				loadAutoUserName1=false;
			}
		},
		/**
		 * @description 选择指派信息
		 * @param userName 用户名
		 * @param userId 用户Id
		 */
		selectRequestUser_byOrgNo2:function(userName,userId){
			if(loadAutoUserName2){
				//请求用户
				autocomplete.bindAutoComplete(userName,'com.wstuo.common.security.entity.User','fullName','fullName','userId','Long',userId,null,false);
				loadAutoUserName2=false;
			}
		},
		init: function() {
			//$("#requestDetail_Category").click(selectTree);
			if($("#flow_action_ul").children('li').length==0)$("#flow_action").remove();
			initNavbar();
			initDetailForm();
			requestDetail.requestHistorytabClick();
			$('#saveToKnowledgeBtn').click(requestDetail.saveSlutionsBoth);
			$('#saveSolutionsBtn').click(requestDetail.saveSlutionsOnly);
			requestDetail.showEventAttachment('show_request_effectAttachment',$('#requestDetails_requestNo').val(),'itsm.request',false);
			
			initValidate();
		}
	 };
}();
//载入
$(document).ready(requestDetail.init);