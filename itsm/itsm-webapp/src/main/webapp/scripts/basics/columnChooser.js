
 /**  
 * @fileOverview 列选择主函数
 * @author QXY
 * @version 1.0  
 */  
 /**  
 * @author QXY  
 * @constructor 列选择主函数
 * @description 列选择主函数
 * @date 2011-07-13
 * @since version 1.0 
 */ 
var columnChooser=function(){
	
	
	//全选、反选
	this.colAllUnSelect=function(gridId){
		if($('#colAllUnSelect_'+gridId).attr('checked'))
    		$('input[name="colNames"]').attr("checked",true);
    	else
    		$('input[name="colNames"]').attr("checked",false);
	};
	
	
	//默认加载显示隐藏列
	this.defaultLoadColumn=function(gridId,categoryNo){
		var _evnetType=gridId;
    	if(categoryNo!=undefined && categoryNo!='')
    		_evnetType=_evnetType+categoryNo;
		$.post('userCustom!findUserCustomByLoginNameAndType.action','queryDTO.eventType='+_evnetType+'&queryDTO.customType=2&queryDTO.loginName='+userName,function(data){
			if(data!=null && data.customResult!=null){
				var _customResult=data.customResult;
				
			
				if(hideCompany){//内部版
					_customResult=_customResult.replace('companyName','');
				}
				
				var LoadColumn_Hide=strToArray(_customResult.substring(8,_customResult.indexOf('showCol=')));
				var LoadColumn_Show=strToArray(_customResult.substring(_customResult.indexOf('showCol=')+8));
				if((LoadColumn_Hide==null || LoadColumn_Hide=='') && (LoadColumn_Show==null || LoadColumn_Show=='')){

				}else{
					$(gridId).jqGrid('hideCol',LoadColumn_Hide);
					$(gridId).jqGrid('showCol',LoadColumn_Show);
				}
			}
		});

	};
	//加载全部列
	this.loadColumnChooserItem=function(gridId,categoryNo){
		$('#columnChooserDiv_'+gridId+' table tbody').html('');
		
		$('#columnChooserDefaultBut_'+gridId+',#columnChooserTopBut_'+gridId).unbind();
		$('#columnChooserTopBut_'+gridId).click(function(){//确定按钮
			if(categoryNo!=undefined && categoryNo!='')
				confirmSelectCol(gridId,categoryNo);
			else
				confirmSelectCol(gridId,'');
		});
		
		$('#columnChooserDefaultBut_'+gridId).click(function(){//默认显示
			if(categoryNo!=undefined && categoryNo!='')
				customColumnSave('','#'+gridId+categoryNo);
			else
				customColumnSave('','#'+gridId);
			$('#columnChooserWin_'+gridId).dialog('colse');
			basics.tab.tabUtils.refreshWindow();
		});
		
		
    	var colNames=$('#'+gridId).jqGrid('getGridParam','colNames');//全部的列标题
    	var colModel=$('#'+gridId).jqGrid('getGridParam','colModel');//全部的列对象
    	
    	var _column_Show='';
    	var evnetType='#'+gridId;
    	if(categoryNo!=undefined && categoryNo!='')
    		evnetType=evnetType+categoryNo;
    	$.post('userCustom!findUserCustomByLoginNameAndType.action','queryDTO.eventType='+evnetType+'&queryDTO.customType=2&queryDTO.loginName='+userName,function(data){
			if(data!=null && data.customResult!=null){
				var _customResult=data.customResult;
				var LoadColumn_Hide=strToArray(_customResult.substring(8,_customResult.indexOf('showCol=')));
				_column_Show=strToArray(_customResult.substring(_customResult.indexOf('showCol=')+8));
			}
		});
    	
		for(var i=1;i<colNames.length-1;i++){
			var colStr='<tr><td><input type="checkbox" name="colNames" class="colNames" value="{value}" /></td><td>{name}</td></tr>';
			var Column_Show=strToArray(_column_Show);//数据库保存显示的值
			
			//默认值
			if(Column_Show==null || Column_Show==''){//如果没值，则显示默认
				
				var column=$('#'+gridId).jqGrid('getGridParam','colModel');
				var columnStr='';
				for(var k=0;k<column.length;k++){
					if(!column[k].hidden){
						columnStr=columnStr+','+column[k].name;
					}
				}
				
				Column_Show=strToArray(columnStr);
			}
			
			if(Column_Show!=null && Column_Show !=''){
				for(var j=0;j<Column_Show.length;j++){
					
					if(Column_Show[j]==colModel[i].name){
						colStr='<tr><td><input type="checkbox" checked name="colNames" class="colNames" value="{value}" /></td><td>{name}</td></tr>';
					}
				}
			}
			if(hideCompany && colNames[i]==i18n['label_belongs_client']){
			}else{
				if(colNames[i]!='')
					$('#columnChooserDiv_'+gridId+' table tbody').append(colStr.replace('{value}',colModel[i].name).replace('{name}',colNames[i]));
			}
			
		}
		windows('columnChooserWin_'+gridId,{width:250,height:300});
		
	};
	//确定选择列
	this.confirmSelectCol=function(gridId,categoryNo){
		var showCol=[];
		var hideCol=[];
		$("#columnChooserDiv_"+gridId+" .colNames").each(function(){
			if($(this).attr('checked')){
				showCol.push($(this).val());
			}else{
				hideCol.push($(this).val());
			}
		});
		
		if(showCol.length<=0){
			msgAlert(i18n.msg_choose_column,'info');
		}else{
			
			$('#'+gridId).jqGrid('hideCol',hideCol);
			$('#'+gridId).jqGrid('showCol',showCol);
			var _gridId='#'+gridId;
			if(categoryNo!=undefined && categoryNo!='')
				_gridId='#'+gridId+categoryNo;
			customColumnSave('hideCol='+hideCol+'showCol='+showCol,_gridId);
			$('#columnChooserWin_'+gridId).dialog('colse');
		}
		
		
	};
	
	this.customColumnSave=function(value,gridId){
		$.post('userCustom!dashboardCustomSet.action','userCustomDTO.eventType='+gridId+'&userCustomDTO.customType=2&userCustomDTO.loginName='+userName+'&userCustomDTO.customResult='+value,function(){
			msgShow(i18n['common_operation_success'],'show');
		});
	};
	
	
}();