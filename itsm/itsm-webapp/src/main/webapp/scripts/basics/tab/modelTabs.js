$package('basics.tab');

basics.tab.modelTabs = {
        'index' : [
                   i18n.label_dashboard_my_panel
        ],
        'request' : [
                     i18n.title_request_requestGrid,
                     i18n.title_request_addRequest,
                     i18n.title_request_editRequest,
                     i18n.request_detail
        ],
        'problem' : [
                     i18n.problem_grid,
                     i18n.problem_edit,
                     i18n.problem_add,
                     i18n.probelm_detail
        ],
        'change' : [
                    i18n.title_changeList,
                    i18n.titie_change_edit,
                    i18n.titie_change_add,
                    i18n.change_detail
        ],
        'release' : [
                    i18n.release_grid,
                    i18n.release_edit,
                    i18n.release_add,
                    i18n.release_details
        ],
        'cim' : [
                 i18n.ci_configureItemAdmin,
                 i18n.ci_addConfigureItem,
                 i18n.ci_editConfigureItem,
                 i18n.ci_configureItemInfo
        ],
        'knowledgeBase' : [
                           i18n.title_request_knowledgeGrid,
                           i18n.title_request_newKnowledge,
                           i18n.knowledge_editKnowledge,
                           i18n['knowledge'] + i18n['details'],
        ],
        'sla' : [
                 i18n.default_page_sla,
                 i18n.title_sla_slaDetail
        ],
        'report' : [
                    i18n.default_report_single,
                    i18n.KPIReport,
                    i18n.CrosstabReport,
                    i18n.MutilGroupReport,
                    
                    i18n.custom_report_uploadedReport
        ],
        'tool' : [
                  i18n.Affiche,
                  i18n.default_title_sms,
                  i18n.default_page_emailMgt,
                  i18n.label_message_manageTitle,
                  i18n.title_personTask,
                  i18n.title_calendar,
                  i18n.Schedule,
                  i18n.label_tools_workloadStatusStatistics
        ],
        'systemSet' : [
                       i18n.label_org_companyInfo,
                       i18n.default_security_orgSet,
                       i18n.default_title_customerManager,
                       i18n.default_title_supplierManager,
                       i18n.default_title_itsop_customer,
                       
                       i18n.resetPassword,
                       i18n.default_security_user,
                       i18n.default_title_role,
                       i18n.default_security_resourceManager,
                       i18n.ldap_authentication_setting,
                       i18n.default_title_LDAPManager,
                       i18n.default_title_areaUser,
                       // ----------------------
                       i18n.DataDictionaryGroup,
                       i18n.label_dc_imode,
                       i18n.label_dc_priority,
                       i18n.label_dc_level,
                       i18n.label_dc_seriousness,
                       i18n.label_dc_effectRange,
                       i18n.label_dc_supplier,
                       i18n.label_dc_useStatus,
                       i18n.label_dc_brand,
                       i18n.label_dc_workloadStatus,
                       i18n.label_dc_itsopType,
                       i18n.label_dc_softwareType,
                       i18n.label_dc_SLAStatus,
                       i18n.label_dc_systemPlatform,
                       i18n.label_dc_offmode,
                       i18n.label_dc_requestStatus,
                       i18n.label_dc_problemStatus,
                       i18n.label_dc_changeStatus,
                       i18n.label_dc_releaseStatus,
                       i18n.label_dc_ciRelationType,
                       i18n.label_dc_location,
                       // ----------------------
                       i18n.label_request_requestType,
                       i18n.default_title_problemCategory,
                       i18n.label_request_changeCategory,
                       i18n.label_release_releaseCategory,
                       i18n.label_knowledge_knowledgeCategory,
                       i18n.label_software_softwareCategory,
                       i18n.ci_configureItemCategory,
                       // ----------------------
                       i18n.PriorityMatrix,
                       i18n.AttributeGroup,
                       i18n.EavEntity,
                       i18n.Visit,
                       i18n.UpdateLevel,
                       i18n.default_cab,
                       i18n.title_service_directory,
                       
                       i18n.UserOnlineLog,
                       i18n.userOptLog,
                       i18n.exportUserErrLog,
                       
                       i18n.default_page_sla,
                       
                       i18n.lable_Process_rules_package_management,
                       i18n.title_rule_ruleList,
                       i18n.request_process_rule,
                       i18n.default_security_changeRule,
                       i18n.label_ur_request_to_problem_rule,
                       
                       i18n.default_security_processManage,
                       i18n.flowExample,
                       i18n.ProcessUse,
                       // 自定义流程 此注释仅仅是提示在菜单项对应有这个选项
                       
                       i18n.label_systemGuide_title,
                       i18n.title_dashboard_setting,
                       i18n.title_scheduled_task_manage,
                       i18n.notitceRule,
                       i18n.notice_noticeRuleManage,
                       i18n.default_security_license,
                       i18n.lable_antuorization,
                       i18n.lable_serverUrlSet,
                       i18n.TimeZone,
                       i18n.Currency,
                       
                       
                       i18n.title_sla_slaDetail,
                       i18n.label_rule_addRequestRule,
                       i18n.label_rule_editRequestRule,
                       i18n.label_rule_addEmailToRequestRule,
                       i18n.label_rule_editEmailToRequestRule,
                       i18n.label_rule_addChangeRule,
                       i18n.label_rule_editChangeRule,
                       i18n.lable_created_adddrl_processing,
                       i18n.lable_created_editdrl_processing,
                       i18n.flowChart,
                       i18n.title_flow_property_setting,
                       i18n.title_request_traceInstance,
                       i18n.title_add_scheuledTask,
                       i18n.title_edit_scheuledTask,
                       i18n.addNotice,
                       i18n.editNoticeRule,
                       i18n.request_add_templet_title,
                       i18n.problem_add_templet_title,
                       i18n.change_add_templet_title,
                       i18n.ci_add_templet_title,
                       i18n.issue_add_templet_title,
                       i18n.request_edit_templet_title,
                       i18n.problem_edit_templet_title,
                       i18n.change_edit_templet_title,
                       i18n.ci_edit_templet_title,
                       i18n.issue_edit_templet_title,
        ]
};