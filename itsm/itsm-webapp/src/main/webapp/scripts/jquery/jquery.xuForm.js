/*
* Name:xuForm0.1
* Author xuwei
* Email xuwei0930@gmail.com
* Date: 2012-12-13
* 简单的表单美化插件
*API==================
wid    //自定义  自适应（100%）
*/

(function($){
    $.fn.xuForm = function(options){
    	var lang=$("#login_lang").val();
        var defaults = {
            wid       :"default"    //default auto
        };
        var options = $.extend(defaults, options);
		this.each(function(){
			currt=$(this);
			//select===========================================================================$("#login_lang").val()
			currt.find("select").each(function(index,element){
				sel=$(element);
				$(element).attr("id","xuSelect"+index).hide();//隐藏select
				var selNum=$(element).find("option");
				if(options.wid=="default"){
					$sel=$("<div>").addClass("xuSelect").css({"width":sel.width()+"px"}).attr("ins",index).insertAfter(sel);
					selTxt='<span class="xuSelect-fa"><span class="xuSelect-currt">'+sel.find("option:selected").text()+'</span><i></i></span><ul class="xuSelect-son" style="width:'+(sel.width()-22)+'px;">';
				}else if(options.wid=="auto"){
					$sel=$("<div>").addClass("xuSelect").css({"width":"80%"}).attr("ins",index).insertAfter(sel);
					selTxt='<span class="xuSelect-fa"><span class="xuSelect-currt">'+sel.find("option:selected").text()+'</span><i></i></span><ul class="xuSelect-son" style="width:'+($sel.width()-22)+'px">';
				}
				var defaults="";
				if((lang==null||lang=='')||(lang!='zh_CN'&&lang!='en_US'&&lang!='ja_JP'&&lang!='zh_TW')){
					defaults="简体中文";
				}
				selNum.each(function(index1,element1){
					selTxt+='<li order="'+index1+'" value="'+$(element1).val()+'">'+$(element1).text()+'</li>';
					if($(element1).val()!=null && $(element1).val()!='' && lang==$(element1).val()){
						defaults=$(element1).text();
					}
				})
				selTxt+='</ul>';
				$sel.append(selTxt);
				$sel.find(".xuSelect-currt").html(defaults);
			})
				//点击弹出option
			$('.xuSelect-fa').click(function(){
				var fa=$(this);
				var son=$(this).next(".xuSelect-son");
				if($(this).next(".xuSelect-son").css('display')=='none'){$(".xuSelect-son").hide();} 
				if(currt.attr('disabled')){return false;}
				son.toggle().find("li.checked").removeClass("checked")
				son.find("li:odd").addClass("odd");
				if(lang=="zh_CN"){
					son.find("li[order=0]").addClass("checked");
				}else if(lang=="zh_TW"){
					son.find("li[order=1]").addClass("checked");
				}else if(lang=="en_US"){
					son.find("li[order=2]").addClass("checked");
				}else if(lang=="ja_JP"){
					son.find("li[order=3]").addClass("checked");
				}else{
					son.find("li[order=0]").addClass("checked");
				}
				bodyClick(fa);
				return false;
			})
	
	
				//弹出option之后点击事件
			var bodyClick =function(obj){
				var ins=obj.parent(".xuSelect").attr("ins");
				$('body').click(function(e){
					var e=e?e:window.event;
					var tar = e.srcElement||e.target;
					if($(tar).is(".xuSelect-son>li")){
						obj.find(".xuSelect-currt").html($(tar).text());
						obj.find(".xuSelect-currt").attr("order",$(tar).attr("order"));
						$("#xuSelect"+ins).find("option").eq($(tar).attr("order")).attr("selected", true);
						obj.next(".xuSelect-son").hide();
						$('body').unbind("click");
						location.href='login.jsp?language='+$("#xuSelect"+ins).find("option").eq($(tar).attr("order")).val();
					}else if($(tar).is(".xuSelect-son")){
						return false;
					}else{
						obj.next(".xuSelect-son").hide();
						$('body').unbind("click");
					}
				})
			}
		})

    };
})(jQuery);