$package('common.report');
$import("common.report.processingTime");
/**  
 * @fileOverview "时间绑定和默认值"
 * @author Martin  
 * @constructor Martin
 * @description 时间绑定和默认值
 * @date 2014-09-13
 * @version 1.0  
 * @since version 1.0 
 */
common.report.defaultTimeAndBind=function(){
	DatePicker97(['#ev_created_end_search','#ev_created_start_search',
	         '#ev_createdOn_end_History','#ev_createdOn_start_History',
	         '#ev_According_statistics_start_search','#ev_According_statistics_end_search',
	         '#ev_Statistical_reports_start_search','#ev_Statistical_reports_end_search',
	         '#ev_createdOn_start_Category','#ev_createdOn_end_Category',
	         '#search_reportcreatedOn_start','#search_report_createdOn_end',
	         '#ev_createdOn_start','#ev_createdOn_end',
	         '#ev_createdOn_start_search','#ev_createdOn_end_search',
	         '#ev_createdOn_req_start_detailed','#ev_createdOn_req_end_detailed',
	         '#optZipStartTime','#optZipEndTime',
	         '#zipStartTime','#zipEndTime',
	         '#errorZipStartTime','#errorZipEndTime'
	         ]);
	return{
		init: function(){
			var firstDate=common.report.processingTime.showMonthFirstDay(currentMonth);
	    	var lastDate=common.report.processingTime.showMonthLastDay(currentMonth); 
			$('#search_reportcreatedOn_start').val(firstDate);
			$('#search_report_createdOn_end').val(lastDate);
			$('#ev_createdOn_start').val(firstDate);
			$('#ev_createdOn_end').val(lastDate);
			$('#ev_createdOn_start_Category').val(firstDate);
			$('#ev_createdOn_end_Category').val(lastDate);
			$('#ev_createdOn_start_search').val(firstDate);
			$('#ev_createdOn_end_search').val(lastDate);
			$('#ev_Statistical_reports_start_search').val(firstDate);
			$('#ev_Statistical_reports_end_search').val(lastDate);
			$('#ev_According_statistics_start_search').val(firstDate);
			$('#ev_According_statistics_end_search').val(lastDate);
			$('#ev_createdOn_start_History').val(firstDate);
			$('#ev_createdOn_end_History').val(lastDate);
			$('#ev_created_start_search').val(firstDate);
			$('#ev_created_end_search').val(lastDate);
			$('#ev_createdOn_req_start_detailed').val(firstDate);
			$('#ev_createdOn_req_end_detailed').val(lastDate);
			$('#optZipStartTime').val(firstDate);
			$('#optZipEndTime').val(lastDate);
			$('#zipStartTime').val(firstDate);
			$('#zipEndTime').val(lastDate);
			$('#errorZipStartTime').val(firstDate);
			$('#errorZipEndTime').val(lastDate);
		}
	}
}();
$(document).ready(common.report.defaultTimeAndBind.init);
