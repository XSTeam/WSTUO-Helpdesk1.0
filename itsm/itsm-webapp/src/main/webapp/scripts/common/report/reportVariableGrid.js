/**  
 * @author Van  
 * @constructor WSTO
 * @description 自定义报表.
 * @date 2010-11-17
 * @since version 1.0 
 */ 
$package('common.report');


/**  
 * @fileOverview "报表返回展示预览查看"
 * @author Van  
 * @constructor WSTO
 * @description 报表返回展示预览查看
 * @date 2011-06-13
 * @version 1.0  
 * @since version 1.0 
 */
common.report.reportVariableGrid=function(){
	

	this.reportVariableOperator='save';
	
	this.Variable='';
	this.loadVariableGridFlag=false;
	
	return {
		
		
		/**
		 * @description 动作格式化.
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 */
		isGroupFormatter:function(cell,event,data){
		
			var arr={'true':i18n['label_dynamicReports_yes'],'false':i18n['label_dynamicReports_no']};
			
			return arr[data.isGrouped];
		},
		
		
		/**
		 * @description 动作格式化.
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 */
		stisticFieldFormatter:function(cell,event,data){
			
			var entityClass=$('#add_edit_KPIReport_entityClass').val();
			
			if(entityClass!=null && entityClass!=''){
				
				return i18n[report_group_fields[entityClass]['xcountField'][data.statField]];
				
			}else{
				
				return data.statField;
			}
		},
		/**
		 * @description 格式化统计字段.
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 */
		stisticTypeFormatter:function(cell,event,data){
		
			
			var arr={'count':'label_dynamicReports_count','sum':'label_dynamicReports_sum'};
			
			return i18n[arr[data.statType]];
			
		},
		/**
		 * @description 刷新列表
		 */
		refreshGrid:function(){
			
			var reportId=$('#add_edit_KPIReport_reportId').val();
			
			if(reportId==null || reportId==''){//新增
				reportId=-1;
			}
			var entityClass=$('#add_edit_KPIReport_entityClass').val();
			if(entityClass==null || entityClass==''){//新增
				entityClass="NO";
			}
			var _postData = $("#reportVariableGrid").jqGrid("getGridParam", "postData");       
			$.extend(_postData, {'reportVariableQueryDTO.reportId':reportId,'reportVariableQueryDTO.entityClass':entityClass});	
			$('#reportVariableGrid').trigger('reloadGrid',[{"page":"1"}]);
			
		},
		
		
		/**
		 * @description  显示列表.
		 * @param  entityClass 类名
		 */
		showGrid:function(entityClass){
			
			
			var reportId=$('#add_edit_KPIReport_reportId').val();
			
			
			
			if(reportId==null || reportId==''){//新增
				reportId=-1;
			}
			if(loadVariableGridFlag==false){//全新加载
				
				loadVariableGridFlag=true;
				common.report.reportVariableGrid.loadGrid(reportId,entityClass);
				
			}else{//刷新列表
				
				var _postData = $("#reportVariableGrid").jqGrid("getGridParam", "postData");       
				$.extend(_postData, {'reportVariableQueryDTO.reportId':reportId});	
				$('#reportVariableGrid').trigger('reloadGrid',[{"page":"1"}]);
			}
		},

		/**
		 * @description 加载列表.
		 * @param reportId  报表编号
		 * @param entityClass 类名
		 */
		loadGrid:function(reportId,entityClass){
			windows('reportVariableGrid_window');
			var params = $.extend({},jqGridParams, {
				url:'variable!findPager.action',
				postData:{'reportVariableQueryDTO.reportId':reportId,'reportVariableQueryDTO.entityClass':entityClass},
				colNames:[i18n['label_dynamicReports_name'],i18n['label_dynamicReports_code'],i18n['label_dynamicReports_statisticField'],i18n['label_dynamicReports_statisticType'],i18n['label_dynamicReports_isGroup'],'','','','',''],
				colModel:[
			   		{name:'variableName',align:'center',width:20},
			   		{name:'variableCode',align:'center',width:20,},
			   		{name:'statField_label',align:'center',sortable:false,width:20,formatter:common.report.reportVariableGrid.stisticFieldFormatter},
			   		{name:'statType_label', align:'center',sortable:false,width:20,formatter:common.report.reportVariableGrid.stisticTypeFormatter},
			   		{name:'isGrouped_label',align:'center',sortable:false,width:20,formatter:common.report.reportVariableGrid.isGroupFormatter,sortable:false},
			   		{name:'isGrouped',hidden:true},
			   		{name:'variableId',hidden:true},
			   		{name:'isRelated',hidden:true},
			   		{name:'statField',hidden:true},
			   		{name:'statType',hidden:true}
			   	],		
				jsonReader: $.extend(jqGridJsonReader, {id: "variableId"}),
				sortname:'variableId',
				pager:'#reportVariableGridPager',
				ondblClickRow:function(rowId){//填充到KPI表达式文本框
					var data=$('#reportVariableGrid').getRowData(rowId);
					var vl=$("#add_edit_KPIReport_KPIExpression").val();
					$("#add_edit_KPIReport_KPIExpression").val(vl+data.variableCode);
				}
			});
			$("#reportVariableGrid").jqGrid(params);
			$("#reportVariableGrid").navGrid('#reportVariableGridPager',navGridParams);
			//列表操作项
			$("#t_reportVariableGrid").css(jqGridTopStyles);
			$("#t_reportVariableGrid").append($('#reportVariableGridToolbar').html());
			

			$('#reportVariableGrid_add,#reportVariableGrid_save').unbind();

			//添加
			$('#reportVariableGrid_add').click(common.report.reportVariableGrid.preAdd);
			//编辑
			$('#reportVariableGrid_edit').click(common.report.reportVariableGrid.preEdit);
			//保存
			$('#reportVariableGrid_save').click(common.report.reportVariableGrid.doSave);
			//删除
			$('#reportVariableGrid_delete').click(common.report.reportVariableGrid.doDelete);

		},
		
		/**
		 * 根据类型查找.
		 *  @param entityClass 类名
		 */
		queryByEntityClass:function(entityClass){
			
			var _postData = $("#reportVariableGrid").jqGrid("getGridParam", "postData");       
			$.extend(_postData, {'reportVariableQueryDTO.entityClass':entityClass});	
			$('#reportVariableGrid').trigger('reloadGrid',[{"page":"1"}]);
			
		},
		/**
		 * 加载统计字段
		 * @param entityClass 类名
		 */
		loadGroupField:function(entityClass){
			//加载统计字段
			$('#add_edit_reportVariable_statField').html('<option value="">-- '+i18n['label_dynamicReports_chooseCountField']+' --</option>');
			
			
			$.each(report_group_fields[entityClass]['xcountField'],function(k,v){
				$('<option value="'+k+'">'+i18n[v]+'</option>').appendTo('#add_edit_reportVariable_statField');
			});
			
		},
		
		
		preAdd:function(){
			
			var entityClass=$('#add_edit_KPIReport_entityClass').val();
			
			if(entityClass!=null && entityClass!=''){
				resetForm('#reportVariableGrid_add_edit_window form');
				windows('reportVariableGrid_add_edit_window',{width:320});
				reportVariableOperator='save';
			}else{
				
				msgAlert(i18n['label_dynamicReports_selectReportType'],'info');
			}
			
			
		},
		
		/**
		 * 保存
		 */
		doSave:function(){
			startProcess();
			if($('#reportVariableGrid_add_edit_window form').form('validate')){
				var KPIExpression=trim($('#add_edit_KPIReport_KPIExpression').val());
				if(KPIExpression!=''){
					var KPIReportKPIExpression=$('#add_edit_KPIReport_KPIExpression').val().replace(Variable,$('#add_edit_reportVariable_variableCode').val());
					$('#add_edit_KPIReport_KPIExpression').val(KPIReportKPIExpression)
				}
				$('#add_edit_reportVariable_reportId').val($('#add_edit_KPIReport_reportId').val());//KPI编号
				
				$('#add_edit_reportVariable_entityClass').val($('#add_edit_KPIReport_entityClass').val());//类型

				var _param = $('#reportVariableGrid_add_edit_window form').serialize();
				var _KPIExpressions = $.param({'reportVariableDTO.xpression':$("#add_edit_KPIReport_KPIExpression").val()},true);

				$.post('variable!'+reportVariableOperator+'.action',_param+'&'+_KPIExpressions,function(data){	
					endProcess();
					$('#reportVariableGrid').trigger('reloadGrid');
					$('#KPIGrid').trigger('reloadGrid');
					$('#reportVariableGrid_add_edit_window').dialog('close');
					msgShow(i18n['label_dynamicReports_addEditMsg'],'show');
				});				
			}else{
				endProcess();
			}
		},
		
		/**
		 * 打开编辑窗口.
		 */
		preEdit:function(){
			
			checkBeforeEditGrid('#reportVariableGrid', function(data){
				
				$('#add_edit_reportVariable_variableName').val($vl(data.variableName));
				$('#add_edit_reportVariable_variableCode').val($vl(data.variableCode));
				$('#add_edit_reportVariable_variableId').val($vl(data.variableId));
				$('#add_edit_reportVariable_isRelated').val($vl(data.isRelated));
				
				Variable=$vl(data.variableCode);
				var isGrouped=$vl(data.isGrouped);
				
				
				if(isGrouped!=null && isGrouped!=''){
					
					if(isGrouped=='true'){
						$('#add_edit_reportVariable_isGrouped_true').attr("checked",true);
					}else{
						$('#add_edit_reportVariable_isGrouped_false').attr("checked",true);;
					}
				}
				//打开窗口
				windows('reportVariableGrid_add_edit_window',{width:320});
				reportVariableOperator='merge';

				setTimeout(function(){//延迟加载

					$('#add_edit_reportVariable_groupField').val($vl(data.groupField));
					$('#add_edit_reportVariable_statField').val($vl(data.statField));
					$('#add_edit_reportVariable_statType').val($vl(data.statType));

				},500);
			});
		},
		/**
		 * 删除
		 */
		doDelete:function(){
				checkBeforeDeleteGrid('#reportVariableGrid', function(ids){
					
					
					var reportId=$('#add_edit_KPIReport_reportId').val();
					
					
					
					if(reportId==null || reportId==''){//新增
						reportId=-1;
					}
					
					var _param = $.param({'variableIds':ids,'reportId':reportId},true);
					$.post("variable!delete.action", _param, function(){
						
						$('#reportVariableGrid').trigger('reloadGrid');
						msgShow(i18n['msg_deleteSuccessful'],'show');
						
					}, "json");
					
					
				});

			
		}
		
		
		

	};
	
}();
