$package('common.report');
/**  
 * @fileOverview "报表弹出点击事件方法"
 * @author Martin  
 * @constructor Martin
 * @description 报表弹出点击事件方法
 * @date 2014-09-13
 * @version 1.0  
 * @since version 1.0 
 */
common.report.conditionsSelectEvent=function(){
	return{
		/**
		 * @description 选择指派组
		 * @param assigneeGroupNo 机构编号Id
		 * @param assigneeGroupName 机构名称Id
		 */
		selectAssginGroup:function(assigneeGroupNo,assigneeGroupName){
			common.security.organizationTreeUtil.showAll_2('#Statistical_reportsrequestAssginGroup_win','#Statistical_reportsassginGroupTree',assigneeGroupName,assigneeGroupNo,topCompanyNo);
		},
		/**
		 * 选择机构
		 * @param GroupNo 机构编号Id
		 * @param GroupName 机构名称Id
		 */
		selectorganization:function(GroupNo,GroupName){
			common.security.organizationTreeUtil.showAll_2('#According_statisticssrequestAssginGroup_win','#According_statisticssassginGroupTree',GroupNo,GroupName,companyNo);
		}
	}
}();
