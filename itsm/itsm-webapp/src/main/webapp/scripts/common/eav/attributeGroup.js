$package('common.eav')
 
 /**  
 * @author Wstuo
 * @constructor 扩展属性分组主函数
 * @description 扩展属性分组主函数
 * @date 2013-03-11
 * @since version 1.0 
 */  
common.eav.attributeGroup = function(){
	return { 
		/**
		 * @private
		 * @description 扩展属性列表
		 **/
		attrGroup_grid:function(){		
			var params = $.extend({},jqGridParams,{	
				url:'attrGroup!find.action?attrGroupQueryDTO.attrGroupId=0',
				colNames:[i18n['name'],i18n['sort'],''],
				colModel:[{name:'attrGroupName',width:40,align:'center',sortable:false},
						{name:'attrGroupSortNo',width:60,align:'center',sortable:false},
						{name:'attrGroupId',hidden:true}
				    ],
				jsonReader: $.extend(jqGridJsonReader, {id: "attrGroupId"}),
				sortname:'attrGroupSortNo',
				pager:'#attrGroupPager',
				});
				$("#attrGroupTable").jqGrid(params);
				$("#attrGroupTable").navGrid('#attrGroupPager',navGridParams);
				//列表操作项
				$("#t_attrGroupTable").css(jqGridTopStyles);
				$("#t_attrGroupTable").append($('#attrGroupToolbar').html());
				
				//自适应宽度
				setGridWidth("#attrGroupTable","regCenter",20);
		},
		/**
		 * @description 打开添加扩展属性面板
		 */
		addAttrGroup:function(){
			$('#addGroup_attrGroupName').val('');
			$('#addGroup_attrGroupSortNo').val(0);
			windows('addAttrGroupDiv',{width:400});
		},
		/**
		 * @description 添加保存扩展属性
		 */
		saveAttrGroup:function(){
			if($('#addAttrGroupForm').form('validate')){
				var _params = $('#addAttrGroupDiv form').serialize();
				$.post('attrGroup!saveAttrGroup.action', _params, function(){
					msgShow(i18n['saveSuccess'],'show');
					$('#attrGroupTable').trigger('reloadGrid');
					$('#addAttrGroupDiv').dialog('close');
				});
			}
		},
		/**
		 * @description 点击列表的编辑按钮，获取要编辑的扩展属性ID
		 */
		editCheck:function(){
			checkBeforeEditGrid("#attrGroupTable",common.eav.attributeGroup.editAttrGroup);
		},
		/**
		 * 
		 * @description 打开编辑扩展属性面板
		 * @param rowsData 选中列对象
		 */
		editAttrGroup:function(rowsData){
			$('#editGroup_attrGroupId').val(rowsData.attrGroupId);
			$('#editGroup_attrGroupName').val(rowsData.attrGroupName);
			$('#editGroup_attrGroupSortNo').val(rowsData.attrGroupSortNo);
			windows('editAttrGroupDiv',{width:400});
		},
		/**
		 * @description 编辑扩展属性
		 */
		updateAttrGroup:function(){
			if($('#editAttrGroupForm').form('validate')){
				var _params = $('#editAttrGroupDiv form').serialize();
				$.post('attrGroup!updateAttrGroup.action', _params, function(){
					msgShow(i18n['editSuccess'],'show');
					$('#attrGroupTable').trigger('reloadGrid');
					$('#editGroup_attrGroupName').val('');
					$('#editGroup_attrGroupSortNo').val('');
					$('#editAttrGroupDiv').dialog('close');
				});
			}
		},
		/**
		 * @description 打开搜索面板
		 */
		searchAttrGroup:function(){
			windows('searchAttrGroupDiv',{width:400,modal: false});
			//禁止回车事件
			$("#search_attrGroupName").keydown(function (e) {
	            var curKey = e.which;
	            if (curKey == 13) {
	                return false;
	            }
	        });
		},
		/**
		 * @description 删除时获取列表ID
		 */
		deleteBefore:function(){
			checkBeforeDeleteGrid("#attrGroupTable",common.eav.attributeGroup.doDelete);
		},
		/**
		 * @description 执行删除
		 */
		doDelete:function(rowIds){
			var _params = $.param({'ids':rowIds},true);
			$.post('attrGroup!deleteAttrGroup.action', _params, function(){
				msgShow(i18n['deleteSuccess'],'show');
				$('#attrGroupTable').trigger('reloadGrid');
				eavNo=null;
				$('#attrGroupTable').jqGrid('setGridParam',{url:'attrGroup!find.action?attrGroupQueryDTO.attrGroupId=0'}).trigger('reloadGrid',[{"page":"1"}]);
			})
		},
		/**
		 * @description 执行搜索
		 */
		doSearch:function(){
			var _params = $('#searchAttrGroupDiv').getForm();
			var postData = $("#attrGroupTable").jqGrid("getGridParam", "postData");       
			$.extend(postData, _params);  //将postData中的查询参数加上查询表单的参数		
			$('#attrGroupTable').trigger('reloadGrid',[{"page":"1"}]);
		},
		/**
		 * @description 初始化
		 */
		init: function(){
			showAndHidePanel("#attrGroup_panel","#attrGroup_loading");
			//扩展名
			common.eav.attributeGroup.attrGroup_grid();
		}
	}
}();
$(document).ready(common.eav.attributeGroup.init);
