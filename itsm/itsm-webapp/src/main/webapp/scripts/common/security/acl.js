$package('common.security');

$import('common.security.roleUtil');
$import('common.security.organizationTreeUtil');
$import('common.security.userUtil');

common.security.acl = function(){
	var _entries_permission_mask;
	var _aclclass='';
	var _identitys;
	return {
		/**
		 * 打开ACL设置窗口
		 * @param gridId  列id
		 * @param aclclass  类名
		 */
		openAclSetWind:function(gridId,aclclass){
			_aclclass = aclclass;
			var rowIds = $('#'+gridId).getGridParam('selarrrow');
			if(rowIds==''){
				msgAlert(i18n.msg_atLeastChooseOneData,'info');
			}else{
				_identitys = rowIds;
				common.security.acl.loadAcl(rowIds,aclclass);
				windows('index_acl_window',{width:450});
			}
			
		},
		/**
		 * 加载ACL默认显示
		 * @param rowIds 行的id
		 * @param aclclass 类名
		 */
		loadAcl:function(rowIds,aclclass){
			$('#objectIdentity_identifier,#entries_sid_table tbody').html('');
			var param = $.param({'identitys':rowIds},true);
			$.post('acls!findAclByClzIds.action',param+'&aclclass='+aclclass,function(data){
				if(data!=null){
					//判断是否是选择多个资源
					var multipleFlag = false;
					if(data.length>1){
						multipleFlag = true;
					}
					
					for(var i=0;i<data.length;i++){
						var identity = data[i].objectIdentity_identifier;
						$('#objectIdentity_identifier').append(identity+';&nbsp;&nbsp;');
						
						//如果不是多个，则直接加载相应值出来
						if(multipleFlag){
							alert('要显示合并的呀');
						}else{
							var entries = data[i].entries;
							if(entries!=null){
								var entries_sid = entries.entries_sid;
								var entries_sid_name = entries.entries_sid_name;
								_entries_permission_mask = entries.entries_permission_mask;
								for(var j=0;j<entries_sid.length;j++){
									var tr = '<tr class="tr_default" id="tr_{id}'+'" onclick=common.security.acl.showPermission("{sid}",{identity})>'+
									'<td><a href="javascript:void(0)">{show}</a></td>'+
									'</tr>';
									var show = '';
									if(entries_sid[j].indexOf('GROUP_')==-1){
										show = entries_sid[j]+'('+entries_sid_name[entries_sid[j]]+')';
									}else{
										show = entries_sid_name[entries_sid[j]];
									
									}
									if(j==0){
										common.security.acl.showPermission(entries_sid[j],identity);
									}
									$('#entries_sid_table tbody').append(tr.replace('{id}',entries_sid[j])
											.replace('{sid}',entries_sid[j])
											.replace('{show}',show)
											.replace('{identity}',identity)
									);
								}
							}
						}
					}
				}
				
			});
		},
		/**
		 * 显示角色的权限
		 * @param sid   编号
		 * @param identity  实体
		 */
		showPermission:function(sid,identity){
			$.post('acls!findAclByClzIds.action','identitys='+identity+'&aclclass='+_aclclass,function(data){
				var entries = data[0].entries;
				if(entries!=null){
					_entries_permission_mask = entries.entries_permission_mask;
					$('#recipients').val(sid);
					$('#aclclass').val(_aclclass);
					$('.allowFlag').attr('checked',false);
					$('.rejectFlag').attr('checked',true);
					$('#reject_a').attr('checked',false);
					$('.tr_selected').attr('class','tr_default');
					$('#tr_'+sid).attr('class','tr_selected');
					var masks = _entries_permission_mask[sid];
					if(masks!=null && masks!=undefined){
						var mask = 0;
						for(var i=0;i<masks.length;i++){
							mask = masks[i];
							if(mask==16){//管理员
								$('.allowFlag').attr('checked',true);
								$('.rejectFlag').attr('checked',false);
							}else if(mask==1){//读取
								$('#allow_r').attr('checked',true);
								$('#reject_r').attr('checked',false);
							}else if(mask==2){//写入
								$('#allow_w').attr('checked',true);
								$('#reject_w').attr('checked',false);
							}else if(mask==8){//删除
								$('#allow_d').attr('checked',true);
								$('#reject_d').attr('checked',false);
							}
							
						}
					}
					
				}
			});
			
		},
		/**
		 * 完全控制
		 * @param id  编号
		 * @param type 类型
		 */
		allControl:function(id,type){
			if($('#'+id).attr('checked')==true){
				if(type=='allow'){
					$('.allowFlag').attr('checked',true);
					$('.rejectFlag').attr('checked',false);
				}else{
					$('.allowFlag').attr('checked',false);
					$('.rejectFlag').attr('checked',true);
				}
			}
		},
		//
		/**
		 * 反选完全控制
		 * @param mark 标识
		 * @param type 类型
		 */
		unAllControl:function(mark,type){
			if($('#'+type+'_'+mark).attr('checked')==false){//如果去掉当前复选框
				//如果读取都没有权限，那么修改和删除也不能有
				if(mark=='r'){
					$('#allow_w,#allow_d').attr('checked',false);
					$('#reject_w,#reject_d').attr('checked',true);
				}
				if(type=='allow'){//如果是允许的
					$('#allow_a').attr('checked',false);
					$('#reject_'+mark).attr('checked',true);
				}else{
					$('#reject_a').attr('checked',false);
					$('#allow_'+mark).attr('checked',true);
				}
			}else{
				if(type=='allow'){
					$('#reject_a').attr('checked',false);
					$('#reject_'+mark).attr('checked',false);
					//如果是允许，并且修改和删除是选中的，那么读取权限自动给
					if(mark=='w' || mark=='d')
						$('#allow_r').attr('checked',true);
						$('#reject_r').attr('checked',false);
				}else{
					$('#allow_a').attr('checked',false);
					$('#allow_'+mark).attr('checked',false);
				}
			}
		},
		/**
		 * 更新授权
		 */
		updatePermission:function(){
			var frm = $('#permissionSetForm').serialize();
			var param = $.param({'identitys':_identitys},true);
			$.post('acls!updatePermission.action',frm+'&'+param,function(){
				msgShow(i18n.saveSuccess,'show');
			});
		},
		/**
		 * 删除角色、用户、组
		 */
		deletePermission:function(){
			msgConfirm(i18n.msg_msg,'<br/>'+i18n.msg_confirmDelete,function(){
				var rejectMasks = 'rejectMasks=16&rejectMasks=8&rejectMasks=2&rejectMasks=1';
				var param = $.param({'identitys':_identitys},true);
				$.post('acls!updatePermission.action','recipients='+$('#recipients').val()+'&aclclass='+_aclclass+'&'+rejectMasks+'&'+param,function(){
					msgShow(i18n.deleteSuccess,'show');
					common.security.acl.loadAcl(_identitys,_aclclass);
				});
			});
		},
		/**
		 * 添加角色、用户、组窗口
		 */
		openAddPermissionWin:function(){
			$('#add_acl_aclclass').val(_aclclass);
			$('#add_sid_table tbody').html('');
			$('#add_acl_allow_a,#add_acl_allow_r,#add_acl_allow_w,#add_acl_allow_d').attr('checked',false);
			windows('add_acl_window',{width:450});
		},
		/**
		 * 添加授权
		 */
		addPermission:function(){
			var frm = $('#add_acl_form').serialize();
			var param = $.param({'identitys':_identitys},true);
			$.post('acls!addPermission.action',frm+'&'+param,function(){
				$('#add_acl_window').dialog('close');
				common.security.acl.loadAcl(_identitys,_aclclass);
			});
		},
		/**
		 * 选择角色
		 */
		selectSid:function(){
			var sid_type = $('#sid_type').val();
			var html = '<tr id="{id}">'+
			'<td><input type="hidden" name="recipients" value="{value}" />{show}</td>'+
			'<td><a href=javascript:common.security.acl.remoreSid("{removeid}")>['+i18n.deletes+']</a></td>'+
			'</tr>';
			if('role'==sid_type){
				common.security.roleUtil.showRoleGrid(function(data){
					for(var i=0;i<data.length;i++){
						var rowData=$('#selectRole_grid').getRowData(data[i]);
						if($('#sid_role_'+rowData.roleId).html()==null || $('#sid_role_'+rowData.roleId).html()==''){
							$('#add_sid_table tbody').append(html.replace('{id}','sid_role_'+rowData.roleId)
																.replace('{value}',rowData.roleCode)
																.replace('{show}',rowData.roleCode+'('+rowData.roleName+')')
																.replace('{removeid}','sid_role_'+rowData.roleId));
						}
					}
				});
			}else if('group'==sid_type){
				common.security.organizationTreeUtil.selectGroup(companyNo,function(e,data){
					var orgName = data.rslt.obj.attr("orgName");
					var orgId = data.rslt.obj.attr("orgNo");
					var orgType = data.rslt.obj.attr("orgType");
					if($('#sid_group_'+orgId).html()==null && (orgType!=='innerPanel' && orgType!=='servicePanel' && orgType!=='ROOT')){
						$('#add_sid_table tbody').append(html.replace('{id}','sid_group_'+orgId)
								.replace('{value}','GROUP_'+orgId)
								.replace('{show}',orgName)
								.replace('{removeid}','sid_group_'+orgId));
						
					}
				});
			}else if('user'==sid_type){
				common.security.userUtil.selectUserFun(function(data){
					for(var i=0;i<data.length;i++){
						var rowData=$('#selectUser_grid').getRowData(data[i]);
						if($('#sid_user_'+rowData.userId).html()==null || $('#sid_user_'+rowData.userId).html()==''){
							$('#add_sid_table tbody').append(html.replace('{id}','sid_user_'+rowData.userId)
																.replace('{value}',rowData.loginName)
																.replace('{show}',rowData.loginName+'('+rowData.fullName+')')
																.replace('{removeid}','sid_user_'+rowData.userId));
						}
					}
				});
			}
		},
		/**
		 * 删除角色 
		 * @param id 编号
		 */
		remoreSid:function(id){
			$('#'+id).remove();
		},
		/**
		 * 新增的完全控制事件
		 */
		addAllControl:function(){
			//如果勾选，则全部勾选 
			if($('#add_acl_allow_a').attr('checked')==true){
				$('#add_acl_allow_r,#add_acl_allow_w,#add_acl_allow_d').attr('checked',true);
			}
		},
		/**
		 * 读的控制事件
		 * @param id 编号
		 */
		addUnControl:function(id){
			if($('#'+id).attr('checked')==false){
				$('#add_acl_allow_a').attr('checked',false)
				if(id=='add_acl_allow_r')
					$('#add_acl_allow_w,#add_acl_allow_d').attr('checked',false);
			}else{
				//如果修改和删除，那么读取的也应该有
				if(id=='add_acl_allow_w' || id=='add_acl_allow_d')
					$('#add_acl_allow_r').attr('checked',true);
			}
			
		},
		init:function(){
			
		}
	};
	
}();
$(document).ready(common.security.acl.init);