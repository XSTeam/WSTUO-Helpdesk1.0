$package('common.security');
$import('itsm.itsop.selectCompany');
/**
 * @author Van
 * @constructor WSTO
 * @description 客户管理主函数.
 * @date 2010-02-25
 * @since version 1.0
 */
common.security.customerMain = function() {
	return {

		/**
		 * @description 动作格式化.
		 * @param cell 字段的值
		 * @param opt  行数据
		 * @param data  行索引
		 */
		customerGridFormatter : function(cell, opt, data) {

			return actionFormat('1', '1').replace(
					'[edit]',
					'common.security.customerMain.editCustomer_openWindow_aff('
							+ opt.rowId + ')').replace(
					'[delete]',
					'common.security.customerMain.deleteCustomerInLine('
							+ opt.rowId + ')');
		},

		/**
		 * @description 加载客户列表.
		 */
		showCustomerGrid : function() {
			var params = $
					.extend(
							{},
							jqGridParams,
							{
								url : 'customer!findByCompany.action',
								colNames : [ i18n['label_belongs_client'],
										i18n['title_customer_name'],
										i18n['title_customer_phone'],
										i18n['title_customer_email'],
										i18n['title_customer_address'],
										i18n['common_action'], '', '', '', '' ],
								colModel : [
										{
											name : 'companyName',
											align : 'center',
											hidden : hideCompany,
											sortable : false
										},
										{
											name : 'orgName',
											width : 100,
											align : 'center'
										},
										{
											name : 'officePhone',
											width : 80,
											align : 'center'
										},
										{
											name : 'email',
											width : 80,
											align : 'center'
										},
										{
											name : 'address',
											width : 150,
											align : 'center'
										},
										{
											name : 'act',
											width : 80,
											align : 'center',
											sortable : false,
											formatter : common.security.customerMain.customerGridFormatter
										}, {
											name : 'orgNo',
											hidden : true
										}, {
											name : 'officeFax',
											hidden : true
										}, {
											name : 'orgType',
											hidden : true
										}, {
											name : 'companyNo',
											hidden : true
										}, ],
								jsonReader : $.extend(jqGridJsonReader, {
									id : "orgNo"
								}),
								sortname : 'orgNo',
								pager : '#customerGridPager'
							});
			$("#customerGrid").jqGrid(params);
			$("#customerGrid").navGrid('#customerGridPager', navGridParams);
			// 列表操作项
			$("#t_customerGrid").css(jqGridTopStyles);
			$("#t_customerGrid").append($('#customerGridToolbar').html());

			// 自适应宽度
			setGridWidth("#customerGrid", "regCenter", 10);
		},

		/**
		 * @description 新增客户.
		 */
		addCustomer_openWindow : function() {

			windows('addCustomer_win', {
				width : 400
			});
		},

		/**
		 * @description 新增客户方法.
		 */
		addCustomer_doAdd : function() {

			if ($('#addCustomer_form').form('validate')) {

				var frm = $('#addCustomer_win form').serialize();
				var url = 'customer!save.action';
				$.post(url, frm, function() {
					$('#addCustomer_win').dialog('close');
					$('#customerGrid').trigger('reloadGrid');
					$('#add_orgName').val('');
					$('#add_officeFax').val('');
					$('#add_officePhone').val('');
					$('#add_email').val('');
					$('#add_addr').val('');
					msgShow(i18n['msg_customer_addSuccessful'], 'show');
				});
			}
		},

		/**
		 * @description 编辑客户.
		 */
		editCustomer_openWindow : function() {
			checkBeforeEditGrid("#customerGrid", function(rowData) {

				$('#editCustomer_orgName').val(rowData.orgName);
				$('#editCustomer_officeFax').val(rowData.officeFax);
				$('#editCustomer_officePhone').val(rowData.officePhone);
				$('#editCustomer_email').val(rowData.email);
				$('#editCustomer_address').val(rowData.address);
				$('#editCustomer_orgNo').val(rowData.orgNo);
				$('#editCustomer_companyName').val(rowData.companyName);

				windows('editCustomer_win', {
					width : 400
				});
			});
		},

		/**
		 * @description 编辑客户.
		 */
		editCustomer_openWindow_aff : function(rowId) {
			var rowData = $("#customerGrid").jqGrid('getRowData', rowId); // 行数据
			$('#editCustomer_orgName').val(rowData.orgName);
			$('#editCustomer_officeFax').val(rowData.officeFax);
			$('#editCustomer_officePhone').val(rowData.officePhone);
			$('#editCustomer_email').val(rowData.email);
			$('#editCustomer_address').val(rowData.address);
			$('#editCustomer_orgNo').val(rowData.orgNo);
			$('#editCustomer_companyName').val(rowData.companyName);
			windows('editCustomer_win', {
				width : 400
			});
		},

		/**
		 * @description 提交编辑客户.
		 */
		editCustomer_doEdit : function() {

			if ($('#editCustomer_form').form('validate')) {
				var frm = $('#editCustomer_win form').serialize();
				var url = 'customer!merge.action';
				$.post(url, frm, function() {
					$('#customerGrid').trigger('reloadGrid');
					$('#editCustomer_win').dialog('close');
					msgShow(i18n['msg_customer_editSuccessful'], 'show');
				});
			}
		},

		/**
		 * @description 删除客户.
		 */
		deleteCustomer_tooBar : function() {
			checkBeforeDeleteGrid("#customerGrid", function(rowIds) {

				var param = $.param({
					'orgNos' : rowIds
				}, true);
				$.post("customer!deleteCustomers.action", param, function() {
					$('#customerGrid').trigger('reloadGrid');
					msgShow(i18n['msg_deleteSuccessful'], 'show');
				}, "json");
			});
		},

		/**
		 * @description 表格行内删除客户方法.
		 * @param orgNo
		 *            客户编号
		 */
		deleteCustomerInLine : function(orgNo) {
			msgConfirm(i18n['msg_msg'], '<br/>' + i18n['msg_confirmDelete'],
					function() {
						var url = "customer!deleteCustomer.action";
						var params = {
							"orgNo" : orgNo
						};
						$.post(url, params, function(r) {
							$('#customerGrid').trigger('reloadGrid');
							msgShow(i18n['msg_customer_deleteSuccessful'],
									'show');
						}, 'json');
					});
		},

		/**
		 * @description 打开搜索客户窗口.
		 */
		searchCustomer_openWindow : function() {
			itsm.app.autocomplete.autocomplete.bindAutoComplete(
					'#searchCustomer_companyName',
					'com.wstuo.itsm.itsop.itsopuser.entity.ITSOPUser',
					'orgName', 'orgName', 'orgNo', 'Long',
					'#searchCustomer_companyNo', userName, 'ITSOPUser');
			windows('searchCustomerGrid', {
				width : 400,
				modal : false
			});
		},

		/**
		 * @description 提交执行搜索.
		 */
		searchCustomer_doSearch : function() {
			var va = $('#searchCustomer_companyName').val().replace(/\s+/g, "");
			if (va == "")
				$('#searchCustomer_companyNo').val('');

			var _url = 'customer!findByCompany.action';
			searchGridWithForm('#customerGrid', '#searchCustomerGrid', _url);
		},

		/**
		 * @description 导出数据客户
		 */
		exportCustomerView : function() {
			var sdata = $('#searchCustomerGrid form').getForm();

			var csData = $("#customerGrid").jqGrid("getGridParam", "postData");

			for (i in sdata) {
				if (sdata[i] != "" && sdata[i] != null) {
					if (i == 'queryDto.orgName')
						$('#exportCustomer_orgName').val(sdata[i]);
					if (i == 'queryDto.officePhone')
						$('#exportCustomer_officePhone').val(sdata[i]);
					if (i == 'queryDto.email')
						$('#exportCustomer_email').val(sdata[i]);
					if (i == 'queryDto.address')
						$('#exportCustomer_address').val(sdata[i]);
				}
			}
			$('#exportCustomer_sidx').val(csData.sidx);
			$('#exportCustomer_sord').val(csData.sord);
			$('#exportCustomer_page').val(csData.page);
			$('#exportCustomer_rows').val(csData.rows);
			$("#exportCustomerWindow form").submit();
		},

		/**
		 * 导入数据.
		 */
		importCustomerData : function() {

			windows('importCustomerDataWindow', {
				width : 400
			});
		},

		/**
		 * @descriptionadd 客户管理导入
		 */
		importCustomerExcel : function() {
			startProcess();
			var filevalue = $('#importCustomerFile').val();
			if (filevalue != "") {
				$
						.ajaxFileUpload({
							url : 'customer!importCustomerData.action',
							secureuri : false,
							fileElementId : 'importCustomerFile',
							dataType : 'json',
							success : function(data, status) {
								$('#importCustomerDataWindow').dialog('close');
								$('#customerGrid').trigger('reloadGrid');
								if (data == "-2") {
									msgAlert(i18n['msg_dc_fileNotExists'],
											'error');
								} else if (data == "-1") {
									msgAlert(i18n['duplicated_customer_names'],
											'error');
								} else if (data == "0") {
									msgAlert(i18n['msg_dc_noEffect'], 'error');
								} else if (data == "-3") {
									msgAlert(
											i18n['ERROR_CUSTOMER_COMPANY_NULL'],
											'error');
								} else {
									msgShow(i18n['msg_dc_importSuccess']
											.replace('N', data), 'show');
								}
								endProcess();
							}
						});
			} else {
				msgAlert(i18n['msg_dc_fileNull'], 'info');
				endProcess();
			}
		},

		/**
		 * 初始化加载
		 */
		init : function() {
			// 清除重复表单
			clearRelForm([ 'searchCustomerGrid', 'addCustomer_win',
					'editCustomer_win', 'importCustomerDataWindow' ]);
			// 隐显加载图标
			showAndHidePanel('#customerMain_content', '#customerMain_loading');

			// 加载列表
			common.security.customerMain.showCustomerGrid();

			// 客户
			$('#addCustomerBtn').click(
					common.security.customerMain.addCustomer_openWindow);
			$('#addCustomerBtn_OK').click(
					common.security.customerMain.addCustomer_doAdd);
			$('#editCustomerBtn').click(
					common.security.customerMain.editCustomer_openWindow);
			$('#editCustomerBtn_OK').click(
					common.security.customerMain.editCustomer_doEdit);
			$('#deleteCustomerBtn').click(
					common.security.customerMain.deleteCustomer_tooBar);
			$('#searchCustomerBtn').click(
					common.security.customerMain.searchCustomer_openWindow);
			$('#searchCustomerBtn_OK').click(
					common.security.customerMain.searchCustomer_doSearch);

			// 导入导出
			$('#customerGrid_export').click(
					common.security.customerMain.exportCustomerView);
			$('#customerGrid_import').click(
					common.security.customerMain.importCustomerData);
			$('#customerGrid_doImport').click(
					common.security.customerMain.importCustomerExcel);

			// 选择公司
			$('#search_Customer_companyName').click(function(){
				itsm.itsop.selectCompany.openSelectCompanyWin('#addCustomer_companyNo',
						'#addCustomer_companyName', '');
			});

/*			itsm.app.autocomplete.autocomplete.bindAutoComplete(
					'#addCustomer_companyName',
					'com.wstuo.itsm.itsop.itsopuser.entity.ITSOPUser',
					'orgName', 'orgName', 'orgNo', 'Long',
					'#addCustomer_companyNo', userName, 'true');*/
			// 选择公司
			$('#Customer_search_companyName').click(function(){
				itsm.itsop.selectCompany.openSelectCompanyWin('#searchCustomer_companyNo',
						'#searchCustomer_companyName', '');
			});
		}
	}
}();
// 载入
$(document).ready(common.security.customerMain.init);
