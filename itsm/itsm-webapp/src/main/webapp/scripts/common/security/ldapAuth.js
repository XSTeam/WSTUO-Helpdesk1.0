$package('common.security');

/**
 * @constructor WSTUO
 * LDAP验证配置JS主函数
 */
common.security.ldapAuth=function(){
	
	return {
		
		inputSetVal:function(value){
			if(value!=null && value!='null')
				return value;
			else
				return '';
		},
		
		/**
		 * 加载LDAP配置信息
		 */
		loadLdapAuth:function(){
			$.post('ldapAuth!findLDAPAuth.action',function(data){
				$('#ldapAuthentication_ldapId').val(common.security.ldapAuth.inputSetVal(data.ldapId));
				if(data.ldapType!=null && data.ldapType!=''){
					$('#ldapAuthentication_ldapType').val(common.security.ldapAuth.inputSetVal(data.ldapType));
				}
				if(common.security.ldapAuth.inputSetVal(data.ldapServer)==null||common.security.ldapAuth.inputSetVal(data.ldapServer)==''){
					$('#ldapAuthentication_ldapServer').val("ldap://");
				}else{
					$('#ldapAuthentication_ldapServer').val(common.security.ldapAuth.inputSetVal(data.ldapServer));
				}
				
				$('#ldapAuthentication_prot').val(common.security.ldapAuth.inputSetVal(data.prot));
				$('#ldapAuthentication_adminName').val(common.security.ldapAuth.inputSetVal(data.adminName));
				$('#ldapAuthentication_adminPassword').val(common.security.ldapAuth.inputSetVal(data.adminPassword));
				$('#ldapAuthentication_searchBase').val(common.security.ldapAuth.inputSetVal(data.searchBase));
				$('#ldapAuthentication_authenticationMode').val(common.security.ldapAuth.inputSetVal(data.authenticationMode));
				$('#ldapAuthentication_commonUserName').val(common.security.ldapAuth.inputSetVal(data.commonUserName));
				$('#ldapAuthentication_commonPassword').val(common.security.ldapAuth.inputSetVal(data.commonPassword));
				if(data.enableAuthentication){
					$('#ldapAuthentication_enableAuthentication').attr('checked',true);
				}
				
				if(data.kerberosEnableAuthentication){
					$('#ldapAuthentication_kerberosEnableAuthentication').attr('checked',true);
				}
				if(data.kerberosType!=null && data.kerberosType!=''){
					$('#ldapAuthentication_kerberosType').val(common.security.ldapAuth.inputSetVal(data.kerberosType));
				}
				if(common.security.ldapAuth.inputSetVal(data.kerberosServer)==null||common.security.ldapAuth.inputSetVal(data.kerberosServer)==''){
					$('#ldapAuthentication_kerberosServer').val("127.0.0.1");
				}else{
					$('#ldapAuthentication_kerberosServer').val(common.security.ldapAuth.inputSetVal(data.kerberosServer));
				}
				
				$('#ldapAuthentication_kerberosName').val(common.security.ldapAuth.inputSetVal(data.kerberosName));
				$('#ldapAuthentication_kerberosPassword').val(common.security.ldapAuth.inputSetVal(data.kerberosPassword));
				$('#ldapAuthentication_kerberosBase').val(common.security.ldapAuth.inputSetVal(data.kerberosBase));
				
			});
			
		},
		
		
		/**
		 * 保存LDAP
		 */
		saveLdapAuth:function(){
			if($('#ldapAuthForm').form('validate')){
				var _frm=$('#ldapAuthForm,#kerberosForm').serialize();
				$.post('ldapAuth!saveLDAPAuth.action',_frm,function(){
					msgShow(i18n['saveSuccess'],'show');
				});
			}
			
		},
		/**
		 * 保存kerberos的访问信息
		 */
		savekerberos:function(){
			if($('#kerberosForm').form('validate')){
				var _frm=$('#ldapAuthForm,#kerberosForm').serialize();
				$.post('ldapAuth!saveLDAPAuth.action',_frm,function(){
					msgShow(i18n['saveSuccess'],'show');
				});
			}
		},
		/**
		 * 连接测试
		 */
		connTest:function(){
			if($('#ldapAuthForm').form('validate')){
				var pp=$('#ldapAuthForm,#kerberosForm').serialize();
				$('#ldapAuthConnTestMessage').text(i18n['label_ldap_connection']);
				$.post('ldap!connTestldap.action',pp,function(data){
					if(data){
						$('#ldapAuthConnTestMessage').text(i18n['label_ldap_connectionSuccessful']);
					}	
					else{
						$('#ldapAuthConnTestMessage').text(i18n['label_ldap_paramError']);
					}	
				})
			}
		},
		/**
		 * 测试kerberos
		 */
		connTestkerberos:function(){
			if($('#kerberosForm').form('validate')){
				var pp=$('#kerberosForm,#kerberosForm').serialize();
				$('#kerberosConnTestMessage').text(i18n['label_ldap_connection']);
				$.post('ldap!connTestkerberos.action',pp,function(data){
					if(data){
						$('#kerberosConnTestMessage').text(i18n['label_ldap_connectionSuccessful']);
					}	
					else{
						$('#kerberosConnTestMessage').text(i18n['label_ldap_paramError']);
					}	
				})
			}
		},
		/**
		 * 初始化加载
		 */
		init:function(){
			$('#ldapAuth_loading').hide();
			$('#ldapAuth_panel').show();
			common.security.ldapAuth.loadLdapAuth();//加载数据
			$('#ldapAuthSave_but').click(common.security.ldapAuth.saveLdapAuth);
			$('#ldapAuthConnTest_but').click(common.security.ldapAuth.connTest);
			$('#kerberosSave_but').click(common.security.ldapAuth.savekerberos);
			$('#kerberosConnTest_but').click(common.security.ldapAuth.connTestkerberos);
		}
		
	}
	
}();
$(document).ready(common.security.ldapAuth.init);