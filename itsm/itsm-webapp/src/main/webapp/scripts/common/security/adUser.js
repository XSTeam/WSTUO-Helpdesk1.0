$import('common.security.user')
$import('itsm.itsop.selectCompany');

 /**  
 * @author QXY  
 * @constructor adUser
 * @description 添加用户主函数.
 * @date 2011-02-25
 * @since version 1.0 
 */
var adUser=function(){
	
	/**
	 * @description 加载LDAP.
	 */
	this.LDAPId=0;
	this.loadLDAP=function(){
		$("").appendTo('#ldap');
		$("<option value='0'>----LDAP---</option>").appendTo('#ldap');
		$.post('ldap!getLDAPAll.action',function(data){
			this.options;
			options="";
			if(data!=null&&data.length>0){
				LDAPId=data[0].ldapId;
				$('#ldapId').val(data[0].ldapId);
				for(var i=0;i<data.length;i++){
					options=options+"<option value='"+data[i].ldapId+"'>"+data[i].ldapName+"</option>"
				}
			}
			$(options).appendTo('#ldap');
		},"json")
		
	}
	/**
	 * @description 状态格式化.
	 * @param cellvalue 行的值
	 * @param options   下拉值
	 */
	this.userStateFormat=function(cellvalue, options){
		if(cellvalue==true){
			return i18n['common_enable'];
		}
		else{
			return i18n['common_disable'];
		}
	}
	/** 
	 * @description 用户列表.
	 */
	this.adUserList=function(){
		var params = $.extend({},jqGridParams, {
			url:'ldap!getADByLDAP.action',
			colNames:['Id',i18n['loginName'],i18n['title_user_lastName'],i18n['title_user_firstName'],i18n['title_customer_email'],i18n['title_customer_phone'],i18n['title_user_mobile'],i18n['common_state'],'','',''],
		 	colModel:[
		 	          {name:'userId',index:'userId',width:80,sortable:false,hidden:true},
		 	          {name:'loginName',index:'loginName',width:80,sortable:false},
		 	          {name:'firstName',index:'firstName',width:80,sortable:false},
		 	          {name:'lastName',index:'lastName',width:50,sortable:false},
		 	          {name:'email',index:'email',width:80,sortable:false},
		 	          {name:'phone',index:'phone',width:80,sortable:false},
		 	          {name:'moblie',index:'moblie',width:80,sortable:false},
		 	          {name:'userState',align:'center',width:40,formatter:userStateFormat,sortable:false},
		 	          {name:'officeAddress',hidden:true},
		 	          {name:'position',hidden:true},
		 	          {name:'userState',hidden:true}
		 	],
		 	jsonReader: $.extend(jqGridJsonReader, {id: "userId"}),
		 	sortname:'userId',
			pager:'#adPager'
		});
		$("#adGrid").jqGrid(params);
		$("#adGrid").navGrid('#adPager',navGridParams);
		//列表操作项
		$("#t_adGrid").css(jqGridTopStyles);
		$("#t_adGrid").append($('#adGridToolbar').html());
		
		//自适应宽度
		setGridWidth("#userGrid","regCenter",20);
		
		
		
	}
	
	/**
	 * @description 角色加载.
	 */
	this.mydata = new Array();
	this.roleLoad=function(){
		$.post("role!findByState.action",function(data){
			mydata=data;
			$("#roleSet table").empty();
			this.roleHtml;
			roleHtml="";
			var n=0;
			for(var i=0;i<mydata.length;i++)
			{	
				if(mydata[i].roleCode!="ROLE_ITSOP_MANAGEMENT"){
					if(n % 2==0){
						roleHtml=roleHtml+"<tr>"
					}
					roleHtml=roleHtml+"<td><input type='checkbox' name='userDto.roleIds' class='userRoleId1'";
					roleHtml=roleHtml+" value='"+mydata[i].roleId+"' />"+mydata[i].roleName+"</td>";
					n++;
				}
				
			}
			$("#roleSet table").html(roleHtml);
		},"json")
	}
	
	
	/**
	 * @description 重新加载LDAP用户.
	 */
	this.LDAPList=function(id){
		$('#searchAdUser_ldapId').val(id);
		var sdata=$('#searchAdUser form').getForm();
		var postData = $("#adGrid").jqGrid("getGridParam", "postData");
		$.extend(postData, sdata);  //将postData中的查询参数覆盖为空值
		var _url = 'ldap!getADByLDAP.action';		
		$('#adGrid').jqGrid('setGridParam',{url:_url})
			.trigger('reloadGrid',[{"page":"1"}]).jqGrid('setGridParam',{url:_url});
		return false;
	}
	/**
	 * @description 页面事件
	 */
	this.webEvent=function(){
		
		/**
		 * 导入窗口
		 */
		$('#link_user_import_win').click(function(){
			
			var rowIds = jQuery("#adGrid").jqGrid('getGridParam','selrow');
			if(rowIds==null)
			{
				msgAlert(i18n['plaseSelectOptInfo'],'info');
			}else
			{
				roleId="";	
				$("#roleSet table").html("");
				roleLoad();
				windows('userImport',{width:550,height:420});
			}
			
			
		});
		/**
		 * 导入操作
		 */
		this.roleId;
		$('#link_user_import_opt').click(function(){
			startProcess();
			if($('#userImport form').form('validate')){
				var _param = $('#userImport form').serialize();
				if(_param.indexOf('userDto.roleIds')>-1){
					
					var rowIds = $("#adGrid").jqGrid('getGridParam','selarrrow');
					var total=0;
					var addNumber=0;
					var upNumber=0;
					$.each(rowIds,function(i,item){
						var data = $("#adGrid").getRowData(item);
						sendData="&userDto.loginName="+data.loginName+"&userDto.email="+data.email
						+"&userDto.lastName="+data.lastName+"&userDto.firstName="+data.firstName+"&userDto.officeAddress="+data.officeAddress
						+"&userDto.moblie="+data.moblie+"&userDto.phone="+data.phone+"&userDto.position="+data.position
						+"&userDto.password="+data.loginName+"&userDto.userState="+data.userState+"&";
						$.post('ldap!adUserImport.action',_param+sendData,function(res){
							if(res || res==true){
								addNumber++
							}else
							{
								upNumber++
							}	
							
						})
						total++;
					});
					setTimeout(function(){
						this.filed=total-addNumber-upNumber
						msgAlert(i18n['opertionTotal']+':'+total+','+i18n['newAdd']+':'+addNumber+','+i18n['update']+':'+upNumber+','+i18n['failure']+':'+filed,'info');
						$('#userImport').dialog('close');
						endProcess();
					},2000)
				}else{
					msgAlert(i18n['msg_plase_set_role'],'info');
					endProcess();
				}
			}else{
				endProcess();
			}
		});
		
	};
	/** 
	 * @description 用户新增、编辑所属机构
	 * */
	this.adImportOrgTree_win=function()
	{
		
		load_adImportOrgTree();
	
		windows('adImportOrgTree');
		initAdImportOrgTab();
		
	};
	
	this.initAdImportOrgTab=function()
	{
		$('#adImportSelectOrg').tabs();
	};
	
	/**载入**/
	return{
		init:function(){
			$("#adUser_loading").hide();
			$("#adUser_content").show();
			loadLDAP();
			adUserList();
			webEvent();
			$('#adUser_orgName').click(function(){
				common.security.organizationTreeUtil.showAll_2('#adUser_org_win','#adUser_org_tree','#adUser_orgName','#adUser_orgNo',$('#adUser_import_companyNo').val());
			});
			
			$('#adUser_import_companyName').click(function(){//选择公司
				itsm.itsop.selectCompany.openSelectCompanyWin_method('#adUser_import_companyNo','#adUser_import_companyName','#adUser_orgNo,#adUser_orgName','',function(orgType){
					if(orgType=='company'){
						common.security.user.add_loadRole(-1,"roleSet",false);
					}else{
						common.security.user.add_loadRole(-1,"roleSet",true);	
					}
					$('#adUser_orgName,adUser_orgNo').val('');	
				});
			});
			//加载默认公司
			common.security.defaultCompany.loadDefaultCompany('#adUser_import_companyNo','#adUser_import_companyName');
			
		}
	}
}();

/**载入**/
$(document).ready(adUser.init);

