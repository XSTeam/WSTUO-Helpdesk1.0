$package('common.security')
/**  
* @fileOverview 修改用户密码主函数.
* @author QXY
* @version 1.0  
*/  
/**  
* @author QXY  
* @constructor 修改用户密码主函数.
* @description 修改用户密码主函数.
* @date 2011-05-28
* @since version 1.0 
*/
common.security.editUserPassword=function(){
	
	return {
		/**
		 * 修改密码
		 */
		editUserPassword:function(){
			var pass=$('#newPassword').val();
			var parent=/^[A-Za-z]+$/;
			if($('#edit_user_password_form').form('validate')){
				/*if(pass.length<6){
					msgAlert(i18n.add_msg_trial_wrong_password,'info');
				}else if(!isNaN(pass)){
					msgAlert(i18n.lable_password_num,'info');
				}
				else if(parent.test(pass)){
					msgAlert(i18n.lable_password_chart,'info');
				}else{*/
					var _param = $('#edit_user_password_form').serialize();
					$.post('user!resetPassword.action',_param,function(data){
						if(data){
							$('#oldPassword,#newPassword,#repeatPassword').val('');
							msgShow(i18n['editSuccess'],'show');
						}else{
							msgAlert(i18n['oldPasswordError'],'info');
						}
					});
				//}
			}	
		},
		/**
		 * 修改密码(用户管理)
		 */
		editUserPassword_manage:function(){
			var pass=$('#editUserPwd_newPassword').val();
			var parent=/^[A-Za-z]+$/;
			if($('#editUserPwd_win form').form('validate')){
				/*if(pass.length<6){
					$('#userOtherInfo').tabs('select',i18n['title_ldap_pwd']);
					msgAlert(i18n['add_msg_trial_wrong_password'],'info');
				}else if(!isNaN(pass)){
					msgAlert(i18n.lable_password_num,'info');
				}else if(parent.test(pass)){
					msgAlert(i18n.lable_password_chart,'info');
				}else{*/
					var _param = $('#editUserPwd_win form').serialize();
					$.post('user!resetPassword.action',_param,function(data){
						if(data){
							$('#editUserPwd_win').dialog('close');
							msgShow(i18n['editSuccess'],'show');
						}else{
							msgAlert(i18n['oldPasswordError'],'info');
						}
					});
				//}
			}	
		},
		/**
		 * 密码重置
		 * @param loginName  登录名
		 */
		userPasswordReset:function(loginName){
			msgConfirm(i18n['msg_msg'],'<br/>'+i18n['confirmUserPasswordReset'],function(){
				$.post('user!resetPassword.action','editUserPasswordDTO.loginName='+loginName+'&editUserPasswordDTO.resetPassword=resetPassword',function(data){
					$('#password,#rpassword').val('Password123');
					msgShow(i18n['editSuccess'],'show');
						$.post('email!sendMailByLoginName.action','loginName='+loginName+'&title='+i18n['passwordReset']+'&content='+loginName+i18n['passwordResetInfo'],function(result){
					});
					$('#password,#rpassword,#itsopUserPassword,#itsopUserPasswordConfirm').val('Password123');
				})
			});
			
		},
		/**
		 * 初始化加载
		 */
		init:function(){
			$('#link_eidt_user_password').click(common.security.editUserPassword.editUserPassword);
		}
	}
}();
$(document).ready(common.security.editUserPassword.init);


