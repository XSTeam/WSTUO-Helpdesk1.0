
$(function(){
    // 绑定需要拖拽改变大小的元素对象
    // bindResize($("div").get(0));
   /* $.each($("#formField div[attrtype=Radio]"),function(ind,val){
    	 bindResize($(this).get(0));
    });*/
});

    function bindResize(el){
    	var element_old_width=$(el).css("width");
        // 初始化参数
        var els = el.style,
            // 鼠标的 X 和 Y 轴坐标
            x = y = 0;
        $(el).mousedown(function(e){
            // 按下元素后，计算当前鼠标与对象计算后的坐标
            x = e.clientX - el.offsetWidth;
            // y = e.clientY - el.offsetHeight;
            el.setCapture ? (
                // 捕捉焦点
                el.setCapture(),
                // 设置事件
                el.onmousemove = function(ev){
                    mouseMove(ev || event);
                },
                el.onmouseup = mouseUp
            ) : (
                // 绑定事件
                $(document).bind("mousemove",mouseMove).bind("mouseup",mouseUp)
            );
            // 防止默认事件发生
            e.preventDefault();
            
        });
        // 移动事件
        function mouseMove(e){
            els.width = e.clientX - x + 'px'
            // els.height = e.clientY - y + 'px'
            if(parseInt(els.width) > parseInt(element_old_width)){
            	$(el).addClass("field_options_lob");
            	$(el).attr("style","");
            	$(el).attr("attrOccupying","2");
            	element_old_width=$(el).css("width");
        	}else{
        		$(el).removeClass("field_options_lob");
            	$(el).attr("style","");
            	$(el).attr("attrOccupying","1");
            	element_old_width=$(el).css("width");
        	}
            
            /*if(parseInt(els.width) < parseInt(element_old_width)){
            	$(el).attr("class","field_options");
            	$(el).attr("style","");
            }*/
        }
        // 停止事件
        function mouseUp(){
            el.releaseCapture ? (
                // 释放焦点
                el.releaseCapture(),
                // 移除事件
                el.onmousemove = el.onmouseup = null
            ) : (
                // 卸载事件
                $(document).unbind("mousemove", mouseMove).unbind("mouseup", mouseUp)
            );
        }
        /*function setElementWidth(){
        	if(parseInt(els.width) > parseInt(element_old_width)){
            	$(el).attr("class","field_options field_options_lob");
            	$(el).attr("style","");
        	}
            else{
            	$(el).attr("class","field_options");
            	$(el).attr("style","");
            }
        }*/
    }