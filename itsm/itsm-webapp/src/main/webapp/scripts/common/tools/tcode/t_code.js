$package('common.tools.tcode');
$import('common.config.includes.includes');
$import('common.security.includes.includes');
$import('common.tools.includes.includes');
$import('common.report.includes.includes');
/**  
 * @author Van  
 * @constructor WSTO
 * @date 2011-10-19
 * @since version 1.0 
 */
common.tools.tcode.t_code = function() {
	this.tcode_foucs='false';
	this.isLoadincludes='false';
	return {
		 /**
		  * @description 加载左菜单  
		  **/
		showLeftMenu:function(url,div)
		{
			$('#'+div).empty();
			$('#'+div).load(url);
		},
		/**
		 * 执行t_code
		 * @param key t_code 
		 */
		docmd:function(key){
			if(isLoadincludes=='false'){
				common.config.includes.includes.loadDashboardDataInfoIncludesFile();
				itsm.request.includes.includes.loadRequestActionIncludesFile();
				common.security.includes.includes.loadSelectCustomerIncludesFile();
				common.config.includes.includes.loadCategoryIncludesFile();
				common.config.includes.includes.loadCustomFilterIncludesFile();
				itsm.request.includes.includes.loadRelatedRequestIncludesFile();
				common.tools.includes.includes.loadEventTimeCostIncludesFile();
				//basics.includes.loadRelatedRequestIncludesFile();
				itsm.problem.includes.includes.loadRelatedProblemIncludesFile();
				common.config.includes.includes.loadServiceDirectoryItemsIncludesFile();
				itsm.cim.includes.includes.loadSelectCIIncludesFile();
				basics.includes.loadImportCsvIncludesFile();
				common.report.includes.includes.loadReportViewIncludesFile();
				isLoadincludes='true';
			}
			if(key=="help" || key.toLowerCase()=="help"){
				windows('index_tcode_help_window',{width:280,height:500,modal: false});
				$('#index_tcode_help_table tbody').empty();
				$.each(t_code_map,function(k,v){
					if(k=='IUMGR'){
						if(isITSOPUser==true){
							$('#index_tcode_help_table tbody').append("<tr><td>"+k+"</td><td>"+v['title']+"</td></tr>");
						}
					}else{
						$('#index_tcode_help_table tbody').append("<tr><td>"+k+"</td><td>"+v['title']+"</td></tr>");
					}
				});
			}else{
				if(t_code_map[key.toUpperCase()]==null){
					msgAlert(i18n["msg_tcode_commandNotExist"],"info");
				}else if(key.toLowerCase()=="rpts"){
					var reporti18n=$('#reportLang').val();
					var Confirm="true";
					startLoading();
					common.security.includes.includes.loadSelectCustomerIncludesFile();
					common.report.includes.includes.loadReportViewIncludesFile();
					common.config.includes.includes.loadCategoryIncludesFile();
					common.config.includes.includes.loadCustomFilterIncludesFile();
					common.tools.tcode.t_code.showLeftMenu('../pages/common/report/leftMenu.jsp','leftMenu');
					if(isRequestHave){
						basics.tab.tabUtils.addTab(default_page_report,"../pages/reports!PurestatReport.action?dto.fileName=requestStatus_month&dto.reporti18n="+reporti18n+"&dto.confirm="+Confirm);
					}else{
						common.report.clickEvent.openReportBean('visit_rate_month_x',label_visit_rate,'YEARS','true');
					}
					$('#main_menu_tabs li').removeClass("tabs-selected");
					$('#reportTopMenu_li').addClass("tabs-selected");
				}else if(key.toLowerCase()=="cstp"){
					win = window.open("",t_code_map[key.toUpperCase()]['title']);
					win.location.href = t_code_map[key.toUpperCase()]['url']; 
				}else{
					basics.tab.tabUtils.refreshTab(t_code_map[key.toUpperCase()]['title'],t_code_map[key.toUpperCase()]['url']);
				}
			}
		},
		/**
		 * 初始化
		 */
		init:function(){
			document.onkeydown = function(evt){
				var evt = window.event?window.event:evt;
				if(evt.keyCode==13){
					/*if(fullsearch_foucs || fullsearch_knowledgeId!=""){
						common.compass.fullSearch.enterFullSearch();
						return;
					}*/
					if(document.activeElement.id=='t_code_cmd'){//当T_code获取焦点时才执行
						if($('#t_code_cmd').val()!="" && tcode_foucs=='true'){
							var input_code=$('#t_code_cmd').val();
							common.tools.tcode.t_code.docmd(input_code);
							tcode_foucs='false';
			/*				$("#t_code_cmd").val(i18n['msg_tcode_command_alert']);
							$("#t_code_cmd").focus();*/
						}
						
					}
				};
				if(evt.keyCode==112){//F1帮助卡;
					openHelp();
				}
				if(evt.keyCode==113){//F2本页的帮助视频
					
				}
				if(evt.keyCode==121){//F10提交请求，跟浏览器有全屏冲突
					
				}
				if(evt.keyCode==122){//F11提交请求，跟浏览器有全屏冲突
				}
				if(evt.keyCode==117 ){//F6显示所有T-CODE
					if($('#t_code_temp').html()==''){
						
						load_t_code();
						
						setTimeout(function(){
							common.tools.tcode.t_code.docmd("help");
						},2000);
					}
					common.tools.tcode.t_code.docmd("help");
				}
			};
			/*$("#t_code_cmd").blur(function(){
				if($("#t_code_cmd").val().length==0)
					$("#t_code_cmd").val("T_Code");
			});	*/
		}
	};

}();

//载入
//$(document).ready(common.tools.tcode.t_code.init);


