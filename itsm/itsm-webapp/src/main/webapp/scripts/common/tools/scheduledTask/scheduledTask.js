$package("common.tools.scheduledTask");
/**  
 * @author QXY  
 * @constructor users
 * @description 定期任务管理
 * @date 2011-10-11
 * @since version 1.0 
 */
common.tools.scheduledTask.scheduledTask=function(){
	
	return {
		
		/**
		 * 周期类型显示相应的选择事件
		 * @param value 周期类型
		 * @param type 类型
		 */
		everyWhatChange:function(value,type){
			$('#'+type+'scheduledTask_endDate_input').removeAttr("disabled");
			if(value=='day'){
				$('#'+type+'everyWhat_show').text(i18n['label_scheduledTask_day']);
				$('#'+type+'scheduledTask_startTime').text(i18n['title_startTime']);
				$('#'+type+'scheduledTask_endTime').text(i18n['title_endTime']);
				$('#'+type+'everyWhat_weekly,#'+type+'everyWhat_monthly,#'+type+'everyWhat_cycle,#'+type+'everyWhat_cycleMinute').hide();
				$('#'+type+'everyWhat_day,#'+type+'scheduledTask_taskDate,#'+type+'scheduledTask_taskHour_taskMinute,#'+type+'scheduledTask_endTime_div').show();
			}
			if(value=='weekly'){
				$('#'+type+'everyWhat_show').text(i18n['label_scheduledTask_weekly']);
				$('#'+type+'scheduledTask_startTime').text(i18n['title_startTime']);
				$('#'+type+'scheduledTask_endTime').text(i18n['title_endTime']);
				$('#'+type+'everyWhat_monthly,#'+type+'everyWhat_cycle,#'+type+'everyWhat_day,#'+type+'everyWhat_cycleMinute').hide();
				$('#'+type+'everyWhat_weekly,#'+type+'scheduledTask_taskHour_taskMinute,#'+type+'scheduledTask_taskDate,#'+type+'scheduledTask_endTime_div').show();
			}
			if(value=='month'){
				$('#'+type+'everyWhat_show').text(i18n['label_scheduledTask_months']);
				$('#'+type+'scheduledTask_startTime').text(i18n['title_startTime']);
				$('#'+type+'scheduledTask_endTime').text(i18n['title_endTime']);
				$('#'+type+'everyWhat_weekly,#'+type+'everyWhat_cycle,#'+type+'everyWhat_day,#'+type+'everyWhat_cycleMinute').hide();
				$('#'+type+'everyWhat_monthly,#'+type+'scheduledTask_taskHour_taskMinute,#'+type+'scheduledTask_taskDate,#'+type+'scheduledTask_endTime_div').show();
			}
			if(value=='cycle'){
				$('#'+type+'everyWhat_show').text(i18n['label_scheduledTask_cycle']+'('+i18n['label_slaRule_days']+')');
				$('#'+type+'scheduledTask_startTime').text(i18n['title_startTime']);
				$('#'+type+'scheduledTask_endTime').text(i18n['title_endTime']);
				$('#'+type+'everyWhat_weekly,#'+type+'everyWhat_monthly,#'+type+'everyWhat_day,#'+type+'everyWhat_cycleMinute').hide();
				$('#'+type+'everyWhat_cycle,#'+type+'scheduledTask_taskDate,#'+type+'scheduledTask_taskHour_taskMinute,#'+type+'scheduledTask_endTime_div').show();
			}
			if(value=='cycleMinute'){
				$('#'+type+'everyWhat_show').text(i18n['label_scheduledTask_cycle']+'('+i18n['label_slaRule_minutes']+')');
				$('#'+type+'scheduledTask_startTime').text(i18n['title_startTime']);
				$('#'+type+'scheduledTask_endTime').text(i18n['title_endTime']);
				$('#'+type+'everyWhat_weekly,#'+type+'everyWhat_monthly,#'+type+'everyWhat_day,#'+type+'everyWhat_cycle,#'+type+'scheduledTask_taskHour_taskMinute').hide();
				$('#'+type+'everyWhat_cycleMinute,#'+type+'scheduledTask_taskDate,#'+type+'scheduledTask_endTime_div').show();
			}
			if(value=='on_off'){
				$('#'+type+'everyWhat_show').text(i18n['label_scheduledTask_on_off']);
				$('#'+type+'scheduledTask_startTime').text(i18n['title_startTime']);
				$('#'+type+'scheduledTask_endTime').text('');
				$('#'+type+'everyWhat_weekly,#'+type+'everyWhat_monthly,#'+type+'everyWhat_day,#'+type+'everyWhat_cycle,#'+type+'scheduledTask_endTime_div,#'+type+'everyWhat_cycleMinute').hide();
				$('#'+type+'scheduledTask_taskDate,#'+type+'scheduledTask_taskHour_taskMinute').show();
				$('#'+type+'scheduledTask_endDate_input').attr("disabled","disabled");
			}	
			
		},
		/**
		 * @description 全选
		 * */
	    checkAll:function (id,divId,name) {
	    	if($('#'+id).attr('checked'))
	    		$('#'+divId+' input[name="'+name+'"]').attr("checked",true);
	    	else
	    		$('#'+divId+'  input[name="'+name+'"]').attr("checked",false);
	    },
		
		init:function(){
			
		}
	}
	
}();
