$package('common.tools.mq');

 /**  
 * @author Wstuo  
 * @constructor ActiveMQManage
 * @description tools/ActiveMQManage.jsp
 * @date 2012-09-18
 * @since version 1.0 
 */
common.tools.mq.activeMQManage = function() 
{
		this._flag = true;
		var refreshMessage;
		return {
			/**
			 * 显示MQ消息
			 */
			showQueues:function(){
				startProcess();
				$.post("activeMQ!showQueues.action",function(data){
					endProcess();
					for(var i=0;i<data.length;i++){
							$('#activeMQManage_content tbody').append("<tr>"+
									"<td align=\"center\"><a href=javascript:common.tools.mq.activeMQManage.showQueueInfo('"+data[i].queueName+"')>"+data[i].queueName+"</a></td>"+
									"<td align=\"center\">"+data[i].consumerCount+"</td>"+
									"<td align=\"center\">"+data[i].dequeueCount+"</td>"+
									"<td align=\"center\">"+data[i].enqueueCount+"</td>"+
									"<td align=\"center\">"+data[i].dispatchCount+"</td>"+
									"<td align=\"center\">"+data[i].expiredCount+"</td>"+
									"<td align=\"center\">"+data[i].maxEnqueueTime+"</td>"+
									"<td align=\"center\">"+data[i].producerCount+"</td>"+
									"<td align=\"center\">"+data[i].memoryPercentUsage+"</td>"+
									"</tr>");
					}
				});
			},
			/**
			 * 显示MQ消息详细
			 */
			showQueueInfo:function(queueName){
				$.post("activeMQ!showQueueInfo.action",{"queueName":queueName},function(data){
					$('#messageDiv').html("<div style=\"margin-left: 5px;\">"+
						"<strong style=\"font-size: 18px;\">Messages</strong>"+
						"</div>"+
						"<hr>"+
						"<div class=\"hisdiv\">"+
						"	<table class=\"histable\" width=\"100%\">"+
						"		<thead>"+
						"			<tr>"+
						"				<th>"+i18n['message_id']+"</th>"+
						"				<th>"+i18n['mqMessage_title']+"</th>"+
						"				<th>"+i18n['mqMessage_Content']+"</th>"+
						"				<th>"+i18n['title_creator']+"</th>"+
						"				<th>"+i18n['message_priority']+"</th>"+
						"				<th>"+i18n['message_redelivered']+"</th>"+
						"				<th>"+i18n['common_createTime']+"</th>"+
						"				<th>"+i18n['operateItems']+"</th>"+
						"			</tr>"+
						"		</thead>"+
						"		<tbody>"+
						"		</tbody>"+
						"	</table>"+
						"</div>	");
					for(var i=0;i<data.length;i++){
							$('#messageDiv tbody').append("<tr>"+
									"<td align=\"center\">"+data[i].messageID+"</td>"+
									"<td align=\"center\">"+data[i].mqTitle+"</td>"+
									"<td align=\"center\">"+data[i].mqContent+"</td>"+
									"<td align=\"center\">"+data[i].mqCreator+"</td>"+
									"<td align=\"center\">"+data[i].priority+"</td>"+
									"<td align=\"center\">"+data[i].redelivered+"</td>"+
									"<td align=\"center\">"+data[i].timestamp+"</td>"+
									"<td align=\"center\"><a href=javascript:common.tools.mq.activeMQManage.deleteMessage('"+data[i].messageID+"','"+queueName+"')>"+i18n['deletes']+"</a></td>"+
									"</tr>");
					}
					
					if(data.length>0){
						if(_flag){
						    refreshMessage=setInterval(function(){
								common.tools.mq.activeMQManage.showQueueInfo(''+queueName+'');
							},500);
							_flag = false;
						}
					}else{
						window.clearInterval(refreshMessage);
					}
				});
			},
			/**
			 * 删除MQ消息
			 */
			deleteMessage:function(messageID,queueName){
				msgConfirm(i8n['tip'],i18n['msg_confirmDelete'],function(){
					$.post("activeMQ!deleteMessage.action",{"messageId":messageID,"queueName":queueName},function(data){
						if(data=="true"){
							msgShow(i18n['deleteSuccess'],'show');
						}
					});
				});
			},
			/**
			 * 初始化
			 */
			init:function(){
				$("#activeMQManage_loading").hide();
				common.tools.mq.activeMQManage.showQueues();
			}
		};
}();
$(document).ready(common.tools.mq.activeMQManage.init);