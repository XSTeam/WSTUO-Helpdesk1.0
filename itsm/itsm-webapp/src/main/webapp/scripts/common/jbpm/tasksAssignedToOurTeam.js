$package('common.jbpm'); 
/**  
 * @fileOverview 指派给我组的任务
 * @author QXY
 * @version 1.0  
 */  
 /**  
 * @author QXY  
 * @constructor tasksAssignedToOurTeam
 * @description 指派给我组的任务
 * @date 2012-12-19
 * @since version 1.0 
 */
common.jbpm.tasksAssignedToOurTeam = function(){

	this.variables1=null;
	return {
		/**
		 *@description       操作项格式化
		 *@param cellValue:  单元格的值；
		 *@param options:    字体样式等；
		 *@param rowObject:  一行的数据
		 */
		viewDetailedForma:function(cellvalue, options, rowObject){
			variables1 = cellvalue;
			var executionId=rowObject.executionId;
			var variables = cellvalue;
			var _href='<a href="javascript:{href}">['+i18n['common_view']+']</a>';
			
			var dto = variables.dto;
			if(dto!=undefined && dto!=null){
				if(executionId.indexOf('Changes')>=0 || executionId.indexOf('Change')>=0 || executionId.indexOf('change')>=0){
					return _href.replace('{href}','basics.tab.tabUtils.reOpenTab(\'change!changeInfo.action?queryDTO.eno='+dto.eno+'\',\''+i18n["change_detail"]+'\')');
				}else if(executionId.indexOf('problem')>=0 || executionId.indexOf('Problem')>=0){
					return _href.replace('{href}','basics.tab.tabUtils.reOpenTab(\'problem!findProblemById.action?eno='+dto.eno+'\',\''+i18n["probelm_detail"]+'\')');
				}else if(executionId.indexOf('release')>=0 || executionId.indexOf('Release')>=0){
					return _href.replace('{href}','basics.tab.tabUtils.reOpenTab(\'release!releaseDetails.action?releaseDTO.eno='+dto.eno+'\',\''+i18n["release_details"]+'\')');
				}else if(executionId.indexOf('request')>=0 || executionId.indexOf('Request')>=0){
					return _href.replace('{href}','basics.tab.tabUtils.reOpenTab(\'request!requestDetails.action?eno='+dto.eno+'\',\''+i18n["request_detail"]+'\')');
				}
			}else if(variables.eno!=null){
				if(executionId.indexOf('Changes')>=0 || executionId.indexOf('Change')>=0 || executionId.indexOf('change')>=0){
					return _href.replace('{href}','basics.tab.tabUtils.reOpenTab(\'change!changeInfo.action?queryDTO.eno='+variables.eno+'\',\''+i18n["change_detail"]+'\')');
				}else if(executionId.indexOf('problem')>=0 || executionId.indexOf('Problem')>=0){
					return _href.replace('{href}','basics.tab.tabUtils.reOpenTab(\'problem!findProblemById.action?eno='+variables.eno+'\',\''+i18n["probelm_detail"]+'\')');
				}else if(executionId.indexOf('release')>=0 || executionId.indexOf('Release')>=0){
					return _href.replace('{href}','basics.tab.tabUtils.reOpenTab(\'release!releaseDetails.action?releaseDTO.eno='+variables.eno+'\',\''+i18n["release_details"]+'\')');
				}else if(executionId.indexOf('request')>=0 || executionId.indexOf('Request')>=0){
					return _href.replace('{href}','basics.tab.tabUtils.reOpenTab(\'request!requestDetails.action?eno='+variables.eno+'\',\''+i18n["request_detail"]+'\')');
				}
			}else{
				return '';
			}
			
		},
		/**
		 *@description       标题格式化
		 *@param cellValue:  单元格的值；
		 *@param options:    字体样式等；
		 *@param rowObject:  一行的数据
		 */
		
		etitleForma:function(cellvalue, options, rowObject){
			var variables = cellvalue;
			if(variables!=undefined && variables!=null){
				return variables.etitle;
			}else{
				return '';
			}
			
		},
		/**
		 *@description       双击跳转到对应页面
		 *@param cellValue:  单元格的值；
		 *@param options:    字体样式等；
		 *@param rowObject:  一行的数据
		 */
		viewDetailedDBClick:function(cellvalue, options, rowObject){
			var executionId=rowObject.executionId;
			var variables = cellvalue;
			var dto = variables.dto;
			if(dto!=undefined && dto!=null){
				if(executionId.indexOf('Changes')>=0 || executionId.indexOf('Change')>=0 || executionId.indexOf('change')>=0){
					basics.tab.tabUtils.reOpenTab('change!changeInfo.action?queryDTO.eno='+dto.eno,i18n["change_detail"]);
				}else if(executionId.indexOf('problem')>=0 || executionId.indexOf('Problem')>=0){
					basics.tab.tabUtils.reOpenTab('problem!findProblemById.action?eno='+dto.eno,i18n["probelm_detail"]);
				}else if(executionId.indexOf('release')>=0 || executionId.indexOf('Release')>=0){
					basics.tab.tabUtils.reOpenTab('release!releaseDetails.action?releaseDTO.eno='+dto.eno,i18n["release_details"]);
				}else if(executionId.indexOf('request')>=0 || executionId.indexOf('Request')>=0){
					basics.tab.tabUtils.reOpenTab('request!requestDetails.action?eno='+dto.eno,i18n["request_detail"]);
				}
			}else if(variables.eno!=null){
				if(executionId.indexOf('Changes')>=0 || executionId.indexOf('Change')>=0 || executionId.indexOf('change')>=0){
					basics.tab.tabUtils.reOpenTab('change!changeInfo.action?queryDTO.eno='+variables.eno,i18n["change_detail"]);
				}else if(executionId.indexOf('problem')>=0 || executionId.indexOf('Problem')>=0){
					basics.tab.tabUtils.reOpenTab('problem!findProblemById.action?eno='+variables.eno,i18n["probelm_detail"]);
				}else if(executionId.indexOf('release')>=0 || executionId.indexOf('Release')>=0){
					basics.tab.tabUtils.reOpenTab('release!releaseDetails.action?releaseDTO.eno='+variables.eno,i18n["release_details"]);
				}else if(executionId.indexOf('request')>=0 || executionId.indexOf('Request')>=0){
					basics.tab.tabUtils.reOpenTab('request!requestDetails.action?eno='+variables.eno,i18n["request_detail"]);
				}
			}else{
				return false;
			}
		},
		
		/**
		 *@description        类型格式化
		 *@param cellValue:  单元格的值；
		 *@param options:    字体样式等；
		 *@param rowObject:  一行的数据
		 */
		taskTypeForma:function(cellvalue, options, rowObject){
			var executionId=rowObject.executionId;
			if(executionId.indexOf('Changes')>=0 || executionId.indexOf('Change')>=0 || executionId.indexOf('change')>=0){
				return i18n['change']
			}else if(executionId.indexOf('problem')>=0 || executionId.indexOf('Problem')>=0){
				return i18n['problem']
			}else if(executionId.indexOf('release')>=0 || executionId.indexOf('Release')>=0){
				return i18n['release']
			}else if(executionId.indexOf('request')>=0 || executionId.indexOf('Request')>=0){
				return i18n['request']
			}
		},
		/**
		 * 显示指派给我组的任务
		 */
		showTaskGrid:function(){
			var params=$.extend({},jqGridParams,{
				url:'jbpm!findPageGroupTasks.action',
				postData:{'assignee':userName},
				colNames:
						[
						 i18n['label_bpm_task_id']
						,i18n['title']
						,i18n['title_request_assignTo']
						,i18n['label_bpm_activity_name']
						,i18n['label_bpm_task_name']
						//,i18n['priority']
						,i18n['common_createTime']
						//,i18n['label_bpm_task_duedate']
						,i18n['label_bpm_task_type']
						,i18n['operateItems']
						],
				colModel:[
				          {name:'id',sortable:false,align:'center'},
				          {name:'variables',formatter:common.jbpm.tasksAssignedToOurTeam.etitleForma,sortable:false,align:'center',width:150},
						  {name:'assignee',width:150,sortable:false},
						  {name:'activityName',hidden:true,sortable:false},
						  {name:'name',width:100,sortable:false},
						 // {name:'priority',width:100,sortable:false,align:'center'},
						  {name:'createTime',formatter:timeFormatter,sortable:false,align:'center'},
						 //{name:'duedate',formatter:timeFormatter,sortable:false,align:'center'},
						  {name:'executionId',sortable:false,formatter:common.jbpm.tasksAssignedToOurTeam.taskTypeForma,align:'center'},
						  {name:'variables',formatter:common.jbpm.tasksAssignedToOurTeam.viewDetailedForma,sortable:false,align:'center'}
					  ],
				toolbar:false,
				ondblClickRow:function(rowId){
					var rowData= $("#tasksAssignedToOurTeamGrid").jqGrid('getRowData',rowId);  // 行数据  
					common.jbpm.tasksAssignedToOurTeam.viewDetailedDBClick(variables1,null,rowData);
				},
				multiselect:false,
				jsonReader: $.extend(jqGridJsonReader,{id:"executionId"}),
				sortname:'id',
				pager:'#tasksAssignedToOurTeamPager'
			});
			$("#tasksAssignedToOurTeamGrid").jqGrid(params);
			$("#tasksAssignedToOurTeamGrid").navGrid('#tasksAssignedToOurTeamPager',navGridParams);
			//自适应大小
			setGridWidth("#tasksAssignedToOurTeamGrid","regCenter",20);
		},
		/**
		 * 初始化
		 * @private
		 */
		init:function(){
			common.jbpm.tasksAssignedToOurTeam.showTaskGrid();
		}
	}
}();
$(document).ready(common.jbpm.tasksAssignedToOurTeam.init);