$package('itsm.request');
$import("common.knowledge.knowledgeDetail");
/**  
 * @author QXY  
 * @constructor relatedRequestAndKnowledge
 * @description 相类似的请求和知识主函数
 * @date 2010-02-26
 * @since version 1.0 
 */
itsm.request.relatedRequestAndKnowledge=function(){
	return {
		/**
		 * 跳转到请求详情
		 * @param eno 请求eno
		 */
		requestDetails:function(eno){
			basics.tab.tabUtils.reOpenTab('request!requestDetails.action?eno='+eno,i18n.request_detail);
		},
		/**
		 * 请求标题格式化
		 */
		requestTitleUrl:function(cell,opt,data){
			return '<a href=javascript:itsm.request.relatedRequestAndKnowledge.requestDetails('+data.eno+')>'+cell+'</a>';
		},
		/**
         * @description 知识库标题格式化
         */
        knowledgeGridTitleFormatter: function (cell, opt, data) {
            return "<a href=javascript:common.knowledge.knowledgeDetail.showKnowledgeDetail('" + data.kid + "')>" + cell + "</a>";
        },
		/**
		 * 查询相类似的请求
		 * @param gridId 列表Id
		 * @param gridPageId 列表页面Id
		 * @param keyword 关键字
		 */
		findLikeRequest:function(gridId,gridPageId,keyword){
			var _url="request!analogousRequest.action";
			var _postData={};
			$.extend(_postData,{'fullTextQueryDTO.alias':'RequestInfo','fullTextQueryDTO.queryString':keyword});
			
			if(!basics.ie6.htmlIsNull(gridId)){
				var params=$.extend({},jqGridParams,{
					caption:i18n.title_related_other_request,
					url:_url,
					postData:_postData,
					colNames:['ID',i18n.common_id,i18n.common_title,i18n.category,i18n.requester,i18n.title_request_assignToGroup,i18n.title_request_assignToTC,i18n.priority,i18n.common_state,i18n.common_createTime],
					colModel:[
					          {name:'eno',sortable:false,width: 40},
					          {name:'requestCode',width:150},
					          {name:'etitle',width:200,formatter:itsm.request.relatedRequestAndKnowledge.requestTitleUrl},
							  {name:'requestCategoryName',width:80,sortable:false,align:'center'},
							  {name:'createdByName',width:80,sortable:false,align:'center'},
							  {name:'assigneeGroupName',width:80,sortable:false,align:'center',hidden:true},
							  {name:'assigneeName',width:90,sortable:false,align:'center',hidden:true},
							  {name:'priorityName',width:90,align:'center',sortable:false,hidden:true},
							  {name:'statusName',width:90,sortable:false,align:'center'},
							  {name:'createdOn',width:90,formatter:timeFormatter,align:'center'}
							  ],
					jsonReader: $.extend({},jqGridJsonReader, {id: "eno"}),
					ondblClickRow:function(rowId){itsm.request.relatedRequestAndKnowledge.requestDetails(rowId);},
					sortname:'eno',
					toolbar:false,
					multiselect:false,
					pager:gridPageId
				});
				$(gridId).jqGrid(params);
				$(gridId).navGrid(gridPageId,navGridParams);
				setGridWidth(gridId,"addRequest_center",8);
			}else{	
				$(gridId).jqGrid('setGridParam',{page:1,url:_url,postData:_postData}).trigger('reloadGrid');
			}
		},
		/**
         * 查询相类的知识
         * @param gridId 列表Id
		 * @param gridPageId 列表页面Id
		 * @param keyword 关键字
         */
		findLikeKnowledge:function(gridId,gridPageId,keyword){
			
        	var _url="knowledgeInfo!fullSearch.action";
        	var _postData={};
        	$.extend(_postData,{'fullTextQueryDto.alias':'KnowledgeInfo','fullTextQueryDto.queryString':keyword});
        	if(!basics.ie6.htmlIsNull(gridId)){
        		var params = $.extend({},jqGridParams, {
                    url:_url,
                    postData:_postData,
                    caption: i18n.title_request_knowledgeGrid,
                    colNames: ['ID',i18n.label_knowledge_relatedService,i18n.title, i18n.knowledge_knowledgeCategory, i18n.title_createTime, '',],
                    colModel: [
                               {name: 'kid',sortable:false,width: 20}, 
                               {name:'serviceDirectoryItemName',width:200,align:'center',sortable: false},
                               {name: 'title', width: 220,formatter: itsm.request.relatedRequestAndKnowledge.knowledgeGridTitleFormatter}, 
                               {name: 'categoryName',width: 200,align: 'center',sortable: false}, 
                               {name: 'addTime',width: 185,align: 'center',formatter: timeFormatter,sortable:false},
                               {name: 'keyWords',width: 185,hidden: true}
                               ],
                    jsonReader: $.extend(jqGridJsonReader, {
                        id: "kid"
                    }),
                    sortname: 'kid',
                    toolbar:false,
					multiselect:false,
                    ondblClickRow:function(rowId){common.knowledge.knowledgeDetail.showKnowledgeDetail(rowId);},
                    pager:gridPageId
                });
                $(gridId).jqGrid(params);
                $(gridId).navGrid(gridPageId, navGridParams);
                setGridWidth(gridId,"addRequest_center",8);
        	}else{
        		$(gridId).jqGrid('setGridParam',{page:1,url:_url,postData:_postData}).trigger('reloadGrid');
        	}
            
        },
        /**
         * 查询知识
         * @param gridId 列表Id
		 * @param gridPageId 列表页面Id
		 * @param keyword 关键字
         */
		findKnowledge:function(gridId,gridPageId,keyword){
			
        	var _url="knowledgeInfo!findAllKnowledges.action?knowledgeQueryDto.opt=";
        	var _postData={};
        	$.extend(_postData,{'knowledgeQueryDto.title':keyword});
        	if(!basics.ie6.htmlIsNull(gridId)){
        		var params = $.extend({},jqGridParams, {
                    url:_url,
                    postData:_postData,
                    caption: i18n.title_request_knowledgeGrid,
                    colNames: ['ID',i18n.label_knowledge_relatedService,i18n.title, i18n.knowledge_knowledgeCategory, i18n.title_createTime, '',],
                    colModel: [
                               {name: 'kid',sortable:false,width: 40}, 
                               {name:'knowledgeServiceName',width:200,align:'center',sortable: false},
                               {name: 'title', width: 220,formatter: itsm.request.relatedRequestAndKnowledge.knowledgeGridTitleFormatter}, 
                               {name: 'categoryName',width: 200,align: 'center',sortable: false}, 
                               {name: 'addTime',width: 185,align: 'center',formatter: timeFormatter,sortable:false},
                               {name: 'keyWords',width: 185,hidden: true
                    }],
                    jsonReader: $.extend(jqGridJsonReader, {
                        id: "kid"
                    }),
                    sortname: 'kid',
                    toolbar:false,
					multiselect:false,
                    ondblClickRow:function(rowId){common.knowledge.knowledgeDetail.showKnowledgeDetail(rowId);},
                    pager:gridPageId
                });
                $(gridId).jqGrid(params);
                $(gridId).navGrid(gridPageId, navGridParams);
                setGridWidth(gridId,"addRequest_center",8);
        	}else{
        		$(gridId).jqGrid('setGridParam',{page:1,url:_url,postData:_postData}).trigger('reloadGrid');
        	}
            
        },
        /**
         * 初始化
         * @private
         */
		init:function(){
			$("#relatedRequestAndKnowledge_loading").hide();
			$("#relatedRequestAndKnowledge_content").show();
		}
	};
}();
$(document).ready(itsm.request.relatedRequestAndKnowledge.init);