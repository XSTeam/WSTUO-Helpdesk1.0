$package("itsm.cim");
$import('common.config.formCustom.formControlImpl');
$import('itsm.itsop.selectCompany');
$import('common.security.userUtil');
$import('common.config.category.eventCategoryTree');
$import('common.security.xssUtil');
/**  
 * @author wing  
 * @constructor cimCommon
 * @description 配置项公共函数
 * @date 2015-11-17
 * @since version 1.0 
 */
itsm.cim.cimCommon=function(){
	var loadAutoUserName=true;
	var loadAutoCompany=true;
	//载入
	return {
		
		/**
		 * 初始化默认表单
		 */
		initDefaultForm:function(htmlDivId){
			
			$("#"+htmlDivId+" .field_opt").remove();
			//$("#"+htmlDivId+" .field_options").css('cursor','auto');
			/*var formCustomContents = $("#"+htmlDivId).html();
			formCustomContents = common.config.formCustom.formControlImpl.replaceSpecialAttr(formCustomContents);
			$("#"+htmlDivId).html(formCustomContents);*/
			if(htmlDivId == "addCim_formField"){
				itsm.cim.cimCommon.setDefaultParamValue(htmlDivId);
			}
			DatePicker97($("#"+htmlDivId+" :input[attrType=Date]"),"yyyy-MM-dd HH:mm:ss");
		},
		/**
		 * 初始化默认表单
		 */
		initDefaultFormByAttr:function(htmlDivId){
			
			$("#"+htmlDivId+" .field_opt").remove();
			$("#"+htmlDivId+" .field_options").css('cursor','auto');
			var formCustomContents = $("#"+htmlDivId).html();
			formCustomContents = common.config.formCustom.formControlImpl.replaceSpecialAttr(formCustomContents,htmlDivId);
			$("#"+htmlDivId).html(formCustomContents);
			if(htmlDivId == "editCim_formField"){
				itsm.cim.cimCommon.setDefaultParamValue(htmlDivId);
			}
		},
		setDefaultParamValue:function(htmlDivId){
			$("#"+htmlDivId+" #ci_add_companyName").val(companyName);
			$("#"+htmlDivId+" #ci_add_companyNo").val(companyNo);
		},
		/**
		 * 选择公司
		 */
		chooseCompany :function(companyNo,companyName,event){
			var formId=$(event).closest('form').attr("id");
			if(formId)
				itsm.itsop.selectCompany.openSelectCompanyWin(
						'#'+formId+' '+companyNo,
						'#'+formId+' '+companyName,
						'#request_userName,#request_userId,#selectCreator,#addRequestUserId,#add_request_ref_ciname,#add_request_ref_ciid');
		},
		
		/**
		 * @description 选择位置.
		 * @param showAttrId 扩展属性Id
		 * @param dtoName 实体对象名
		 */
		selectCimLocation:function(categoryName,categoryNo,event){
			var formId=$(event).closest('form').attr("id");
			if(formId)
				common.config.category.eventCategoryTree.showSelectTree(
						'#ci_loc_select_window',
                        '#ci_loc_select_tree',
                        'Location',
                        '#'+formId+' '+categoryName,
                        '#'+formId+' '+categoryNo,
                        null,
                        null,
                        null,
                        'ciDto');
		},
		selectCimUser:function(event,namePut,idPut,tcCostPut,showType,companyId,callBack){
			var formId=$(event).closest('form').attr("id");
			namePut = '#'+formId+' '+namePut;
			idPut = '#'+formId+' '+idPut;
			companyId = '#'+formId+' '+companyId;
			var _companyNo = $(companyId).val() 
			common.security.userUtil.selectUser(namePut,idPut,tcCostPut,showType,_companyNo,callBack);
		},
		selectCimDepartment:function(event,windowPanel,treePanel,namePut,idPut,companyId){
			var formId=$(event).closest('form').attr("id");
			namePut = '#'+formId+' '+namePut;
			idPut = '#'+formId+' '+idPut;
			companyId = '#'+formId+' '+companyId;
			var _companyNo = $(companyId).val() 
			common.security.organizationTreeUtil.showAll_2(windowPanel,treePanel,namePut,idPut,_companyNo);
		},
		/**
		 * 对旧表单的值进行赋值
		 * @param htmlDivId 表单Id
		 * @param data 数据源
		 */
		setCimParamValueToOldFormByDataDictionaray:function(htmlDivId,data,callback){
			var num = 0;
			$(htmlDivId+" :input[attrtype=DataDictionaray]").each(function(i,obj){
				var attrId=$(obj).attr("id");
				common.config.dictionary.dataDictionaryUtil.loadOptionsByCode($(obj).attr("attrdatadictionary"),htmlDivId+' #'+attrId,function(){
					if(attrId=="ci_statusAdd"){
						var statusId = (data.statusId == 0 ? "":data.statusId);
						$(htmlDivId+" #ci_statusAdd").val(statusId);
						num++;
					}else if(attrId=="ci_brandNameAdd"){
						var brandId = (data.brandId == 0 ? "":data.brandId);
						$(htmlDivId+" #ci_brandNameAdd").val(brandId);
						num++;
					}else if(attrId=="ci_providerAdd"){
						var providerId = (data.providerId == 0 ? "":data.providerId);
						$(htmlDivId+" #ci_providerAdd").val(providerId);
						num++;
					}else if(attrId=="ci_systemPlatform"){
						var systemPlatformId = (data.systemPlatformId == 0 ? "":data.systemPlatformId);
						$(htmlDivId+" #ci_systemPlatform").val(systemPlatformId);
						num++;
					}else{
						var _value = (($(obj).attr('val') == 0 || $(obj).attr('val') ==undefined) ? "" : $(obj).attr('val'));
						$(obj).val(_value);
					}
					if(num==5 && callback!=null && callback!="" ){
						callback();
					}
				});
			});
		},
		setCimDataDictionarayValueByDetail :function(htmlDivId,data){
			$(htmlDivId+" :input[attrtype=DataDictionaray]").each(function(i,obj){
				var attrId=$(obj).attr("id");
				//common.config.dictionary.dataDictionaryUtil.loadOptionsByCode($(obj).attr("attrdatadictionary"),htmlDivId+' #'+attrId,function(){
					$(obj).val($(obj).attr('val'));
					if(attrId=="ci_statusAdd"){
						$(htmlDivId+" #ci_statusAdd").closest('.field,.field_lob').html(data.status);
					}else if(attrId=="ci_brandNameAdd"){
						$(htmlDivId+" #ci_brandNameAdd").closest('.field,.field_lob').html(data.brandName);
					}else if(attrId=="ci_providerAdd"){
						$(htmlDivId+" #ci_providerAdd").closest('.field,.field_lob').html(data.providerName);
					}else if(attrId=="ci_systemPlatform"){
						$(htmlDivId+" #ci_systemPlatform").closest('.field,.field_lob').html(data.systemPlatform);
					}

				//});
			});
		},
		/**
		 * 对旧表单的值进行赋值
		 * @param htmlDivId 表单Id
		 * @param data 数据源
		 */
		setCimParamValueToOldForm:function(htmlDivId,data){
		    $(htmlDivId+" #ci_add_cino").val(data.cino);
			
			$(htmlDivId+" #ciname").val(data.ciname);
			$(htmlDivId+" #ci_add_companyName").val(data.companyName);
			$(htmlDivId+" #ci_add_companyNo").val(data.companyNo);
			
			
			$("#ci_add_categoryNo").val(data.categoryNo);
			$("#ci_add_categoryName").val(data.categoryName);
			$("#ciEdit_categoryNo2").val(data.categoryNo);
			$("#ciEdit_categoryName2").val(data.categoryName);
	
			$(htmlDivId+" #model").val(data.model);
			$(htmlDivId+" #serialNumber").val(data.serialNumber);
			$(htmlDivId+" #barcode").val(data.barcode);
			  
			$(htmlDivId+" #ci_brandNameAdd").val(data.edesc);
			
			$(htmlDivId+" #configureItem_add_buyDate").val(data.buyDate);
			$(htmlDivId+" #configureItem_add_arrivalDate").val(data.arrivalDate);
			$(htmlDivId+" #warningDate").val(data.warningDate);
			$(htmlDivId+" #poNo").val(data.poNo);
			$(htmlDivId+" #assetsOriginalValue").val(data.assetsOriginalValue);
			//$(htmlDivId+" #ci_locid").val(data.locId);
			if(data.loc){
				$(htmlDivId+" #ci_loc").val(data.loc);
			}
			$(htmlDivId+" #ci_locid").val(data.locId);
			
			
			//console.log(data);
			$(htmlDivId+" #lifeCycle").val(data.lifeCycle);
			$(htmlDivId+" #warranty").val(data.warranty);
			$(htmlDivId+" #configureItem_add_department").val(data.department);
			$(htmlDivId+" #ci_add_originalUser").val(data.originalUser);
			$(htmlDivId+" #ci_add_originalUserId").val(data.originalUserId);
			$(htmlDivId+" #ci_add_useName").val(data.userName);
			$(htmlDivId+" #ci_add_useNameId").val(data.userNameId);
			$(htmlDivId+" #ci_add_owner").val(data.owner);
			$(htmlDivId+" #ci_add_ownerId").val(data.ownerId);
			$(htmlDivId+" #depreciationIsZeroYears").val(data.depreciationIsZeroYears);
			$(htmlDivId+" #CDI").val(data.CDI);
		},
		
		/**
		 * 对旧表单的值进行赋值
		 * @param htmlDivId 表单Id
		 * @param data 数据源
		 */
		setCimParamValueToOldFormByDetail:function(htmlDivId,data){
			//console.log(data);
		    $(htmlDivId+" #ci_add_cino").closest('.field,.field_lob').html(data.cino);
			$(htmlDivId+" #ciname").closest('.field,.field_lob').html(data.ciname);
			$(htmlDivId+" #ci_add_companyName").closest('.field,.field_lob').html(data.companyName);
			$(htmlDivId+" #ci_add_companyNo").closest('.field,.field_lob').html(data.companyNo);
			$(htmlDivId+" #model").closest('.field,.field_lob').html(data.model);
			$(htmlDivId+" #serialNumber").closest('.field,.field_lob').html(data.serialNumber);
			$(htmlDivId+" #barcode").closest('.field,.field_lob').html(data.barcode);
			$(htmlDivId+" #ci_brandNameAdd").closest('.field,.field_lob').html(data.edesc);
			$(htmlDivId+" #configureItem_add_buyDate").closest('.field,.field_lob').html(data.buyDate);
			$(htmlDivId+" #configureItem_add_arrivalDate").closest('.field,.field_lob').html(data.arrivalDate);
			$(htmlDivId+" #warningDate").closest('.field,.field_lob').html(data.arrivalDate);
			$(htmlDivId+" #poNo").closest('.field,.field_lob').html(data.poNo);
			$(htmlDivId+" #assetsOriginalValue").closest('.field,.field_lob').html(data.assetsOriginalValue);
			//$(htmlDivId+" #ci_locid").closest('.field,.field_lob').html(data.locationNos);
			$(htmlDivId+" #ci_loc").closest('.field,.field_lob').html(data.loc);
			
			$(htmlDivId+" #lifeCycle").closest('.field,.field_lob').html(data.lifeCycle);
			$(htmlDivId+" #warranty").closest('.field,.field_lob').html(data.warranty);
			$(htmlDivId+" #configureItem_add_department").closest('.field,.field_lob').html(data.department);
			$(htmlDivId+" #ci_add_originalUser").closest('.field,.field_lob').html(data.originalUser);
			$(htmlDivId+" #ci_add_useName").closest('.field,.field_lob').html(data.userName);
			$(htmlDivId+" #ci_add_owner").closest('.field,.field_lob').html(data.owner);
			$(htmlDivId+" #depreciationIsZeroYears").closest('.field,.field_lob').html(data.depreciationIsZeroYears);
			$(htmlDivId+" #CDI").closest('.field,.field_lob').html(data.CDI);
		},
		/**
		 * 自动补全
		 */
		bindAutoCompleteToCim:function(htmlDivId){
			loadAutoCompany=true;//所属客户
			if(versionType==="ITSOP"){
				$('#'+htmlDivId+' #ci_add_companyName').focus(function(){
					if(loadAutoCompany){
						loadAutoCompany=false;//所属客户
						itsm.app.autocomplete.autocomplete.bindAutoComplete('#'+htmlDivId+' #request_companyName','com.wstuo.common.security.entity.Organization','orgName','orgName','orgNo','Long','#'+htmlDivId+' #request_companyNo',userName,'false');
					}
				});
			}
		},
		changeCompany:function(htmlDivId){
			var temp_companyName = $(htmlDivId+' #ci_add_companyName').val();
			$(htmlDivId+' #ci_add_companyName').focus(function(){
				temp_companyName = $(htmlDivId+' #ci_add_companyName').val();
			});
			
		},
		/**
		 * 给请求编辑form赋值
		 * @data 数据对象
		 * @formId 表单id
		 */
		setCimFormValuesByScheduledTask:function(eventdata,formId,eventEavVals,eavNo,callback){
			itsm.cim.cimCommon.setCimParamValueToOldForm(formId,eventdata);
			
			itsm.cim.cimCommon.setCimParamValueToOldFormByDataDictionaray(formId,eventdata,callback);
			
			itsm.cim.cimCommon.setAttributesValue(eventdata,formId,eventEavVals,eavNo,callback);
		},
		/**
		 * 赋值扩展属性
		 */
		setAttributesValue:function(eventdata,formId,eventEavVals,eavNo,callback){
			$.post('attrs!findAttributesByEavNo.action','attrsQueryDTO.eavNo='+eavNo,function(data){
				if(data!==null && data.length>0 && data!==''){
					for(var i=0;i<data.length;i++){
						var value='';
						var attrName=data[i].attrName;
						if(eventEavVals && eval('eventEavVals.'+attrName)!==undefined && eval('eventEavVals.'+attrName)!==''){
							value = eval('eventEavVals.'+attrName);
						}
						if(data[i].attrType=="Radio"){
							$(formId+" input[name*='"+attrName+"'][value='"+value+"']").attr("checked","checked");
						}else if(data[i].attrType=="Checkbox"){
							var values=value.split(',');
							for ( var j = 0; j < values.length; j++) {
								$(formId+" input[name*='"+attrName+"'][value='"+values[j]+"']").attr("checked","checked");
							}
						}else if(data[i].attrType=="DataDictionaray"){
							var selectId = $(formId+" select[name*='"+attrName+"']").attr("id");
							common.config.dictionary.dataDictionaryUtil.loadOptionsByCode(data[i].attrdataDictionary,formId+' #'+selectId,function(){
								 $(formId+" select[name*='"+attrName+"']").val(value);
							});
//							var selectVal = value;
//							alert(value);
//							common.config.dictionary.dataDictionaryUtil.setSelectOptionsByCode(data[i].attrdataDictionary,formId+' #'+selectId,selectVal);
						}else{
							//alert(value);
							$(formId+" :input[name*='"+attrName+"']").val(value);
						}
					}
				}
				
			});
		},
		/**
		 * 给配置项编辑form赋值
		 * @data 数据对象
		 * @formId 表单id
		 */
		setCimFormValues:function(eventdata,formId,eventType,eavId,eventEavVals,callback){
			itsm.cim.cimCommon.setCimParamValueToOldForm(formId,eventdata);
			itsm.cim.cimCommon.setCimParamValueToOldFormByDataDictionaray(formId,eventdata);
				$.post('ciCategory!findByCategoryId.action','categoryNo='+eventdata.categoryNo,function(data){
					if(data!="null" && data!==null && data!=0){
						$.post('attrs!findAttributesByEavNo.action','attrsQueryDTO.eavNo='+data,function(data){
							if(data!==null && data.length>0 && data!==''){
								for(var i=0;i<data.length;i++){
									var value='';
									var attrName=data[i].attrName;
									if(eventEavVals && eval('eventEavVals.'+attrName)!==undefined && eval('eventEavVals.'+attrName)!==''){
										value = eval('eventEavVals.'+attrName);
									}
									if(data[i].attrType=="Radio"){
										$(formId+" input[name*='"+attrName+"'][value='"+value+"']").attr("checked","checked");
									}else if(data[i].attrType=="Checkbox"){
										var values=value.split(',');
										for ( var j = 0; j < values.length; j++) {
											$(formId+" input[name*='"+attrName+"'][value='"+values[j]+"']").attr("checked","checked");
										}
									}else{
										value = common.security.xssUtil.html_code(value);
										$(formId+" :input[name*='"+attrName+"']").val(value);
									}
									if(i==(data.length-1) && callback!=null && callback!="" ){
										callback();
									}
								}
							}else{
								callback();
							}
						});
					}else{
						callback();
					}
				
				});
				
		},
		/**
		 * 给详情form赋值
		 * @data 数据对象
		 * @formId 表单id
		 */
		setCimFormValuesByDetail:function(eventdata,formId,eventType,eavId,eventEavVals,callback){
			itsm.cim.cimCommon.setCimParamValueToOldFormByDetail(formId,eventdata);
				if(eavId!="null" && eavId!==null && eavId!=0){
					$.post('attrs!findAttributesByEavNo.action','attrsQueryDTO.eavNo='+eavId,function(data){
						if(data!==null && data.length>0 && data!==''){
							for(var i=0;i<data.length;i++){
								var value='';
								var attrName=data[i].attrName;
								if(eventEavVals && eval('eventEavVals.'+attrName)!==undefined && eval('eventEavVals.'+attrName)!==''){
									value = eval('eventEavVals.'+attrName);
								}
								$(formId+" :input[name*='"+attrName+"']").closest('.field,.field_lob').html(value);
							}
						}
						callback();
						itsm.cim.cimCommon.setCimDataDictionarayValueByDetail(formId,eventdata);
					});
				}else{
					
					itsm.cim.cimCommon.setCimDataDictionarayValueByDetail(formId,eventdata);
					callback();
				}
					
		},
		/**
		 * @description 根据扩展属性id查询数据字典名称
		 * @param attrValNo
		 * @param showAttrId 显示控件id
		 */
		showDataDicInfo:function(attrValNo,formId,attrName){
			$.post('dataDictionaryItems!findByDcode.action?groupNo='+attrValNo,function(res){
				$(formId+" :input[name*='"+attrName+"']").closest('.field,.field_lob').html(res.dname);
			});
		},
		setfieldOptionsLobCss:function(htmlDivId){
			var len=$(htmlDivId+" .field_options").not('.field_options_2Column,.field_options_lob').length;
			if(len%2!=0){
				$(htmlDivId).append('<div class="field_options"><div class="label"></div><div class="field"></div></div>');
			}
		},
		showFormBorder:function(isShowBorder,htmlDivId,isCssId,noCssId){
			if(isShowBorder==="1"){
				$(isCssId).attr("rel","stylesheet");
				$(noCssId).attr("rel","");
			}else{
				$(isCssId).attr("rel","");
				$(noCssId).attr("rel","stylesheet");
				$(htmlDivId+" :input").not(".control").css("margin-top","10px");
			}
		},
		removeRequiredField:function(page){
			if(page==="setting"){
				//$("#request_userName").attr("class","control-img").removeAttr("required");
				//$("#request_userName").parent().prev().children("div[class=required_div]").remove();
				$("#ci_add_companyName").attr("class","control-img").removeAttr("required");
				$("#ci_add_companyName").parent().prev().children("div[class=required_div]").remove();
			}
		}
	}
}();