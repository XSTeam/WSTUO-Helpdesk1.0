<%@page import="java.util.Map"%>
<%@page import="com.wstuo.common.security.service.IUserInfoService"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/pages/language.jsp" %>

<script type="text/javascript">
var serviceDirId = '${param.serviceDirId}';
var related_request_and_knowledge='<fmt:message key="related_request_and_knowledge" />';
var title_add_request_attr='<fmt:message key="common.attachment" />';
var request_add_eavAttributet='<fmt:message key="config.extendedInfo"/>';
var phoneNum = '${param.phoneNum}';        //更新添加这一句
var pageType="${param.pageType}";
var editTemplateId="${param.templateId}";
var voiceCarduserId='${param.voiceCarduserId}';
var addRequest_processDefinitionId = '${param.processDefinitionId}';
var relatedRequestAndKnowledge=false;
var fixRequestAndKnowledge="0";
var belongsClient="0";
var requestUser="0";
var defaultFormId=0;
</script>
<sec:authorize url="request_manage_interfixRequestAndKnowledge">
<script>
relatedRequestAndKnowledge = true;
</script>
</sec:authorize>
<%
if(session.getAttribute("loginUserName")==null){
	out.print("<script>window.location='login.jsp';</script>");
}else{

    WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext());
	IUserInfoService userInfoService = (IUserInfoService)ctx.getBean("userInfoService");
	if(session.getAttribute("loginUserName")!=null){
		Map<String,Object> resAuth=userInfoService.findResAuthByUserName(session.getAttribute("loginUserName").toString(),new String[]{
		  "/pages/user!find.action"
		  });
		request.setAttribute("resAuth",resAuth);
	}
}
%>
<sec:authorize url="request_manage_interfixRequestAndKnowledge">
	<script>fixRequestAndKnowledge="1";</script>
</sec:authorize>
<sec:authorize url="/pages/itsopUser!findITSOPUserPager.action">
	<script>belongsClient="1";</script>
</sec:authorize>
<sec:authorize url="/pages/user!find.action">
	<script>requestUser="1";</script>
</sec:authorize>
<script src="../js/itsm/request/addRequest.js"></script>
<form id="addRequestForm" event="itsm.request.addRequest.saveRequest">
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-plus"></i> 添加请求</h2>
            </div>
            <div class="box-content buttons" id="">
            	<div class="row">
   				 	<div class="box col-md-12">
						<button class="btn btn-default btn-xs" ><i class="glyphicon glyphicon-floppy-disk"></i> <fmt:message key="common.submit" /></button>
						<a id="add_request_backList" class="btn btn-default btn-xs" onclick="basics.index.back()"><i class=" glyphicon glyphicon-share-alt"/> <fmt:message key="common.returnToList" /></a>
						<%-- <span>
							<fmt:message key="label.template.request" />&nbsp;&nbsp;
							<select id="requestTemplate" onchange="javascript:itsm.request.addRequest.getTemplateValue(this.value)"></select> 
						</span>
						
						<sec:authorize url="TEMPLATE_MAIN">
						<a id="saveRequestTemplateBtn" class="btn btn-default btn-xs" ><i class="glyphicon glyphicon-floppy-disk"></i> <fmt:message key="label.template.saveTemplate" /></a>
						<a id="editRequestTemplateBtn" class="btn btn-default btn-xs" style="display: none;"><i class="glyphicon glyphicon-edit"></i> <fmt:message key="label.template.editTemplate" /></a>
						</sec:authorize>
						 <a id="addRequest_ServicesCatalogNavigation" ><fmt:message key="request.servicesCatalogNavigation" /> </a> <span id="servicesCatalogNavigationName"></span>
						<span>
						<a id="cleanServicesCatalogNavigation" style="display: none" class="btn btn-default btn-xs" title="<fmt:message key="label.request.clear" /><fmt:message key="request.servicesCatalogNavigation" />"></a>
						</span> --%>
					</div>
				</div>
			<div class="row">
   				<div class="box col-md-12">
			            <div class="box-content" id="basicInfo_field">
			                <input type="hidden" id="addRequest_serviceDirName" />
							<input type="hidden" name="requestDTO.technicianName" value="${loginUserName}"  />
							<input type="hidden" name="requestDTO.creator" value="${loginUserName}"  />
							<input type="hidden" name="requestDTO.actionName" value="<fmt:message key="common.add" />"  />
							<input type="hidden" name="requestDTO.piv.approvalNo" value="0" />
							<input type="hidden" name="requestDTO.serviceDirIds" id="addRequest_serviceDirIds" value="">
							<input id="addRequest_formId" type="hidden" name ="requestDTO.formId" value="" />
							<input type="hidden" name="templateDTO.templateType" value="request"  />
							<div class="form-group col-md-6 ui-sortable-handle">
								<label class="form-control-label"><fmt:message key="label.belongs.client"/><span class="required">*</span></label>
								<input type="hidden" value="${sessionScope.companyNo}" id="request_companyNo" name="requestDTO.companyNo" required='true'>
								<input name="requestDTO.companyName" id="request_companyName" value="${sessionScope.companyName}" class="form-control" type="text" required='true'>
								<sec:authorize url="/pages/itsopUser!findITSOPUserPager.action"><a title='<fmt:message key="common.select"/>' onclick="itsm.request.requestCommon.chooseCompany('#request_companyNo','#request_companyName',this)" ><span class="glyphicon glyphicon-user form-control-feedback choose" ></span></a></sec:authorize>
							</div>
							<div class="form-group col-md-6 ui-sortable-handle">
								<label class="form-control-label"><fmt:message key="label.request.requestUser"/><span class="required">*</span></label>
								<input type="hidden" value="${sessionScope.userId}" id="request_userId" name="requestDTO.createdByNo"  required='true'>
								<input name="requestDTO.createdByName" id="request_userName" value="${loginUserName}" class="form-control" type="text" required='true'>
								<sec:authorize url="/pages/user!find.action"><a title='<fmt:message key="common.select"/>' onclick="itsm.request.requestCommon.selectRequestCreator('#request_userName','#request_userId','#request_companyNo',this)" ><span class="glyphicon glyphicon-user form-control-feedback choose" ></span></a></sec:authorize>
							</div>
							<div class="form-group col-md-6 ui-sortable-handle">
								<label class="form-control-label"><fmt:message key="setting.requestCategory"/><span class="required">*</span></label>
								<input type="hidden" value="" id="request_categoryNo" name="requestDTO.requestCategoryNo" required='true'>
								<input name="requestDTO.requestCategoryName"  required='true' onclick="itsm.request.requestCommon.selectRequestCategory('#request_categoryName','#request_categoryNo','#request_etitle','#request_edesc',this)" id="request_categoryName" value="" class="form-control" type="text">
							</div>
							<div class="form-group col-md-6 ui-sortable-handle">
								<label class="form-control-label"><fmt:message key="common.title"/><span class="required">*</span></label>
								<input name="requestDTO.etitle" id="request_etitle" value=""  required='true' class="form-control" type="text">
							</div>
							<div class="form-group col-md-12 ui-sortable-handle">
								<label class="form-control-label"><fmt:message key="label.common.desc"/><span class="required">*</span></label>
								<textarea class="form-control" required="true" name="requestDTO.edesc" id="request_edesc" title='<fmt:message key="label.common.desc" />' style="height: 53px;"></textarea>
							</div>
							<div class="box-content" id="addRequest_formField">

			            	</div>
			            </div>
			        </div>
			</div>
			<%-- <div class="row" id="addRequest_formField_row" style="display: none">
   				<div class="box col-md-12">
   					<div class="box-inner">
			            <div class="box-header well" data-original-title="">
			                <h2><i class="glyphicon glyphicon-list-alt"></i> <fmt:message key="config.extendedInfo" /></h2>
			                <div class="box-icon">
			                    <button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
			                   		<i class="glyphicon glyphicon-chevron-left"></i> 选择表单
			                   </button>
			                    <ul class="dropdown-menu" id="customform" style="position: relative;">
				                </ul>
			                </div>
			            </div>
			            
			        </div>
				</div>
			</div> --%>
			</div>
		</div>	
	</div>
</div>
<div class="row">
   				<div class="box col-md-12">
   				    <div class="box-inner" >	
						<div class="box-content" >
   				<%-- 关联配置项 
					<c:if test="${cimHave eq true}">
					<div title="<fmt:message key="ci.relatedCI" />" style="padding: 2px;">
						<div  id="requestRelatedCIShow"  class="hisdiv" >
							<form>
							<table class="histable" width="100%" height="100%" cellspacing="1">
								<thead>
								<tr>
									<td colspan="5" style="text-align: left;">
										<a id="addrequestRelatedCIBtn" class="btn btn-default btn-xs" plain="true" icon="icon-add" title="<fmt:message key="common.add" />"></a>
									</td>
								</tr>
								<tr height="20px">
									<th align="center"><fmt:message key="lable.ci.assetNo" /></th>
									<th align="center"><fmt:message key="label.name"/></th>
									<th align="center"><fmt:message key="common.category" /></th>
									<th align="center"><fmt:message key="common.state" /></th>
									<th align="center"><fmt:message key="ci.operateItems" /></th>
								</tr>
								</thead>
								<tbody></tbody>
							</table>
							</form>
						</div>
					</div>
					</c:if> --%>
	   				 <ul class="nav nav-tabs" id="myTab">
	                   <%-- <sec:authorize url="request_manage_interfixRequestAndKnowledge"> <li class="active"><a href="#related_request_and_knowledge"><fmt:message key="related_request_and_knowledge" /></a></li></sec:authorize> --%>
	                    <li><a href="#attachment"><fmt:message key="common.attachment" /></a></li>
	                </ul>
	
	                <div id="myTabContent" class="tab-content">
						<%-- <sec:authorize url="request_manage_interfixRequestAndKnowledge">
						<!-- 相关联的请求和分类 -->
						<div class="tab-pane active" id="related_request_and_knowledge">
							<table id="findLikeKnowledgeGrid"></table>
							<div id="findLikeKnowledgePager"></div>
							<br>
							<table id="findLikeRequestGrid"></table>
							<div id="findLikeRequestPager"></div>
						</div>
						</sec:authorize> --%>
						<div class="tab-pane" id="attachment">
							<input type="hidden" name="requestDTO.attachmentStr" id="add_request_attachmentStr"/>
			                <div class="form-group" id="add_request_attachment_div" >
			                    <input id="add_request_file" type="file" name="filedata" multiple class="file" data-overwrite-initial="false">
			                </div>
							<div class="form-group">
                                 <a href="javascript:wstuo.tools.chooseAttachment.showAttachmentGrid('add_request_success_attachment','requestDTO.aids')" class="btn btn-default btn-xs" icon="icon-selectOldFile" id="chooseAtt">
                                     <fmt:message key="common.attachmentchoose" />
                                 </a>
                             </div>
                           <!-- <div id="add_request_success_attachment" style="line-height:35px;color:#555"></div> -->
								<%-- <div class="lineTableBgDiv" id="add_request_attachment_div" >
									
									<table width="100%" cellspacing="1" class="lineTable">
		 								<tr>
											<td> 
											 	<div style="color:#ff0000;line-height:28px">
		                                            <fmt:message key="msg.attachment.maxsize" />
		                                        </div>
		                                        <div class="diyLinkbutton">
		                                            <div style="float: left;cursor: pointer;">
		    											<input type="file"  name="filedata" id="add_request_file">
		                                            </div>
		                                            
		                                        </div>
		                                        <div style="clear: both;"></div>
		                                        <div style="height:5px;margin-top:10px;margin-bottom:10px;height:auto" id="addRequestQueId"></div>
											</td>
										</tr> 
										<tr>
											<td>
												<div style="padding-top:3px">
									              
									            </div>
											</td>
										</tr>
									</table>
								</div> --%>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<div id="requestTemplateName" class="WSTUO-dialog" title="<fmt:message key="label.template.name"/>" style="width:auto;height:auto">
	<form id="requestTemplateForm">
	<div style="margin: 10px;"><fmt:message key="label.template.requestName" />&nbsp;&nbsp;<input id="requestTemplateNameInput" name="templateDTO.templateName" style="width: 150px;" class="easyui-validatebox input" required="true"/>
	<input type="hidden" id="requestTemplateId" name="templateDTO.templateId"/>
	
	<a class="btn btn-default btn-xs" id="addRequestTemplateOk" style="margin-top: 5px;" icon="icon-save" plain="true"><fmt:message key="common.save"/></a>
	
	</div>
	</form>
</div>
