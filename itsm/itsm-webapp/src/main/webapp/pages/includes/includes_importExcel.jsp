<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="../language.jsp" %>       

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>导入includes文件</title>
</head>
<body>

<!-- 导入数据 -->
<div id="index_import_excel_window" class="WSTUO-dialog" title="<fmt:message key="label.dc.import"/>" style="width:400px;height:auto">
	<form class="form-inline" >
	<div class="form-group has-success has-feedback">
          <label for="importFile"><fmt:message key="label.dc.filePath"/></label>
          <input type="file" id="importFile" name="importFile">
          <p class="help-block"><a id="index_import_href" href="" target="_blank">[<fmt:message key="label.csv.template.download"></fmt:message>]</a></p>
      </div>
      <button type="button" id="index_import_confirm" class="btn btn-primary btn-sm"><fmt:message key="label.dc.import"/></button>
    </form>
</div>
</body>
</html>