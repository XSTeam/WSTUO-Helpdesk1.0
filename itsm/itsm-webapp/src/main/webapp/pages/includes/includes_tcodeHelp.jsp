<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="../language.jsp" %>    

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>TCODE帮助 includes文件</title>
</head>
<body>
<!-- TCODE帮助 -->
<div id="index_tcode_help_window" class="WSTUO-dialog" title='<fmt:message key="common.Help" />' style="width:280px;height:500px;">
	<div class="hisdiv" >
		<table style="width:100%" class="histable" cellspacing="1" id="index_tcode_help_table">
			<thead>
			<tr>
			<th>T-Code</th>
			<th><fmt:message key="label.role.roleDescription" /></th>
			</tr>
			</thead>
			<tbody style="text-align:center"></tbody>
		</table>
	</div>
</div>
</body>
</html>