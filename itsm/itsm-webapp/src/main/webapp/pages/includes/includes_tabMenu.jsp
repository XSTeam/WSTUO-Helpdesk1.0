<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div style="display:none">
<div id="mm" class="easyui-menu" style="width:150px;">
	   <div id="mm-refresh"><fmt:message key="common.refresh" /></div>
       <div id="mm-tabclose"><fmt:message key="label.request.close" /></div>
       <div id="mm-tabcloseall"><fmt:message key="lable.closeAll" /></div>
       <div id="mm-tabcloseother"><fmt:message key="lable.onlyCloseAll" /></div>
       <div class="menu-sep"></div>
       <div id="mm-tabcloseright"><fmt:message key="lable.currentWebRightAllClose" /></div>
       <div id="mm-tabcloseleft"><fmt:message key="lable.currentWebLeftAllClose" /></div>
</div>
</div>