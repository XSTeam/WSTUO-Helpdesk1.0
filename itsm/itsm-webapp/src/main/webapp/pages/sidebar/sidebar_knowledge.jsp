<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../language.jsp" %>
<li class="nav-header"> 知识导航</li>
<sec:authorize url="pages/knowledgeInfo!sharedKnowledge.action">
<li><a class="link" href="javascript:wstuo.knowledge.knowledgeGrid.showKw('share')"><span>共享知识</span><span style="color: #F00" id="share_kw"></span></a></li>
</sec:authorize>
<sec:authorize url="KNOWLEDGEINFO_MYKNOWLEDGE">
<li><a class="link" href="javascript:wstuo.knowledge.knowledgeGrid.showKw('my')"><span>我发布的知识</span><span style="color: #F00" id="share_my"></span></a></li>
</sec:authorize>
<!-- 我的待审的知识 -->
<sec:authorize url="KNOWLEDGEINFO_MYSTAYCHECKKNOWLEDGE">
<li><a class="link" href="javascript:wstuo.knowledge.knowledgeGrid.showKw('myap')"><span>我的待审知识</span><span style="color: #F00" id="share_myapp"></span></a></li>
</sec:authorize>
<sec:authorize url="knowledge_menu_allStayCheckKnowledge">
<li><a class="link" href="javascript:wstuo.knowledge.knowledgeGrid.showKw('allap')"><span>所有待审知识</span><span style="color: #F00" id="share_allapp"></span></a></li>
</sec:authorize>
<!-- 我审核未通过的知识 -->
<sec:authorize url="KNOWLEDGEINFO_MYNONCHECKKNOWLEDGE">
<li><a class="link" href="javascript:wstuo.knowledge.knowledgeGrid.showKw('myapf')" ><span>我未审核通过的知识</span><span style="color: #F00" id="share_myappf"></span></a></li>
</sec:authorize>
<sec:authorize url="pages/knowledgeInfo!alluntreated.action">
<li><a class="link" href="javascript:wstuo.knowledge.knowledgeGrid.showKw('allapf')"><span>所有未审核通过的知识</span><span style="color: #F00" id="share_allappf"></span></a></li>
</sec:authorize>

