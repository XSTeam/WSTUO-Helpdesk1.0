<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../language.jsp" %>
<%@ page import="java.io.File,java.util.List,java.util.ArrayList,java.util.Date,com.wstuo.common.security.utils.AppConfigUtils"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="com.wstuo.common.tools.service.IPagesSetSerice"%>
<%@page import="com.wstuo.common.tools.entity.PagesSet"%>
<%
String images="logo.jpg";

ApplicationContext ac = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
IPagesSetSerice systemPage = (IPagesSetSerice)ac.getBean("pagesSetSerice");
PagesSet pagesSet=systemPage.showPagesSet();
request.setAttribute("pagesSetCom",pagesSet);

%>
<c:set var="images" value="<%=images%>"/>

<script>var merge_security='no';</script>


<sec:authorize url="/pages/organization!mergeCompany.action">

<script>merge_security='yes';</script>

</sec:authorize>

<script>
$(document).ready(function(){
	 $("#company_loading").hide();
	 $("#company_content").show();
	 
	 if(merge_security=="no"){
	 	$("#companyInfoDiv .easyui-validatebox,#init_address").attr('disabled','disabled'); 
	 }
}); 
</script>
<script src="${pageContext.request.contextPath}/js/wstuo/sysMge/company.js?random=<%=new java.util.Date().getTime()%>"></script>

<div class="content" id="companyInfoDiv" >
	<div align="center" >
		<div class="box-header well" data-original-title="">
	          <h2><i class="glyphicon glyphicon-list"></i>&nbsp;软件设置</h2>
	          <div class="box-icon"></div>
	    </div>
		<form id="companyInfoForm" >
			<table style="width:100%;" class="table table-bordered">
				<tr style="height: 50px;" >
               		<td  style="width:20%;" >
               			<h5 style="padding-left: 20px;color:black; "><fmt:message key="label.basicSettings.companyName"/>：</h5>
               		</td>              			
                	<td>
                		<input id="init_orgName" name="organizationDto.orgName" style="width:80%" required="true" class="form-control" maxlength="200" minlength="1"/>
                	</td>
                </tr>
                <c:if test="${sessionScope.topCompanyNo==sessionScope.companyNo }">
				<tr style="height: 50px;" >
					<td  style="width:20%;" >
						<h5 style="padding-left: 20px;color:black; "><fmt:message key="label.logo.sysname"/>：</h5>					
					</td>
					<td >
						<input id="pagesSysName" style="width:80%" value="${pagesSetCom.paName}" class="form-control" maxlength="200" minlength="1"/>
					</td>
				</tr></c:if>
				<tr style="height: 50px;" id="logo" class="formTableTd">
               		<td style="width:20%;" class="formTableTd">
               			<h5 style="padding-left: 20px;color:black; "><fmt:message key="label.basicSettings.companyLOGO"/>：</h5>	
               		</td>
               		<td ><input type="hidden" id="init_logo" name="organizationDto.logo" />
               			<div id="showCompanyCompanyLogo">
               
              			</div>
						<sec:authorize url="COMPANY"><input id="uploadCompanyLogo" type="file" size="16" name="myFile" onchange="return commonUploadFile('uploadCompanyLogo','init_logo','showCompanyCompanyLogo')"/>
						(36px*36px)</sec:authorize>
               		</td>
               	</tr>
             	<tr>
             	<c:if test="${sessionScope.topCompanyNo==sessionScope.companyNo }">
              	<tr style="height: 50px;" >
					<td style="width:20%;" class="formTableTd">
						<h5 style="padding-left: 20px;color:black; "><fmt:message key="label.syslogo"/>：</h5>	
               		</td>
					<td >
					<div id="showLoginLogo">
					
						<c:if test="${!empty pagesSetCom.imgname}">
			          		<img style="height:30px;max-width: 135px;" src="logo.jsp?picName=${pagesSetCom.imgname}" />
			          	</c:if>
			          	<c:if test="${empty pagesSetCom.imgname}">
			          		<img style="height:30px;max-width: 135px;" src="${pageContext.request.contextPath}/images/logo.png" />	
			          	</c:if>							          		          	
					</div>
					<input type="hidden" id="Login_logo"/>
					<sec:authorize url="COMPANY"><input id="uploadLoginLogo" type="file" size="16" name="myFile" onchange="return commonUploadFile('uploadLoginLogo','Login_logo','showLoginLogo')"/>
					(40px*135px)</sec:authorize>
					</td>
				</tr></c:if>
				<sec:authorize url="COMPANY">
      		 	<tr style="height: 50px;">
			        <td colspan="2" style="height:35px;text-align: center;" >
				     	<input type="hidden" id="init_OrgNo" name="organizationDto.orgNo" />	
				     	
					    <input type="button" id="saveReportInfo" class="btn btn-primary btn-sm" onclick="saveCompanyInfo()" value="<fmt:message key="common.save" />"/>	
				    </td>
	        	</tr>	  
	        	</sec:authorize>
	    	</table>
		</form>
	</div>		
</div>
