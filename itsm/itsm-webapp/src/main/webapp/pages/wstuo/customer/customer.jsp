<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<%-- <script>var technicianNumber="<%=com.wstuo.common.config.register.service.RegisterService.licenseNumber%>";</script> --%>
<script type="text/javascript">
<!--
var _itsop_user_companyNo=0;
var _itsop_user_edit=0;
var _itsop_user_delete=0;
<sec:authorize url="/pages/itsopUser!updateITSOPUser.action">
_itsop_user_edit=1;
</sec:authorize>

<sec:authorize url="/pages/itsopUser!deleteITSOPUser.action">
_itsop_user_delete=1;
</sec:authorize>
//-->
</script>
<script src="../js/wstuo/customer/ITSOPUserMain.js?random=<%=new java.util.Date().getTime()%>"></script>

	<!-- 客户列表 start-->
	<div class="row">
		<div class="box col-md-12">
			<div class="box-inner">
				<div class="box-header well" data-original-title="">
					<h2>
						<i class="glyphicon glyphicon-list-alt"></i> 客户列表
					</h2>
					<div class="box-icon">
					</div>
				</div>

				<div class="box-content ">
				    <div class="content" id="ITSOPUserMain_panel">
					    <table id="ITSOPUserGrid"></table>
                        <div id="ITSOPUserGridPager"></div>
						<div id="ITSOPUserGridToolbar" style="display: none">
						<div class="panelBar">
						<sec:authorize url="/pages/itsopUser!addITSOPUser.action">
						<button class="btn btn-default btn-xs" id="ITSOPUserGrid_add" >
						  <i class="glyphicon glyphicon-plus"></i>&nbsp;<fmt:message key="common.add"/>
						</button>
						</sec:authorize>
						
						<sec:authorize url="/pages/itsopUser!updateITSOPUser.action">
						<button class="btn btn-default btn-xs" id="ITSOPUserGrid_edit" >
						 <i class="glyphicon glyphicon-edit"></i>&nbsp;<fmt:message key="common.edit"/>
						</button>
						</sec:authorize>
						
						<sec:authorize url="/pages/itsopUser!deleteITSOPUser.action">
						<button class="btn btn-default btn-xs"  id="ITSOPUserGrid_delete" >
						   <i class="glyphicon glyphicon-trash"></i>&nbsp;<fmt:message key="common.delete"/>
						</button> 
						</sec:authorize>
						
						<button class="btn btn-default btn-xs" id="ITSOPUserGrid_search" >
						  <i class="glyphicon glyphicon-search"></i>&nbsp;<fmt:message key="common.search"/>
						</button>
						
						<sec:authorize url="/pages/user!importITSOPUser.action"> 
						  <button class="btn btn-default btn-xs" id="importITSOPUser" >
						    <i class="glyphicon glyphicon-import"></i>&nbsp;<fmt:message key="label.dc.import"/>
						  </button>
						</sec:authorize>
						
						<sec:authorize url="/pages/user!exportITSOPUser.action"> 
						  <button class="btn btn-default btn-xs" id="exportITSOPUser"  >
						   <i class="glyphicon glyphicon-share"></i>&nbsp;<fmt:message key="label.dc.export"/>
						  </button>
						</sec:authorize>
						
						</div>
					
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- 客户列表 end-->
<!-- 客户用户列表 start-->
<div class="row" id="ITSOPCustomer" style="display: none;">
	<div class="box col-md-12">
		<div class="box-inner">
			<div class="box-header well" data-original-title="">
				<h2>
					<i class="glyphicon glyphicon-list-alt"></i> 客户用户列表
				</h2>
				<div class="box-icon">
					<a href="#" class="btn btn-minimize btn-round btn-default"><i
						class="glyphicon glyphicon-chevron-up"></i></a>
				</div>
			</div>

			<div class="box-content ">
				  <table id="ITSOPCustomer_userGrid"></table>
	              <div id="ITSOPCustomer_userGridPager"></div>
			      <div id="ITSOPCustomer_userGridToolbar" style="background-color: #EEEEEE; height: 28px;display: none;">
						<sec:authorize url="/pages/user!save.action">
							<button class="btn btn-default btn-xs" id="ITSOPCustomer_userGrid_add" >
							  <i class="glyphicon glyphicon-plus"></i>&nbsp;<fmt:message key="common.add" />
							</button> 
						</sec:authorize>
						
						<sec:authorize url="/pages/user!merge.action">
							<button class="btn btn-default btn-xs" id="ITSOPCustomer_userGrid_edit" >
							   <i class="glyphicon glyphicon-edit"></i>&nbsp;<fmt:message key="common.edit" />
							</button> 
						</sec:authorize> 
						
						<sec:authorize url="/pages/user!delete.action">
						    <button class="btn btn-default btn-xs" id="ITSOPCustomer_userGrid_delete" >
						       <i class="glyphicon glyphicon-trash"></i>&nbsp;<fmt:message key="common.delete" />
						    </button> 
						</sec:authorize>
						
						<sec:authorize url="/pages/user!find.action">
						     <button class="btn btn-default btn-xs" id="ITSOPCustomer_userGrid_search">
						       <i class="glyphicon glyphicon-search"></i>&nbsp;<fmt:message key="common.search" />
						     </button> 
						</sec:authorize>
						
						<sec:authorize url="/pages/user!updateRoleByUserId.action"> 
							<button class="btn btn-default btn-xs" id="ITSOPCustomer_userGrid_role" >
							   <i class="glyphicon glyphicon-cog"></i>&nbsp;<fmt:message key="label.user.roleSettings" />
							</button> 
						</sec:authorize>
				 </div>
				 <div id="ITSOPCustomer_userGrid_action" style="display:none"><sec:authorize url="/pages/user!updateRoleByUserId.action"> <a href="javascript:common.security.user.opt_setRole_aff()" title="<fmt:message key="label.user.roleSettings" />"><i class="glyphicon glyphicon-edit"></i></a></sec:authorize>&nbsp;&nbsp;&nbsp;&nbsp;<sec:authorize url="/pages/user!merge.action"><a href="javascript:common.security.user.editUser_aff()" title="<fmt:message key="common.edit" />"><i class="glyphicon glyphicon-edit"></i></a></sec:authorize>&nbsp;&nbsp;&nbsp;<sec:authorize url="/pages/user!delete.action"><a href="javascript:common.security.user.opt_delete()" title="<fmt:message key="common.delete" />"><i class="glyphicon glyphicon-trash"></i></a></sec:authorize></div>
			</div>
		</div>
	</div>
</div>
<!-- 客户用户列表 end-->
<!-- 新增、编辑外包客户start -->
<div id="Window_Add_Edit_ITSOPUser" class="WSTUO-dialog" title='<fmt:message key="label.add.edit.customer" />' style="width:450px; height:auto">

       <div class="panel panel-default">
					<div class="panel-body">
						<form class="form-horizontal" event="wstuo.customer.ITSOPUserMain.save">
							<div class="form-group">
								<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.itsop.technicianInCharge" /></label>
								<div class="col-sm-5">
									 <input name="itsopUserDTO.technicianNamesStr" id="ITSOPUser_technicianNamesStr" class="form-control" required="true"  />
									 <a href="#"  id="add_edit_itsop_technician" title="<fmt:message key="common.select"/>"><span class="glyphicon glyphicon-user form-control-feedback choose" ></span></a>
				                     <%-- <a id="add_edit_itsop_technician" class="easyui-linkbutton" plain="true" icon="icon-user" title="<fmt:message key="common.select"/>"></a> --%>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.itsop.customerName" /></label>
								<div class="col-sm-5">
									<input name="itsopUserDTO.orgName" id="ITSOPUser_orgName" class="form-control" required="true" maxlength="200" minlength="1" />
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label" for="lastname"><fmt:message key="common.category" /></label>
								<div class="col-sm-5">
									<select name="itsopUserDTO.typeNo" id="ITSOPUser_typeNo" class="form-control" ></select>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.telephone" /></label>
								<div class="col-sm-5">
									<input name="itsopUserDTO.officePhone" id="ITSOPUser_officePhone" class="form-control" validType="phone" />
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.fax" /></label>
								<div class="col-sm-5">
									<input name="itsopUserDTO.officeFax" id="ITSOPUser_officeFax" class="form-control" validType="phone" />
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.itsop.corporate" /></label>
								<div class="col-sm-5">
									<input name="itsopUserDTO.corporate" id="ITSOPUser_corporate" class="form-control" />
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.itsop.companySize" /></label>
								<div class="col-sm-5">
									<select name="itsopUserDTO.companySize" id="ITSOPUser_companySize" class="form-control" >
										<option value="1"><fmt:message key="label.itsop.moreThan50" /></option>
										<option value="2"><fmt:message key="label.itsop.moreThan100" /></option>
										<option value="3"><fmt:message key="label.itsop.moreThan200" /></option>
										<option value="4"><fmt:message key="label.itsop.moreThan500" /></option>
										<option value="5"><fmt:message key="label.itsop.moreThan1000" /></option>
									</select>
								</div>
							</div>
								
							<div class="form-group">
								<label class="col-sm-4 control-label" for="regNumber"><fmt:message key="label.itsop.regNumber" /></label>
								<div class="col-sm-5">
									<input name="itsopUserDTO.regNumber" id="ITSOPUser_regNumber" class="form-control" />
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label" for="regCapital"><fmt:message key="label.itsop.regCapital" /></label>
								<div class="col-sm-5">
									<input name="itsopUserDTO.regCapital" id="ITSOPUser_regCapital" class="form-control" />
									<span class="glyphicon form-control-feedback choose"><fmt:message key="label.itsop.wan" /></span>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label" for="email"><fmt:message key="label.e-mail" /></label>
								<div class="col-sm-5">
									<input name="itsopUserDTO.email" id="ITSOPUser_email" class="form-control" validType="email" />
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label" for="address"><fmt:message key="label.address" /></label>
								<div class="col-sm-5">
									<input name="itsopUserDTO.address" id="ITSOPUser_address" class="form-control"  />
								</div>
							</div>
														
							<div class="form-group">
								<div class="col-sm-9 col-sm-offset-4">
								    <input name="itsopUserDTO.orgNo" id="ITSOPUser_orgNo" type="hidden" />
		                            <button    type="submit" class="btn btn-primary btn-sm"><fmt:message key="common.save"/></button>
								</div>
							</div>
						</form>
					</div>
			</div>  
 </div>
<!-- 新增、编辑外包客户end -->

<!-- 搜索外包客户start -->
<div id="Window_Search_ITSOPUserGrid" class="WSTUO-dialog" title='<fmt:message key="title.customer.searchCustomer" />' style="width: 400px; height:auto">
       <div class="panel panel-default">
					<div class="panel-body">
						<form class="form-horizontal">
							<div class="form-group">
								<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.itsop.customerName" /></label>
								<div class="col-sm-5">
									<input name="itsopUserQueryDTO.orgName" class="form-control"/>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-4 control-label" for="lastname"><fmt:message key="common.category" /></label>
								<div class="col-sm-5">
									<select name="itsopUserQueryDTO.typeNo" id="search_ITSOPUser_typeNo" class="form-control"></select>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.phone" /></label>
								<div class="col-sm-5">
								   <input name="itsopUserQueryDTO.officePhone" class="form-control"/>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.e-mail" /></label>
								<div class="col-sm-5">
								  <input name="itsopUserQueryDTO.email" class="form-control"/>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-9 col-sm-offset-4">
		                            <button id="SEARCH_ITSOPUSER_SUBMIT"  type="button" class="btn btn-primary btn-sm"><fmt:message key="common.search"/></button>
								</div>
							</div>
						</form>
					</div>
			</div>  
</div>
<!-- 搜索外包客户end -->

<!-- 添加/编辑用户start -->
<div id="add_edit_itsop_user_window" class="WSTUO-dialog" title="<fmt:message key="title.user.addOrEditUser" />" style="width:550px; height:auto;padding:3px">
 <form id="itsop_user_form" event="wstuo.customer.ITSOPCustomer_userGrid.saveUser">
	<div class="box-content">
		<ul class="nav nav-tabs" id="myTab">
			<li class="active"><a href="#basic"><fmt:message key="common.basicInfo" /></a></li>
			<li><a href="#contact"><fmt:message key="title.user.contactInfo" /></a></li>
			<li><a href="#userrole"><fmt:message key="title.user.userRole" /></a></li>
		</ul>
		<div id="myTabContent" class="tab-content" class="form-inline">
		            <!-- basic start -->
					<div class="tab-pane active" id="basic" style="padding-top: 15px;">
					  
						   <div class="panel panel-default">
							  <div class="panel-body form-horizontal">
									<div class="form-group">
										<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.user.loginName" /></label>
										<div class="col-sm-5">
										     <input name="userDto.loginName" id="itsop_user_loginName" class="form-control" required="true" />
										</div>
									</div>
									
									<div class="form-group" id="itsop_reset_user_password">
										<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.user.loginPassword" /></label>
										<div class="col-sm-5">
										     [<a href="#" id="itsop_reset_user_password_link" ><fmt:message key="common.reset" /></a>]
			                                 [<a href="#" id="itsop_edit_pwd" ><fmt:message key="label.edit.user.password" /></a>]
										</div>
									</div>
									
									<div class="form-group" id="itsop_user_password">
										<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.user.loginPassword" /></label>
										<div class="col-sm-5">
										     <input type="password" name="userDto.password" validType="useEnCharPwd" id="itsopUserPassword" class="form-control" required="true"/>
										</div>
									</div>
									
									<div class="form-group" id="itsop_user_confirm_password">
										<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.user.comfirmLoginPassword" /></label>
										<div class="col-sm-5">
										     <input type="password" id="itsopUserPasswordConfirm" class="form-control" equalTo="#itsopUserPassword" required="true"/>
										</div>
									</div>
									
									<div class="form-group" >
										<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.user.userLastName" /></label>
										<div class="col-sm-5">
										     <input name="userDto.firstName" id="itsop_user_firstName" class="form-control" required="true"/>
										</div>
									</div>
									
									<div class="form-group" >
										<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.user.userFirstName" /></label>
										<div class="col-sm-5">
										     <input name="userDto.lastName" id="itsop_user_lastName" class="form-control" required="true"/>
										</div>
									</div>
									
									<div class="form-group" >
										<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.user.userOwnerOrg" /></label>
										<div class="col-sm-5">
										     <input id="itsop_user_orgName" class="form-control" style="cursor:pointer" required="true" readonly/>
				                             <input type="hidden" name="userDto.orgNo" id="itsop_user_orgNo" />
										</div>
									</div>
									
									<div class="form-group" >
										<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.user.userTitle" /></label>
										<div class="col-sm-5">
										     <input name="userDto.job" id="itsop_user_job" class="form-control"/>
										</div>
									</div>
									
									<div class="form-group" >
										<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.user.position" /></label>
										<div class="col-sm-5">
										     <input name="userDto.position" id="itsop_user_position" class="form-control"/>
										</div>
									</div>
									
									<div class="form-group" >
										<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.user.userState" /></label>
										<div class="col-sm-5">
										        <input type="radio" name="userDto.userState" id="itsop_user_userState" value="true" checked />
												<fmt:message key="common.enable" /> 
												<input type="radio"name="userDto.userState" id="itsop_user_userState1" value="false" />
												<fmt:message key="common.disable" />
										</div>
									</div>
									
									<div class="form-group" >
										<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.user.sex" /></label>
										<div class="col-sm-5">
										       <input type="radio" name="userDto.sex" id="itsop_user_sex_man" value="true" checked="checked" /><fmt:message key="label.user.man" /> 
			                                   <input type="radio" name="userDto.sex" id="itsop_user_sex_woman" value="false" /><fmt:message key="label.user.woman" />
										</div>
									</div>
									<input type="hidden" name="userDto.userId" value="0" id="itsop_user_userId" />
		                           <input type="hidden" name="userDto.companyNo" id="itsop_user_companyNo" />
							   </div>
							</div>
					</div>
					<!-- basic end -->
					<!-- contact start -->
					<div class="tab-pane " id="contact" style="padding-top: 15px;">
					   <div class="panel panel-default">
						 <div class="panel-body form-horizontal">
							<div class="form-group">
								<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.user.icCard"/></label>
								<div class="col-sm-5">
								    <input name="userDto.icCard" id="itsop_user_icCard" class="form-control" />
								</div>
						      </div>
						      
						      <div class="form-group">
								<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.user.pinyin"/></label>
								<div class="col-sm-5">
								    <input name="userDto.pinyin" id="itsop_user_pinyin" class="form-control" />
								</div>
						      </div>
						      
						      <div class="form-group">
								<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.user.birthday"/></label>
								<div class="col-sm-5">
								    <input name="userDto.birthday" id="itsop_user_birthday" class="choose form-control" readonly />
								</div>
						      </div>
						      
						       <div class="form-group">
								<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.user.email"  /></label>
								<div class="col-sm-5">
								    <input name="userDto.email" id="itsop_user_email" class="form-control" validType="email"/>
								</div>
						      </div>
						      
						       <div class="form-group">
								<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.user.mobile"/></label>
								<div class="col-sm-5">
								    <input name="userDto.moblie" id="itsop_user_moblie" class="form-control" validType="mobile"/>
								</div>
						      </div>
						      
						      <div class="form-group">
								<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.user.phone" /></label>
								<div class="col-sm-5">
								   <input name="userDto.phone" id="itsop_user_phone" class="form-control" validType="phone"/>
								</div>
						      </div>
						      
						      <div class="form-group">
								<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.user.fax"/></label>
								<div class="col-sm-5">
								   <input name="userDto.fax" id="itsop_user_fax" class="form-control"/>
								</div>
						      </div>
						      
						      <div class="form-group">
								<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.user.qqOrMsn" /></label>
								<div class="col-sm-5">
								   <input name="userDto.msn" id="itsop_user_msn" class="form-control"/>
								</div>
						      </div>
						      
						      <div class="form-group">
								<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.user.workAddress" /></label>
								<div class="col-sm-5">
								   <input name="userDto.officeAddress" id="itsop_user_officeAddress" class="form-control"/>
								</div>
						      </div>
						      
						      <div class="form-group">
								<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.user.description" /></label>
								<div class="col-sm-5">
								   <textarea name="userDto.description" id="itsop_user_description"  style="height:50px" class="form-control"/></textarea>
								</div>
						      </div>
						  </div>
						</div>
					</div>
					<!-- contact end -->
					<!-- userrole start -->
					<div class="tab-pane" id="userrole" style="padding-top: 15px;">
					  <div id="itsop_user_roles">
						   <table style="width:100%" class="lineTable" cellspacing="1"></table>
					 </div>
					</div>
		            <!-- userrole end -->
		</div>
	</div>

	<div class="saveuser_button" style="padding-right: 10px;padding-bottom: 10px;">
		<button type="submit" class="btn btn-primary btn-sm"> <!-- id="itsop_user_save_btn"  -->
		  <fmt:message key="common.save" />
		</button>
	</div>
</form>
</div>
<!-- 添加/编辑用户end -->

<!-- 搜索客户用户 start  -->
<div id="search_itsop_user_window" class="WSTUO-dialog" title="<fmt:message key="title.user.searchUser" />" style="width:400px;height:auto">
	<form >
	<div class="panel panel-default">
		<div class="panel-body form-horizontal">
		  <div class="form-group">
			<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.orgSettings.ownerOrg" /></label>
			<div class="col-sm-5">
				<input id="itsop_user_search_orgName" class="form-control" readonly onclick="wstuo.orgMge.organizationTreeUtil.showAll_2('#index_selectORG_window','#index_selectORG_tree','#itsop_user_search_orgName','#itsop_user_search_orgNo',_itsop_user_companyNo);"/>
				<input type="hidden" name="userQueryDto.orgNo" id="itsop_user_search_orgNo" />
			</div>
		  </div>
		  
		  <div class="form-group">
			<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.user.loginName" /></label>
			<div class="col-sm-5">
				<input name="userQueryDto.loginName"  class="form-control"/>
			</div>
		  </div>
		  
		  <div class="form-group">
			<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.user.userLastName" /></label>
			<div class="col-sm-5">
				<input name="userQueryDto.firstName" class="form-control"  />
			</div>
		  </div>
		  
		  <div class="form-group">
			<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.user.userFirstName" /></label>
			<div class="col-sm-5">
				<input name="userQueryDto.lastName" class="form-control"  />
			</div>
		  </div>
		  
		   <div class="form-group">
			<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.user.email" /></label>
			<div class="col-sm-5">
				<input name="userQueryDto.email"  class="form-control" />
			</div>
		  </div>
		  
		  <div class="form-group">
				<div class="col-sm-9 col-sm-offset-4">
				    <input type="hidden" name="userQueryDto.limit" id="limit" value="45" />
		            <button id="search_itsop_user_search"  type="button" class="btn btn-primary btn-sm"><fmt:message key="common.save"/></button>
				</div>
		 </div>
		  
	    </div>
	</div>
  </form>
</div>
<!-- 搜索客户用户 end  -->

<!-- 设置用户角色 start -->
<div id="set_itsop_user_role_window" title="<fmt:message key="label.user.roleSettings" />" class="WSTUO-dialog" style="width:400px;height:auto">
<form>
	<div class="lineTableBgDiv">
		<table style="width:100%" class="lineTable" cellspacing="1"></table>
		<div class="saveuser_button">
				<button type="button" id="set_itsop_user_role_save" class="btn btn-primary btn-sm">
					<fmt:message key="common.save" />
				</button>
			</div>
		
	</div>
</form>
</div>
<!-- 设置用户角色 end -->