<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page import="com.wstuo.common.security.service.IUserInfoService"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="com.wstuo.common.security.dto.UserDTO"%>
<%@ include file="../../language.jsp" %>       
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>进展及成本includes文件</title>
</head>
<body>
<%
ApplicationContext ac = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
IUserInfoService userInfoService = (IUserInfoService)ac.getBean("userInfoService");
UserDTO user = userInfoService.getUserDetailByLoginName(session.getAttribute("loginUserName").toString());
if(user!=null&&user.getUserCost()!=null)
request.setAttribute("userCost", user.getUserCost());
%>
<script type="text/javascript">
//Clac Start To Time
function getTs(date){
	var d = date.split('-');
	return Date.parse((d[0]+'-'+d[1]+'-'+d[2]).replace(/-/g,"/"));
}
function getDate(ts){
	ts = new Date(ts);
	var year='',month='',day='';
	with(ts){
		year = getFullYear();
		month = getMonth()+1;
		day = getDate();
	}
	if(month<10){
		month = '0'+month
	}
	if(day<10){
		day = '0'+day
	}
	return year+'-'+month+'-'+day;
}
function getTwoDayDateLong(time){
	//如果大于下班结束时间则返回为0
	//如果小于上班开始时间，则返回为7*60分钟
	//如果在是上午上班时间段内，则返回（上午上班结束时间-开始时间）+（下午上班小时*60）
	//如果在是下午上班时间段内，则返回（下午上班结束时间-开始时间）
}

function getOneDayDateLong(startTime,endTime){
	//如果两时间在上午上班时间内
	//如果两时间在下午上班时间内
	//如果结束时间在上午上班时间前,返回0
	
}

function clacStartToEedTime(){
	var startTime=$('#index_timeCost_startTime').val();
	var endTime=$('#index_timeCost_endTime').val();
	if(startTime!='' && endTime!=''){
		var date1Long=Date.parse(startTime.replace(/-/g,"/"));
		var date2Long=Date.parse(endTime.replace(/-/g,"/"));
		if(date2Long>=date1Long){
			var m=parseInt((date2Long-date1Long)/1000/60);
			$('#timeCostHour').text(Math.floor(m/60));

			$('#timeCostMinute').text(m%60);

			$('#index_timeCost_startToEedTime').val(m);
		}else{
			$('#index_timeCost_startToEedTime').val(0);
		}
	}else{
		$('#timeCostHour').text(0);

		$('#timeCostMinute').text(0);
	}
	clacCost()
}
//Clac Cost
function clacCost(){
	var useTime=$('#index_timeCost_actualTime').val()
	var perHour=$('#index_timeCost_perHourFees').val();
	var otherFees=$('#index_timeCost_othersFees').val();
	var materialCost=$('#index_timeCost_materialCost').val();
	if(useTime=="" ){
		useTime=0;
	}
	if(otherFees==""){
		otherFees=0.00;
	}
	if(materialCost==""){
		materialCost=0.00;
	}
	var skillFees=eval(useTime)/60*eval(perHour);
	var totalFees=parseFloat(skillFees)+parseFloat(otherFees)+parseFloat(materialCost);
	$('#index_timeCost_skillFees').val(Math.floor(skillFees*100)/100);
	$('#index_timeCost_totalFees').val(Math.floor(totalFees*100)/100);
}
//Actual Time Change To Minute
function actualTimeChangeToMinute(){
	if( Math.floor($('#input_timeCostHour').val())<0){
		$('#input_timeCostHour').val("0");
	}
	if( Math.floor($('#input_timeCostMinute').val())<0){
		$('#input_timeCostMinute').val("0");
	}
	var m=$('#input_timeCostHour').val()*60+$('#input_timeCostMinute').val()*1;
	$('#index_timeCost_actualTime').val(m);
	clacCost();
}


</script>

	<div id="index_timeCost_window" class="WSTUO-dialog" title="<fmt:message key="title.request.addEditTimeDetail" />" style="width:500px;height:auto">
	<form id="index_timeCost_form">
		<input type="hidden" name="costDTO.progressId" id="index_timeCost_progressId" />
		<input type="hidden" name="costDTO.eno" id="index_timeCost_eno" />
		<input type="hidden" name="costDTO.eventType" id="index_timeCost_eventType"/>
		<input type="hidden" id="index_timeCost_responseTime"/>
		<div class="lineTableBgDiv">
      		<table style="width:100%" class="lineTable" cellspacing="1">
			<tr>
				<td><fmt:message key="label.request.theTechnician" />&nbsp;<span style="color:red">*</span></td>
				<td>
					<input id="index_timeCost_tcName" value="${sessionScope.fullName}" onclick="wstuo.user.userUtil.selectUser('#index_timeCost_tcName','#index_timeCost_tcId','#index_timeCost_perHourFees','fullName',topCompanyNo,clacCost)"  class="easyui-validatebox choose" required="true"  readonly="readonly"/>
					<input type="hidden" name="costDTO.userId" id="index_timeCost_tcId" value="${sessionScope.userId}"  /></td>
			</tr>

			<tr>
				<td>
				<fmt:message key="label.sla.slaStartTime" />&nbsp;<span style="color:red">*</span>
				</td>
				<td><input name="costDTO.startTime" id="index_timeCost_startTime" class="easyui-validatebox choose" onblur="clacStartToEedTime()" onfocus="clacStartToEedTime()"  required="true" readonly/></td>
			</tr>
			<tr>
				<td><fmt:message key="label.sla.slaEndTime" />&nbsp;<span style="color:red">*</span></td>
				<td><input name="costDTO.endTime" id="index_timeCost_endTime" class="easyui-validatebox choose" onblur="clacStartToEedTime()" onfocus="clacStartToEedTime()" validType="DateComparison['index_timeCost_startTime']" required="true" readonly/></td>
			</tr>
			
			<tr>
				<td><fmt:message key="label.startToEedTime" /></td>
				<td>
					<span id="timeCostHour">0</span><fmt:message key="label.sla.hour" />
					<span id="timeCostMinute">0</span><fmt:message key="label.sla.minute" />
				
					<input type="hidden" name="costDTO.startToEedTime" id="index_timeCost_startToEedTime" class="easyui-numberbox choose" value="0" readonly/></td>
			</tr>
			
			<tr>
				<td><fmt:message key="label_actualTime" /></td>
				<td>
					<input style="width: 40px" value="0" id="input_timeCostHour" class="easyui-numberbox" validType="twoTimeEq['index_timeCost_startToEedTime','input_timeCostHour','input_timeCostMinute']" onKeyUp="actualTimeChangeToMinute()" /><fmt:message key="label.sla.hour" />
					<input style="width: 40px" value="0" id="input_timeCostMinute" class="easyui-numberbox" validType="twoTimeEq['index_timeCost_startToEedTime','input_timeCostHour','input_timeCostMinute']"  onKeyUp="actualTimeChangeToMinute()"/><fmt:message key="label.sla.minute" />
					<input type="hidden" name="costDTO.actualTime" id="index_timeCost_actualTime" class="easyui-numberbox input" onKeyUp="clacCost()" />
				</td>
			</tr>

			<tr>
				<td><fmt:message key="titie.technicianPerHourCost" />(${currency_sign})</td>
				<td><input name="costDTO.perHourFees" id="index_timeCost_perHourFees" class="easyui-numberbox input" style="color:#999;cursor:pointer" value="${sessionScope.userCost}" onKeyUp="clacCost()"  precision="2" readonly/></td>
			</tr>
			
			<tr>
				<td><fmt:message key="titie.technicianCost" />(${currency_sign})</td>
				<td><input name="costDTO.skillFees" id="index_timeCost_skillFees" class="easyui-numberbox input" value="0" style="color:#999;" onKeyUp="clacCost()" precision="2" readonly/></td>
			</tr>
			<tr>
				<td><fmt:message key="title.material.cost"/>(${currency_sign})</td>
				<td><input name="costDTO.materialCost" id="index_timeCost_materialCost" class="easyui-numberbox input" value="0"  onKeyUp="clacCost()"  precision="2" /></td>
			</tr>
			<tr>
				<td><fmt:message key="title.otherCost" />(${currency_sign})</td>
				<td><input name="costDTO.othersFees" id="index_timeCost_othersFees" class="easyui-numberbox input" value="0" onKeyUp="clacCost()" validtype="length[1,18]" precision="2"/>
				
				</td>
			</tr>
			
			<tr>
				<td><fmt:message key="title.totalCost" />(${currency_sign})</td>
				<td><input name="costDTO.totalFees" id="index_timeCost_totalFees" class="easyui-numberbox input" value="0.00" validtype="length[1,20]" precision="2"  readonly/>
				</td>
			</tr>
			
			<tr>
				<td><fmt:message key="label.user.description" /></td>
				<td>
					<textarea class="textarea" name="costDTO.description" id="index_timeCost_description" style="height:38px"></textarea>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="title.change.belongsStage" /></td>
				<td>
					<input name="costDTO.stage" id="index_timeCost_belongsStage" class="input"/>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="common.state" /></td>
				<td>
					<input type="radio" name="costDTO.status" id="index_timeCost_statusProcessing" value="1" checked="checked"/><fmt:message key="title.processing" />
					<input type="radio" name="costDTO.status" id="index_timeCost_statusComplete" value="2" /><fmt:message key="titie.complete" />
				</td>
			</tr>
			<tr>
				<td><fmt:message key="title.request.task" /></td>
				<td>
					<input type="hidden" name="costDTO.taskId" id="index_timeCost_costTaskId">
					<input name="wdDTO.eventTask" id="index_timeCost_costTaskTitle" class="choose" readonly style="width:70%;"/>
					
					<a class="btn btn-primary btn-sm" plain="true" onclick="cleanIdValue('index_timeCost_costTaskId','index_timeCost_costTaskTitle')" title="<fmt:message key="label.request.clear" />">
						<i class=" glyphicon glyphicon-trash"></i>
					</a>
				
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<br>
					<a class="btn btn-primary btn-sm" id="index_timeCost_save"><fmt:message key="common.save"/></a>
				</td>
			</tr>
		</table>
		</div>
	</form>
	</div>
	
	<div id="index_timeCostDetail_window" class="WSTUO-dialog" title="<fmt:message key="title.request.timeDetail" />" style="width:500px;height:auto">
		<div class="lineTableBgDiv">
      		<table style="width:100%" class="lineTable" cellspacing="1">
			<tr>
				<td style="width:25%"><fmt:message key="label.request.theTechnician" /></td>
				<td style="width:25%">
					<span id="index_timeCostDetail_fullName"></span>
				</td>
				<td style="width:25%"><fmt:message key="title.change.belongsStage" /></td>
				<td style="width:25%"><span id="index_timeCostDetail_belongsStage"></span></td>
			</tr>
			<tr>
				<td><fmt:message key="label.sla.slaStartTime" /></td>
				<td><span id="index_timeCostDetail_startTime"></span></td>
				<td><fmt:message key="label.sla.slaEndTime" /></td>
				<td><span id="index_timeCostDetail_endTime"></span></td>
			</tr>
			<tr>
				<td><fmt:message key="label.startToEedTime" /></td>
				<td><span id="index_timeCostDetail_startToEedTime"></span></td>
				<td><fmt:message key="label_actualTime" /></td>
				<td><span id="index_timeCostDetail_actualTime"></span></td>
			</tr>
			<tr>
				<td><fmt:message key="titie.technicianPerHourCost" />(${currency_sign})</td>
				<td><span id="index_timeCostDetail_perHourFees"></span></td>
				<td><fmt:message key="titie.technicianCost" />(${currency_sign})</td>
				<td><span id="index_timeCostDetail_skillFees"></span></td>
			</tr>
			<tr>
				<td><fmt:message key="title.material.cost"/>(${currency_sign})</td>
				<td><span id="index_timeCostDetail_materialCost"></span></td>
				<td><fmt:message key="title.otherCost" />(${currency_sign})</td>
				<td><span id="index_timeCostDetail_othersFees"></span></td>
			</tr>
			<tr>
				<td><fmt:message key="title.totalCost" />(${currency_sign})</td>
				<td><span id="index_timeCostDetail_totalFees"></span></td>
				<td><fmt:message key="common.state" /></td>
				<td><span id="index_timeCostDetail_state"></span></td>
			</tr>
			<tr>
				<td><fmt:message key="label.user.description" /></td>
				<td colspan="3"><span id="index_timeCostDetail_description"></span></td>
			</tr>
			<tr>
				<td><fmt:message key="title.request.task" /></td>
				<td colspan="3"><span id="index_timeCostDetail_task"></span></td>
			</tr>
		</table>
		</div>
	</div>
</body>
</html>
