<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="../../language.jsp" %>     

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>所属客户includes文件</title>

</head>
<body>

<!-- 选择所属客户列表-->
<div id="selectCompanyWin" class="WSTUO-dialog" title="<fmt:message key="label.select.belongs.client" />" style="width:550px;height:auto;padding:3px">
	<div style="margin-top: 10px;margin-bottom: 5px;">
	 <table style="width: 545px;">
	 	<tr style="height: 30px;">
	 		<td style="width: 30%;">
	 			<button class="btn btn-primary btn-sm" onclick="itsm.itsop.selectCompany.selectMyCompanyConfirm()" style="margin-right: 10px;"><fmt:message key="label.select.my.company"/></button>
			</td>
	 		<td style="width: 55%;">
	 			<input id="selectCompanyNameByJqgrid" type="text" class="form-control" style="width: 80%;margin-left: 50px;" />
			</td>
	 		<td style="width: 15%;">
	 			<button class="btn btn-primary btn-sm"  onclick="itsm.itsop.selectCompany.searchcompanyByName()" style="margin-right: 15px;"><fmt:message key="common.search"/></button>
			</td>
	 	</tr>
	 </table>
	 </div> 
	 <table id="selectCompany_grid"></table>
	<div id="selectCompany_gridPager"></div>	

</div>
<%--  <div id="selectMyCompanyToolbar" >
	 <table>
	 	<tr>
	 		<td>
	 			<button class="btn btn-primary btn-sm" onclick="itsm.itsop.selectCompany.selectMyCompanyConfirm()"><fmt:message key="label.select.my.company"/></button>
			</td>
	 		<td>
	 			<input id="selectCompanyNameByJqgrid" type="text" class="form-control" style="margin-left:160px;" />
			</td>
	 		<td>
	 			<button class="btn btn-primary btn-sm"  onclick="itsm.itsop.selectCompany.searchcompanyByName()"><fmt:message key="common.search"/></button>
			</td>
	 	</tr>
	 </table>
	 </div> --%>
<%-- 选择我所在的公司 --%>
<%-- <div id="selectMyCompanyToolbar" >

	<button onclick="itsm.itsop.selectCompany.selectMyCompanyConfirm()"><fmt:message key="label.select.my.company"/></button>
	
	<input id="selectCompanyNameByJqgrid" type="text"  style="margin-left:160px;" />
	
	<button  onclick="itsm.itsop.selectCompany.searchcompanyByName()"><fmt:message key="common.search"/></button>

</div> 
 --%>
<%-- AD选择我所在的公司 --%>
<div id="selectMyCompanyToolbar_AD"><a class="btn btn-primary btn-sm"  onclick="itsm.itsop.selectCompany.selectMyCompanyConfirm_AD()"><fmt:message key="label.select.my.company"/></a></div>

<!-- 选择所属客户列表 多选-->
<div id="selectCompanyDiv" class="WSTUO-dialog" title="<fmt:message key="label.select.belongs.client" />" style="width:500px;height:auto;padding:3px">
	
	<div id="CompanyGridToolbar" >
		<a class="btn btn-default btn-xs" id="selectCompany_grid_select_User">
	     	 <i class="glyphicon glyphicon-ok"></i> <fmt:message key="label.select.my.company"/>
	    </a>
		<a class="btn btn-default btn-xs"  id="selectCompany_grid_select"><i class="glyphicon glyphicon-ok"></i> <fmt:message key="common.select"/></a>
	</div>
	<table id="selectCompanySLA_grid"></table>
	<div id="selectCompany_gridPagerDiv"></div>
</div>

<%--选择优先级，紧急度，影响范围等 弹出多项框 --%>
<div id="multitermSLA"  class="WSTUO-dialog" title='<fmt:message key="label.selecting.data"/>' style="width:395px;height:auto">
	<form>
		<table width="387" height="270" border="0">
			  <tr>
			    <td rowspan="6" align="center">
				    <select id="leftop" multiple="multiple" size="12" style="width:150px; height:180px;" class="form-control"></select>
			    </td>
		    	<td width="20" height="12"> </td>
		    
		    	<td rowspan="6" align="center">
		    		<select id="rightop" multiple="multiple" size="12" style="width:150px; height:180px;" class="form-control"></select>
		    	</td>
			  </tr>
			  <tr>
			    <td height="20" align="center" valign="middle"><input id="leftbtn" type="button" value=">>" class="form-control"/></td>
			  </tr>
			  <tr>
			    <td height="27" align="center"> </td>
			  </tr>
			  <tr>
			    <td height="42" align="center" valign="middle">&nbsp;</td>
			  </tr>
			  <tr>
			    <td height="37" align="center" valign="middle"> </td>
			  </tr>
			  <tr>
			    <td height="34" align="center" valign="middle"><input id="rightbtn" type="button" value="<<" class="form-control"/></td>
			  </tr>
		 		
			<tr>
				<td colspan="3" style="text-align: center;"><input type="hidden" id="ruleValueId">

				<a class="btn btn-primary btn-sm" id="multiterm_saveRuleBtn" 
				  style="margin-right: 15px"><fmt:message key="label.request.add" /></a>
				</td>
			</tr>
		 
		</table>
		
	</form>
</div>


<!-- 搜索客户 -->
<div id="select_company_search_win" class="WSTUO-dialog" title="<fmt:message key="common.search"/>" style="width: 400px; height: auto; padding:4px;">
	<div class="lineTableBgDiv" >
	<form>
		<table style="width:100%" class="lineTable" cellspacing="1">
			<tr>
	             <td><fmt:message key="label.name"/></td>
	             <td>
	              	<input name="ciQueryDTO.ciname"  style="width: 97%" />
	             </td>        
	        </tr>
	        <tr>
	        	<td><fmt:message key="common.no"/></td>
	            <td>
	               <input name="ciQueryDTO.cino" style="width: 97%" />
	            </td>
	        </tr>
	    	<tr>
	    		<td colspan="2">
	    			<a class="btn btn-primary btn-sm"  id="selectCompany_common_dosearch" style="margin-top: 8px;"><fmt:message key="common.search" /></a>
	    		</td>
	    	</tr>      
		</table>
	</form>
	</div>
</div>

<!-- 选择外包客户 -->

<div id="index_itsop_customer_window" class="WSTUO-dialog" title="<fmt:message key="label.report.epiboly.company" />" style="width:600px;height:400px;padding:3px;">
	<table id="index_itsop_customer_grid"></table>
  	<div id="index_itsop_customer_grid_pager"></div>
  		
</div>
</body>
</html>