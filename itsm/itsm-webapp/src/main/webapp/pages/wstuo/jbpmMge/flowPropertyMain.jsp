<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../../language.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
.flowSelect a hover{
	background-color: red;
}
</style>
<script type="text/javascript">
	var processDefinitionId="${param.processDefinitionId}";
	var cNo=companyNo;
</script>
<script src="../js/wstuo/jbpmMge/flowPropertyMain.js"></script>
</head>
<body>

<!-- <div class="loading" id="flowPropertyMain_loading"><img src="../images/icons/loading.gif" /></div> -->

<div class="tab-pane active" >
	<div class="box-inner">
		<div class="box-header well" data-original-title="">
	        <h2><i class="glyphicon glyphicon-list"></i>&nbsp;流程属性设置</h2>
	        <div class="box-icon"></div>
        </div>		
		<div id="flowPropertyMainContent" class="content" border="none" fit="true" style="height: 98%;">
			<div id="flowPropertyMainContent_layout" fit="true">
				<div region="north" border="false" class="panelTop" id="flowPropertyMainTop">
					<ul class="dashboard-list">
                        <li><fmt:message key="title.bpm.process.type" />:<span id="processType"></span></li>
                     </ul>  				                
				</div>
			
			<div class="row">
			<div class="box col-md-3" >
			<div class="box-inner" style="padding-left: 15px;">
		        <div class="box-content" id="flowPropertyMain_west" region="west" split="true">
		           
		               <ul class="nav nav-tabs" id="myTab">
							<li class="active"><a><span><fmt:message key="label.flow.node" /></span></a></li>					
	            		</ul>
		               			
						<input type="hidden" id="flowPropertyMain_flow_key" >
						<div id="myTabContent" class="tab-content">
                    	<div class="tab-pane active" id="info">
						<div class="hisdiv" id="flowNodeShow">
							<table style="font-size: 14px;" class="table table-bordered" cellspacing="1">
								<thead>
								<tr>
									<th><fmt:message key="label.flow.nodeName" /></th>
									<th><fmt:message key="label.flow.nodeOutcome" /></th>
								</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>	
					</div>                    
                  </div>
		            </div>		          
		        </div>
		    </div>
		   
			<div class="box col-md-9">
        		<div class="box-inner">
					
				<div class="box-content">
				<ul class="nav nav-tabs" id="myTab">
					<li class="active"><a href="#list1"><span><fmt:message key="label.flow.node.property" /></span></a></li>	
					<li><a href="#list2"><span><fmt:message key="label.flow.chart" /></span></a></li>	
	             </ul>
             <div id="myTabContent" class="tab-content">
				<div region="center" class="tab-pane active" id="list1">
					<form id="flowPropertyForm">
						<input type="hidden" id="flowProperty_Id" name="flowActivityDTO.id"  />
						<input type="hidden" name="flowActivityDTO.dynamicAssignee" value="true"  />
						<table style="width:100%" class="table table-bordered" cellspacing="1">
							<tr>
								<td style="padding-left:15px; ;width: 25%;"><fmt:message key="label.request.chooseAssignInfo" />:</td>
								<td >
									<select name="flowActivityDTO.assignType" id="flowProperty_assignType" onchange="wstuo.jbpmMge.flowPropertyMain.assignType(this.value)" class="form-control" style="width: 60%;">
										<option value="autoAssignee" ><fmt:message key="title.bpm.use.event.assign" /></option>
										<option value="variablesAssignee" ><fmt:message key="label.bpm.variablesAssignee" /></option>
										<option value="ruleAssignee" ><fmt:message key="lable.ruleAssignee" /></option>													
									</select>											
								</td>
							</tr>
							
							<tr class="flowProperty_variablesAssignee_tr" style="display: none;">
								<td style="padding-left:15px; ;width: 25%;"><fmt:message key="title.change.assigneGroup" />:</td>
								<td >
								<input id="flowProperty_variablesAssigneeGroupNo" name="flowActivityDTO.variablesAssigneeGroupNo" type="hidden" />
								<input id="flowProperty_variablesAssigneeGroupName" name="flowActivityDTO.variablesAssigneeGroupName" style="width: 60%" readonly="readonly"/>
								&nbsp;<a  class="btn btn-primary btn-sm" plain="true" icon="icon-clean" onclick="cleanIdValue('flowProperty_variablesAssigneeGroupNo','flowProperty_variablesAssigneeGroupName')" title="<fmt:message key="label.request.clear" />"></a>
								</td>
							</tr>
							<tr class="flowProperty_variablesAssignee_tr" style="display: none;">
								<td style="padding-left:15px; ;width: 25%;"><fmt:message key="title.change.assigneeTaskdoer" />:</td>
								<td >
									<select id="flowProperty_variablesValueType" name="flowActivityDTO.variablesAssigneeType" style="width: 60%" class="form-control">
										<option value="">--<fmt:message key="common.pleaseSelect" />--</option>
										<option value="creator"><fmt:message key="common.Requester" /></option>
										<option value="technician"><fmt:message key="lable.bpm.nowAssignee" /></option>
										<option value="groupHead"><fmt:message key="lable.bpm.assigneeGroupHead" /></option>
										<option value="leader_0"><fmt:message key="lable.bpm.creatorGroupHeadOne" /></option>
										<option value="leader_1"><fmt:message key="lable.bpm.creatorGroupHeadTwo" /></option>
										<option value="leader_2"><fmt:message key="lable.bpm.creatorGroupHeadThr" /></option>
										<option value="leader_3"><fmt:message key="lable.bpm.creatorGroupHeadFour" /></option>
									</select>
								</td>
							</tr>
							<tr class="flowProperty_normalAssignee_tr">
								<td style="padding-left:15px; ;width: 25%;"><fmt:message key ="label.allowUdateDefaultAssignee" />:</td>
								<td >									
               						<label class="checkbox-inline">
									<input type="checkbox" name="flowActivityDTO.allowUdateDefaultAssignee" value="true" id="flowProperty_allowUdateDefaultAssignee" />
									</label>
								</td>
							</tr>
							<tr class="flowProperty_normalAssignee_tr" >
								<td style="padding-left:15px; ;width: 25%;"><fmt:message key ="label.allowUdateDefaultAssigneeGroup" />:</td>
								<td >
									<label class="checkbox-inline">
									<input type="checkbox" name="flowActivityDTO.allowUdateDefaultAssigneeGroup"  value="true" id="flowProperty_allowUdateDefaultAssigneeGroup" />
									</label>
								</td>
							</tr>
							<tr class="flowProperty_isUpdateEventAssign_tr">
								<td style="padding-left:15px; ;width: 25%;"><fmt:message key ="label.allowUseDynamicAssignee" />:</td>
								<td >
									<label class="checkbox-inline">
									<input type="checkbox" name="flowActivityDTO.isUpdateEventAssign"  value="true" id="flowProperty_isUpdateEventAssign" />
									</label>
								</td>
							</tr>
							<tr >
								<td style="padding-left:15px; ;width: 25%;"><fmt:message key="label.flow.candidateUsers" />:</td>
								<td >
									<input id="flowProperty_candidate_UsersNo" name="flowActivityDTO.candidateUsersNo" type="hidden" />
									&nbsp;<input id="flowProperty_candidate_UsersName" name="flowActivityDTO.candidateUsersName" style="width: 60%;float: left;" class="form-control" />
									<h5 style="float: left;padding-left: 8px;" ><a class="glyphicon glyphicon-trash" onclick="cleanIdValue('flowProperty_candidate_UsersNo','flowProperty_candidate_UsersName')" title="<fmt:message key="label.request.clear" />"></a></h5>
								</td>
							</tr>
							<tr >
								<td style="padding-left:15px; ;width: 25%;"><fmt:message key="label.flow.candidateGroups" />:</td>
								<td >
									<input id="flowProperty_candidate_GroupsNo" name="flowActivityDTO.candidateGroupsNo" type="hidden" />
									<input id="flowProperty_candidate_GroupsName" name="flowActivityDTO.candidateGroupsName" style="width: 60%;float: left;" class="form-control" />
									<h5 style="float: left;padding-left: 8px;" ><a style="float: left;" class="glyphicon glyphicon-trash" onclick="cleanIdValue('flowProperty_candidate_GroupsNo','flowProperty_candidate_GroupsName')" title="<fmt:message key="label.request.clear" />"></a></h5>
								</td>
							</tr>
							<tr style="display: none;">
								<td style="padding-left:15px; ;width: 25%;"><fmt:message key ="title.bpm.use.event.assign" />:</td>
								<td >
									<label class="checkbox-inline">
										<input type="checkbox" name="flowActivityDTO.eventAssign" checked="checked" value="true" id="flowProperty_eventAssign" />
									</label>
								</td>
							</tr>
							<tr class="flowProperty_roleAssignee_tr"  style="display: none;">
								<td style="padding-left:15px; ;width: 25%;"><fmt:message key ="title.orgSettings.role" />:</td>
								<td >
									<table id="flowProperty_role_table" style="width:100%" cellspacing="1"></table>
								</td>
							</tr>
							<tr>
								<td style="padding-left:15px; ;width: 25%;"><fmt:message key="label.role.roleState" />:</td>
								<td >
									<select id="flowProperty_status" name="flowActivityDTO.statusNo"  style="width: 60%" required="true" class="form-control">
										<option value="">--<fmt:message key="common.pleaseSelect" />--</option>
									</select>
								</td>
							</tr>
							<tr>
								<td style="padding-left:15px; ;width: 25%;"><fmt:message key ="label.toThisNodeValidationCondition" />:</td>
								<td >
									<select id="flowProperty_validMethod" name="flowActivityDTO.validMethod" style="width: 60%" class="form-control">
										<option value="">--<fmt:message key="common.pleaseSelect" />--</option>
									</select>
								</td>
							</tr>
							<tr>
								<td style="padding-left:15px; ;width: 25%;"><fmt:message key ="title.bpm.remark.required" />:</td>
								<td >
									<label class="checkbox-inline">
									<input type="checkbox" name="flowActivityDTO.remarkRequired" value="true" id="flowProperty_remarkRequired" />
									</label>
								</td>
							</tr>
							<tr>
								<td style="padding-left:15px; ;width: 25%;"><fmt:message key ="lable.mailHandlingProcess" />:</td>
								<td >
									<label class="checkbox-inline">
									<input type="checkbox" name="flowActivityDTO.mailHandlingProcess" value="true" id="flowProperty_mailHandlingProcess" />
									</label>
								</td>
							</tr>
							<tr>
								<td style="padding-left:15px; ;width: 25%;"><fmt:message key="title.bpm.returnLastAssigned" />:</td>
								<td >
									<label class="checkbox-inline">
									<input type="checkbox" name="flowActivityDTO.isFallbackToTechnician" value="true" id="flowProperty_isFallbackToTechnician" />
									</label>
								</td>
							</tr>
							<tr class="flowProperty_approverTask_tr">
								<td style="padding-left:15px; ;width: 25%;"><fmt:message key="title.bpm.approverTask" />:</td>
								<td >
									<label class="checkbox-inline">
									<input type="checkbox" name="flowActivityDTO.approverTask" value="true" id="flowProperty_approverTask" />
									</label>
								</td>
							</tr>
							
							<tr>
								<td style="padding-left:15px; ;width: 25%;"><fmt:message key="label.itsop.process.action" />:</td>
								<td ><div id="taskActions"></div></td>
							</tr>								
							<tr>
								<td colspan="2" style="background-color: #dae6fc;font-weight: 900;">
									<fmt:message key="setting.notitceRule.notificationRule" />
								</td>
							</tr>
							<tr>
								<td style="padding-left:15px; ;width: 25%;"><fmt:message key ="lable.notice.task.assignee" />:</td>
								<td >								
									<label class="checkbox-inline">
									<input type="checkbox" name="flowActivityDTO.taskAssigneeUserNotice" value="true" id="flowProperty_taskAssigneeUserNotice" />																
									<fmt:message key="label.notice.assignee.user" />
									</label>	
									<label class="checkbox-inline">
									<input type="checkbox" name="flowActivityDTO.taskAssigneeGroupNotice" value="true" id="flowProperty_taskAssigneeGroupNotice" />																
									<fmt:message key="label.notice.assignee.group.members" />
									</label>	
								</td>
							</tr>
							<tr>
								<td style="padding-left:15px; ;width: 25%;"><fmt:message key="lable.bpm.noticeModule" />:</td>
								<td >
									<a id="flowProperty_noticeRule" class="btn btn-primary btn-sm"><fmt:message key="common.select" /></a>
								
									<div id="showNoticeRule"></div>
								</td>
							</tr>
				
							<tr class="showMatchRule_tr1">
								<td colspan="2" style="background-color: #dae6fc;font-weight: 900;">
									<fmt:message key="title.sla.fitRule" />
								</td>
							</tr>
							<tr class="showMatchRule_tr2">
								<td style="padding-left:15px; ;width: 25%;"><fmt:message key="title.sla.fitRule" />:</td>
								<td >
									<a id="flowProperty_matchRule" class="btn btn-primary btn-sm"><fmt:message key="common.select" /></a>
									<div id="showMatchRule"></div>
								</td>
							</tr>
							<tr>
								<td colspan="2" style="text-align: center;">
									<a class="btn btn-primary btn-sm" id="saveFlowProperty" plain="true" title=""><fmt:message key="common.save"/></a>
								</td>
							</tr>

						</table>
					</form>
				</div>
                <div class="tab-pane" id="list2">
                	<img src="jbpm!getProcessImage.action?processDefinitionId=${param.processDefinitionId}" />	
                </div>                
               </div>
          </div>
				</div>
			</div>
		</div>
			</div>
		</div>
	
		<!-- 选择通知模板列表-->
		<div id="flowPropertyNoticeRuleSelectWin" title='<fmt:message key="lable.com.Notification.Template.List"/>' style="width:530px;height:380px;padding:3px;display: none;">
			<div id="noticeRuleSelectGridToolbar" >
				<a class="btn btn-primary btn-sm" plain="true" id="noticeRuleSelect_grid_select"><fmt:message key="common.select"/></a>
			</div>
			<table id="noticeRuleSelect_grid"></table>
			<div id="noticeRuleSelect_gridPager"></div>
		</div>
		<!-- 选择流程列表-->
		<div id="flowPropertyMatchRuleSelectWin" title='<fmt:message key="lable.com.drools.drl.proces.List"/>' style="width:530px;height:380px;padding:3px;display: none;">
			<div id="matchRuleSelectGridToolbar">
				<a class="btn btn-primary btn-sm" plain="true" id="matchRuleSelect_grid_select"><fmt:message key="common.select"/></a>
			</div>
			<table id="matchRuleSelect_grid"></table>
			<div id="matchRuleSelect_gridPager"></div>
		</div>
	</div>
</div>
</body>
</html>