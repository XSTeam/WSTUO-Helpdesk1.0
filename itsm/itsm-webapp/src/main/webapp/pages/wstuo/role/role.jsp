﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<script src="../js/wstuo/role/role.js?random=<%=new java.util.Date().getTime()%>"></script>
  <div class="row">
        <div class="box col-md-12">
            <div class="box-inner">
                <div class="box-header well" data-original-title="">
                    <h2><i class="glyphicon glyphicon-list-alt"></i> 角色列表</h2>
                    <div class="box-icon">
                        <!-- <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                class="glyphicon glyphicon-chevron-up"></i></a> -->
                    </div>
                </div>
                
                 <div class="box-content ">
                <div id="roleTable_div" class="">
								<!-- OrgGrid start -->
								<sec:authorize url="/pages/role!find.action">
									<div title="<fmt:message key="title.orgSettings.gridView" />"  style="padding: 3px;">
									<!-- 列表start -->
									 <table id="roleGrid"></table>
									  <div id="rolePager"></div>
									
								<div id="roleGridAct" style="background-color: #EEEEEE; height: 28px; display: none;"><sec:authorize url="/pages/role!setResourceByRoleIds.action"><a href="javascript:wstuo.role.role.open_func({id},true)"  title="<fmt:message key="label.role.permissionAssign" />"><i class="glyphicon glyphicon-asterisk"></i></a></sec:authorize>&nbsp;&nbsp;&nbsp;<sec:authorize url="/pages/role!edit.action"><a href="javascript:wstuo.role.role.editRole({id})"title="<fmt:message key="common.edit" />"><i class="glyphicon glyphicon-edit"></i></a></sec:authorize>&nbsp;&nbsp;&nbsp;<sec:authorize url="/pages/role!delete.action"><a href="javascript:wstuo.role.role.delRole({id})" title="<fmt:message key="common.delete" />"><i class="glyphicon glyphicon-trash"></i></a></sec:authorize></div>
								
								<div id="roleTopMenu" style="background-color: #EEEEEE; height: 28px; display: none;">
									<div class="panelBar">
										<sec:authorize url="/pages/role!save.action">
											<button class="btn btn-default btn-xs" id="roleGrid_add" onclick="wstuo.role.role.addWin()">
												<i class="glyphicon glyphicon-plus"></i>&nbsp;添加
											</button>
										</sec:authorize>

										<sec:authorize url="/pages/role!edit.action">
											<button class="btn btn-default btn-xs" id="roleGrid_edit">
												<i class="glyphicon glyphicon-edit"></i>&nbsp;修改
											</button>
										</sec:authorize>

										<sec:authorize url="/pages/role!delete.action">
											<button class="btn btn-default btn-xs" id="roleGrid_delete">
												<i class="glyphicon glyphicon-trash"></i>&nbsp;删除
											</button>
										</sec:authorize>
										
										<sec:authorize url="/pages/role!find.action">
										   <button class="btn btn-default btn-xs" id="link_role_search" >
										     <i class="glyphicon glyphicon-search"></i>&nbsp;<fmt:message key="common.search" />
										   </button>
										</sec:authorize>
																				
										<sec:authorize url="roleGrid_import">
											<button class="btn btn-default btn-xs" id="roleGridImport">
												<i class="glyphicon glyphicon-import"></i>&nbsp;<fmt:message key="label.dc.import" />
											</button>
										</sec:authorize>
										<sec:authorize url="roleGrid_export">
											<button class="btn btn-default btn-xs" id="roleGrid_Export">
												<i class="glyphicon glyphicon-share"></i>&nbsp;导出
											</button>
										</sec:authorize>
									</div>
								</div>
							</div>
								</sec:authorize>
							</div>
            </div>
                
            </div>
        </div>
    <!-- 角色添加/修改 start -->
    <div id="roleWindow" class="WSTUO-dialog" title="<fmt:message key="title.role.addEdit" />" style="width:400px;height:auto">
             <div class="panel panel-default">
					<div class="panel-body">
						<form id="roleWindowForm" class="form-horizontal" event="wstuo.role.role.addRole">
							<div class="form-group">
								<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.role.roleName" /></label>
								<div class="col-sm-5">
									 <input name="roleDto.roleName" id="roleName" class="form-control" required="true"  />
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.role.roleCode" /></label>
								<div class="col-sm-5">
									<input name="roleDto.roleCode" id="roleCode" class="form-control" required="true" />
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.role.roleState" /></label>
								<div class="col-sm-5">
								   <input type="radio" name="roleDto.roleState" id="roleState"  value="true" checked="checked" />
								   <fmt:message key="common.enable" />
								   <input type="radio"name="roleDto.roleState" id="roleState1" value="false" />
								   <fmt:message key="common.disable" /> 
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.role.roleDescription" /></label>
								<div class="col-sm-5">
									<input class="form-control" name="roleDto.description" id="role_description">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.role.roleMark" /></label>
								<div class="col-sm-5">
									<input class="form-control" style="height: 45px"name="roleDto.remark" id="remark">
								</div>
							</div>
							
							<input id="back_roleName" type="hidden" /> 
							<input id="back_roleCode" type="hidden" />
							<input type="hidden" name="roleDto.companyNo" value="${sessionScope.companyNo}" /> 
							<input type="hidden" name="roleDto.roleId" value="0" id="roleId" />
							<div class="form-group">
								<div class="col-sm-9 col-sm-offset-4">
									<button type="reset" class="btn btn-default btn-sm">重置</button>
					                <button   type="submit" class="btn btn-primary btn-sm"><fmt:message key="common.save" /></button><!-- id="saverole" -->
								</div>
							</div>
						</form>
					</div>
			</div>  
	</div>
   <!-- 角色添加/修改 end -->
<!-- 角色导出 隐藏-->
	<div style="display: none;">
	   <form action="role!exportRole.action" id="exportRoleForm" method="post">
	       <table>
	        <tr>
	            <td>
	               <input id="ex_roleName" name="roleQueryDto.roleName" class="input"/>
	               <input name="roleQueryDto.roleCode" class="input" />
	               <input name="roleQueryDto.description" class="input" />
	               <input name="roleQueryDto.remark" class="input" />
	            </td>
	        </tr>
	       </table>
	   </form>
	</div>
<!-- 分配角色权限 -->
<div id="assignFunc" title="<fmt:message key="label.role.permissionAssign" />" class="WSTUO-dialog" style="width:660px;height:450px;padding:3px">
    <div class="row">
        <div class="box col-md-5">
           <div class="box-content">
				<div id="functionTree" style="height: 330px;position: relative;overflow: auto;"></div>
			</div>
		</div>
		<div class="box col-md-7">
           <div class="box-content ">
				<div id="operationOptions"></div>
			</div>
		</div>
	</div>	
    <button class="btn btn-default btn-xs" id="link_func_ok" onclick="wstuo.role.role.save_func()" style="margin-right: 16px;">
		  <i class="glyphicon glyphicon-floppy-disk"></i> <fmt:message key="common.save"/>
		</button>
</div>
<!-- 搜索角色 start -->
<div id="searchRole" class="WSTUO-dialog" title="<fmt:message key="title.role.searchRole" />" style="width:400px;height:auto">

	<div class="lineTableBgDiv">
		<div class="panel panel-default">
					<div class="panel-body">
						<form action="role!exportRole.action" method="post" class="form-horizontal">
							<div class="form-group">
								<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.role.roleName" /></label>
								<div class="col-sm-5">
									 <input id="ex_roleName" name="roleQueryDto.roleName" class="form-control"/>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.role.roleCode" /></label>
								<div class="col-sm-5">
									<input name="roleQueryDto.roleCode" class="form-control" />
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.role.roleDescription" /></label>
								<div class="col-sm-5">
									<input name="roleQueryDto.description" class="form-control" />
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.role.roleMark" /></label>
								<div class="col-sm-5">
									<input name="roleQueryDto.remark" class="form-control" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-9 col-sm-offset-4">
		                            <button id="link_role_mysearch"  type="button" class="btn btn-primary btn-sm" onclick="wstuo.role.role.search()"><fmt:message key="common.search" /></button>
								</div>
							</div>
						</form>
					</div>
			</div>  
		</div>
</div>	
	<!-- 搜索角色 end -->
</div>