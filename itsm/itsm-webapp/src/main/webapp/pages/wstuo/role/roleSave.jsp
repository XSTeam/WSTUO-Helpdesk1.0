<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
   <%@ include file="../../language.jsp" %>
       
<div class="pageContent">
<form method="post" class="pageForm required-validate" id="addOrEditRoleForm">
<div class="pageFormContent" layoutH="56">
		<dl class="nowrap">
			<dt><fmt:message key="label.role.roleName" /></dt>
			<dd>
				<input name="roleDto.roleName" id="roleName" class="easyui-validatebox input" required="true"  validtype="length[1,200]"/>
			</dd>
		</dl>
		<dl class="nowrap">
			<dt><fmt:message key="label.role.roleCode" /></dt>
		<dd>
		    <input name="roleDto.roleCode" id="roleCode"  required="true" />
		</dd>
		</dl>
		<dl class="nowrap">
			<dt><fmt:message key="label.role.roleState" /></dt>
		<dd>
			<input type="radio" name="roleDto.roleState" id="roleState" value="true" checked="checked" /><fmt:message key="common.enable" />
			<input type="radio" name="roleDto.roleState" id="roleState1" value="false" /><fmt:message key="common.disable" />
		</dd>
		</dl>
		<dl class="nowrap">
			<dt><fmt:message key="label.role.roleDescription" /></dt>
			<dd>
				<textarea class="input" style="height:45px" name="roleDto.description" id="role_description"></textarea>
			</dd>
		</dl>
		<dl class="nowrap">
			<dt><fmt:message key="label.role.roleMark" /></dt>
			<dd>
			   <textarea class="input" style="height:45px" name="roleDto.remark" id="remark"></textarea>
			</dd>
		</dl>
		
	</div>
		<input id="back_roleName"  type="hidden"/>
		<input id="back_roleCode"  type="hidden"/>
		<input type="hidden" name="roleDto.companyNo" value="${sessionScope.companyNo}"/>
		<input type="hidden" name="roleDto.roleId" value="0" id="roleId" />
	</form>
	<div class="formBar">
		<ul>
			<li><div class="buttonActive"><div class="buttonContent"><button onclick="wstuo.role.role.addRole()" ><fmt:message key="common.save" /></button></div></div></td></li>
			</ul>
		</div>
	</div>
</div>