<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../../language.jsp" %>


 <li id="thirdMenu_dataDictionaryManage" onClick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="title.security.dataDictionaryManage" />','../pages/common/config/dictionary/dataDictionaryGroupManage.jsp')" >
                    <span class="leftMenu_thirdMenu_item_icon"></span>
                    <span class="leftMenu_thirdMenu_item_title"><fmt:message key="title.security.dataDictionaryManage" /></span>
 </li>

<div style="border-bottom:#ccc 1px dashed;height:10px;margin-bottom:5px;width:80%"></div>

<c:forEach items="${dataDictionaryGroups}" var="dcgroup">
<c:choose>

<c:when test="${dcgroup.groupCode eq 'requestStatus'}">
	<c:if test="${requestHave eq true}">
	<c:if test="${dcgroup.groupCode !='itsopType' }">
		 <li  onClick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="label.dc.${dcgroup.groupCode}" />','../pages/common/config/dictionary/dataDictionaryGrid.jsp?groupCode=${dcgroup.groupCode}',function(){ startLoading();setTimeout(function(){endLoading();},300);})">
                    <span class="leftMenu_thirdMenu_item_icon"></span>
                    <span class="leftMenu_thirdMenu_item_title"><fmt:message key="label.dc.${dcgroup.groupCode}" /></span>
 		 </li>
	
	</c:if>
	<c:if test="${versionType == 'ITSOP' and dcgroup.groupCode == 'itsopType'}">
	
		 <li  onClick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="label.dc.${dcgroup.groupCode}" />','../pages/common/config/dictionary/dataDictionaryGrid.jsp?groupCode=${dcgroup.groupCode}',function(){ startLoading();setTimeout(function(){endLoading();},300);})">
              <span class="leftMenu_thirdMenu_item_icon"></span>
              <span class="leftMenu_thirdMenu_item_title"><fmt:message key="label.dc.${dcgroup.groupCode}" /></span>
 		 </li>
	
	</c:if>
	</c:if>
</c:when>


<c:when test="${dcgroup.groupCode eq 'changeStatus'}">
	<c:if test="${changeHave eq true}">
	<c:if test="${dcgroup.groupCode !='itsopType' }">
	 	<li  onClick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="label.dc.${dcgroup.groupCode}" />','../pages/common/config/dictionary/dataDictionaryGrid.jsp?groupCode=${dcgroup.groupCode}',function(){ startLoading();setTimeout(function(){endLoading();},300);})">
              <span class="leftMenu_thirdMenu_item_icon"></span>
              <span class="leftMenu_thirdMenu_item_title"><fmt:message key="label.dc.${dcgroup.groupCode}" /></span>
 		 </li>
	</c:if>
	<c:if test="${versionType == 'ITSOP' and dcgroup.groupCode == 'itsopType'}">
		 <li onClick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="label.dc.${dcgroup.groupCode}" />','../pages/common/config/dictionary/dataDictionaryGrid.jsp?groupCode=${dcgroup.groupCode}',function(){ startLoading();setTimeout(function(){endLoading();},300);})">
              <span class="leftMenu_thirdMenu_item_icon"></span>
              <span class="leftMenu_thirdMenu_item_title"><fmt:message key="label.dc.${dcgroup.groupCode}" /></span>
 		 </li>
	</c:if>
	</c:if>
</c:when>


<c:when test="${dcgroup.groupCode eq 'problemStatus'}">
	<c:if test="${problemHave eq true}">
	<c:if test="${dcgroup.groupCode !='itsopType' }">
		 <li  onClick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="label.dc.${dcgroup.groupCode}" />','../pages/common/config/dictionary/dataDictionaryGrid.jsp?groupCode=${dcgroup.groupCode}',function(){ startLoading();setTimeout(function(){endLoading();},300);})">
              <span class="leftMenu_thirdMenu_item_icon"></span>
              <span class="leftMenu_thirdMenu_item_title"><fmt:message key="label.dc.${dcgroup.groupCode}" /></span>
 		 </li>
	</c:if>
	<c:if test="${versionType == 'ITSOP' and dcgroup.groupCode == 'itsopType'}">
		 <li  onClick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="label.dc.${dcgroup.groupCode}" />','../pages/common/config/dictionary/dataDictionaryGrid.jsp?groupCode=${dcgroup.groupCode}',function(){ startLoading();setTimeout(function(){endLoading();},300);})">
              <span class="leftMenu_thirdMenu_item_icon"></span>
              <span class="leftMenu_thirdMenu_item_title"><fmt:message key="label.dc.${dcgroup.groupCode}" /></span>
 		 </li>
	</c:if>
	</c:if>
</c:when>


<c:when test="${dcgroup.groupCode eq 'releaseStatus'}">
	<c:if test="${releaseHave eq true}">
	<c:if test="${dcgroup.groupCode !='itsopType' }">
		<li  onClick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="label.dc.${dcgroup.groupCode}" />','../pages/common/config/dictionary/dataDictionaryGrid.jsp?groupCode=${dcgroup.groupCode}',function(){ startLoading();setTimeout(function(){endLoading();},300);})">
              <span class="leftMenu_thirdMenu_item_icon"></span>
              <span class="leftMenu_thirdMenu_item_title"><fmt:message key="label.dc.${dcgroup.groupCode}" /></span>
 		 </li>
	</c:if>
	<c:if test="${versionType == 'ITSOP' and dcgroup.groupCode == 'itsopType'}">
		<li  onClick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="label.dc.${dcgroup.groupCode}" />','../pages/common/config/dictionary/dataDictionaryGrid.jsp?groupCode=${dcgroup.groupCode}',function(){ startLoading();setTimeout(function(){endLoading();},300);})">
              <span class="leftMenu_thirdMenu_item_icon"></span>
              <span class="leftMenu_thirdMenu_item_title"><fmt:message key="label.dc.${dcgroup.groupCode}" /></span>
 		 </li>
	</c:if>
	</c:if>
</c:when>


<c:otherwise>  
<c:if test="${dcgroup.groupCode !='itsopType' }">
	<c:if test="${dcgroup.dataFlag == 1 }">
		<li  onClick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="label.dc.${dcgroup.groupCode}" />','../pages/common/config/dictionary/dataDictionaryGrid.jsp?groupCode=${dcgroup.groupCode}',function(){ startLoading();setTimeout(function(){endLoading();},300);})">
	              <span class="leftMenu_thirdMenu_item_icon"></span>
	              <span class="leftMenu_thirdMenu_item_title"><fmt:message key="label.dc.${dcgroup.groupCode}" /></span>
	    </li>
    </c:if>
    <c:if test="${dcgroup.dataFlag == 0 }">
		<li  onClick="javascript:basics.tab.tabUtils.addTab('${dcgroup.groupName}','../pages/common/config/dictionary/dataDictionaryGrid.jsp?groupCode=${dcgroup.groupCode}',function(){ startLoading();setTimeout(function(){endLoading();},300);})">
	              <span class="leftMenu_thirdMenu_item_icon"></span>
	              <span class="leftMenu_thirdMenu_item_title">${dcgroup.groupName}</span>
	    </li>
    </c:if>
</c:if>
<c:if test="${versionType == 'ITSOP' and dcgroup.groupCode == 'itsopType'}">
	<li onClick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="label.dc.${dcgroup.groupCode}" />','../pages/common/config/dictionary/dataDictionaryGrid.jsp?groupCode=${dcgroup.groupCode}',function(){ startLoading();setTimeout(function(){endLoading();},300);})">
              <span class="leftMenu_thirdMenu_item_icon"></span>
              <span class="leftMenu_thirdMenu_item_title"><fmt:message key="label.dc.${dcgroup.groupCode}" /></span>
    </li>
   
	</c:if>
</c:otherwise>
</c:choose>
</c:forEach>
<li  onClick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="label.dc.location" />','../pages/common/config/category/eventCategory.jsp?categoryCode=Location')">
              <span class="leftMenu_thirdMenu_item_icon"></span>
              <span class="leftMenu_thirdMenu_item_title"><fmt:message key="label.dc.location" /></span>
</li>

<sec:authorize url="/pages/dataDictionaryItems!find.action">
<div style="border-bottom:#ccc 1px dashed;height:10px;margin-bottom:5px;width:80%"></div>
<c:if test="${requestHave eq true}">
<li  onClick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="setting.requestCategory" />','../pages/common/config/category/eventCategory.jsp?categoryCode=Request')">
              <span class="leftMenu_thirdMenu_item_icon"></span>
              <span class="leftMenu_thirdMenu_item_title"><fmt:message key="setting.requestCategory" /></span>
</li>
</c:if>
<c:if test="${problemHave eq true}">
<li  onClick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="setting.problemCategory" />','../pages/common/config/category/eventCategory.jsp?categoryCode=Problem')">
              <span class="leftMenu_thirdMenu_item_icon"></span>
              <span class="leftMenu_thirdMenu_item_title"><fmt:message key="setting.problemCategory" /></span>
</li>
</c:if>
<c:if test="${changeHave eq true}">
<li  onClick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="report.Change.class" />','../pages/common/config/category/eventCategory.jsp?categoryCode=Change')">
              <span class="leftMenu_thirdMenu_item_icon"></span>
              <span class="leftMenu_thirdMenu_item_title"><fmt:message key="report.Change.class" /></span>
</li>
</c:if>
<c:if test="${releaseHave eq true}">
<li  onClick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="setting.releaseCategory" />','../pages/common/config/category/eventCategory.jsp?categoryCode=Release')">
              <span class="leftMenu_thirdMenu_item_icon"></span>
              <span class="leftMenu_thirdMenu_item_title"><fmt:message key="setting.releaseCategory" /></span>
</li>
</c:if>

<li  onClick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="knowledge.knowledgeCategory" />','../pages/common/config/category/eventCategory.jsp?categoryCode=Knowledge')">
              <span class="leftMenu_thirdMenu_item_icon"></span>
              <span class="leftMenu_thirdMenu_item_title"><fmt:message key="knowledge.knowledgeCategory" /></span>
</li>
<c:if test="${cimHave eq true}">
<li  onClick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="report.Configuration.class" />','../pages/common/config/category/CICategory.jsp')">
              <span class="leftMenu_thirdMenu_item_icon"></span>
              <span class="leftMenu_thirdMenu_item_title"><fmt:message key="report.Configuration.class" /></span>
</li>
</c:if>
<li  onClick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="label_software_category" />','../pages/common/config/category/eventCategory.jsp?categoryCode=Software')">
              <span class="leftMenu_thirdMenu_item_icon"></span>
              <span class="leftMenu_thirdMenu_item_title"><fmt:message key="label_software_category" /></span>
</li>



<div style="border-bottom:#ccc 1px dashed;height:10px;margin-bottom:5px;width:80%"></div>

<li  onClick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="label.priority.matrix" />','../pages/common/config/priorityMatrix/priorityMatrix.jsp')">
              <span class="leftMenu_thirdMenu_item_icon"></span>
              <span class="leftMenu_thirdMenu_item_title"><fmt:message key="label.priority.matrix" /></span>
</li>

<li onClick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="label.attrGroup.attrGroup" />','../pages/common/eav/attributeGroup.jsp')">
              <span class="leftMenu_thirdMenu_item_icon"></span>
              <span class="leftMenu_thirdMenu_item_title"><fmt:message key="label.attrGroup.attrGroup" /></span>
</li>
<li  onClick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="common.extendedAttribute" />','../pages/common/eav/eavCi.jsp')">
              <span class="leftMenu_thirdMenu_item_icon"></span>
              <span class="leftMenu_thirdMenu_item_title"><fmt:message key="common.extendedAttribute" /></span>
</li>
<li onClick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="setting.title.returnItem" />','../pages/itsm/app/visit/visitManage.jsp')">
              <span class="leftMenu_thirdMenu_item_icon"></span>
              <span class="leftMenu_thirdMenu_item_title"><fmt:message key="setting.title.returnItem" /></span>
</li>
<li id="thirdMenu_escalateLevel" onClick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="setting.escalateLevel" />','../pages/common/config/updateLevel/updateLevelGrid.jsp')">
              <span class="leftMenu_thirdMenu_item_icon"></span>
              <span class="leftMenu_thirdMenu_item_title"><fmt:message key="setting.escalateLevel" /></span>
</li>
<c:if test="${changeHave eq true}">	
<li id="thirdMenu_cab" onClick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="setting.cab" />','../pages/common/config/cab/cabAdmin.jsp')">
              <span class="leftMenu_thirdMenu_item_icon"></span>
              <span class="leftMenu_thirdMenu_item_title"><fmt:message key="setting.cab" /></span>
</li>
</c:if>
<li  onClick="javascript:basics.tab.tabUtils.addTab('<fmt:message key="setting.serviceDirectory" />','../pages/common/config/category/eventCategory.jsp?categoryCode=Service')">
              <span class="leftMenu_thirdMenu_item_icon"></span>
              <span class="leftMenu_thirdMenu_item_title"><fmt:message key="setting.serviceDirectory" /></span>
</li>
</sec:authorize>  
<script>
	$(function(){
	    leftMenu.isSecondMenuScroll();
   		leftMenu.thirdMenuClickAction();
	    secondMenu && $('#thirdMenu_' + secondMenu).click();
	});
</script>