<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../language.jsp" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<script src="../js/wstuo/dictionary/dataDictionaryGroupManage.js?random=<%=new java.util.Date().getTime()%>"></script>
<script type="text/javascript" src="../js/jquery/icolor/iColorPicker.js"></script>
<script src="../js/wstuo/dictionary/dataDictionaryGrid.js?random=<%=new java.util.Date().getTime()%>"></script>


<div class="row">
    <div class="box col-md-6">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list-alt"></i> 数据字典目录</h2>
            </div>
            <div class="box-content buttons">

				<table id="dataDictionaryGroupsGrid"></table>
			    <div id="dataDictionaryGroupsPager"></div>
			    <div id="dataDictionaryGroupsToolbar" style="display:none">
			    	<button class="btn btn-default btn-xs" onclick="addDictionaryItem()"><i class="glyphicon glyphicon-plus"></i> <fmt:message key="common.add"/></button>
					<button class="btn btn-default btn-xs" onclick="editDictionaryGroups()"><i class="glyphicon glyphicon-edit"></i> <fmt:message key="common.edit"/></button>
					<button class="btn btn-default btn-xs" onclick="delDictionaryGroups();"><i class="glyphicon glyphicon-trash"></i> <fmt:message key="common.delete"/></button>
					<button class="btn btn-default btn-xs" onclick="searchDictionaryItem();"><i class="glyphicon glyphicon-search"></i> <fmt:message key="common.search"/></button>
					<sec:authorize url="dataDictionary_export">
			       <button class="btn btn-default btn-xs" id="dataDictionaryGroupManage_export"><i class="glyphicon glyphicon-share"></i> <fmt:message key="label.dc.export" /></button>
					</sec:authorize>
			    </div>
			  </div>
	    </div>
   </div>
   <div class="box col-md-6 " id="dictionary_item">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list-alt"></i> 数据字典</h2>
            </div>
            <div class="box-content buttons">
				<table id="dictionaryItemGrid"></table>
			    <div id="dictionaryItemGridPager"></div>
			    <div id="dataDictionaryGridToolbar" style="display:none">
			    
			    
			<sec:authorize url="/pages/dataDictionaryItems!save.action">
				<button class="btn btn-default btn-xs"  onclick="wstuo.dictionary.dataDictionaryGrid.addDictionaryItem()"><i class="glyphicon glyphicon-plus"></i> <fmt:message key="common.add"/></button>
			</sec:authorize>
			
			<sec:authorize url="/pages/dataDictionaryItems!merge.action">
				<button class="btn btn-default btn-xs" onclick="wstuo.dictionary.dataDictionaryGrid.editDictionary()"><i class="glyphicon glyphicon-edit"></i> <fmt:message key="common.edit"/></button>
			</sec:authorize>
			
			<sec:authorize url="/pages/dataDictionaryItems!delete.action">
					<button class="btn btn-default btn-xs" onclick="wstuo.dictionary.dataDictionaryGrid.delDictionary();"><i class="glyphicon glyphicon-trash"></i> <fmt:message key="common.delete"/></button>
			</sec:authorize>
			
			<button class="btn btn-default btn-xs" onclick="wstuo.dictionary.dataDictionaryGrid.openSearchWindow();"><i class="glyphicon glyphicon-search"></i> <fmt:message key="common.search"/></button>
					
			<sec:authorize url="dataDictionary_import">
			 <button class="btn btn-default btn-xs" id="importDataDc"><i class="glyphicon glyphicon-import"></i> <fmt:message key="label.dc.import" /></button>
			</sec:authorize>
			
			<!-- 字典导出 DATADICTIONARY_EXPORT -->
			<sec:authorize url="DATADICTIONARY_EXPORT">
			 <button class="btn btn-default btn-xs" id="exportDataDc"><i class="glyphicon glyphicon-share"></i> <fmt:message key="label.dc.export" /></button>
			</sec:authorize>
			<input type="hidden" >
			</div>
			  </div>
	    </div>
   </div>
   <div class="box col-md-6" id="dictionary_tree" style="display: none">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list-alt"></i> 树结构</h2>
            </div>
            <div class="box-content buttons" id="dictionary_div_tree">
            	
            </div>
            <input id="eventCategoryCode" type="hidden" />
       </div>
   </div>
 </div> 
<div class="WSTUO-dialog" title="<fmt:message key="common.add"/>" style="width:400px;height:auto; padding:10px; line-height:20px;" id="addEventCategoryTree">
 <form id="addEventCategoryForm"  event="wstuo.category.eventCategory.addCategory">
	<table style="width: 100%" class="lineTable" cellspacing="1">
		<tr>
			<td><fmt:message key="label.attrGroup.attrGroupName" /></td>
			<td><input name="eventCategoryDto.eventName" id="addeventName" class="form-control" maxlength="200" minlength="1" required="true" /></td>
		</tr>
		<tr id="add_category_form">
			<td><fmt:message key="formCustom"/></td>
			<td>
				<select class="form-control" name="eventCategoryDto.formId" id="add_form" ></select>
			</td>
		</tr>
		<c:if test="${param.categoryCode eq 'Service'}">
		<tr id="add_scores_mito">
			<td><fmt:message key="label.service.scores"/></td>
			<td>
				<input class="form-control" min="0"  maxlength="3" minlength="1" max="100" name="eventCategoryDto.scores" id="add_scores" />
			</td>
		</tr>
		</c:if>
		<tr>
			<td>
					<fmt:message key="common.code" />
				</td>
			<td><input name="eventCategoryDto.categoryCodeRule" id="addcategoryCodeRule" class="form-control" maxlength="10" minlength="1"></input>
			</td>
		</tr>
		<tr>
			<td>
					<fmt:message key="lable.change.checkDesc" />
				</td>
			<td>
				<textarea name="eventCategoryDto.eventDescription" id="addeventDescription" class="form-control"></textarea>
			</td>
		</tr>

		<tr>
			<td colspan="2" style="height: 50px">
				<sec:authorize url="/pages/dataDictionaryItems!save.action">
					<input name="eventCategoryDto.parentEventId" id="parentEventId" type="hidden" />
					<button id="save_sub" type="submit" class="btn btn-primary btn-sm"><fmt:message key="common.save" /></button>
				</sec:authorize>
				</td>
			</tr>
		</table>
</form>
</div>
 <div class="WSTUO-dialog" title="<fmt:message key="common.edit"/>" style="width:400px;height:auto; padding:10px; line-height:20px;" id="editEventCategoryTree">
		<form id="editCategoryForm" event="wstuo.category.eventCategory.updateCategory">
			<table style="width: 100%;" class="lineTable" cellspacing="1" >
				<tr>
					<td>
						<fmt:message key="label.attrGroup.attrGroupName" />
					</td>
					<td><input name="eventCategoryDto.eventName" id="eventName" class="form-control" maxlength="200" required="true" /></td>
				</tr>
				<tr id="edit_category_form">
					<td><fmt:message key="formCustom"/></td>
					<td>
						<select class="form-control" name="eventCategoryDto.formId" id="edit_form" ></select>
					</td>
				</tr>
				<c:if test="${param.categoryCode eq 'Service'}">
				<tr id="edit_scores_mito">
					<td><fmt:message key="label.service.scores"/></td>
					<td>
						<inputclass="form-control"  min="0" name="eventCategoryDto.scores" id="edit_scores" />
					</td>
				</tr>
				</c:if>
				<tr>
					<td>
							<fmt:message key="common.code" />
					</td>
					<td><input name="eventCategoryDto.categoryCodeRule" id="categoryCodeRule"class="form-control" maxlength="10"  minlength="1"></input>
					</td>
				</tr>
				<tr>
					<td>
						<fmt:message key="lable.change.checkDesc" />
					</td>
					<td><textarea name="eventCategoryDto.eventDescription" id="eventDescription" class="form-control"></textarea>
					</td>
				</tr>

				<tr>
					<td colspan="2" style="height: 50px">
						<sec:authorize url="/pages/dataDictionaryItems!merge.action">
							<input name="eventCategoryDto.eventId" id="eventId" type="hidden" />
							<button id="save_data" type="submit" class="btn btn-primary btn-sm"><fmt:message key="common.save" /></button>
						</sec:authorize>
					</td>
				</tr>
			</table>
		</form>

	</div>
<!-- 添加 数据字典目录DIV start -->
	<div id="addDataDictionaryGroupDiv"  class="WSTUO-dialog" title="<fmt:message key="common.add"/>" style="height:auto; padding:15px; line-height:20px;">
		<form class="form-horizontal" event="saveDictionaryGroups">
			<div class="form-group">
				<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.role.roleName"/></label>
				<div class="col-sm-5">
					 <input name="ddgDto.groupName" class="form-control" validType="nullValueValid"  required="true" id="add_groupName" />
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-4 control-label" for="lastname"><fmt:message key="common.code"/></label>
				<div class="col-sm-5">
					<input name="ddgDto.groupCode" class="form-control" validType="numLetters" required="true" id="add_groupCode"/>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.type"/></label>
				<div class="col-sm-5">
					<select id="add_groupType" name="ddgDto.groupType" class="form-control">
							<option value="select">下拉框</option>
							<option value="checkbox">多选</option>
							<option value="radio">单选</option>
							<option value="tree">树结构</option>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-9 col-sm-offset-4">
					<input name="ddgDto.groupNo" type="hidden" id="add_groupNo">
					<input name="ddgDto.treeNo" type="hidden" id="add_treeNo" value="0">
                          <button id="link_dataDictionary_add_ok"  type="submit" class="btn btn-primary btn-sm"><fmt:message key="common.save"/></button>
				</div>
			</div>
		</form>
		
	</div>
	
	<!-- 添加 数据字典目录DIV end -->
	<!-- 搜索 数据字典目录DIV start -->
	<div id="searchDataDictionaryGroupDiv"  class="WSTUO-dialog" title="<fmt:message key="common.search"/>" style="height:auto; padding:15px; line-height:20px;">
		<form  method="post" class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.role.roleName"/></label>
				<div class="col-sm-5">
					 <input name="reDto.groupName" class="form-control" id="search_groupName" />
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-4 control-label" for="lastname"><fmt:message key="common.code"/></label>
				<div class="col-sm-5">
					<input name="reDto.groupCode" class="form-control"  id="search_groupCode"/>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.type"/></label>
				<div class="col-sm-5">
					<select id="search_groupCode" name="reDto.groupType" class="form-control">
						<option value="">请选择</option>
						<option value="select">下拉框</option>
						<option value="checkbox">多选</option>
						<option value="radio">单选</option>
						<option value="tree">树结构</option>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-9 col-sm-offset-4">
					<button id="link_dataDictionary_search_ok"  type="button" class="btn btn-primary btn-sm"><fmt:message key="common.search"/></button>
				</div>
			</div>
		</form>
	</div>
<!-- 新增  start-->
<div id="addEditDictionaryWindow" class="WSTUO-dialog" title="<fmt:message key="label.dc.addDc"/>" style="width:400px;height:auto">
	
	<form id="addDataDictionaryForm" class="form-horizontal">
		<div class="form-group">
			<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.name"/></label>
			<div class="col-sm-5">
				<input type="text" class="form-control" id="dname" name="dataDictionaryItemsDto.dname" required="true" validType="nullValueValid" placeholder="请输入<fmt:message key="label.name"/>" />
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.common.desc"/></label>
			<div class="col-sm-5">
				<textarea id="description" name="dataDictionaryItemsDto.description" class="form-control" validType="length[1,200]"></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-4 control-label" for="username"><fmt:message key="common.remark"/></label>
			<div class="col-sm-5">
				<textarea id="remark" name="dataDictionaryItemsDto.remark" class=" form-control" validType="length[1,200]"></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-4 control-label" for="email"><fmt:message key="label.color"/></label>
			<div class="col-sm-5">
				<input id="color" name="dataDictionaryItemsDto.color"  value="#ffcc00" class="form-control iColorPicker"/>
			</div>
		</div>

         <input type="hidden" name="dataDictionaryItemsDto.dno" id="dno" />
		 <input id="dcode" name="dataDictionaryItemsDto.dcode" type="hidden"/>
	     <input type="hidden" id="groupCode" name="dataDictionaryItemsDto.groupCode" />
		<div class="form-group">
			<div class="col-sm-9 col-sm-offset-4">
				<button id="saveDataDictionaryBtn" type="button" class="btn btn-primary"><fmt:message key="common.save"/></button>
			</div>
		</div>
	</form>
</div>
<!-- 新增  end-->
<!-- 搜索 数据字典DIV start -->
<div id="searchDataDictionaryDiv"  class="WSTUO-dialog" title="<fmt:message key="common.search"/>" style="width:380px;height:auto">
	<form action="dataDictionaryItems!exportDictionaryItemData.action" method="post" class="form-horizontal">
		<div class="form-group">
			<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.name"/></label>
			<div class="col-sm-5">
				 <input id="dataDictionaryQueryDto_dname" name="dataDictionaryQueryDto.dname" class="form-control " />
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.common.desc"/></label>
			<div class="col-sm-5">
				<input id="dataDictionaryQueryDto_description" name="dataDictionaryQueryDto.description" class="form-control" />
			</div>
		</div>
        <input name="dataDictionaryQueryDto.groupCode" type="hidden" id="searchItem_groupCode"/>
		<div class="form-group">
			<div class="col-sm-9 col-sm-offset-4">
				<button id="searchDataDictionaryBtn"  type="button" class="btn btn-primary btn-sm"><fmt:message key="common.search"/></button>
			</div>
		</div>
	</form>
</div>