<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<sec:authorize url="/pages/user!updateRoleByUserId.action">
<script>
roleResourceAuthorize = true;
</script>
</sec:authorize>
<script src="../js/wstuo/user/user.js?random=<%=new java.util.Date().getTime()%>"></script>
<div id="userMain_layout" class="easyui-layout" fit="true" border="false" style="height:100%">

 <div class="row">

		<div class="box col-md-2">
			<div class="box-inner">
				<div class="box-header well" data-original-title="">
					<h2>
						<i class="glyphicon glyphicon-list-alt"></i> 机构树
					</h2>

					<div class="box-icon">
						<!-- <a href="#" class="btn btn-minimize btn-round btn-default"><i
							class="glyphicon glyphicon-chevron-up"></i></a> -->
					</div>
				</div>
				<div class="box-content buttons" id="user_orgTreeDIV"></div>
			</div>
		</div>
		<!--/span-->

		<div class="box col-md-10">
            <div class="box-inner">
                <div class="box-header well" data-original-title="">
                    <h2><fmt:message key="label.userList" /></h2>
                </div>
                
                 <div class="box-content ">
                <div id="orgMgeTable_div" class="">
						<!-- OrgGrid start -->
						<sec:authorize url="/pages/user!find.action">
							<div title="<fmt:message key="title.orgSettings.gridView" />"  style="padding: 3px;">
							<!-- 列表start -->
							<table id="userGrid"></table>
                            <div id="userPager"></div>
							
						<div id="userToolbarAct" style="background-color: #EEEEEE; height: 28px; display: none;"><sec:authorize url="/pages/user!merge.action"><a href="javascript:wstuo.user.user.editUser_affs('{userId}')"title="<fmt:message key="common.edit" />"><i class="glyphicon glyphicon-edit"></i></a></sec:authorize>&nbsp;&nbsp;&nbsp;<sec:authorize url="/pages/user!delete.action"><a href="javascript:wstuo.user.user.opt_delete_aff('{userId}')" title="<fmt:message key="common.delete" />"><i class="glyphicon glyphicon-trash"></i></a></sec:authorize></div>
						
						<div id="userTopMenu" style="background-color: #EEEEEE; height: 28px; display: none;">
							<div class="panelBar">
								<sec:authorize url="/pages/user!save.action">
									<button class="btn btn-default btn-xs" id="userGrid_add" onclick="wstuo.user.user.user_add(true)">
										<i class="glyphicon glyphicon-plus"></i>&nbsp;添加
									</button>
								</sec:authorize>

								<sec:authorize url="/pages/user!merge.action">
									<button class="btn btn-default btn-xs" id="userGrid_edit">
										<i class="glyphicon glyphicon-edit"></i>&nbsp;修改
									</button>
								</sec:authorize>
						        <sec:authorize url="/pages/user!mergeByOrg.action"> 
									<button class="btn btn-default btn-xs"  id="userGrid_org" >
									  <i class=" glyphicon glyphicon-indent-left"></i>&nbsp;<fmt:message key="label.user.orgSetting" />
									</button>
								</sec:authorize>
								<sec:authorize url="/pages/user!delete.action">
									<button class="btn btn-default btn-xs" id="userGrid_delete">
										<i class="glyphicon glyphicon-trash"></i>&nbsp;删除
									</button>
								</sec:authorize>
								
								<sec:authorize url="/pages/user!find.action">
								    <button class="btn btn-default btn-xs" id="userGrid_search" >
								    <i class="glyphicon glyphicon-search"></i>&nbsp;<fmt:message key="common.search" />
								    </button>
								</sec:authorize>
								
								<sec:authorize url="userGrid_import">
									<button class="btn btn-default btn-xs" id="userGrid_Import">
										<i class="glyphicon glyphicon-import"></i>&nbsp;<fmt:message key="label.dc.import" />
									</button>
								</sec:authorize>
								<sec:authorize url="userGrid_export">
									<button class="btn btn-default btn-xs" id="userGrid_Export">
										<i class="glyphicon glyphicon-share"></i>&nbsp;导出
									</button>
								</sec:authorize>
							</div>
						</div>
					</div>
						</sec:authorize>
					</div>
            </div>
                
            </div>
        </div>
    </div><!--/span-->

<!-- 添加用户 start -->
<div id="userWindow" class="WSTUO-dialog" title="<fmt:message key="title.user.addOrEditUser" />" style="width:700px; height:auto;padding:3px">
<form id="userWindowForm" event="wstuo.user.user.addUser">
			<div class="box-content">
				<ul class="nav nav-tabs" id="myTab">
					<li class="active"><a href="#basic"><fmt:message key="common.basicInfo" /></a></li>
					<li ><a disabled="true" href="#contact"><fmt:message key="title.user.contactInfo" /></a></li>
					<li><a href="#userrole"><fmt:message key="title.user.userRole" /></a></li>
				</ul>

				<div id="myTabContent" class="tab-content" class="form-inline">
						   <!-- basic start -->
					<div class="tab-pane active" id="basic" style="padding-top: 15px;">
							<div class="panel panel-default">
							      <div class="panel-body form-horizontal">
									<div class="form-group">
										<label class="col-sm-4 control-label" for="firstname" ><fmt:message key="label.user.loginName" /></label>
										<div class="col-sm-5">
											  <input name="userDto.loginName"id="loginName" readonly="readonly" class="form-control"  required="true" />
										</div>
									</div>
		
									<div class="form-group" id="resetPassword_tr">
										<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.loginPassword" /></label>
										<div class="col-sm-5">
											 [<a href="#" style="cursor: pointer;" id="link_user_password_reset"><fmt:message key="common.reset" /></a>] 
									         [<a href="#" style="cursor: pointer;" id="link_edit_user_pwd"><fmt:message key="label.edit.user.password" /></a>]
										</div>
									</div>
									
									<div class="form-group" id="password_tr">
										<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.loginPassword" /></label>
										<div class="col-sm-5">
											<input type="password" name="userDto.password" validType="useEnCharPwd" id="password" class="form-control" <c:if test="${passwordType ne 'Simple'}">validType="passwordsecurity"</c:if> required="true" />
										</div>
									</div>
									
									<div class="form-group" id="rpassword_tr">
										<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.comfirmLoginPassword" /></label>
										<div class="col-sm-5">
											<input type="password" id="rpassword" name="rpassword"  class="form-control" equalTo="#password" required="true" />
										</div>
									</div>
									
									<div class="form-group" >
										<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.userLastName" /></label>
										<div class="col-sm-5">
											<input name="userDto.firstName" id="firstName" class="form-control" required="true" />
										</div>
									</div>
									
									<div class="form-group" >
										<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.userFirstName" /></label>
										<div class="col-sm-5">
											 <input name="userDto.lastName" id="lastName" class="form-control" />
										</div>
									</div>
									
									<div class="form-group" >
										<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.userOwnerOrg" /></label>
										<div class="col-sm-5">
											  <input id="user_orgName"name="orgName" readonly="readonly"class="choose form-control" required="true"  /> 
								              <input type="hidden" name="userDto.orgNo" id="user_orgNo" />
										</div>
									</div>
									
									<div class="form-group" >
										<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.technical.userOwnerGroup" /></label>
										<div class="col-sm-5">
											  <div id="belongsGroupShowId"></div>
									          <a href="#" id="selectBelongsGroupBut" style="text-decoration: underline;"><fmt:message key="common.pleaseSelect" /></a>
										</div>
									</div>
									
									<div class="form-group" >
										<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.userTitle" /></label>
										<div class="col-sm-5">
											  <input name="userDto.job" id="job" class="form-control" />
										</div>
									</div>
									
									<div class="form-group" >
										<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.position" /></label>
										<div class="col-sm-5">
											  <input name="userDto.position" id="user_position" class="form-control" />
										</div>
									</div>
									
									<div class="form-group" >
										<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.userState" /></label>
										<div class="col-sm-5">
											     <input type="radio" name="userDto.userState" id="userState_true" value="true" checked="checked" />
											     <fmt:message key="common.enable" />
											     <input type="radio" name="userDto.userState" id="userState_flase" value="false" />
											     <fmt:message key="common.disable" />
										</div>
									</div>
									
									<div class="form-group" >
										<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.sex" /></label>
										<div class="col-sm-5">
											     <input type="radio" name="userDto.sex" id="user_sex_man" value="true" checked="checked" />
											     <fmt:message key="label.user.man" />
											     <input type="radio" name="userDto.sex" id="user_sex_woman" value="false" />
											     <fmt:message key="label.user.woman" />
										</div>
									</div>
									
									<div class="form-group" >
										<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.holidayStatus" /></label>
										<div class="col-sm-5">
											     <input type="radio" name="userDto.holidayStatus" id="user_holidayStatus_false" value="false" checked="checked" />
											     <fmt:message key="label.user.duty.ing" />
											     <input type="radio" name="userDto.holidayStatus" id="user_holidayStatus_true" value="true" />
											     <fmt:message key="label.user.holiday.ing" />
										</div>
									</div>
									<div class="form-group ">
										<div class="col-xs-6 col-md-4 col-center-block" >
											<button type="reset" class="btn btn-default btn-sm "><fmt:message key="i18n.reset"/></button>
				                            <button id="link_dataDictionary_add_ok"  type="button" class="btn btn-primary btn-sm" onclick="wstuo.user.user.validNextPage(1)"><fmt:message key="label.next"/></button>
										</div>
									</div>
									<%-- <div class="form-group">
										<div class="col-sm-9 col-sm-offset-4">
											<input name="ddgDto.groupNo" type="hidden" id="add_groupNo">
				                            <button id="link_dataDictionary_add_ok"  type="button" class="btn btn-primary btn-sm"><fmt:message key="common.save"/></button>
										</div>
									</div> --%>
							</div>
					</div>  
				</div>
			    <!-- basic end -->
			    <!-- contact start -->
				<div class="tab-pane" id="contact" style="padding-top: 15px;">
					<div class="panel panel-default">
						<div class="panel-body form-horizontal">
									<div class="form-group">
										<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.user.icCard"/></label>
										<div class="col-sm-5">
											  <input name="userDto.icCard" id="user_icCard" class=" form-control" /> 
										</div>
									</div>
		
									<div class="form-group">
										<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.pinyin"/></label>
										<div class="col-sm-5">
											 <input name="userDto.pinyin" id="user_pinyin" class=" form-control" validType="chars" /> 
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.birthday"/></label>
										<div class="col-sm-5">
											<input name="userDto.birthday" id="user_birthday" class=" form-control" validType="gtNowTimeReg" readonly />
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.email"/></label>
										<div class="col-sm-5">
											 <input name="userDto.email" id="email" class="form-control" type="email" /> 
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.mobile"/></label>
										<div class="col-sm-5">
											 <input name="userDto.moblie" id="moblie" class="form-control"  validType="mobile" />  
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.phone" /></label>
										<div class="col-sm-5">
											 <input name="userDto.phone" id="phone"  class="form-control"  validType="phone" />   
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label_extension" /></label>
										<div class="col-sm-5">
											 <input name="userDto.extension" id="extension"  class=" form-control"/>  
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.fax"/></label>
										<div class="col-sm-5">
											 <input name="userDto.msn" id="msn" class=" form-control" />   
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.qqOrMsn" /></label>
										<div class="col-sm-5">
											 <input name="userDto.fax" id="fax"   class=" form-control"/>  
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.workAddress" /></label>
										<div class="col-sm-5">
											 <input name="userDto.officeAddress" id="officeAddress" class=" form-control" />
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.description" /></label>
										<div class="col-sm-5">
											 <textarea name="userDto.description" id="description"  class=" form-control" /></textarea>  
										</div>
									</div>
									<div class="form-group ">
										<div class="col-xs-6 col-md-5 col-center-block" >
											<button  type="reset" class="btn btn-default btn-sm "><fmt:message key="i18n.reset"/></button>
											<button  type="button" class="btn btn-sm" onclick="wstuo.user.user.validAbovePage(0)"><fmt:message key="label.above" /></button>
				                            <button  type="button"  class="btn btn-primary btn-sm" onclick="wstuo.user.user.validNextPage(2)"><fmt:message key="label.next"/></button>
										</div>
									</div>						
								</div>
							</div>  
						</div>
			        <!-- contact end -->
			        <!-- userrole start -->
					<div class="tab-pane" id="userrole">

						<sec:authorize url="/pages/user!updateRoleByUserId.action">
							<div title="<fmt:message key="title.user.userRole" />">
								<div id="roleSet" class="lineTableBgDiv" style="width: 92%">
									<table style="width: 100%" class="lineTable" cellspacing="1"></table>
								</div>
							</div>
						</sec:authorize>
						<div class="col-xs-6 col-md-6 col-center-block" >
								<button  type="reset" class="btn btn-default btn-sm "><fmt:message key="i18n.reset"/></button>
								<button  type="button" class="btn btn-sm" onclick="wstuo.user.user.validAbovePage(1)"><fmt:message key="label.above" /></button>
	                            <button  type="submit"  class="btn btn-primary btn-sm" onclick="wstuo.user.user.validNextPage(2)"><fmt:message key="Save"/></button>
						</div>
					</div>
					<!-- userrole end -->
				</div>
			</div>

				<input type="hidden" name="userDto.userId" value="0" id="userId" />
				<input type="hidden" name="userDto.companyNo" id="user_companyNo" />
			<%-- <div class="saveuser_button ">
				<button id="saveuser" type="submit"   class="btn btn-primary btn-sm">
					<fmt:message key="common.save" />
				</button>
			</div> --%>
		</form>
  </div>
  <!-- 添加用户 end -->
  <!-- 弹出所属机构树start -->
  <div id="userGroup_win" class="WSTUO-dialog" title="<fmt:message key="label.user.userOwnerOrg" />" style="max-height:400px;overflow:auto;width:240px;height:auto;padding:3px">
  <div id="userGroupTree"></div>
   <!-- 弹出所属机构树end -->
</div>


<!-- start修改用户密码 -->
<div id="editUserPwd_win" class="WSTUO-dialog" title="<fmt:message key="label.edit.user.password" />" style="width:380px;height:190px;padding:3px;">
	<form>
	
	<div class="lineTableBgDiv" >
		<table  class="lineTable" width="100%" cellspacing="1" >
			<tr>
				<td><fmt:message key="label.user.loginName" /></td>
				<td><span id="editUserPwd_loginName_span"></span>
				<input type="hidden" id="editUserPwd_loginName" name="editUserPasswordDTO.loginName" />
				<input type="hidden" id="editUserPwd_oldPassword" name="editUserPasswordDTO.oldPassword" />
				</td>
			</tr>
			<tr>
				<td ><fmt:message key="label.new.password" /></td>
				<td><input type="password" id="editUserPwd_newPassword" validType="useEnCharPwd" name="editUserPasswordDTO.newPassword" class="easyui-validatebox input"  <c:if test="${passwordType ne 'Simple'}">validType="passwordsecurity"</c:if>  required="true"/></td>
			</tr>
			<tr>
				<td ><fmt:message key="label.user.comfirmLoginPassword" /></td>
				<td><input type="password" id="editUserPwd_repeatPassword" class="easyui-validatebox input" validType="equalTo['editUserPwd_newPassword']" required="true"/></td>
			</tr>
			
			<tr>
				<td colspan="2" align="center" ><button type="button" class="btn btn-primary btn-sm" id="edit_user_pwd_but" ><fmt:message key="common.update" /></button></td>
			</tr>
		</table>
	</div>
	</form>
</div>
<!-- end修改用户密码 -->


<!-- 组织设置 -->
<div id="orgSet-win" class="WSTUO-dialog" title="<fmt:message key="title.user.userOwnerOrgSetting" />" style="width:400px; height:auto">
	<div id="orgSetInfo" style="text-align:center" class="lineTableBgDiv">
		<form style="margin:0px">
		
			<table style="width:100%" class="lineTable" cellspacing="1">
				<tr>
					<td style="width:35%;height:32px">
					<fmt:message key="label.user.userOwnerOrg" />
					</td>
					<td style="width:65%">
					<input id="orgSet_orgName" name="orgName1" class="form-control" style="width:96%;cursor:pointer" readonly/>
					<input type="hidden" name="orgNo1" id="ortSet_orgNo" />
					<input type="hidden" name="userId1" id="userId1" />
					</td>
				</tr>
				<tr>
					<td style="text-align:left;height:32px" colspan="2">
					<button type="button" class="btn btn-primary btn-sm" id="link_orgSet_save"><fmt:message key="common.save" /></button> 
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>
<!--end组织设置 -->

<!-- 搜索用户 -->
<div id="searchUser" class="WSTUO-dialog" title="<fmt:message key="title.user.searchUser" />" style="width:400px;height:auto">
    <div class="lineTableBgDiv" id="searchUserForm">
      <div class="panel panel-default">
					<div class="panel-body">
						<form action="user!exportUser.action" method="post" class="form-horizontal">
							<div class="form-group">
								<label class="col-sm-4 control-label" for="firstname"><fmt:message key="label.orgSettings.ownerOrg" /></label>
								<div class="col-sm-5">
									<input id="user_search_orgName" class="form-control"  readonly />
									<input type="hidden" name="userQueryDto.orgNo" id="user_search_orgNo" />
									<a class="easyui-linkbutton" plain="true" icon="icon-clean" onclick="cleanIdValue('user_search_orgNo','user_search_orgName')" title="<fmt:message key="label.request.clear" />"></a>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.technical.userOwnerGroup" /></label>
								<div class="col-sm-5">
									<input id="search_user_belongsGroup" name="userQueryDto.belongsGroup" class="form-control"  readonly/>
						            <a class="easyui-linkbutton" plain="true" icon="icon-clean"  onclick="cleanIdValue('search_user_belongsGroup')" title="<fmt:message key="label.request.clear" />"></a>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label" for="lastname"><fmt:message key="title.orgSettings.role" /></label>
								<div class="col-sm-5">
									<select id="search_user_roles" name="userQueryDto.roleNo" class="form-control" ></select>
								</div>
							</div>
							
							<div class="form-group" id="search_user_state_tr" style="display: none">
								<label class="col-sm-4 control-label" for="lastname"><fmt:message key="common.state" /></label>
								<div class="col-sm-5">
									<select id="search_user_state" name="state" class="form-control" >
										<option value="all">-- <fmt:message key="common.pleaseSelect" />--</option>
										<option value="true"><fmt:message key="common.enable" /></option>
										<option value="false"><fmt:message key="common.disable" /></option>
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.loginName" /></label>
								<div class="col-sm-5">
									<input name="userQueryDto.loginName" id="user_loginName" class="form-control"  onblur="javascript:wstuo.user.user.trimLoginName()"/>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.userLastName" /></label>
								<div class="col-sm-5">
									<input name="userQueryDto.firstName" class="form-control"  />
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.userFirstName" /></label>
								<div class="col-sm-5">
									<input name="userQueryDto.lastName" class="form-control"  />
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.email" /></label>
								<div class="col-sm-5">
									<input name="userQueryDto.email" class="form-control"  />
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.mobile" /></label>
								<div class="col-sm-5">
									<input name="userQueryDto.mobilePhone" class="form-control" />
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label" for="lastname"><fmt:message key="label.user.phone" /></label>
								<div class="col-sm-5">
									<input name="userQueryDto.officePhone" class="form-control"  />
								</div>
							</div>
							
								<input type="hidden" name="userQueryDto.companyNo" id="user_search_companyNo" />
			                    <input type="hidden" name="userQueryDto.limit" id="limit" value="45" />
			                    <input type="hidden" name="userQueryDto.orgType" id="search_user_orgType" value=""/>
							<div class="form-group">
								<div class="col-sm-9 col-sm-offset-4">
		                            <button type="button" class="btn btn-default btn-sm" onclick="$('#searchUserForm input').val('');$('#search_user_roles').val('0');$('#search_user_state').val('all');"  >
									   <fmt:message key="i18n.reset"/>
									</button>
									<button type="button" class="btn btn-primary btn-sm" id="user_link_search">
									    <fmt:message key="common.search" />
									</button>
								</div>
							</div>
					</form>
				</div>
		</div>  
    </div>
</div>
<!-- end搜索用户 -->
<style>
.col-center-block {
    float: none;
    display: block;
    margin-left: auto;
    margin-right: auto;
}
.saveuser_button{
width: 100%;
padding-right: 10px;
padding-bottom: 5px;
}
#saveuser{
float: right;
}
</style>