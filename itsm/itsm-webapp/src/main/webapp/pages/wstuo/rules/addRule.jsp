<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="../../language.jsp" %>

<script src="../js/wstuo/jbpmMge/processUse.js"></script>
<script src="../js/wstuo/rules/addRule.js?random=<%=new java.util.Date().getTime()%>"></script>

<script>
var _rulePackageNo = "${param.rulePackageNo}";
var _flag = "${param.flag}";
var approveGrid='<fmt:message key="label.sla.ruleList" />';
if(_flag=='requestFit' || _flag=='requestProce'){
	approveGrid='<fmt:message key="label.rulePackage.requestRule" />';
}else if(_flag=='mailToRequest'){
	approveGrid='<fmt:message key="title.rule.mailToRequest" />';
}else if(_flag=='changeApproval' || _flag=='changeProce'){
	approveGrid='<fmt:message key="title.rule.changeRules" />';
}
</script>

<c:if test="${param.flag =='requestFit' or param.flag=='requestProce' }">
	<c:set var="patternType" value="RequestDTO" />
</c:if>

<c:if test="${param.flag =='mailToRequest' }">
	<c:set var="patternType" value="RequestDTO" />
</c:if>

<c:if test="${param.flag =='changeApproval' or param.flag=='changeProce' }">
	<c:set var="patternType" value="ChangeDTO" />
</c:if>

<div class="tab-pane active" >
	<div class="box-inner">
		<div class="box-header well" data-original-title="">
	        <h2><i class="glyphicon glyphicon-list"></i>&nbsp;
	        
	        <c:if test="${param.flag =='mailToRequest' }">
				<fmt:message key="common.add" /><fmt:message key="title.rule.mailToRequest" />
			</c:if>
			<c:if test="${param.flag =='requestFit' }">
				<fmt:message key="common.add" /><fmt:message key="label.rulePackage.requestRule" />
			</c:if>
	        </h2>
        </div>

	<div id="addRule_content" class="content" border="none" fit="true" style="height: 98%;">

		<!-- 新的面板 start-->
		<div id="flowPropertyMainContent_layout"  >

			<!-- 左边规则信息start -->
			
			<div class="row">
			<div class="box col-md-4" >
			<div class="box-inner" style="padding-left: 5px;">
		        <div class="box-content" id="flowPropertyMain_west" region="west" split="true">	        
            		<div class="box-header well" data-original-title="">
			       		<h2><fmt:message key="common.basicInfo" /></h2>
			      	  	<div class="box-icon"></div>
		        	</div>	
		           	<form id="addRuleform">
						<c:if test="${param.flag=='mailToRequest' }">
						<input type="hidden" name="rule.rulePackage.rulePackageNo" id="Proce_rulePackageNo" value="${param.rulePackageNo}" /> 
						</c:if>
						<input type="hidden" name="rule.dialect" value="mvel">
						<input type="hidden" name="rule.condition.patternBinding" value="dto">
						<input type="hidden" name="rule.condition.patternType" value="${patternType} ">
					
						<div>
							<table style="width:100%;" class="table table-bordered" cellspacing="1">
								<tr>
									<td style="width: 20%;"><h5 style="color: black;"><fmt:message key="label.rule.ruleName" /></h5></td>
									<td style="width: 80%"><input name="rule.ruleName" class="form-control" validType="length[1,200]" required="true" id="addRule_Name" style="width:90%"/></td>
								</tr>
								<tr>
									<td style="width: 20%"><h5 style="color: black;"><fmt:message key="label.rule.salience" /></h5></td>
									<td style="width: 80%">
									
										<select name="rule.salience" id="ruleSalience" style="width:90%" class="form-control">
											
										</select>
									</td>
								</tr>
								<tr>
									<td style="width: 20%;"><h5 style="color: black;"><fmt:message key="label.rule.ruleDecription" /></h5></td>
									<td><textarea name="rule.description" class="form-control" validType="length[1,200]" required="true" style="width:90%"></textarea></td>
								</tr>
								<c:if test="${param.flag!='mailToRequest'}">
								<tr>
									<td style="width: 20%;"><h5 style="color: black;"><fmt:message key="lable.com.drools.rule.pages" /></h5></td>
									<td><input class="input choose" type="hidden" id="Proce_ruleFlagName"/>
										<input class="input choose" name="rule.rulePackage.rulePackageNo" type="hidden" id="Proce_rulePackageNo"/>
										<input class="form-control" id="Proce_rulePackageName" readonly="readonly" style="width:80%;float: left;"/>
										<h5 style="float: left;padding-left: 8px;">
										<a  class="glyphicon glyphicon-trash" plain="true" href="javascript:wstuo.rules.addRule.cleanIdValue('rulePackageNo','rulePackageName')" title="<fmt:message key="label.request.clear" />"></a>
										</h5>
									</td>									
								</tr>
								</c:if>
							</table>
						</div>
					</form>		               	
		        </div>		          
		      </div>
		    </div>		
		    <!-- 左边规则信息 end-->
		    <!-- 右边详细信息（规则集，动作集） start -->
		    <div class="box col-md-8">
        		<div class="box-inner">	
        		<div class="box-header well" >
			        <h2><fmt:message key="common.detailInfo" /></h2>
		        </div>			
				<div class="box-content">
					<ul class="nav nav-tabs" id="myTab">
						<li class="active"><a href="#list1"><span><fmt:message key="title.rule.ruleSet" /></span></a></li>	
						<li><a href="#list2"><span><fmt:message key="title.rule.actionSet" /></span></a></li>	
	             	</ul>
             		<div id="myTabContent" class="tab-content">
						<div region="center" class="tab-pane active" id="list1">
							<form>
							<div style="height:120px;width:100%">
								<div style="margin-top: 10px;"><b style="margin-top: 8px;"><fmt:message key="title.rule.addOrEdirRuleSet" /></b></div>
								<hr style="margin-top: 3px;margin-bottom: 5px;">	
								<table style="width:100%;" cellspacing="1">									
									<tr height="30px">
										<td style="white-space:nowrap;width:40px;">&nbsp;&nbsp;&nbsp;<fmt:message key="label.rule.ruleSetDetail" /></td>
										<td style="text-align:center;">
											<table style="width:100%;">
												<tr>
												<td style="width:20%;">
													<select id="addRule_team" class="form-control" style="width:90%"></select>
												</td>
												<td style="width:20%;">
													
													<select id="addRule_matical" class="form-control" style="width:90%"></select>
													
												</td>
												<td style="width:30%;">
													<div id="addRule_propertyValueDIV" >
														<input id="addRule_propertyValue" style="width:80%" size="30" value="" class="form-control"/>
													</div>
												</td>
												
												<td style="width:25%">
													<a class="btn btn-primary btn-sm" plain="true" id="addRule_addRulesToList" style="margin-right: 15px;min-width:50px;"><fmt:message key="label.rule.confirm" /></a>
												</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr height="30px">
										<td style="width:25%">&nbsp;&nbsp;&nbsp;<fmt:message key="lable.customFilter.relation" /></td>
										<td style="width:75%">	
										<br/>										
	                						<label class="radio-inline">
												<input id="add_rule_or" type="radio" name="orand" checked/>or
											</label>
	               							<label class="radio-inline">
												<input id="add_rule_and" type="radio" name="orand"  />and
											</label>    
										</td>
									</tr>
								</table>																		
							</div>
							<hr/>
							<div style="max-height:300px;width:auto;overflow:auto;">								
								<table id="addRule_constraintsTable"  style="width:100%;" class="table table-bordered" cellspacing="1">
									<thead>
									<tr height="30px" ><th align="left" colspan="4"><fmt:message key="title.rule.ruleSet" /></th></tr>
									<tr>
										<td style="width: 20%; font-weight: bold; text-align: center; "><fmt:message key="label.rule.condition" /></td>
										<td style="width: 40%; font-weight: bold; text-align: center; "><fmt:message key="label.rule.ruleContent" /></td>
										<td style="width: 20%; font-weight: bold; text-align: center; "><fmt:message key="lable.customFilter.relation" /></td>
										<td style="width: 20%; font-weight: bold; text-align: center; "><fmt:message key="label.rule.operation" /></td>
									</tr>
									</thead>
									<tbody>
									
									</tbody>
								</table>
							</div>
						</form>
					</div>
					<%--  规则集end --%>
					<%--  动作集start --%>
					<c:if test="${param.tag!='autoUpdate'}">
	                <div class="tab-pane" id="list2">
	                	<form>
						<div style="height: 100px;width:auto">
							<div style="margin-top: 10px;"><b><fmt:message key="title.rule.addOrEditActionSet" /></b></div>							
							<hr style="margin-top: 3px;margin-bottom: 5px;">	
							<table style="width:100%"  cellspacing="1" >						
								<tr style="height:30px;">
									<td style="width: 25%">&nbsp;&nbsp;&nbsp;<fmt:message key="label.rule.actionType" /></td>
									<td style="width: 75%">
										<select id="addRule_executeAction" style="width: 170px;" class="form-control"></select>
									</td>
								</tr>
								<tr style="height:30px;">
									<td>&nbsp;&nbsp;&nbsp;<fmt:message key="label.rule.actionDetail" /></td>
									<td>
										<br/>
										<table style="width:100%;">
											<tr>
												<td style="width:60%;">
													<div id="givenValueDIV" >
														<input style="width:170px;" id='givenValue' class="form-control"/>													
													</div>
												</td>
												<td style="width:40%">
													<a class="btn btn-primary btn-sm" plain="true" id="addRule_addActionToList" style="margin-right:15px;min-width:50px;"><fmt:message key="label.rule.confirm" /></a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</div>
						<hr>
						<div style="max-height:300px;width:auto;overflow:auto;">
							<div >
								<table id="addRule_actionTable" style="width:100%" class="table table-bordered" cellspacing="1">
									<tr height="30px" ><th align="left" colspan="3"><fmt:message key="title.rule.actionSet" /></th></tr>
									<tr>
										<td style="width: 25%; font-weight: bold; text-align: center; "><fmt:message key="label.rule.actionType" /></td>
										<td style="width: 50%; font-weight: bold; text-align: center; "><fmt:message key="label.rule.actionDetail" /></td>
										<td style="width: 25%; font-weight: bold; text-align: center; "><fmt:message key="label.rule.operation" /></td>
									</tr>
								</table>
							</div>
						</div>
						</form>
	                </div>  
	                <%--  动作集end --%>
					</c:if>              
                    </div>
				</div>
     
          		</div>
			</div>
			<!-- 右边详细信息（规则集，动作集） end -->
		</div>
		</div>
</div>


<%--选择审批成员 --%>
	<div id="selectRuleCABMember" class="WSTUO-dialog" title="<fmt:message key="title.selectCabMember" />" style="width:520px;padding: 2px;">
	 	<div align="center"><span id="selectRuleCABMember_tip" style="color:red;font-size:14px"></span></div>
	 	<div style="width:500px;">
	 	<table id="selectRuleCABMemberGrid"></table>
		<div id="selectRuleCABMemberPager"></div>
		</div>
	 </div>
<!-- 选择流程列表-->
<div id="selectRuleMatchRuleSelectWin" title='<fmt:message key="lable.com.drools.drl.proces.List"/>' style="width:630px;height:380px;padding:3px;display: none;">
	<table id="matchRuleSelectgrid"></table>
	<div id="matchRuleSelectgridPager"></div>
</div>
<div id="selectRuleCABMemberToolBar" style="display: none;">&nbsp;&nbsp;CAB:&nbsp;<select id="cabMemberRuleOption" onchange="itsm.change.changeApproval.selectRuleChange(this.value)"></select>
	&nbsp;&nbsp;&nbsp;&nbsp;
	<a class="btn btn-primary btn-sm" plain="true" onclick="itsm.change.changeApproval.select_approval_rule_aff_multi()"><fmt:message key="common.select"/></a>
</div>
 	<div border="false" style="overflow: hidden; height: 35px; padding: 4px; text-align:center;">
		<button class="btn btn-primary btn-sm" plain="true" style="margin-right: 15px" id="addRule_saveRuleBtn" title="<fmt:message key="common.save" />"><fmt:message key="common.save" /></button>
		<button class="btn btn-primary btn-sm" plain="true"  style="margin-right: 15px" id="addRule_returnRuleGrid" title="<fmt:message key="common.returnToList" />"><fmt:message key="common.returnToList" /></button>
	</div>
	<br/> 
</div>
</div>

