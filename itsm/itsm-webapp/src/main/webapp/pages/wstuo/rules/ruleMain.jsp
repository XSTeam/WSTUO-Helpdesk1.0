<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>

<script>
var _flag = '${param.flag}';
var _rulePackageNo = '${param.rulePackageNo}';

</script>
<script src="../js/wstuo/rules/ruleMain.js"></script>

<span id="rulsPackageId"></span>
<div class="row">
<div class="tab-pane active" >
	<div class="box-inner">
		<div class="box-header well" data-original-title="">
	        <h2><i class="glyphicon glyphicon-list"></i>&nbsp;
	        <c:if test="${param.flag =='mailToRequest' }">
				<fmt:message key="title.rule.mailToRequest" />
			</c:if>
			<c:if test="${param.flag =='requestFit' }">
				<fmt:message key="label.rulePackage.requestRule" />
			</c:if>	        
	        </h2>
        </div>
	<div class="box-content " id="ruleMain_content" style="padding:3px;">
		<table id="rulesGrid" style="width: 99%;"></table>
		<div id="rulesGridPager" ></div>
		<div id="rulesGridToolbar" style="display: none">
			<div class="panelBar">	
				<sec:authorize url="/pages/callBusinessRuleSet!save.action">
				<a class="btn btn-default btn-xs" href="javascript:wstuo.rules.ruleMain.showAddRule()" title="<fmt:message key="common.add" />"><i class="glyphicon glyphicon-plus"></i>&nbsp;<fmt:message key="common.add" /></a>
				</sec:authorize>
				
				<sec:authorize url="/pages/callBusinessRuleSet!merge.action">
				<a class="btn btn-default btn-xs"  href="javascript:wstuo.rules.ruleMain.showEditRule()" title="<fmt:message key="common.edit" />"><i class="glyphicon glyphicon-edit"></i>&nbsp;<fmt:message key="common.edit" /></a> 
				</sec:authorize>
				
				<sec:authorize url="/pages/callBusinessRule!delete.action">
				<a class="btn btn-default btn-xs" href="javascript:wstuo.rules.ruleMain.delRules()" title="<fmt:message key="common.delete" />"><i class="glyphicon glyphicon-trash"></i>&nbsp;<fmt:message key="common.delete" /></a> 
				</sec:authorize>
				
				<sec:authorize url="YUWU_GUEIZHESHEZHI">
				<a class="btn btn-default btn-xs" href="javascript:wstuo.rules.ruleMain.openSearchRules()" title="<fmt:message key="common.search" />"><i class="glyphicon glyphicon-search"></i>&nbsp;<fmt:message key="common.search" /></a>
				</sec:authorize>
				<%--
				&nbsp;&nbsp;<a id="requestRuleImport"><fmt:message key="label.dc.import" /></a>
				 --%> 
				<a id="requestRuleExport" style="display: none;" class="btn btn-default btn-xs" title="<fmt:message key="label.dc.export" />"><i class="glyphicon glyphicon-circle-arrow-down"></i>&nbsp;<fmt:message key="label.dc.export" /></a>
			</div>
		</div>
	</div>
	</div>
	
	<!-- 搜索 -->
	<div id="searchRuleDiv"  class="WSTUO-dialog" title="<fmt:message key="title.rule.searchRule" />" style="width:400px;height:auto">
		<form>
			<table style="width:100%" class="table" cellspacing="1">
        		<tr>
            		<td width="100px"><fmt:message key="label.rule.ruleName" /></td>
            		<td><input name="ruleQueryDTO.ruleName" id="ruleQueryDTO_ruleName" class="form-control" /></td>
       			</tr>
       			<tr>
            		<td width="100px"><fmt:message key="label.rule.ruleDecription" /></td>
           			<td><input name="ruleQueryDTO.description" id="ruleQueryDTO_description" class="form-control" /></td>
        		</tr>
        		<tr>
           			<td colspan="2" >	           				
                		<input type="button" id="rulesGrid_doSearch" class="btn btn-primary btn-sm" value="<fmt:message key="common.search" />" />	        		
        			</td>
       			</tr>
    		</table>
		</form>
	</div>

	<!-- 选择服务机构 -->
	<div id="ruleSelectServiceOrgDiv" title="<fmt:message key="title.rule.chooseServiceOrg" />" class="WSTUO-dialog" style="width:200px;height:280px; padding:10px; line-height:20px;" >
		<div id="ruleSelectServiceOrg"></div>
	</div>
	<%-- 规则导入数据 --%>
	<div id="index_import_drl_window"  class="WSTUO-dialog" title="<fmt:message key="label.dc.import"/>" style="width:400px;height:auto">
		<form>
	   		<div class="lineTableBgDiv" >
	    		<table  class="lineTable"  width="100%" cellspacing="1"> 
	        		<tr>
	            		<td width="100px"><fmt:message key="label.dc.filePath"/></td>
	           			<td>
	           				<input type="file" id="importFile_drl" name="importFile" class="easyui-validatebox input" required="true" onchange="checkFileType(this,'drl')"/>
	           			</td>
			        </tr>
			        <tr>
			            <td colspan="2">
			                <div style="padding-top:8px;">
			                	<a id="index_import_drl_confirm" class="btn btn-primary btn-sm"><fmt:message key="label.dc.import"/></a>
			                </div>
			        	</td>
			        </tr>
	    		</table>
	    	</div>
    	</form>
	</div>
	<%--选择审批成员 --%>
	<div id="selectMatchRule" class="WSTUO-dialog" title="<fmt:message key="label.sla.selectSLA" />" style="width:520px;padding: 2px;">
	 	<div align="center"><span id="selectMatchRule_tip" style="color:red;font-size:14px"></span></div>
	 	<div style="width:500px;">
		 	<table id="selectMatchRuleGrid"></table>
			<div id="selectMatchRulePager"></div>
		</div>
	 </div>
</div>
</div>






