<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<script type="text/javascript">
var _flag = '';
var _rulePackageNo = '';
</script>
<script src="../js/wstuo/slaMge/slaMain.js?random=<%=new java.util.Date().getTime()%>"></script>
<script>
var slaBaseInfo='<fmt:message key="title.sla.slaDetail"/>';</script>

<!-- <div style="padding:10px;color:#555555" id="contarctManage_loading"><img src="../images/icons/loading.gif" /></div>
 -->	
<div class="tab-pane active" >
	<div class="box-inner">
		<div class="box-header well" data-original-title="">
	        <h2><i class="glyphicon glyphicon-list"></i>&nbsp;SLA服务管理</h2>
	        <div class="box-icon"></div>
        </div>
		<div id="contarctManage_content" class="box-content">
			<table id="contractGrid" ></table>
			<div id="contractGridPager"></div>		
			<div id="contractGridToolbar"  style="display: none">
				<div class="panelBar">	
					<sec:authorize url="/pages/slaContractManage!save.action">
					<a class="btn btn-default btn-xs" id="contractGrid_add" ><i class="glyphicon glyphicon-plus"></i>&nbsp;<fmt:message key="common.add" /></a>				
					</sec:authorize>
					
					<sec:authorize url="/pages/slaContractManage!merge.action">
					<a class="btn btn-default btn-xs" id="contractGrid_edit" ><i class="glyphicon glyphicon-edit"></i>&nbsp;<fmt:message key="common.edit" /></a>
					</sec:authorize>
					
					<sec:authorize url="/pages/slaContractManage!delete.action">
					<a class="btn btn-default btn-xs" id="contractGrid_delete" ><i class="glyphicon glyphicon-trash"></i>&nbsp;<fmt:message key="common.delete" /></a>
					</sec:authorize>
					
					<sec:authorize url="/pages/slaContractManage!find.action">
					<a class="btn btn-default btn-xs" id="contractGrid_search"><i class="glyphicon glyphicon-search"></i>&nbsp;<fmt:message key="common.search" /></a>
					</sec:authorize>
					<sec:authorize url="SLA_DC_EXPROT">
					<a class="btn btn-default btn-xs" id="SLAExport"><i class="glyphicon glyphicon-export"></i>&nbsp;<fmt:message key="label.dc.export" /></a>
					</sec:authorize>
					<sec:authorize url="/pages/slaRule!find.action">
					<a class="btn btn-default btn-xs" id="contractGrid_showSLADetail"><i class="glyphicon glyphicon-screenshot"></i>&nbsp;<fmt:message key="label.sla.rule.title" /></a>
					</sec:authorize>
					
				</div>
			</div>
		</div>
	</div>
	
	<div id="contractGridFormatter" style="display:none"><sec:authorize url="/pages/slaContractManage!merge.action"><a id="contractGrid_act_edit" href="javascript:wstuo.slaMge.slaMain.editSlaOpenWindow_aff('{contractNo}')"  title="<fmt:message key="common.edit"/>"><i class="glyphicon glyphicon-edit"></i></a></sec:authorize>&nbsp;&nbsp;&nbsp;&nbsp;<sec:authorize url="/pages/slaContractManage!delete.action"><a id=""  href="javascript:wstuo.slaMge.slaMain.deleteSLA_aff('{contractNo}')"  title="<fmt:message key="common.delete"/>"><i class="glyphicon glyphicon-trash"></i></a>	</sec:authorize></div>
</div>

<!-- 搜索SLA -->
<div id="searchContractDiv" title="<fmt:message key="common.search"/>&nbsp;SLA" class="WSTUO-dialog" style="width:350px;height:auto" >
	<form>
		<table style="width:100%" cellspacing="1">
    	<tr>
        	<td style="width:20%;text-align: center;"><fmt:message key="label.name"/></td>
        	<td style="width:90%;">
        		<input name="qdto.contractName" id="search_contractName" style="width:90%" class="form-control"/>
        	</td>
        </tr>
        <tr>
    	<td colspan="2" style="height:50px">       	
        	<input id="contractGrid_doSearch" type="button" class="btn btn-primary btn-sm" style="margin-right: 15px;" value="<fmt:message key="common.search"/>">			
        </td>
    </tr>
    </table>
  </form>
</div>

<!-- 选择被服务机构 -->
<div id="selectByServiceOrgDiv" title="<fmt:message key="title.sla.chooseByServiceOrg"/>" class="WSTUO-dialog" style="width:250px;height:400px; padding:10px; line-height:20px;" >		
	<div id="selectByServiceOrgTreeDiv"></div>
	<br/>
	<div>
	<input type="button" class="btn btn-primary btn-sm"  plain="true" id="selectByServiceOrg_getSelectedNodes" value="<fmt:message key="label.sla.confirmChoose"/>">
	</div>
</div>

<div id="selectServiceDirDiv" title="<fmt:message key="label.sla.selectServiceDir"/>" class="WSTUO-dialog" style="width:250px;height:400px; padding:10px; line-height:20px;" >	
	<div id="selectServiceDirTreeDiv"></div>
	<br/>
	<div >
		<input type="button" class="btn btn-primary btn-sm"  plain="true" id="selectServiceDir_getSelectedNodes" value="<fmt:message key="label.sla.confirmChoose"/>">
	</div>
</div>

<!-- 新增编辑SLA -->
<div style="width: 0px;height: 0px;overflow: hidden;position:relative">
	<div id="SLAOperationDiv">
	<div id="SLAOperationWindow" title="<fmt:message key="common.add"/>SLA" style="padding:3px;line-height: 15px;">
		<form id="SLAOperationForm">
			<input id="slaContract_contractNo" name="slaContractDTO.contractNo" type="hidden"/>
			<input type="hidden" name="byServicesNosStr" id="byServicesNosStr">
			<input type="hidden" name="servicesNosStr" id="servicesNosStr">
			<input type="hidden" name="slaContractDTO.dataFlag" id="slaContract_dataFlag">
				
			<div class="box-inner" >	
			<div class="box-content" >
				<ul class="nav nav-tabs" id="myTab">
					<li class="active"><a href="#list1"><span><fmt:message key="title.sla.slaDetail"/></span></a></li>	
					<li><a href="#list2"><span><fmt:message key="title.sla.byServiceOrg"/></span></a></li>	
					<li><a href="#list3"><span><fmt:message key="label.sla.relatedServiceDirGrid"/></span></a></li>	
             	</ul>
            	<div id="myTabContent" class="tab-content" style="height:100%;width: 100%;" border="none">
            		<!-- SLA 明细 stard -->
					<div region="center" class="tab-pane active" id="list1" >
						<table style="width:100%"  cellspacing="1" class="table">
							<tr>
								<td style="width:25%"><fmt:message key="label.sla.slaName"/></td>
								<td style="width:75%">
								<input id="slaContract_contractName" name="slaContractDTO.contractName" style="width:90%" validtype="length[1,200]" class="form-control" required="true" />
								</td>
							</tr>
							<tr>
								<td style="width:25%"><fmt:message key="label.sla.serviceOrg"/></td>
								<td>
									<input value="<fmt:message key="label.sla.chooseAlert"/>" id="slaContract_serviceOrgName" style="width:90%" class="form-control" required="true" readonly/>
									<input type="hidden" id="slaContract_serviceOrgNo" name="slaContractDTO.serviceOrgNo" />
								</td>
							</tr>						
							<tr>
								<td style="width:25%"><fmt:message key="label.sla.slaVersion"/></td>
								<td><input id="slaContract_versionNumber" name="slaContractDTO.versionNumber" style="width:90%" class="form-control" required="true" validtype="length[1,200]" /></td>
							</tr>					
							<tr>
								<td style="width:25%"><fmt:message key="label.sla.slaStartTime"/></td>
								<td><input id="slaContract_beginTime" name="slaContractDTO.beginTime" validType="date" style="width: 50%;" class="form-control" required="true" readonly/></td>
							</tr>
							<tr>
								<td style="width:25%"><fmt:message key="label.sla.slaEndTime"/></td>
								<td><input id="slaContract_endTime" name="slaContractDTO.endTime" class="form-control" style="width: 50%;" required="true" validType="DateComparison['slaContract_beginTime']" readonly/></td>
							</tr>
							<tr>
								<td style="width:25%"><fmt:message key="label.sla.slaServiceAgreement"/></td>
								<td><textarea id="slaContract_agreement" name="slaContractDTO.agreement" class="form-control" validType="length[1,200]" required="true" style="width:90%"></textarea></td>
							</tr>
							<tr>
								<td style="width:25%"><fmt:message key="label_sla_setIsDefault"/></td>
								<td>
									<label class="checkbox-inline">
										<input type="checkbox" id="isDefaultSla" name="slaContractDTO.isDefault">
								 	</label>                
								</td>
							</tr>
						</table>
					</div>
					<!-- SLA 明细 End -->
					<!--被服务机构 START -->
					<div class="tab-pane" id="list2">
                		<table id="byServicesOrgGrid" style="width:100%;padding-top: 3px;"></table>
						<div id="byServicesOrgGridPager"></div>
						
						<div id="byServicesOrgGridToolbar" style="display:none;">
							<div style="margin-right: 520px;">
								<button type="button" class="btn btn-default btn-xs"    id="byServicesOrgGrid_add" title="<fmt:message key="label.sla.append"/>">
									<i class="glyphicon glyphicon-plus"></i>&nbsp;<fmt:message key="label.sla.append" />
								</button>
								<button type="button" class="btn btn-default btn-xs"  id="byServicesOrgGrid_remove" title="<fmt:message key="label.sla.remove"/>">
									<i class="glyphicon glyphicon-trash"></i>&nbsp;<fmt:message key="label.sla.remove"/>
								</button>
							</div>
						</div>
					</div>  
					<!--被服务机构 END -->
					<!--相关服务列表 START -->
					<div class="tab-pane" id="list3">
                		<table id="relatedServiceDirGrid" style="width:100%;padding-top: 3px;"></table>			
						<div id="relatedServiceDirGridPager"></div>
						
						<div id="relatedServiceDirGridToolbar" style="display:none;text-align: left;">
							<div style="margin-right: 520px;">
								<button type="button" class="btn btn-default btn-xs"  id="relatedServiceDirGrid_add" title="<fmt:message key="label.sla.append"/>">
									<i class="glyphicon glyphicon-plus"></i>&nbsp;<fmt:message key="label.sla.append" />
								</button>
								<button type="button" class="btn btn-default btn-xs"  id="relatedServiceDirGrid_remove" title="<fmt:message key="label.sla.remove"/>">
									<i class="glyphicon glyphicon-trash"></i>&nbsp;<fmt:message key="label.sla.remove"/>
								</button>
							</div>
						</div>
					</div>  
			    	<!--相关服务列表 END -->
                  	</div>    
                  	</div> 
                  	<div style="height:50px;padding:10px;text-align: center;">
						<input type="button"  id="contractGrid_doSave" class="btn btn-primary btn-sm"  style="margin-right:15px" value="<fmt:message key="common.save"/>" >
					</div>	
            	</div>   
			</form>
		</div>     
	</div>  
</div>


<!-- 隐藏导出表单 -->
<form style="display:none" id="exportSLAForm" action="slaContractManage!exportSLA.action" method="post">
<input name="qdto.contractName" id="exportSLA_contractName"/>
</form>

