<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>快速登录</title>
<link rel="stylesheet" type="text/css" href="../scripts/jquery/easyui/themes/default/easyui.css">
<script src="../scripts/jquery/jquery-1.4.2.min.js"></script>
<script src="../scripts/jquery/easyui/jquery.easyui.min.js"></script>

<style type="text/css">
body {TEXT-ALIGN: center;}
</style>

</head>
<body>

<div id="w" class="WSTUO-dialog" title="快速登录" data-options="iconCls:'icon-save'" style="width:550px;height:300px;padding:10px;">  
	<form id="userLogin" name="userLogin" method="POST" action="${pageContext.request.contextPath}/j_spring_security_check" style="text-align:center;margin:0px">
		<input type="hidden" name="j_loginModel" id="j_loginModel" value="cookie">
		<input type="hidden" name="j_username" id="j_username" />
		<input type="hidden" name="j_password" id="j_password" />
	</form>
	<br>
</div>  
<img src="../images/comm/loading.gif"/>
<script type="text/javascript">
function autologin(){
	$('#j_username').val('${param.user}');
	$('#j_password').val('${param.key}');
	$('#userLogin').submit();
}
$(document).ready(function(){
	autologin();
});
</script>
</body>
</html>