<%@page import="java.util.Map"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="com.wstuo.common.security.service.IUserInfoService"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<%
    WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext());
	IUserInfoService userInfoService = (IUserInfoService)ctx.getBean("userInfoService");
	if(session.getAttribute("loginUserName")!=null){
	 	Map<String,Object> resAuth=userInfoService.findResAuthByUserName(session.getAttribute("loginUserName").toString(),new String[]{
			  "CofnigureItem_AssetOriginalValue_Res",
			  "CofnigureItem_Barcode_Res",
			  "CofnigureItem_Supplier_Res",
			  "CofnigureItem_PurchaseDate_Res",
			  "CofnigureItem_ArrivalDate_Res",
			  "CofnigureItem_PurchaseNo_Res",
			  "CofnigureItem_Owner_Res",
			  "CofnigureItem_LifeCycle_Res",
			  "CofnigureItem_WarrantyDate_Res",
			  "CofnigureItem_WarningDate_Res",
			  "CofnigureItem_SerialNumber_Res",
			  "CofnigureItem_Department_Res",
			  "CofnigureItem_Fimapping_Res",
			  "CofnigureItem_ExpiryDate_Res",
			  "CofnigureItem_LendTime_Res",
			  "CofnigureItem_OriginalUser_Res",
			  "CofnigureItem_RecycleTime_Res",
			  "CofnigureItem_PlannedRecycleTime_Res",
			  "CofnigureItem_UsePermission_Res"
			  });
	 	request.setAttribute("resAuth",resAuth);
	}
 	
%>

<script type="text/javascript">
var pageType="${param.pageType}";
var editTemplateId="${param.templateId}";
var ci_add_eavAttributet = '<fmt:message key="config.extendedInfo"/>';
var fixRequestAndKnowledge="0";
var belongsClient="1";
var pageType="${param.pageType}";
</script>
<link href="${pageContext.request.contextPath}/scripts/jquery/uploadify/uploadify.css"  rel="stylesheet" type="text/css"/>
<!-- <link id="addCimFormCustom_setting" href="../styles/common/addCimFormCustom-no.css"  rel="stylesheet" type="text/css"/>   --> 
<script src="${pageContext.request.contextPath}/scripts/itsm/cim/addCimFormCustom.js?random=<%=new java.util.Date().getTime()%>"></script>


<div class="loading" id="configureItemAdd_loading"><img src="${pageContext.request.contextPath}/images/icons/loading.gif" /></div>
	
<div id="configureItemAdd_panel" class="content" fit="true" border="none" style="height: 99%;">

	
	<!-- 新的面板 start-->
	<div class="easyui-layout" style="width: 100%;" id="configureItemAdd_layout" fit="true">
		<!-- 新的面板 start-->
		<div region="north" border="false" class="panelTop">
			<div align="left">
				<form id="ci_add_category_from">
				<a id="itemAddSave" class="easyui-linkbutton" icon="icon-save" plain="true" title="<fmt:message key="common.save"/>"></a>
				<a id="add_configureItem_backList" class="easyui-linkbutton" plain="true" icon="icon-undo" style="margin-right:15px" title="<fmt:message key="common.returnToList" />"></a>
				<span><fmt:message key="label.template.configureItem" />&nbsp;&nbsp;<select id="configureItemTemplate" onchange="javascript:itsm.cim.addCimFormCustom.getTemplateValue(this.value)"></select> </span>
				<input type="hidden" name="templateDTO.templateType" value="configureItem"  />
				<sec:authorize url="TEMPLATE_MAIN">
				<a id="saveconfigureItemTemplateBtn" class="easyui-linkbutton" plain="true" icon="icon-save" style="margin-right:15px"><fmt:message key="label.template.saveTemplate" /></a>			
				<a id="editconfigureItemTemplateBtn" class="easyui-linkbutton" plain="true" icon="icon-save" style="display: none;"><fmt:message key="label.template.editTemplate" /></a>
				</sec:authorize>
				<a id="cim_categoryName"><fmt:message key="title.request.CICategory"/></a>
				<span id="cim_ciCategoryName"></span>
				
				
				
				
<%-- 				<fmt:message key="title.request.CICategory"/><span style="color:red;">*</span> --%>
<!-- 				<input id="ci_add_categoryName2" type="text" class="easyui-validatebox input" required="true"   readonly style="cursor:pointer;width: 10%;" /> -->
				</form>
			</div>
		</div>
		
		<div region="center" class="easyui-panel" fit="true" title="" id="configureItemAdd_tabs" style="height: 100%;">
			<!-- 基本信息 -->
			<div style="background-color: #E0ECFF;float:left;width:100%;margin-top:5px;padding:5px;font-weight: bold;"><fmt:message key="common.basicInfo" /></div>
				<form id="addCim_formField_from">
				   <input type="hidden" id="ci_add_categoryName2" name="ciDto.categoryName">
				   <input type="hidden" name="ciDto.categoryNo" id="ci_add_categoryNo2" value="" />
				   <input id="addcim_formId" type="hidden" name ="ciDTO.formId" value="" />
					<div id="addCim_formField" style="width: 90%;margin:0 auto;">
						
					</div>
				</form>
			<!-- 基本信息end -->
			<div class="easyui-tabs" fit="true" border="false" id="addCim_tabs">
				<!-- 其他信息  -->
				<div title="<fmt:message key="common.attachment"/>">
					<form>
                	<div class="lineTableBgDiv">
					<table style="width:100%" class="lineTable" cellspacing="1">
                		<tr>
			              <td>
			              <div style="color:#ff0000;line-height:28px"><fmt:message key="msg.attachment.maxsize" /></div>
			               <div class="diyLinkbutton">
                               <div style="float: left;cursor: pointer;">
                                <input type="file"  name="filedata" id="addConfigureItem_uploadAttachments">
    			               </div>
                               <div style="float: left;margin-left: 15px;">
    				 		    <a class="easyui-linkbutton" icon="icon-upload" href="javascript:if($('#addConfigureItem_fileQueue').html()!='')$('#addConfigureItem_uploadAttachments').uploadifyUpload();else msgShow(i18n['msg_add_attachments'],'show');" ><fmt:message key="label.attachment.upload" /></a>
    	             		   </div>
    	             		   <div style="float: left;margin-left: 15px;"><a href="javascript:common.config.attachment.chooseAttachment.showAttachmentGrid('addConfigureItem_uploadedAttachments','ciDto.aids')" class="easyui-linkbutton" icon="icon-selectOldFile" id="chooseAtt"><fmt:message key="common.attachmentchoose" /></a></div>
			               </div>
                           <div style="clear: both;"></div>
                           <div id="addConfigureItem_uploadedAttachments" style="line-height:25px;color:#555"></div>
                           <div id="addConfigureItem_fileQueue"></div>
			               <input style=" width: 100%" type="hidden" name="ciDto.attachmentStr" id="addConfigureItem_attachments"/>
			              </td>
			        	</tr>
			        	
                	</table>
                	</div>
                	</form>
				</div>
				<!-- 软件配置参数 start 
				<div title="<fmt:message key="label.ci.softSettingParam"/>">
					
					<form>
					<div class="hisdiv" id="ci_add_softSetting">
					<table style="width:100%" class="histable" cellspacing="1">
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSetParam"/></td>
							<td width="60%" align="left"><textarea style="width: 80%" name="ciDto.softSetingParam" id="addci_softSetingParam"></textarea> </td>
						</tr>
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSettingAm"/></td>
							<td width="60%" align="left"><textarea style="width: 80%" name="ciDto.softConfigureAm" id="addci_softConfigureAm"></textarea> </td>
						</tr>
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSettingRemark1"/></td>
							<td width="60%" align="left"><textarea style="width: 80%" name="ciDto.softRemark1" id="addci_softRemark1"></textarea></td>
						</tr>
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSettingRemark2"/></td>
							<td width="60%" align="left"><textarea style="width: 80%" name="ciDto.softRemark2" id="addci_softRemark2"></textarea></td>
						</tr>
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSettingRemark3"/></td>
							<td width="60%" align="left"><textarea style="width: 80%" name="ciDto.softRemark3" id="addci_softRemark3"></textarea></td>
						</tr>
             		</table>
               		</div>
					<div class="lineTableBgDiv">
					<table style="width:100%" class="lineTable" cellspacing="1">
                		<tr>
			              <td>
			              <div style="color:#ff0000;line-height:28px"><fmt:message key="msg.attachment.maxsize" /></div>
			             	<div class="diyLinkbutton">
			             		<div style="float: left;cursor: pointer;">
                              <input type="file"  name="filedata" id="addConfigureItem_uploadSoftAttachments">
    			               </div>
			             		<div style="float: left;margin-left: 15px;">
					 		    	<a class="easyui-linkbutton" icon="icon-upload" href="javascript:if($('#addConfigureItem_SoftfileQueue').html()!='')$('#addConfigureItem_uploadSoftAttachments').uploadifyUpload();else msgShow(i18n['msg_add_attachments'],'show');" ><fmt:message key="label.attachment.upload" /></a>
		             		   </div>
	             		   <div style="float: left;margin-left: 15px;"><a href="javascript:common.config.attachment.chooseAttachment.showAttachmentGrid('add_uploadedSoftAttachments','ciDto.softAids')" class="easyui-linkbutton" icon="icon-selectOldFile" id="chooseAtt"><fmt:message key="common.attachmentchoose" /></a></div>
			               </div>
			               <br>
				 		   <div style="clear: both;"></div>
			               <div id="addConfigureItem_SoftfileQueue"></div>
			               <input style=" width: 100%" type="hidden" name="ciDto.softAttachmentStr" id="addConfigureItem_Softattachments"/>
              				<div id="add_uploadedSoftAttachments" style="line-height:25px;color:#555"></div>
			              </td>
			        	</tr>
			        	
                	</table>
                	
                	</div>
				</form>	
				</div>-->
				<!-- 软件配置参数end -->
				<div title='<fmt:message key="label.ci.ciServiceDir" />'>
				
							<form id="add_ci_relatedService">

								<div class="hisdiv">
									<table class="histable" id="add_ci_serviceDirectory" style="width:100%" cellspacing="1">
									<thead>
									<tr>
										<td colspan="3" style="text-align:left">
											<a id="add_ci_service_add" class="easyui-linkbutton" plain="true" icon="icon-add" title="<fmt:message key="common.add" />"></a>
										</td>
									</tr>
									<tr height="20px">
										<%-- <th align="center"><fmt:message key="common.id"/></th> --%>
										<th align="center"><fmt:message key="lable.ci.ciServiceDirName"/> </th>
										<th align="center"><fmt:message key="label.rule.operation"/> </th>
									</tr>
									</thead>
									<!-- 服务目录显示 -->
									<tbody id="add_ci_serviceDirectory_tbody">
										
									</tbody>
									
									</table>
								</div>
							</form>
				</div>
	    	</div>
			<!-- 扩展信息 end -->
		</div>
	</div>
<!-- 新的面板 end-->

<!-- 选择配置项分类 -->
<div id="configureItemAddCategory" class="WSTUO-dialog" title="<fmt:message key="title.request.CICategory"/>" style="width:400px;height:auto;padding:5px;">
	<div id="configureItemAddCategoryTree"></div>
</div> 

<div id="configureItemTemplateName" class="WSTUO-dialog" title="<fmt:message key="label.template.name"/>" style="width:auto;height:auto">
	<form>
	<div style="margin: 10px;"><fmt:message key="label.template.configureItemName" />&nbsp;&nbsp;<input id="configureItemTemplateNameInput" name="templateDTO.templateName" style="width: 150px;" class="easyui-validatebox input" required="true" validtype="length[1,200]"/>
	<a class="easyui-linkbutton" style="margin-top: 5px;" id="addTemplateOk" icon="icon-save" plain="true"><fmt:message key="common.save"/></a>
	<input type="hidden" id="configureItemTemplateId" name="templateDTO.templateId"/>
	</div>
	</form>
</div>

<div id="CIselectServiceDirDiv" title="<fmt:message key="label.sla.selectServiceDir"/>" class="WSTUO-dialog" style="width:250px;height:400px; padding:10px; line-height:20px;" >
	
	<div id="CISelectServiceDirTreeDiv"></div>
	<br/>
	<div style="border:#99bbe8 1px solid;padding:5px;text-align:center">
	<a class="easyui-linkbutton" icon="icon-ok" plain="true" id="CISelectServiceDir_getSelectedNodes"><fmt:message key="label.sla.confirmChoose"/></a>
	</div>
</div>



</div>




