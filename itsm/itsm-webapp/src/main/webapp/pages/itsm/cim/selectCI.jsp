<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ include file="../../language.jsp" %>    

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>配置项选择includes文件</title>
</head>
<body>
<div style="width: 0px;height: 0px;overflow: hidden;position:relative">
<!-- 配置项选择窗口-->
<div id="selectCI_Window" title="<fmt:message key="ci.relatedCI" />" style="width:750px;height:450px;padding:5px">
    <div class="easyui-layout" style="width:740px;height:400px;">
		<div region="west" split="true" style="width:190px;">
		 <div id="selectCI_CICategoryTree"></div>
		</div>
		<div region="center" border="false">
		<div id="selectCI_Window_Tabs" style="width:530px; height:auto;" >
			<div id ="selectCI_Search" style="border-top: #99bbe8 1px solid; border-bottom: #99bbe8 1px solid; border-left:#99bbe8 1px solid ; border-right:#99bbe8 1px solid ;padding:8px;margin-left: 2px;">
					<form id="ciUser_searchForm" >
					&nbsp;
					<fmt:message key="ci.use" />&nbsp;&nbsp;
					<input type="text" id="selectCIUse_ciGrid_fullName" name="ciQueryDTO.userName" />
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<fmt:message key="ci.owner" />&nbsp;&nbsp;
					<input type="text" id="selectCIOwner_ciGrid_fullName" name="ciQueryDTO.owner" />
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a class="easyui-linkbutton" plain="true" icon="icon-search" id="selectCI_SearchSelect" title="<fmt:message key="common.search" />"></a>
					</form>
			</div>
			<div title="<fmt:message key="ci.ciList" />" style="padding:3px">
				<table id="selectCI_CIGrid"></table>
				<div id="selectCI_CIGridPager"></div>
			</div>
		</div>
		</div>
		<div region="south" style="padding:8px;height:45px">
			<div id="selectCI_south_btn" style="text-align: right;">
				<a class="easyui-linkbutton" icon="icon-ok" id="selectCI_ConfirmSelect"><fmt:message key="label.sla.confirmChoose" /></a>
			</div>
			<div id="selectCI_south_msg">
				<fmt:message key="label.selectCI.confirm" />
			</div>
		</div>
	</div>
</div>

<!--  配置项选择窗口,只显示配置项列表,不显示配置项分类-->
<div id="selectCI_list_window" title="<fmt:message key="ci.relatedCI" />" style="width:750px;height:450px;padding:5px">
    <div class="easyui-layout" style="width:725px;height:400px;">
		<div region="center" border="false">
		<div id="selectCI_Window_Tabs" class="easyui-tabs" fit="true" style="width:300px; height:200" >
			<div title="<fmt:message key="ci.ciList" />" style="padding:3px">
				<table id="selectCI_list_CIGrid"></table>
				<div id="selectCI_list_CIGridPager"></div>
			</div>
		</div>
		</div>
		<div region="south" style="padding:8px;height:45px">
			<div id="selectCI_list_south_btn" style="text-align: right;">
				<a class="easyui-linkbutton" icon="icon-ok" id="selectCI_list_ConfirmSelect"><fmt:message key="label.sla.confirmChoose" /></a>
			</div>
			<div id="selectCI_list_south_msg">
				<fmt:message key="label.selectCI.confirm" />
			</div>
		</div>
	</div>
</div>
</div>
<!-- 搜索CI -->
<div id="select_configureItem_search_win" class="WSTUO-dialog" title="<fmt:message key="common.search"/>" 
	 	style="width: 400px; height: auto; padding:0px;">
	<div class="lineTableBgDiv" >
	<form>
		<table style="width:100%" class="lineTable" cellspacing="1">
			<tr>
	             <td><fmt:message key="label.name"/></td>
	             <td>
	              	<input name="ciQueryDTO.ciname"  style="width: 97%" />
	             </td>        
	        </tr>
	        <tr>
	        	<td><fmt:message key="common.no"/></td>
	            <td>
	               <input name="ciQueryDTO.cino" style="width: 97%" />
	            </td>
	        </tr>
	    	<tr>
	    		<td colspan="2">
	    			<a class="easyui-linkbutton"  id="selectConfigureItem_common_dosearch" icon="icon-search" style="margin-top: 8px;"><fmt:message key="common.search" /></a>
	    		</td>
	    	</tr>      
		</table>
	</form>
	</div>
</div>
<!-- 根据用户查找配置项 -->
<div id="select_configureItem_by_userName_win" class="WSTUO-dialog" title="<fmt:message key="ci.relatedCI" />" 
	 	style="width: 630px; height:370px; padding:3px;">
	 	<table id="select_configureItem_by_userName_grid"></table>
		<div id="select_configureItem_by_userName_pager"></div>
</div>	 	
<div id="select_configureItem_by_userName_search_win" class="WSTUO-dialog" title="<fmt:message key="common.search"/>" 
	 	style="width: 400px; height: auto; padding:0px;">
	<div class="lineTableBgDiv" >
	<form>
		<table style="width:100%" class="lineTable" cellspacing="1">
			<tr>
	             <td><fmt:message key="label.name"/></td>
	             <td>
	              	<input name="ciQueryDTO.ciname"  style="width: 97%" />
	             </td>        
	        </tr>
	        <tr>
	        	<td><fmt:message key="common.no"/></td>
	            <td>
	               <input name="ciQueryDTO.cino" style="width: 97%" />
	            </td>
	        </tr>
	    	<tr>
	    		<td colspan="2">
	    			<a class="easyui-linkbutton"  id="select_configureItem_by_userName_dosearch" icon="icon-search" style="margin-top: 8px;"><fmt:message key="common.search" /></a>
	    		</td>
	    	</tr>      
		</table>
	</form>
	</div>
</div>

<div id="search_ciCategoryTreeDiv" class="WSTUO-dialog" title="<fmt:message key="title.request.CICategory"/>" style="width:300px;height:320px;padding:5px;">
		<div>
			<div id="search_ciCategoryTreeTree" style="width:165px;height:95%;"></div>
		</div>
	</div>

</body>
</html>


