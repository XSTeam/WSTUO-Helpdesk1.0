<%@page import="java.util.Map"%>
<%@page import="com.wstuo.common.security.service.IUserInfoService"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../../language.jsp" %>
<%
    WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext());
	IUserInfoService userInfoService = (IUserInfoService)ctx.getBean("userInfoService");
	if(session.getAttribute("loginUserName")!=null){
	 	Map<String,Object> resAuth=userInfoService.findResAuthByUserName(session.getAttribute("loginUserName").toString(),new String[]{
			  "CofnigureItem_AssetOriginalValue_Res",
			  "CofnigureItem_Barcode_Res",
			  "CofnigureItem_Supplier_Res",
			  "CofnigureItem_PurchaseDate_Res",
			  "CofnigureItem_ArrivalDate_Res",
			  "CofnigureItem_PurchaseNo_Res",
			  "CofnigureItem_Owner_Res",
			  "CofnigureItem_LifeCycle_Res",
			  "CofnigureItem_WarrantyDate_Res",
			  "CofnigureItem_WarningDate_Res",
			  "CofnigureItem_SerialNumber_Res",
			  "CofnigureItem_Department_Res",
			  "CofnigureItem_Fimapping_Res",
			  "CofnigureItem_ExpiryDate_Res",
			  "CofnigureItem_LendTime_Res",
			  "CofnigureItem_OriginalUser_Res",
			  "CofnigureItem_RecycleTime_Res",
			  "CofnigureItem_PlannedRecycleTime_Res",
			  "CofnigureItem_UsePermission_Res"
			  });
	 	request.setAttribute("resAuth",resAuth);
	}
%>
<script type="text/javascript">
var kno="${ciDetailDTO.serviceDirDtos}";
var ci_edit_eavAttributet = '<fmt:message key="config.extendedInfo"/>';
var ci_edit_hardware = '<fmt:message key="label_hardware"/>';
var ciEditId  = '${param.ciEditId}';
</script>
<link href="${pageContext.request.contextPath}/scripts/jquery/uploadify/uploadify.css"  rel="stylesheet" type="text/css"/>
<!-- <link id="editCimFormCustom_setting" href="../styles/common/editCimFormCustom-no.css"  rel="stylesheet" type="text/css"/>   -->
<script src="${pageContext.request.contextPath}/scripts/itsm/cim/editCimFormCustom.js?random=<%=new java.util.Date().getTime()%>"></script> 



<div class="loading" id="configureItemEdit_loading"><img src="${pageContext.request.contextPath}/images/icons/loading.gif" /></div>
	
<div id="configureItemEdit_panel" class="content" fit="true" border="none"  style="height: 98%">
	
	<div class="easyui-layout"  fit="true" id="configureItemEdit_layout">
		<!-- 新的面板 start-->
		
		<div region="north" border="false" class="northBar">
			<DIV align="left">
				<a id="itemEditSave" class="easyui-linkbutton" icon="icon-save" plain="true" title="<fmt:message key="common.save"/>"></a>
				<a class="easyui-linkbutton" plain="true" icon="icon-undo" style="margin-right:10px" onclick="itsm.cim.editCimFormCustom.returnConfigureItemList()" title="<fmt:message key="common.returnToList" />"></a>
				
			</DIV>
		</div>
		
		<%-- <div region="center" split="true" title="<fmt:message key="common.basicInfo" />" style="width:450px">
		        <form id="configureItemEdit_form">
         </form>
		</div> --%>
			
		<div region="center" id="configureItemEdit_tabs"  class="easyui-panel" fit="true"  style="height: 98%;" >
			<!-- 基本信息 -->
			<div style="background-color: #E0ECFF;float:left;width:100%;margin-top:5px;padding:5px;font-weight: bold;"><fmt:message key="common.basicInfo" /></div>
				<form id="editCim_formField_from">
					<input id="cimEdit_isNewForm" type="hidden" name="ciDTO.isNewForm" />
					<input type="hidden" name="ciDto.ciId" id="editCiId" value="${ciDetailDTO.ciId}"/>
					<input type="hidden" id="ciEdit_categoryName2" name="ciDto.categoryName">
				    <input type="hidden" name="ciDto.categoryNo" id="ciEdit_categoryNo2" value="" />
					<div id="editCim_formField" style="width: 90%;margin:0 auto;">
					
					</div>
				</form>
			<!-- 基本信息end -->
			
			<!-- 扩展信息 start -->
			<div class="easyui-tabs" fit="true" border="false" id="editCim_tabs">
				<!-- 软件配置参数 start 
				<div title="<fmt:message key="label.ci.softSettingParam"/>">
					
					<form>
					<div class="hisdiv" id="ci_add_softSetting">
					<input type="hidden" name="ciDto.softSetId" value="${ciDetailDTO.softSetId}"/>
					<table style="width:100%" class="histable" cellspacing="1">
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSetParam"/></td>
							<td width="60%" align="left"><textarea style="width: 80%" name="ciDto.softSetingParam">${ciDetailDTO.softSetingParam}</textarea> </td>
						</tr>
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSettingAm"/></td>
							<td width="60%" align="left"><textarea style="width: 80%" name="ciDto.softConfigureAm">${ciDetailDTO.softConfigureAm}</textarea> </td>
						</tr>
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSettingRemark1"/></td>
							<td width="60%" align="left"><textarea style="width: 80%" name="ciDto.softRemark1">${ciDetailDTO.softRemark1}</textarea> </td>
						</tr>
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSettingRemark2"/></td>
							<td width="60%" align="left"><textarea style="width: 80%" name="ciDto.softRemark2">${ciDetailDTO.softRemark2}</textarea></td>
						</tr>
						<tr>
							<td width="30%"><fmt:message key="label.ci.softSettingRemark3"/></td>
							<td width="60%" align="left"><textarea style="width: 80%" name="ciDto.softRemark3">${ciDetailDTO.softRemark3}</textarea> </td>
						</tr>
             		</table>
               		</div>
					<div class="lineTableBgDiv">
					<table style="width:100%" class="lineTable" cellspacing="1">
                		<tr>
			              <td>
			             <div style="color:#ff0000;line-height:28px"><fmt:message key="msg.attachment.maxsize" /></div>
			               <div class="diyLinkbutton">
                            
                            <div style="float: left;cursor: pointer;">
                                <input type="file"  name="filedata" id="edit_uploadSoftAttachments">
    			               </div>
                            <div style="float: left;margin-left: 15px;">
				 		    <a class="easyui-linkbutton" icon="icon-upload" href="javascript:if($('#editConfigureItem_SoftfileQueue').html()!='')$('#edit_uploadSoftAttachments').uploadifyUpload();else msgShow(i18n['msg_add_attachments'],'show');" ><fmt:message key="label.attachment.upload" /></a>
	             		   </div>
    	             		   <div style="float: left;margin-left: 15px;"><a href="javascript:common.config.attachment.chooseAttachment.showAttachmentGrid('edit_uploadedSoftAttachments','ciDto.softAids')" class="easyui-linkbutton" icon="icon-selectOldFile" id="chooseAtt"><fmt:message key="common.attachmentchoose" /></a></div>
			               <br>
				 		   
	             		   <input style=" width: 100%" type="hidden" name="ciDto.softAttachmentStr" id="editConfigureItem_Softattachments"/>
			                 </div>
                             <div style="clear: both;"></div>
                             <div id="edit_uploadedSoftAttachments" style="line-height:25px;color:#555"></div>
                            <div id="editConfigureItem_SoftfileQueue"></div>
                          </td>
			        	</tr>
                	</table>
                	</div>
				</form>	
				</div>
				-->
				
				<div title='<fmt:message key="common.attachment" />' style="padding: 3px;">
							<div class="lineTableBgDiv" id="configureItemEdit_effect_atta_div">
							<form>
								<input type="hidden" id="configureItemEdit_effect_attachmentStr"/>
								<table width="100%" cellspacing="1" class="lineTable">
									
									<tr>
										<td>
										<div id="configureItemEdit_effect_success_attachment" style="line-height:25px;color:#555;display: none"></div>
										<div class="hisdiv" id="show_configureItemEdit_effectAttachment">
											<table style="width:100%" class="histable" cellspacing="1">
												<thead>
													<tr>
														<th><fmt:message key="common.id" /></th>
														<th><fmt:message key="label.problem.attachmentName" /></th>
														<th><fmt:message key="label.problem.attachmentUrl" /></th>
														<th><fmt:message key="label.sla.operation" /></th>
													</tr>
												</thead>
												<tbody></tbody>
						             			</table>
						               	</div>
										</td>
									</tr>
									<tr>
										<td>
                                            <div class="diyLinkbutton">
                                            <div style="float: left;cursor: pointer;">
											     <input type="file"  name="filedata" id="configureItemEdit_effect_file">
											</div>
                                            <div style="float: left;margin-left: 15px;">
												<a class="easyui-linkbutton" icon="icon-upload" href="javascript:itsm.cim.editCimFormCustom.configureitemEdit_uploadifyUpload()" ><fmt:message key="label.attachment.upload" /></a>
								            </div>
								            <div style="float: left;margin-left: 15px;"><a href="javascript:common.config.attachment.chooseAttachment.showAttachmentGrid('ConfigureItem','edit')" class="easyui-linkbutton" icon="icon-selectOldFile" id="chooseAtt"><fmt:message key="common.attachmentchoose" /></a></div>
                                             </div>
                                            <div style="clear: both;"></div>
                                            <div id="configureItemEdit_effect_fileQueue"></div>
										</td>
									</tr> 
								</table>
							</form>
							<%-- <form id="uploadForm_CiEdit" method="post" enctype="multipart/form-data">
							  <table border="0" cellspacing="1" class="fu_list">
					              <tr height="40px">
						        	<td colspan="2">
						        	<table border="0" cellspacing="0"><tr><td >
							       			<a href="javascript:void(0);" class="files" id="idFile_CiEdit"></a>
							       		</td><td>
									        <a class="easyui-linkbutton" style="float:left" plain="true"  icon="icon-ok" id="idBtnupload_CiEdit"><fmt:message key="label.startUpload" /></a>
									         &nbsp;&nbsp;&nbsp;
											<a class="easyui-linkbutton" plain="true" icon="icon-cancel" onclick="javascript:void(0);" id="idBtndel_CiEdit"><fmt:message key="label.allcancel" /></a>
											 &nbsp;&nbsp;&nbsp;
											<a plain="true" href="javascript:common.config.attachment.chooseAttachment.showAttachmentGrid('ConfigureItem','edit')" class="easyui-linkbutton" icon="icon-ok" id="chooseAtt"><fmt:message key="common.attachmentchoose" /></a>
										</td></tr></table>
									</td>
						      		</tr>
						          <tbody id="idFileList_CiEdit">
						          </tbody>
								</table>
							</form> --%>
						</div>
				</div>
				<div id="ciEditEffectServiceShow" title='<fmt:message key="label.ci.ciServiceDir" />'>
				
							<form id="edit_ci_relatedService">

								<div class="hisdiv">
									<table class="histable" id="edit_ci_serviceDirectory" style="width:100%" cellspacing="1">
									<thead>
									<tr>
										<td colspan="3" style="text-align:left">
											<a id="edit_ci_service_add" class="easyui-linkbutton" plain="true" icon="icon-add" title="<fmt:message key="common.add" />"></a>
										</td>
									</tr>
									<tr height="20px">
										<%-- <th align="center"><fmt:message key="common.id"/></th> --%>
										<th align="center"><fmt:message key="lable.ci.ciServiceDirName"/> </th>
										<th align="center"><fmt:message key="label.rule.operation"/> </th>
									</tr>
									</thead>
									<!-- 服务目录显示 -->
									<tbody id="edit_ci_serviceDirectory_tbody">
										<c:forEach var="data" items="${ciDetailDTO.serviceDirDtos}">
											<tr id="edit_ci_${data.eventId}"><td>${data.eventName}</td><td><a onclick="common.knowledge.knowledgeTree.serviceRm(${data.eventId})">删除</a><input type=hidden name=ciDto.serviceDirectoryNos value="${data.eventId}"></td></tr>
										</c:forEach>
									</tbody>
									
									</table>
								</div>
							</form>
				</div>
	    	</div>
			<!-- 扩展信息 end -->
		</div>
	
	</div>
<!-- 新的面板 end-->
</div>

    <!-- 选择配置项分类 -->
    <div id="configureItemEditCategory" class="WSTUO-dialog" title="<fmt:message key="title.request.CICategory"/>" style="width:400px;height:400px;padding:3px">
		<div id="configureItemEditCategoryTree"></div>
	</div> 

<div id="CIselectServiceDirDivEdit" title="<fmt:message key="label.sla.selectServiceDir"/>" class="WSTUO-dialog" style="width:250px;height:400px; padding:10px; line-height:20px;" >
	
	<div id="CISelectServiceDirTreeDivEdit"></div>
	<br/>
	<div style="border:#99bbe8 1px solid;padding:5px;text-align:center">
	<a class="easyui-linkbutton" icon="icon-ok" plain="true" id="CISelectServiceDir_getSelectedNodesEdit"><fmt:message key="label.sla.confirmChoose"/></a>
	</div>
</div> 
<!-- 配置项分类 -->
    <div id="ci_edit_addfeld" style="display: none">
    	<div class="field_options">
			<div class="label">
				<div class="fieldName" title="<fmt:message key="label.changeCode" />"><fmt:message key="label.changeCode" /></div>
			</div>
			<div class="field"><input  id="changeCode"  style="width: 90%;"  class="easyui-validatebox input control-img " name="ciDto.changeCode" type="text">
			</div>
	   </div>
		<div class="field_options">
			<div class="label">
				<div class="fieldName" title="<fmt:message key="title.request.CICategory" />"><fmt:message key="title.request.CICategory" /></div>
			</div>
			<div class="field"><input  id="ci_add_categoryName"  style="width: 90%;" disabled="disabled" value=""  class="easyui-validatebox input control-img validatebox-text ui-autocomplete-input" type="text">
			</div>
	   </div>
		
	</div>

	
	
	
	
	
	
	
	
	
	
	
	
	