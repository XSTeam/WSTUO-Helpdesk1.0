$package("wstuo.scheduledTask");
$import("wstuo.scheduledTask.scheduledTask")
$import('itsm.itsop.selectCompany');
if(isCIHave){
	$import("itsm.cim.configureItemUtil");
	$import('itsm.cim.ciCategoryTree');
}
$import("wstuo.dictionary.dataDictionaryUtil");
$import("wstuo.category.eventCategoryTree");
$import("wstuo.user.userUtil");
$import("wstuo.schedule.setMonthly");
$import('wstuo.category.serviceCatalog');
$import('wstuo.formCustom.formControlImpl');
$import('itsm.request.requestCommon');
$import('wstuo.security.defaultCompany');

/**  
 * @author WSTUO
 * @constructor users
 * @description 定期任务添加

 */
wstuo.scheduledTask.addScheduledTaskFormCustom=function(){
	var add_requestScheduledTask_attrJson="";
	var st_is_show_border = "0";
	return {
		/**
		 * 保存定期任务
		 */
		saveScheduledTask:function(){
			var _edesc="";
			if(CKEDITOR.instances['add_scheduledTask_edesc']){
				var oEditor = CKEDITOR.instances.add_scheduledTask_edesc;
				_edesc = oEditor.getData();
			}
			$('#add_scheduledTask_edesc').val(_edesc);
			$.each($("#addRequestScheduledTaskForm :input[attrType=Lob]"),function(index,obj){
				var oEditorObj = CKEDITOR.instances[$(obj).attr("id")];
				$(obj).val(trim(oEditorObj.getData()));
			});
			var bool=trim(_edesc)==''?false:true;
			if(!bool){
				msgAlert(i18n['titleAndContentCannotBeNull'],'info');
			}else{
				$('#addScheduledTaskRequest_formField #add_scheduledTask_edesc').val(trim(_edesc));
				//if($("#addScheduledTimeSettings form").form('validate')){
					//if($('#addRequestScheduledTaskForm').form('validate')){
						if($("#addScheduledTimeSettings input[name='scheduledTaskDTO.timeType']:checked").val() == "month" && $("#monthDay_select").val()==null){
							msgAlert(i18n.scheduledTask_monthPlan_monthDayIsNotNull,'info');
						}else{
							var myDate=new Date() 
				    		var month=myDate.getMonth()+1;
							if(!DateComparison($('#add_scheduledTask_endTime_input').val(),myDate.getFullYear()+'-'+month+'-'+(myDate.getDate()-1))){
								var url = 'scheduledTask!saveScheduledTask.action';
								itsm.cim.ciCategoryTree.getFormAttributesValue("#addRequestScheduledTaskForm");
								var frm = $('#addScheduledTask_layout form').serialize();
								//调用
								$.extend(frm,{"requestDTO.isShowBorder":st_is_show_border,"requestDTO.isNewForm":true});
								startProcess();
								$.post(url,frm, function(res){
										endProcess();
										basics.tab.tabUtils.closeTab(i18n['title_add_scheuledTask']);
										$('#scheduledTasksGrid').trigger('reloadGrid');
										
										basics.index.initContent("../pages/wstuo/sysMge/sysMge_scheduledTask.jsp");
										
										msgShow(i18n['msg_add_successful'],'show');
									
								});
								
							}else{
								msgAlert(i18n['tip_endTime_cannot_be_before_startTime'],'info');
								$('#add_scheduledTask').tabs('select', i18n.scheduledTask_time_setting);
							}
						}
						/*}
				}else{
					$('#add_scheduledTask').tabs('select', i18n.scheduledTask_time_setting);
					//为了防止验证信息不在左上角显示
					$("#addScheduledTimeSettings form").form('validate');
				}*/
			}
		},
		/**
		 * 选择请求分类
		 * @param showAttrId 显示div的id
		 * @param dtoName 
		 */	
		selectRequestCategory:function(showAttrId,dtoName){
			wstuo.category.eventCategoryTree.showSelectTree('#request_category_select_window'
					,'#request_category_select_tree'
					,'Request'
					,'#add_scheduledTask_categoryName'
					,'#add_scheduledTask_categoryNo'
					,''
					,'',showAttrId,dtoName);
		},
		initAddRequest_formField:function(htmlDivId){
			$("#"+htmlDivId).load('wstuo/formCustom/defaultField.jsp',function(){
				$('#'+htmlDivId+' #request_edesc').attr("id","add_scheduledTask_edesc");//将编辑器的id改为add_scheduledTask_edesc
				add_requestScheduledTask_attrJson = itsm.request.requestCommon.initDefaultForm(htmlDivId);
				wstuo.formCustom.formControlImpl.formCustomInitByDataDictionaray('#'+htmlDivId);
				itsm.request.requestCommon.changeCompany();

				$.each($("#"+htmlDivId+" :input[attrType=Lob]"),function(ind,val){
					$(val).parent().parent().attr("class","field_options field_options_lob");
				});
				$("#"+htmlDivId+" :input[attrType=Lob]").parent().attr("class","field_lob");
				$("#"+htmlDivId).find("div[class=field_options] div[class=label]:odd").css("border-left","none");
				//移除边框样式
				$("#addRequestFormCustom_is_scheduled").attr("rel","");
				$("#addRequestFormCustom_no_scheduled").attr("rel","stylesheet");
				//无边框
				$("#"+htmlDivId+" :input").not(".control").css("margin-top","10px");
				$("#addScheduledTaskRequest_formField").find("div[attrtype='Lob']").find("div[class='field_lob']").css("margin-left","0px");
				itsm.request.requestCommon.bindAutoCompleteToRequest(htmlDivId);
				//$.parser.parse('#'+htmlDivId);//加上这句，必填才有用
			});
		},
		cleanServicesCatalogNavigation:function(){
			$("#addRequestScheduledTask_serviceDirIds").val('');
			$("#addRequestScheduledTask_serviceDirName").val('');
			$("#addRequestScheduledTask_formId").val(0);
			$("#addRequestScheduledTask_servicesCatalogNavigationName").html('');
			$('#addRequestScheduledTask_cleanServicesCatalogNavigation').hide();
			wstuo.scheduledTask.addScheduledTaskFormCustom.initAddRequest_formField('addScheduledTaskRequest_formField');
		},
		showFormCustomDesign:function(){
			wstuo.category.serviceCatalog.selectSingleServiceDirCallback('#addRequestScheduledTask_serviceDirName','#addRequestScheduledTask_serviceDirIds',function(){
				$("#addRequestScheduledTask_servicesCatalogNavigationName").html(": &nbsp;"+$("#addRequestScheduledTask_serviceDirName").val());
				var formId = $('#addRequestScheduledTask_formId').val();
				if(formId!=null && formId!="null" & formId!=""){
					wstuo.scheduledTask.addScheduledTaskFormCustom.loadRequestFormHtmlByFormId(formId,'addScheduledTaskRequest_formField');
				}else{
					wstuo.scheduledTask.addScheduledTaskFormCustom.initAddRequest_formField('addScheduledTaskRequest_formField');
				}
				$('#addRequestScheduledTask_cleanServicesCatalogNavigation').css("display","");
			},"#addRequestScheduledTask_formId");
		},
		//根据表单Id加载表单内容
		loadRequestFormHtmlByFormId:function(formId,htmlDivId){
			$.post("formCustom!findFormCustomById.action","formCustomId="+formId,function(data){
				if(data.formCustomContents!=""){
					add_requestScheduledTask_attrJson = wstuo.sysMge.base64Util.decode(data.formCustomContents);
					var formCustomContents = wstuo.formCustom.formControlImpl.editHtml(data.formCustomContents,htmlDivId);
					$('#'+htmlDivId).html(formCustomContents);
					$('#'+htmlDivId+' #request_edesc').attr("id","add_scheduledTask_edesc");//将编辑器的id改为add_scheduledTask_edesc
					$.parser.parse($('#'+htmlDivId));//加上这句，必填才有用
					$.each($("#"+htmlDivId+" :input[attrType=Lob]"),function(ind,val){
						$(val).parent().parent().attr("class","field_options_lob field_options");
					});
					$("#"+htmlDivId+" :input[attrType=Lob]").parent().attr("class","field_lob");
					$("#"+htmlDivId).find("div[class=field_options] div[class=label]:odd").css("border-left","none");
					st_is_show_border = data.isShowBorder;
					wstuo.scheduledTask.addScheduledTaskFormCustom.oneRowCss(data);
					itsm.request.requestCommon.showFormBorder(st_is_show_border,"#"+htmlDivId,"#addRequestFormCustom_is_scheduled","#addRequestFormCustom_no_scheduled");
					itsm.request.requestCommon.setDefaultParamValue(htmlDivId);
					wstuo.formCustom.formControlImpl.formCustomInit('#'+htmlDivId);
					wstuo.formCustom.formControlImpl.formCustomInitByDataDictionaray('#'+htmlDivId);
					itsm.request.requestCommon.changeCompany('#'+htmlDivId);
					itsm.request.requestCommon.bindAutoCompleteToRequest(htmlDivId);
				}else{
					wstuo.scheduledTask.addScheduledTaskFormCustom.initAddRequest_formField(htmlDivId);
				}
			});
		},
		oneRowCss:function(res){
			if(res.isShowBorder=="1"){
				var leng = $("#addScheduledTaskRequest_formField").find("div[class='field_options_2Column field_options']").length;
				if(leng>0){
					$.each($("#addScheduledTaskRequest_formField").find("div[class='field_options_2Column field_options']"),function(ind,obj){
						$(obj).find("div[class='field_lob']").css("margin-top","10px");
					});
				}else{
					$.each($("#addScheduledTaskRequest_formField").find("div[class='field_options_lob field_options']"),function(ind,obj){
						$(obj).find("div[class='field_lob']").css("margin-left","5px");
					});
				}
			}else{
				var leng = $("#addScheduledTaskRequest_formField").find("div[class='field_options_2Column field_options']").length;
				if(leng>0){
					$.each($("#addScheduledTaskRequest_formField").find("div[class='field_options_2Column field_options']"),function(ind,obj){
						$(obj).find("div[class='field_lob']").css("margin-top","0px");
					});
				}else{
					$.each($("#addScheduledTaskRequest_formField").find("div[class='field_options_lob field_options']"),function(ind,obj){
						$(obj).find("div[class='field_lob']").css("margin-left","0px");
					});
				}
			}
		},
		/**
		 * 初始化
		 */	
		init:function(){
			//绑定日期控件
			DatePicker97(['#add_scheduledTask_startTime_input','#add_scheduledTask_endTime_input']);
			$("#addScheduledTask_loading").hide();
			$("#addScheduledTask_layout").show();
			//设定月份总天数
			wstuo.schedule.setMonthly.setmonthDay('add_everyWhat_monthly','monthDay_select');
			wstuo.dictionary.dataDictionaryUtil.loadOptionsByCode('effectRange','#add_scheduledTask_effectRange');
			wstuo.dictionary.dataDictionaryUtil.loadOptionsByCode('seriousness','#add_scheduledTask_seriousness');
			wstuo.dictionary.dataDictionaryUtil.loadOptionsByCode('imode','#add_scheduledTask_imode');
			wstuo.dictionary.dataDictionaryUtil.loadOptionsByCode('level','#add_scheduledTask_level');
			wstuo.dictionary.dataDictionaryUtil.loadOptionsByCode('priority','#add_scheduledTask_priority');
			
			setTimeout(function(){
				//getUploader('#add_scheduledTask_file','#add_scheduledTask_attachmentStr','#add_scheduledTask_success_attachment','');
			},0)
			
//			lazyInitEditor('#add_scheduledTask_edesc','streamline');
			
			
			$('#add_scheduledTask_companyName').click(function(){//选择公司
				itsm.itsop.selectCompany.openSelectCompanyWin('#add_scheduledTask_companyNo','#add_scheduledTask_companyName','#add_scheduledTask_createdName,#add_scheduledTask_createdNo');
			});
			//$('#add_scheduledTask_categoryName').click(wstuo.scheduledTask.addScheduledTaskFormCustom.selectRequestCategory);
			
			$('#add_scheduledTask_createdName').click(function(){
				wstuo.user.userUtil.selectUser('#add_scheduledTask_createdName','#add_scheduledTask_createdNo','','loginName',$('#add_scheduledTask_companyNo').val());
			})
			
			//绑定选择配置项
			$('#add_scheduledTask__ref_ci_btn').click(function(){
				itsm.cim.configureItemUtil.requestSelectCI('#scheduledTaskRelatedCIShow',$('#add_scheduledTask_companyNo').val(),'');
			});
			
			
			$('#saveScheduledTaskBtn').click(wstuo.scheduledTask.addScheduledTaskFormCustom.saveScheduledTask);//保存定期任务
			
			$('#add_scheduledTask_backList').click(function(){
				
				basics.index.initContent("../pages/wstuo/sysMge/sysMge_scheduledTask.jsp");
		
			});//返回列表
			
			//加载默认公司
			wstuo.security.defaultCompany.loadDefaultCompany('#add_scheduledTask_companyNo','#add_scheduledTask_companyName');

			//服务目录
			
			wstuo.scheduledTask.addScheduledTaskFormCustom.initAddRequest_formField('addScheduledTaskRequest_formField');
			
			//服务目录导航
			$('#addRequestScheduledTask_ServicesCatalogNavigation').click(function(){
				wstuo.scheduledTask.addScheduledTaskFormCustom.showFormCustomDesign();
			});
			//服务目录导航清除按钮
			$('#addRequestScheduledTask_cleanServicesCatalogNavigation').click(function(){
				msgConfirm(i18n.msg_msg,'<br/>'+i18n.msg_changeForm,function(){
					wstuo.scheduledTask.addScheduledTaskFormCustom.cleanServicesCatalogNavigation();
				});
			});
		}
	}
}();
$(document).ready(wstuo.scheduledTask.addScheduledTaskFormCustom.init);