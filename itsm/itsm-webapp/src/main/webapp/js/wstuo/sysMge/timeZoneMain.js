$package('wstuo.sysMge')
/**  
 * 时区设置
 * @author QXY  
 * @constructor WSTO
 * @description 时区设置
 * @date 2010-11-17
 * @since version 1.0 
 */  
wstuo.sysMge.timeZoneMain=function(){
	var timeZoneHtml = "<option value='-1200&(UTC-12:00)国际日期变更线西'>"+i18n.label_UTC_1+"</option>"+
	"<option value='-1100&(UTC-11:00)协调世界时-11'>"+i18n.label_UTC_2+"</option>"+
	"<option value='-1000&(UTC-10:00)夏威夷'>"+i18n.label_UTC_3+"</option>"+
	"<option value='-0900&(UTC-09:00)阿拉斯加'>"+i18n.label_UTC_4+"</option>"+
	"<option value='-0800&(UTC-08:00)太平洋时间(美国和加拿大)'>"+i18n.label_UTC_5+"</option>"+
	"<option value='-0800&(UTC-08:00)下加利福尼亚州'>"+i18n.label_UTC_6+"</option>"+
	"<option value='-0700&(UTC-07:00)奇瓦瓦，拉巴斯，马萨特兰'>"+i18n.label_UTC_7+"</option>"+
	"<option value='-0700&(UTC-07:00)山地时间(美国和加拿大)'>"+i18n.label_UTC_8+"</option>"+
	"<option value='-0700&(UTC-07:00)亚利桑那'>"+i18n.label_UTC_9+"</option>"+
	"<option value='-0600&(UTC-06:00)瓜达拉哈拉，墨西哥城，蒙特雷'>"+i18n.label_UTC_10+"</option>"+
	"<option value='-0600&(UTC-06:00)萨斯喀彻温'>"+i18n.label_UTC_11+"</option>"+
	"<option value='-0600&(UTC-06:00)中部时间(美国和加拿大)'>"+i18n.label_UTC_12+"</option>"+
	"<option value='-0600&(UTC-06:00)中美洲'>"+i18n.label_UTC_13+"</option>"+
	"<option value='-0500&(UTC-05:00)波哥大，利马，基多'>"+i18n.label_UTC_14+"</option>"+
	"<option value='-0500&(UTC-05:00)东部时间(美国和加拿大)'>"+i18n.label_UTC_15+"</option>"+
	"<option value='-0500&(UTC-05:00)印地安那州(东部)'>"+i18n.label_UTC_16+"</option>"+
	"<option value='-0430&(UTC-04:30)加拉加斯'>"+i18n.label_UTC_17+"</option>"+
	"<option value='-0400&(UTC-04:00)大西洋时间(加拿大)'>"+i18n.label_UTC_18+"</option>"+
	"<option value='-0400&(UTC-04:00)库亚巴'>"+i18n.label_UTC_19+"</option>"+
	"<option value='-0400&(UTC-04:00)乔治敦，拉巴斯，马瑙斯，圣胡安'>"+i18n.label_UTC_20+"</option>"+
	"<option value='-0400&(UTC-04:00)圣地亚哥'>"+i18n.label_UTC_21+"</option>"+
	"<option value='-0400&(UTC-04:00)亚松森'>"+i18n.label_UTC_22+"</option>"+
	"<option value='-0330&(UTC-03:30)纽芬兰'>"+i18n.label_UTC_23+"</option>"+
	"<option value='-0300&(UTC-03:00)巴西利亚'>"+i18n.label_UTC_24+"</option>"+
	"<option value='-0300&(UTC-03:00)布宜诺斯艾利斯'>"+i18n.label_UTC_25+"</option>"+
	"<option value='-0300&(UTC-03:00)格陵兰'>"+i18n.label_UTC_26+"</option>"+
	"<option value='-0300&(UTC-03:00)卡宴，福塔雷萨'>"+i18n.label_UTC_27+"</option>"+
	"<option value='-0300&(UTC-03:00)蒙得维的亚'>"+i18n.label_UTC_28+"</option>"+
	"<option value='-0300&(UTC-03:00)萨尔瓦多'>"+i18n.label_UTC_29+"</option>"+
	"<option value='-0200&(UTC-02:00)协调世界时-02'>"+i18n.label_UTC_30+"</option>"+
	"<option value='-0200&(UTC-02:00)中大西洋'>"+i18n.label_UTC_31+"</option>"+
	"<option value='-0100&(UTC-01:00)佛得角群岛'>"+i18n.label_UTC_32+"</option>"+
	"<option value='-0100&(UTC-01:00)亚速尔群岛'>"+i18n.label_UTC_33+"</option>"+
	"<option value='+0000&(UTC)都柏林，爱丁堡，里斯本，伦敦'>"+i18n.label_UTC_34+"</option>"+
	"<option value='+0000&(UTC)卡萨布兰卡'>"+i18n.label_UTC_35+"</option>"+
	"<option value='+0000&(UTC)蒙罗维亚，雷克雅未克'>"+i18n.label_UTC_36+"</option>"+
	"<option value='+0000&(UTC)协调世界时'>"+i18n.label_UTC_37+"</option>"+
	"<option value='+0100&(UTC+01:00)阿姆斯特丹，柏林，伯尔尼，罗马，斯德哥尔摩，维也纳'>"+i18n.label_UTC_38+"</option>"+
	"<option value='+0100&(UTC+01:00)贝尔格莱德，布拉迪斯拉发，布达佩斯，卢布尔雅那，布拉格'>"+i18n.label_UTC_39+"</option>"+
	"<option value='+0100&(UTC+01:00)布鲁塞尔，哥本哈根，马德里，巴黎'>"+i18n.label_UTC_40+"</option>"+
	"<option value='+0100&(UTC+01:00)萨拉热窝，斯科普里，华沙，萨格勒布'>"+i18n.label_UTC_41+"</option>"+
	"<option value='+0100&(UTC+01:00)温得和克'>"+i18n.label_UTC_42+"</option>"+
	"<option value='+0100&(UTC+01:00)中非西部'>"+i18n.label_UTC_43+"</option>"+
	"<option value='+0200&(UTC+02:00)安曼'>"+i18n.label_UTC_44+"</option>"+
	"<option value='+0200&(UTC+02:00)贝鲁特'>"+i18n.label_UTC_45+"</option>"+
	"<option value='+0200&(UTC+02:00)大马士革'>"+i18n.label_UTC_46+"</option>"+
	"<option value='+0200&(UTC+02:00)哈拉雷，比勒陀利亚'>"+i18n.label_UTC_47+"</option>"+
	"<option value='+0200&(UTC+02:00)赫尔辛基，基辅，里加，索非亚，塔林，维尔纽斯'>"+i18n.label_UTC_48+"</option>"+
	"<option value='+0200&(UTC+02:00)开罗'>"+i18n.label_UTC_49+"</option>"+
	"<option value='+0200&(UTC+02:00)尼科西亚'>"+i18n.label_UTC_50+"</option>"+
	"<option value='+0200&(UTC+02:00)雅典，布加勒斯特'>"+i18n.label_UTC_51+"</option>"+
	"<option value='+0200&(UTC+02:00)耶路撒冷'>"+i18n.label_UTC_52+"</option>"+
	"<option value='+0200&(UTC+02:00)伊斯坦布尔'>"+i18n.label_UTC_53+"</option>"+
	"<option value='+0300&(UTC+03:00)巴格达'>"+i18n.label_UTC_54+"</option>"+
	"<option value='+0300&(UTC+03:00)加里宁格勒，明斯克'>"+i18n.label_UTC_55+"</option>"+
	"<option value='+0300&(UTC+03:00)科威特，利雅得'>"+i18n.label_UTC_56+"</option>"+
	"<option value='+0300&(UTC+03:00)内罗毕'>"+i18n.label_UTC_57+"</option>"+
	"<option value='+0330&(UTC+03:30)德黑兰'>"+i18n.label_UTC_58+"</option>"+
	"<option value='+0400&(UTC+04:00)阿布扎比，马斯喀特'>"+i18n.label_UTC_59+"</option>"+
	"<option value='+0400&(UTC+04:00)埃里温'>"+i18n.label_UTC_60+"</option>"+
	"<option value='+0400&(UTC+04:00)巴库'>"+i18n.label_UTC_61+"</option>"+
	"<option value='+0400&(UTC+04:00)第比利斯'>"+i18n.label_UTC_62+"</option>"+
	"<option value='+0400&(UTC+04:00)路易港'>"+i18n.label_UTC_63+"</option>"+
	"<option value='+0400&(UTC+04:00)莫斯科，圣彼得堡，伏尔加格勒'>"+i18n.label_UTC_64+"</option>"+
	"<option value='+0430&(UTC+04:30)喀布尔'>"+i18n.label_UTC_65+"</option>"+
	"<option value='+0500&(UTC+05:00)塔什干'>"+i18n.label_UTC_66+"</option>"+
	"<option value='+0500&(UTC+05:00)伊斯兰堡，卡拉奇'>"+i18n.label_UTC_67+"</option>"+
	"<option value='+0530&(UTC+05:30)钦奈，加尔各答，孟买，新德里'>"+i18n.label_UTC_68+"</option>"+
	"<option value='+0530&(UTC+05:30)斯里加亚渥登普拉'>"+i18n.label_UTC_69+"</option>"+
	"<option value='+0545&(UTC+05:45)加德满都'>"+i18n.label_UTC_70+"</option>"+
	"<option value='+0600&(UTC+06:00)阿斯塔纳'>"+i18n.label_UTC_71+"</option>"+
	"<option value='+0600&(UTC+06:00)达卡'>"+i18n.label_UTC_72+"</option>"+
	"<option value='+0600&(UTC+06:00)叶卡捷琳堡'>"+i18n.label_UTC_73+"</option>"+
	"<option value='+0630&(UTC+06:30)仰光'>"+i18n.label_UTC_74+"</option>"+
	"<option value='+0700&(UTC+07:00)曼谷，河内，雅加达'>"+i18n.label_UTC_75+"</option>"+
	"<option value='+0700&(UTC+07:00)新西伯利亚'>"+i18n.label_UTC_76+"</option>"+
	"<option value='+0800&(UTC+08:00)北京，重庆，香港特别行政区，乌鲁木齐' selected='selected'>"+i18n.label_UTC_77+"</option>"+
	"<option value='+0800&(UTC+08:00)吉隆坡，新加坡'>"+i18n.label_UTC_78+"</option>"+
	"<option value='+0800&(UTC+08:00)克拉斯诺亚尔斯克'>"+i18n.label_UTC_79+"</option>"+
	"<option value='+0800&(UTC+08:00)珀斯'>"+i18n.label_UTC_80+"</option>"+
	"<option value='+0800&(UTC+08:00)台北'>"+i18n.label_UTC_81+"</option>"+
	"<option value='+0800&(UTC+08:00)乌兰巴托'>"+i18n.label_UTC_82+"</option>"+
	"<option value='+0900&(UTC+09:00)大阪，札幌，东京'>"+i18n.label_UTC_83+"</option>"+
	"<option value='+0900&(UTC+09:00)首尔'>"+i18n.label_UTC_84+"</option>"+
	"<option value='+0900&(UTC+09:00)伊尔库茨克'>"+i18n.label_UTC_85+"</option>"+
	"<option value='+0930&(UTC+09:30)阿德莱德'>"+i18n.label_UTC_86+"</option>"+
	"<option value='+0930&(UTC+09:30)达尔文'>"+i18n.label_UTC_87+"</option>"+
	"<option value='+1000&(UTC+10:00)布里斯班'>"+i18n.label_UTC_88+"</option>"+
	"<option value='+1000&(UTC+10:00)关岛，莫尔兹比港'>"+i18n.label_UTC_89+"</option>"+
	"<option value='+1000&(UTC+10:00)霍巴特'>"+i18n.label_UTC_90+"</option>"+
	"<option value='+1000&(UTC+10:00)堪培拉，墨尔本，悉尼'>"+i18n.label_UTC_91+"</option>"+
	"<option value='+1000&(UTC+10:00)雅库茨克'>"+i18n.label_UTC_92+"</option>"+
	"<option value='+1100&(UTC+11:00)符拉迪沃斯托克'>"+i18n.label_UTC_93+"</option>"+
	"<option value='+1100&(UTC+11:00)所罗门群岛，新喀里多尼亚'>"+i18n.label_UTC_94+"</option>"+
	"<option value='+1200&(UTC+12:00)奥克兰，惠灵顿'>"+i18n.label_UTC_95+"</option>"+
	"<option value='+1200&(UTC+12:00)斐济'>"+i18n.label_UTC_96+"</option>"+
	"<option value='+1200&(UTC+12:00)马加丹'>"+i18n.label_UTC_97+"</option>"+
	"<option value='+1200&(UTC+12:00)协调世界时+12'>"+i18n.label_UTC_98+"</option>"+
	"<option value='+1300&(UTC+13:00)萨摩亚群岛'>"+i18n.label_UTC_99+"</option>";
	
	return {
		/**
		 * 加载当前时区
		 * @private
		 */
		loadTimeZone:function(){
			$.post('timeZone!findTimeZone.action',function(data){
				if(data!=null){
					if(data.centerTimeZone!=null && data.centerTimeZone!='')
						$('#centerTimeZone').val(data.centerTimeZone);
					if(data.enduserTimeZone!=null && data.enduserTimeZone!='')
						$('#enduserTimeZone').val(data.enduserTimeZone);
					$('#timeZoneId').val(data.tid);
				}
			});
		},
		/**
		 * 保存时区
		 * @private
		 */
		saveTimeZone:function(){
			var _form = $('#timeZone_form').serialize();
			$.post('timeZone!saveTimeZone.action',_form,function(data){
				msgShow(i18n['saveSuccess'],'show');
			});
		},
		/**
		 * 初始化加载.
		 * @private
		 */
		init:function(){
			$('#timeZoneMain_loading').hide();
			$('#timeZoneMain_content').show();
			$(timeZoneHtml).appendTo("#centerTimeZone");
			$(timeZoneHtml).appendTo("#enduserTimeZone");
			$('#timeZone_linkBut').click(wstuo.sysMge.timeZoneMain.saveTimeZone);
			wstuo.sysMge.timeZoneMain.loadTimeZone();
		}
	}
}();
$(document).ready(wstuo.sysMge.timeZoneMain.init);
