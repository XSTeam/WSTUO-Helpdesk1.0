$package('wstuo.customForm');
$import('wstuo.category.serviceCatalog');
$import('wstuo.category.CICategory');
$import('wstuo.includes');
wstuo.customForm.customFormMain = function() {
	this.customForm = $('#customForm');
	this.opt;
	var formCustomType = "";
	return {
		/**
		 * @description 是否是默认数据显示格式化；
		 * @param cell 列显示值
		 * @param event 事件
		 * @param data 行数据
		 */
		isDefaultFormatter:function(cell,event,data){
			if(cell){
				return "<span style='color:#ff0000'>"+i18n.label_basicConfig_deafultCurrencyYes+"</span>";
			}else{
				return i18n.label_basicConfig_deafultCurrencyNo;
			}
		},
		nameFormatter:function(cell,event,data){
			return data.formCustomName;
		},
		typeFormatter:function(cell,event,data){
			return data.type;
		},
		/**
		 * @description 自定义表单列表.
		 */
		showFormCustomGrid : function() {
			var _url = "formCustom!findPagerFormCustom.action";
			var params = $.extend({}, jqGridParams, {
				url :_url,
				colNames : ['ID','',i18n.formName,i18n.whether_the_default_form,i18n.moduleName,'',''],
				colModel : [{
					name : 'formCustomId',
					align : 'center',
					width : 45
				}, {
					name : 'formCustomName',
					align : 'left',
					width : 220,
					hidden : true
				},
				{
					name : 'formCustomName',
					align : 'left',
					width : 220,
					formatter:wstuo.customForm.customFormMain.nameFormatter
				},
				{name:'isDefault',width:60,align:'center',formatter:wstuo.customForm.customFormMain.isDefaultFormatter},
				{name:'typeName',width:60,align:'center',formatter:wstuo.customForm.customFormMain.typeFormatter},
				{name:'type',width:60,align:'center',hidden : true},
				{name:'ciCategoryNo',width:60,align:'center',hidden : true}],
				toolbar : [true, "top"],
				jsonReader : $.extend(jqGridJsonReader, {
					id : "formCustomId"
				}),
				sortname : 'formCustomId',
				pager : '#formCustomGridPager'
			});
			$("#formCustomGrid").jqGrid(params);
			$("#formCustomGrid").navGrid('#formCustomGridPager', navGridParams);
			// 列表操作项
			$("#t_formCustomGrid").css(jqGridTopStyles);
			$("#t_formCustomGrid").append($('#formCustomGridToolbar').html());
			// 自适应宽度
			setGridWidth("#formCustomGrid", 20);
		},
		/**
		 * 添加
		 */
		addEditFormCustom_openwindow : function(){
			basics.index.initContent("../pages/wstuo/customForm/customFormDesign.jsp");
		},
		/**
		 * 执行添加动作前的数据传递
		 */
		addEditFormCustom_do : function(){
			var formCustom_module = $('#formCustom_module').val();
			var formCustomName = $("#formCustom_name").val();
			var _url = "formCustom!isFormCustomNameExisted.action";
			var param = $.param({'formCustomQueryDTO.type':formCustom_module,"formCustomQueryDTO.formCustomName":formCustomName},true);
			$.post(_url, param, function(bool){
				if(bool){
					formCustomName = encodeURI(encodeURI(formCustomName));
					if(formCustom_module=="request"){
						/*var form_eavNo = $("#formCustom_eav").val();
						var form_eavName = encodeURI(encodeURI($("#formCustom_eav").find("option:selected").text()));*/
						customForm.dialog('close');
						basics.index.initContent("../pages/wstuo/customForm/customFormDesign.jsp?formCustomName="
								+formCustomName+"&fromCustomType="+$("#formCustom_module").val());
					}/*else{
						var formCustom_ciCategoryNo = $("#formCustom_ciCategoryNo").val();
						var formCustom_ciCategoryName = encodeURI(encodeURI($("#formCustom_ciCategoryName").val()));
						var url="formCustom!findFormCustomByCiCategoryNo.action";
						var param = $.param({'formCustomDTO.ciCategoryNo':formCustom_ciCategoryNo},true);
						$.post(url, param, function(res){
							if(res==null){
								customForm.dialog('close');
								basics.tab.tabUtils.reOpenTab("../pages/common/config/formCustom/formCustomDesign.jsp?formCustomName="
										+formCustomName+"&formCustom_ciCategoryNo="+formCustom_ciCategoryNo+"&formCustom_ciCategoryName="+formCustom_ciCategoryName+"&fromCustomType="+$("#formCustom_module").val(),i18n.formDesigner);
							}else{
								msgAlert(i18n.msg_formCustomCiCategoryExist,'info');
							}
						});
						
					}*/
				}else{
					msgAlert(i18n.msg_formCustomNameExist,"info");
				}
			});
			
		},
		/**
		 * 编辑
		 */
		editFormCustom_aff : function(){
			checkBeforeEditGrid('#formCustomGrid', wstuo.customForm.customFormMain.showEditFormCustom);
		},
		/**
		 * @description 自定义表单编辑打开
		 * @param rowData 要编辑行的数据对象
		 */
		showEditFormCustom:function(rowData){
			basics.index.initContent("../pages/wstuo/customForm/customFormDesign.jsp?formCustomId="+rowData.formCustomId+"&fromCustomType="+rowData.type);
		},
		
		/**
		 * @description 删除自定义表单
		 */
		deleteFormCustom_aff : function() {
			checkBeforeDeleteGrid('#formCustomGrid', wstuo.customForm.customFormMain.formCustom_deleteFormCustom);
		},

		/**
		 * @description 删除自定义表单
		 */
		deleteFormCustom : function(formCustomId) {
			msgConfirm(i18n['msg_msg'], '<br/>' + i18n['msg_confirmDelete'], function() {
				wstuo.customForm.customFormMain.formCustom_deleteFormCustom(formCustomId);
			});
		},

		/**
		 * @description 执行删除.
		 * @param rowIds 任务id数组
		 */
		formCustom_deleteFormCustom : function(rowIds) {
			var url="formCustom!deleteFormCustom.action";
			var param = $.param({'formCustomIds':rowIds},true);
			$.post(url, param, function(){
				//重新统计				
				$('#formCustomGrid').trigger('reloadGrid');
				msgShow(i18n.msg_deleteSuccessful,'show');
				//itsm.request.requestStats.loadRequestCount();
			}, "json");	
		},
		/**
		 * @description 打开搜索窗口.
		 */
		search_openwindow : function() {

			$('#searchVisibleState').click(function() {
				if ($('#searchVisibleState').attr("checked")) {
					$('#searchVisibleState').val(1);
				} else
					$('#searchVisibleState').val(0);
			});

			windows('searchFormCustom', {
				width : 450
			});
		},
		/**
		 * @description 提交查询自定义表单
		 */
		search_do : function() {
			var sdata = $('#searchFormCustom form').getForm();
			var postData = $("#formCustomGrid").jqGrid("getGridParam", "postData");
			$.extend(postData, sdata); // 将postData中的查询参数加上查询表单的参数
			$('#formCustomGrid').trigger('reloadGrid', [{
				"page" : "1"
			}]);
			return false;
		},
		searchByformCustomType:function() {
			formCustomType = $("#formCustomTypeSelect").val();
			var _url = "formCustom!findPagerFormCustom.action";
			if(formCustomType!=""){
				 _url = _url+"?formCustomQueryDTO.type="+formCustomType;
			}
			$('#formCustomGrid').jqGrid('setGridParam',{postData:null,page:1,url:_url}).trigger('reloadGrid');
			
			if(formCustomType=="ci"){
				$("#formCustomGrid").setGridParam().hideCol("isDefault").trigger("reloadGrid");
			}else{
				$("#formCustomGrid").setGridParam().showCol("isDefault").trigger("reloadGrid");
			}
		},
		init : function() {
			formCustomType = $("#formCustomTypeSelect").val();
			// 绑定日期控件
			DatePicker97(['#searchTaskStartTime', '#searchTaskEndTime']);
			$("#formCustom_loading").hide();
			$("#formCustom_content").show();
			wstuo.includes.loadSelectCIIncludesFile();
			wstuo.customForm.customFormMain.showFormCustomGrid();
			$('#link_formCustom_search_ok').click(wstuo.customForm.customFormMain.search_do);
			$('#editFormCustomBtn').click(wstuo.customForm.customFormMain.editFormCustom_aff);
			$('#save_formCustom_info').click(wstuo.customForm.customFormMain.addEditFormCustom_do);
			//wstuo.category.CICategory.loadEavsToSelect("#formCustom_eav");//加载列表
			$("#formCustomTypeSelect").change(wstuo.customForm.customFormMain.searchByformCustomType);
		}
	}
}();
$(document).ready(wstuo.customForm.customFormMain.init);
