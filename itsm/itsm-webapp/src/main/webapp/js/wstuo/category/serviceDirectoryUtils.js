$package('wstuo.category')
$import('wstuo.knowledge.knowledgeTree');
$import('wstuo.category.serviceCatalog');

/**
 * 服务目录工具项，用于选择服务.
 * @author Van
 * @date 2011-10-20
 * @last modify 2011-10-20
 */
wstuo.category.serviceDirectoryUtils=function(){
	this.putItemName="";
	this.putItemId="";
	return {
		/**
		 * 打开选择服务窗口.
		 * @param p_putName name元素
		 * @param p_putId Id元素
		 * @param pageName 页面
		 */
		showSelect:function(p_putName,p_putId,pageName){
			putItemName=p_putName;
			putItemId=p_putId;
			windows('index_select_service_dir_window',{width:470});
			$('#index_select_service_dir_table tbody').empty();//清空列表
			//加载服务目录
			$.post("sd!findPagerServiceDirectory.action",function(data){
				var defaultLoadId=0;
				$.each(data.data,function(k,v){
					if(defaultLoadId==0){//加载默认项
						defaultLoadId=v.serviceDirectoryId;
						setTimeout(function(){
							wstuo.category.serviceDirectoryUtils.loadServiceDirectoryItems(v.serviceDirectoryId);
						},500);
					}
					$('#index_select_service_dir_table tbody').append("<tr id='service_dir_table_tr_"+v.serviceDirectoryId+"'><td>"+v.serviceDirectoryId+"</td><td onclick=javascript:wstuo.category.serviceDirectoryUtils.loadServiceDirectoryItems("+v.serviceDirectoryId+")>"+v.serviceDirectoryName+"</td></tr>");
				});
			});
		},
		/**
		 * 根据服务目录加载服务列表.
		 * @param id 服务目录Id
		 */
		loadServiceDirectoryItems:function(id){
			$("#index_select_service_dir_table td").css({"background":"#fff"});//全部修改为白色
			$("#service_dir_table_tr_"+id+" td").css({"background":"#ffef8f"});//修改为选定
			$('#index_select_service_dir_items_table tbody').empty();//清空列表
			//加载服务
			$.post("sd!findPageSubService.action?ssqDto.serviceDirectoryId="+id,function(data){
				
				$.each(data.data,function(k,v){
					$('#index_select_service_dir_items_table tbody').append("<tr><td>"+v.subServiceId+"</td><td>"+v.subServiceName+"</td><td><a style='cursor:pointer' href=javascript:wstuo.category.serviceDirectoryUtils.confirmSelect('"+v.subServiceName+"','"+v.subServiceId+"')><img src='../images/icons/ok.png'/></a></td></tr>");
				});
			});
			
		},
		/**
		 * 查询服务目录
		 * @param windowId 窗口元素
		 * @param treeId 树元素
		 * @param namePut 名称元素
		 * @param valueId 值元素
		 */
		searchKnowledgeService:function(windowId,treeId,nameId,valueId){
			wstuo.includes.loadCategoryIncludesFile();
			wstuo.category.serviceCatalog.selectServiceDir('#search_service_name');
			//wstuo.knowledge.knowledgeTree.selectKnowledgeServiceEdit('#knowledge_services_select_window','#knowledge_services_select_tree','#search_service_name','#addKnowledge_serviceNo');
		},
		/**
		 * 查询服务目录
		 * @param windowId 窗口元素
		 * @param treeId 树元素
		 * @param valueId 值元素
		 */
		searchKnowledgeService2:function(windowId,nameId,valueId){
			wstuo.knowledge.knowledgeTree.selectKnowledgeServiceFilter('#knowledge_services_select_window','#knowledge_services_select_tree',nameId,valueId);
		},
		/**
		 * 选定服务目录
		 * @param name 名称
		 * @param id   编号
		 */
		confirmSelect:function(name,id){
			$(putItemName).val(name);
			$(putItemId).val(id);
			$('#index_select_service_dir_window').dialog('close');
		},
		/**
		 * @description 选择服务目录.(规则集)
		 * @param putID 选中服务目录ID赋值ID
		 * @param putName 选中的机构名称赋值ID
		 */
		selectRequestServiceDir:function(putID,putName){
			windows('selectServicesDirDiv',{width:250,height:400});
			wstuo.category.serviceDirectoryUtils.selectServicesDir();
			$('#selectServicesDir_getServicesdNodes').unbind().click(function(){
				wstuo.category.serviceDirectoryUtils.getSelectedServicesDirNodes(putID,putName);
			});
		},
		/**
		 * @description 选择服务目录.显示机构信息(规则集)
		 */
		selectServicesDir:function(){
			$('#selectServicesDirTreeDiv').jstree({
				"json_data":{
				    ajax: {
				    	url : "event!getCategoryTree.action?num=0",
				    	data:function(n){
				    		return {'types': 'Service','parentEventId':n.attr ? n.attr("id").replace("node_",""):0};//types is in action
				    	},
				    	cache:false}
				},
				"plugins" : [ "themes", "json_data", "checkbox" ]
			});	
		},
		/**
		 * 
		 * @description 选择服务目录显示，获取windwos面板上选择的值(规则集)
		 * @param putID 选中服务目录ID赋值ID
		 * @param putName 选中的机构名称赋值ID
		 */
		getSelectedServicesDirNodes:function(putID,putName){
			var namePut1="";
			 var classiftyid="";
			 var categoryIds=new Array();
			$("#selectServicesDirTreeDiv").jstree("get_checked",false,true).each(function (i,n) { 
				 var node = jQuery(this); 
				 var ids=node.attr('id');
				 if(ids==null){//校正orgNo
					 ids=1;
				 }
				 categoryIds[i]=ids;
			});
			if(categoryIds.length>0){
				var url="event!findSubCategoryArray.action";
				var param = $.param({'categoryNos':categoryIds},true);
				$.post(url,param,function(res){
					 for(var i=0;i<res.length;i++){
						  namePut1+=res[i].eventName+",";
						  classiftyid+=res[i].eventId+",";
					 }
					 if(classiftyid.length>1){
						  namePut1=namePut1.substring(0,namePut1.length-1);
						  classiftyid=classiftyid.substring(0,classiftyid.length-1);
					 }
					  $(putName).val(namePut1);
					  $(putID).val(classiftyid);
					  
				});
			}
			$('#selectServicesDirDiv').dialog('close');
		}
	}
}();