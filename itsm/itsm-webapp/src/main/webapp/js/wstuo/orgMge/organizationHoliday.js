$package('wstuo.orgMge');
/**  
 * @author WSTUO  
 */
wstuo.orgMge.organizationHoliday = function() {
	//动作
	this.holidayOperation="";
	return {
		/**
		 * @description 状态格式化.
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 * @private
		 */
		holidayGridFormatter:function(cell,opt,data){
			
			return actionFormat('1','1')
			.replace('[edit]','wstuo.orgMge.organizationHoliday.editHolidayOpenwindow()')
			.replace('[delete]','wstuo.orgMge.organizationHoliday.deleteHolidayInLine('+data.hid+')');
		},
		/**
		 * @description 加载节假日列表.
		 */
		showHoliday:function() {
			var params = $.extend({},jqGridParams, {	
				url:'holiday!findHolidayPage.action?holidayQueryDTO.companyNo='+companyNo,
				colNames:[i18n['common_id'],i18n['common_date'],i18n['common_desc'],i18n['title_user_org'],i18n['common_action'],'',''],
				colModel:[
						  {name:'hid',hidden:true,align:'center'},						
						  {name:'hdate',sortable:true,width:80,align:'center',formatter:timeFormatter3},
						  {name:'hdesc',sortable:true,width:60,align:'center'},
						  {name:'orgName',sortable:false,width:60,align:'center'},
						  {name:'act', sortable:false,width:60,align:'center',formatter:wstuo.orgMge.organizationHoliday.holidayGridFormatter},
						  {name:'orgNo',hidden:true},
						  {name:'orgType',hidden:true}
						  ],
				jsonReader: $.extend(jqGridJsonReader, {id: "hid"}),
				sortname:'hid',
				pager:'#holidayGridPager'
			});
			$("#holidayGrid").jqGrid(params);
			$("#holidayGrid").navGrid('#holidayGridPager',navGridParams);
			//列表操作项
			$("#t_holidayGrid").css(jqGridTopStyles);
			$("#t_holidayGrid").append($('#holidayGridToolbar').html());
			//自适应宽度
			setGridWidth('#holidayGrid', 15);
		},
		
		

		/**
		 * @description 打开新增节假日窗口.
		 */
		addHolidayOpenwindow:function(){

			$('#hoiday_tr').css('display','');
			var holiday_orgNo=$("#hoilday_orgNo").val();
			var hoilday_orgType = $("#hoilday_orgType").val();
			if(holiday_orgNo==""||hoilday_orgType=="innerPanel"||hoilday_orgType=="servicePanel"){
				msgAlert(i18n['msg_chooseOrg'],'info');
			}else{
				resetForm('#holidayForm');
				$("#hoilday_orgNo").val(holiday_orgNo);
				holidayOperation="addHoliday";
				$('#saveHolidayBtn').text(i18n['common_saveAdd']);
				windows('addHolidayWindow',{title: i18n['title_holiday_add'],width:360});
			}

			
		},
		
		
		/**
		 * @description 打开编辑节假日窗口.
		 */
		editHolidayOpenwindow:function(){
			checkBeforeEditGrid("#holidayGrid",wstuo.orgMge.organizationHoliday.editHolidayMethod);
		},
		
		
		
		/**
		 * @description 提交编辑节假日.
		 * @param rowData  行数据
		 */
		editHolidayMethod:function(rowData){
			
			 //填充数据
			 $('#hoilday_hdate').val(timeFormatter(rowData.hdate,'',''));
			 $('#hoilday_hedate').val(timeFormatter(rowData.hdate,'',''));
			 $('#hoilday_hdesc').val(rowData.hdesc);
			 $('#hoilday_hid').val(rowData.hid);
			 $('#hoilday_orgNo').val(rowData.orgNo);

			holidayOperation="updateHoliday";
			
			$('#saveHolidayBtn').text(i18n['common_saveEdit']);
			$('#hoiday_tr').css('display','none');
			windows('addHolidayWindow',{title: i18n['title_holiday_edit'],width:360});
		},

		
		/**
		 * @description 保存节假日.
		 */
		saveHoliday:function(){
			var frm = $('#addHolidayWindow form').serialize();
			var url="holiday!"+holidayOperation+".action";
			startProcess();
			$.post(url,frm, function(){
				
				$('#addHolidayWindow').dialog('close');
				$('#holidayGrid').trigger('reloadGrid');
				
				$('#hoilday_hdate').val('');
				$('#hoilday_hedate').val('');
				$('#hoilday_hdesc').val('');
				$('#hoilday_hid').val('');
				
				endProcess();
				msgShow(i18n['label_logo_name_alert'],'show');
			});
		},
		
		
		/**
		 * @description 删除节假日.
		 */
		deleteHoliday:function(){
			
			checkBeforeDeleteGrid("#holidayGrid",wstuo.orgMge.organizationHoliday.deleteHolidayMethod);
		},
		
		/**
		 * @description 提交删除节假日.
		 * @param rowIds  行编号
		 */
		deleteHolidayMethod:function(rowIds){
			
			var param = $.param({'ids':rowIds},true);
			$.post("holiday!deleteHoliday.action", param, function()
			{
				$('#holidayGrid').trigger('reloadGrid');
				
				msgShow(i18n['deleteSuccess'],'show');
				
			}, "json");
			
		},

		

		/**
		 * @description 提交删除节假日.
		 *  @param rowIds  行编号
		 */
		deleteHolidayInLine:function(rowId){
			
			confirmBeforeDelete(function(){
				
				var param = $.param({'ids':rowId},true);
				$.post("holiday!deleteHoliday.action", param, function(){
					$('#holidayGrid').trigger('reloadGrid');				
					msgShow(i18n['deleteSuccess'],'show');
				}, "json");
				
			});

		},
		
		
		/**
		 * @description 显示机构对应的假期
		 *  @param orgNo  机构编号
		 */
		showHolidayByOrganization:function(orgNo){
			
			var _url = 'holiday!findHolidayPage.action?holidayQueryDTO.orgNo='+orgNo;	
			
			if(orgNo=="0"){
				
				_url = 'holiday!findHolidayPage.action?holidayQueryDTO.orgNo='+companyNo;
			}
			
			$('#holidayGrid').jqGrid('setGridParam',{url:_url,page:1}).trigger('reloadGrid');
		},
		
		/**
		 * 载入.
		 */
		init:function(){
			//加载列表
			wstuo.orgMge.organizationHoliday.showHoliday();
			
			//绑定事件
			$('#holidayGrid_add').click(wstuo.orgMge.organizationHoliday.addHolidayOpenwindow);
			$('#holidayGrid_edit').click(wstuo.orgMge.organizationHoliday.editHolidayOpenwindow);
			$('#holidayGrid_delete').click(wstuo.orgMge.organizationHoliday.deleteHoliday);
		}
		
		
	};
}();
