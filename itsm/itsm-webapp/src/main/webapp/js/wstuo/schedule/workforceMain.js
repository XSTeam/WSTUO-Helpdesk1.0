$package('wstuo.schedule');
$import('basics.dateUtility');
$import('wstuo.tools.xssUtil');
$import('itsm.portal.costCommon');
/**  
 * @author wing  
 * @constructor wing
 * @description 排班管理
 * @date 2015-06-29
 * @since version 1.0 
 */ 
wstuo.schedule.workforceMain = function(){
	/**
	 * 时间对象的格式化;
	 */
	Date.prototype.format = function(format) {
	    /*
	     * eg:format="YYYY-MM-dd hh:mm:ss";
	     */
	    var o = {
	        "M+" :this.getMonth() + 1, // month
	        "d+" :this.getDate(), // day
	        "h+" :this.getHours(), // hour
	        "m+" :this.getMinutes(), // minute
	        "s+" :this.getSeconds(), // second
	        "q+" :Math.floor((this.getMonth() + 3) / 3), // quarter
	        "S" :this.getMilliseconds()
	    // millisecond
	    }

	    if (/(y+)/.test(format)) {
	        format = format.replace(RegExp.$1, (this.getFullYear() + "")
	                .substr(4 - RegExp.$1.length));
	    }

	    for ( var k in o) {
	        if (new RegExp("(" + k + ")").test(format)) {
	            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
	                    : ("00" + o[k]).substr(("" + o[k]).length));
	        }
	    }
	    return format;
	}
	//开始时间
	this._startDate = new Date().format("yyyy-MM-dd");
	//结束时间
	this._endDate = '';
	//总列数
	this._totalCol = 8;
	//开始小时
	this._startHour = 8;
	//结束小时
	this._endHour = 19;
	this._showTask = '';
	
	this._importUrl='';
	this._currDay = '';
	return {
		/***
		 * 加载数据
		 * @param startTime 开始时间
		 * @param endTime 结束时间
		 * @param param 查询参数
		 */
		
		
		initMonthSchedule:function(startDate,endDate,param){
			_showTask = '';
			startProcess();
			$.post('tasks!findWrokForce.action',param,function(data){
				$("#workTop").show();
				$("#work_top2").hide();
				$('#workforce_category_technician,#workforce_day,#workforce_day_technician,#workforce_day_ticket').html('');
				
				$('#workforce_category_technician').append('<div align="center" style="border-right:1px none;overflow: hidden;border: 1px solid;">'+i18n['title_technician']+'/'+i18n['time']+'</div>');
				//排班管理管理时间轴
				//本月的所有日期
				var monthDay = wstuo.schedule.workforceMain.dateBetweenDayByStartAndEndTime(startDate,endDate).split('<>');
				//设置宽度
				var schedule_main_width = (36*(monthDay.length-1))+($('#workforce_category_technician').width()+5);
				//显示日期(头部)
				$('#workforce_day').append('<div id="workforce_day_center" style="display:inline-block;border"></div>');
				//时间头部
				//$('#workforce_h').append('<div id="workforce_h_center" style="display:inline-block;"></div>');
				$('#workforce_day_center').css('width',(schedule_main_width-143)+'px');
				$('#workforce_show_main').css('width',(schedule_main_width+80)+'px')
//				if(schedule_main_width<1305){
//					$('#workforce_main').css('width','100%');
//				}else{
//					$('#workforce_show_main,#workforce_main').css('width',(schedule_main_width+monthDay.length)+'px');
//					
//				}
				$('#workforce_day').css('width',(schedule_main_width-160)+'px');
				
				for(var j=0;j<monthDay.length-1;j++){
					if(wstuo.schedule.workforceMain.ifSatSun(monthDay[j])){
						$('#workforce_day_center').append('<div  onclick="wstuo.schedule.workforceMain.hours(\''+monthDay[j]+'\');"  style="width:35px;display:inline-block;border-right:1px solid;cursor:pointer;overflow: auto;background-color: #D8D8D8;" align="center" title="'+monthDay[j]+'">'+
								wstuo.schedule.workforceMain.dateFormat(monthDay[j])+
								'<input type="hidden" id="workforce_day_'+j+'" value="'+monthDay[j]+'" />'+
								'</div>');	
					}else{
						$('#workforce_day_center').append('<div onclick="wstuo.schedule.workforceMain.hours(\''+monthDay[j]+'\');"  style="width:35px;display:inline-block;cursor:hand;border-right:1px solid;cursor:pointer;overflow: auto;"  align="center" title="'+monthDay[j]+'" >'+
								wstuo.schedule.workforceMain.dateFormat(monthDay[j])+
								'<input type="hidden" id="workforce_day_'+j+'" value="'+monthDay[j]+'" />'+
								'</div>');	
					}
				}
				//根据请求数控制行
				if(data!=null && data.taskShowDataDTO!=null&&data.taskShowDataDTO.length>0){
					//得到行程数据
					var _showData = data.taskShowDataDTO;
					
					for(var k=0;k<_showData.length;k++){//对行程进行循环遍历
							//判断是否要换行显示
							//输出行程行
							//技术员格标题显示
							$('#workforce_day_technician').append('<div  id="workforce_technician_tr_'+k+'" style="border:1px solid;height: 15px;"></div>');
							//时间行标题显示
							$('#workforce_day_ticket').append('<div id="workforce_event_task_tr_'+k+'" style="border:1px solid;height: 15px;"></div>');
							//设置时间行宽度
							$('#workforce_event_task_tr_'+k).css('width',(schedule_main_width-147)+'px')
							//技术员+模块标题显示
							$('#workforce_technician_tr_'+k).append('<div style="width:157px;height: 22px;display:inline-block;overflow: auto;border-right:1px solid;">'+
									'<span style="margin-left: 10px;" title="'+_showData[k].technician+'">'+wstuo.schedule.workforceMain.showLengthFormat(_showData[k].technician,20)+'</span>'+
//									'<div style="width:147px;display:inline-block;overflow: auto;border-right:1px solid;">&nbsp;</div>'+
									'</div>');
							//得到当前行程行下的行程数据
							var _data=_showData[k].data;
							for(var j=0;j<monthDay.length-1;j++){
								//判断是否是周六周日，如果是则背景色为灰色
								//if(wstuo.schedule.workforceMain.ifSatSun(monthDay[j]))
									//$('#schedule_event_task_tr_'+k).append('<div id="schedule_day_'+k+'_'+j+'" style="width:35px;display:inline-block;border-right:1px solid;overflow: auto;background-color: #D8D8D8;" align="center" >&nbsp;</div>');
								//else
									$('#workforce_event_task_tr_'+k).append('<div id="workforce_day_'+k+'_'+j+'" style="width:35px;display:inline-block;border-right:1px solid;overflow: auto;" align="center" >&nbsp;</div>');
							
								//显示相应进度条
								for(var n=0;n<_data.length;n++){
									if(wstuo.schedule.workforceMain.sameTime($('#workforce_day_'+j).val(),_data[n].start,_data[n].end)){
										//if(_data[k].taskType=='task_workforce'){
											var background_task ='../images/schedule/{img}.png';
											if(wstuo.schedule.workforceMain.isLastEndDate($('#workforce_day_'+j).val(),_data[n].start,_data[n].end)){
												background_task = background_task.replace('{img}','task_02');
												_showTask=_showTask+'&task_'+_data[n].taskId;
											}else{
												background_task = background_task.replace('{img}','task_01');
											}
											$('#workforce_day_'+k+'_'+j).html('<div '+
													'onMouseMove="wstuo.schedule.workforceMain.showTaskTip(event,\''+_data[n].title+'\',\''+_data[n].location+'\',\''+_data[n].introduction+'\',\''+_data[n].taskStatus+'\',\''+_data[n].start+'\',\''+_data[n].end+'\',\''+_data[n].allDay+'\')" '+
													'onMouseOut="wstuo.schedule.workforceMain.hideTaskTip()" '+
													'ondblclick="wstuo.schedule.workforceMain.openTaskUpdateWin1('+_data[n].taskId+',\''+ monthDay[j]+'\',\''+ monthDay[j]+'\')"'+
													'style="width: 35px;display:inline-block;background-image: url('+background_task+');">&nbsp;</div>');
											
										//}
										//
									}
								}	
							}
							if($('#workforce_event_task_tr_'+k).html().indexOf('onmouseout')==-1){
								
								$('#workforce_event_task_tr_'+k+',#workforce_technician_tr_'+k).hide();
							}	
					}	
				}else{
					$('#workforce_day_ticket').append('<div class="workforce_event_task_tr"><span style="color:red;">'+i18n['label_Not_task']+'!</span></div>');
				}
				endProcess();
			});
			
		},
		//根据日期遍历小时的安排 
		hours:function(day){
			_currDay = day;
			var d=day +" 23:59:00";
			startProcess();
			var param='taskQueryDto.endTime='+d+'&taskQueryDto.startTime='+day+'&sidx=owner'+'&sord=desc'+"&taskQueryDto.typeDno=task_workforce"
			$.post('tasks!findAllTaskShowSchedue.action',param,function(data){
				
				$("#show_workforce").hide();
				$("#work_top2").show();
				$("#task_show").show();
				$("#workTop").hide();
				$('#workforce_type_technician,#workforce_h,#workforce_h_technician,#workforce_h_ticket').html('');
				var schedule_main_width = (36*(24-1))+($('#workforce_type_technician').width()+5);
				//显示日期(头部)
				$('#workforce_h').append('<div id="workforce_h_center" style="display:inline-block;"></div>');
				
				$('#workforce_type_technician').append('<div align="center" style="border-right:1px none;overflow: hidden;width:195px;border: 1px solid;">'+i18n['title_technician']+'/'+i18n['hours']+'</div>');
				for(var i=0;i<=23;i++){
					$('#workforce_h_center').append('<div style="width:35px;display:inline-block;border-right:1px solid;overflow: auto;background-color: #D8D8D8;" align="center" title="'+i+'">'+
							i+
							'<input type="hidden" id="workforce_h_'+i+'" value="'+i+'" />'+
							'</div>');	
				}
				if(data!=null&&data.length>0){
					//遍历行数据
					for(var i=0;i<data.length;i++){
						//技术员格标题显示
						$('#workforce_h_technician').append('<div  id="workforce_owner_tr_'+i+'" style="border:1px solid;height: 15px;"></div>');
						//时间行标题显示
						$('#workforce_h_ticket').append('<div id="workforce_task_tr_'+i+'" style="border:1px solid;height: 15px;"></div>');
						//技术员+模块标题显示
						$('#workforce_owner_tr_'+i).append('<div style="width:195px;height: 22px;display:inline-block;overflow: auto;border-right:1px solid;">'+
								'<span style="margin-left: 10px;" title="'+data[i].owner+'">'+wstuo.schedule.workforceMain.showLengthFormat(data[i].owner,20)+'</span>'+
								'</div>');
						for(var j=0;j<=23;j++){
							$('#workforce_task_tr_'+i).append('<div id="workforce_h_'+i+'_'+j+'" style="width:35px;display:inline-block;border-right:1px solid;overflow: auto;" align="center" >&nbsp;</div>');
							
							//显示相应进度条
							for(var n=0;n<data.length;n++){
								//如果是只有一天的值班安排
								if(wstuo.schedule.workforceMain.isToday(day,data[i].start,data[i].end)){
									
									if(wstuo.schedule.workforceMain.isEndDate(j,data[i].start,data[i].end)){
										var background_task ='../images/schedule/{img}.png';
										if(data[i].taskType=='task_workforce'){
											if(wstuo.schedule.workforceMain.isLastHours(j,data[i].start,data[i].end)){
												background_task = background_task.replace('{img}','task_02');
												_showTask=_showTask+'&task_'+data[i].taskId;
											}else{
												background_task = background_task.replace('{img}','task_01');
											}
											wstuo.schedule.workforceMain.dataShow(data,i,j,'',background_task);			
										}	
									}
								}else{//多天的值班安排
									var result =wstuo.schedule.workforceMain.isStartAndEnd(day,data[i].start,data[i].end);
									if(result=='0'){//多天排班任务中的中的第一天
										if(wstuo.schedule.workforceMain.isStartAndEndFirst(j,data[i].start,data[i].end)){
											var background_task ='../images/schedule/{img}.png';
											if(data[i].taskType=='task_workforce'){
												if(j==23){
													background_task = background_task.replace('{img}','task_02');
													_showTask=_showTask+'&task_'+data[i].taskId;
												}else{
													background_task = background_task.replace('{img}','task_01');
												}
												wstuo.schedule.workforceMain.dataShow(data,i,j,'',background_task);												
											}	
										}
										
									}else if(result=='1'){//多天排班任务中的中间
										var background_task ='../images/schedule/{img}.png';
										if(data[i].taskType=='task_workforce'){
											if(j==23){
												background_task = background_task.replace('{img}','task_02');
												_showTask=_showTask+'&task_'+data[i].taskId;
											}else{
												background_task = background_task.replace('{img}','task_01');
											}
											wstuo.schedule.workforceMain.dataShow(i,j,'');										}	
										
									}else{//多天排班任务中的最后一天
										if(wstuo.schedule.workforceMain.isManyTaskLast(j,data[i].start,data[i].end)){
										var background_task ='../images/schedule/{img}.png';
										if(data[i].taskType=='task_workforce'){
											if(wstuo.schedule.workforceMain.isLastHours(j,data[i].start,data[i].end)){
												background_task = background_task.replace('{img}','task_02');
												_showTask=_showTask+'&task_'+data[i].taskId;
											}else{
												background_task = background_task.replace('{img}','task_01');
											}
											wstuo.schedule.workforceMain.dataShow(data,i,j,'',background_task);										}	
									}
									}
									
								}	
								}
						}
					}
					
				}else{
					$('#workforce_h_ticket').append('<div  style="width:867px;text-align:center"><span style="color:red;">'+i18n['label_Not_task']+'!</span></div>');
				}
				endProcess();
			})
		}
		,
		getTs:function(date){
			var d = date.split('-');
			return Date.parse((d[0]+'-'+d[1]+'-'+d[2]).replace(/-/g,"/"));
		},
		getDate:function(ts){
			ts = new Date(ts);
			var year='',month='',day='';
			with(ts){
				year = getFullYear();
				month = getMonth()+1;
				day = getDate();
			}
			if(month<10){
				month = '0'+month
			}
			if(day<10){
				day = '0'+day
			}
			return year+'-'+month+'-'+day;
		},
		/**
		 * 详细信息
		 */
		dataShow:function(data,i,j,n,background_task){
			$('#workforce_h_'+i+'_'+j).html('<div '+
					'onMouseMove="wstuo.schedule.workforceMain.showTaskTip(event,\''+data[i].title+'\',\''+data[i].location+'\',\''+data[i].introduction+'\',\''+data[i].taskStatus+'\',\''+data[i].start+'\',\''+data[i].end+'\',\''+data[i].allDay+'\')" '+
					'onMouseOut="wstuo.schedule.workforceMain.hideTaskTip()" '+
					'ondblclick="wstuo.schedule.workforceMain.openTaskUpdateWin('+data[i].taskId+')"'+
					'style="width: 35px;display:inline-block;background-image: url('+background_task+'); -moz-background-size:100% 100%;-o-background-size: 100% 100%; ">&nbsp;</div>');
		},
		
		/**
		 * 根据开始时间和结束时间返回两时间之间的日期JSON数据
		 * @param startTime 开始时间
		 * @param endTime 结束时间
		 */
		dateBetweenDayByStartAndEndTime:function(startTime,endTime){
			var t1 = wstuo.schedule.workforceMain.getTs(startTime);
			var t2 = wstuo.schedule.workforceMain.getTs(endTime);
			var str = '';
			while(t1<=t2){
				str +=wstuo.schedule.workforceMain.getDate(t1)+'<>';
				t1 +=3600*24*1000;
			}
			return str;
		},
		/**
		 * 根据日期获取星期
		 */
		getWeek:function(date){
			
			var t1 = new Date(Date.parse(date.replace(/-/g,"/")));
			var y = t1.getDay() ; //返回0～6，表示星期日～星期六
			
		    var week = "";
		    if (y == 0){
		    	week = ""+i18n['label_calendar_Sunday']+"" ;
		    }
		    else if (y == 1)
		    {
		    	week = ""+i18n['label_calendar_Monday']+"" ;
		    }
		    else if (y == 2)
		    {
		    	week = ""+i18n['label_calendar_Tuesday']+"" ;
		    }
		    else if (y == 3)
		    {
		    	week = ""+i18n['label_calendar_Wednesday']+"" ;
		    }
		    else if (y == 4)
		    {
		    	week = ""+i18n['label_calendar_Thursday']+"" ;
		    }
		    else if (y == 5)
		    {
		    	week = ""+i18n['label_calendar_Friday']+"" ;
		    }
		    else if (y == 6)
		    {
		    	week = ""+i18n['label_calendar_Saturday']+"" ;
		    }
			
			return week;
		},
		
		/**
		 * 判断日期是否在同一日期内
		 */
		sameTime:function(p,startTime,endTime){
			//要比较的日期
			var t1 = new Date(Date.parse(p.replace(/-/g,"/")));
			
			var t2 = new Date(Date.parse(startTime.replace('T',' ').replace(/-/g,"/")));
			
			var t2_1 = t2.format("yyyy-MM-dd")
			
			var t2_2 = Date.parse(t2_1.replace(/-/g,"/"))
			
			var t3 = new Date(Date.parse(endTime.replace('T',' ').replace(/-/g,"/")));

			if(t2_2<=t1.getTime() && t1.getTime()<=t3.getTime()){
				return true;
			}else
				return false;
		},
		/**
		 * 判断是否是最后的结束时间，用于显示最后的进度箭头
		 */
		isLastEndDate:function(p,startTime,endTime){
			var t1 = new Date(Date.parse(p.replace(/-/g,"/")));
			var t2 = new Date(Date.parse(startTime.replace('T',' ').replace(/-/g,"/")));
			var t3 = new Date(Date.parse(endTime.replace('T',' ').replace(/-/g,"/")));
			var end = t3.format("yyyy-MM-dd")
			if(p==end)
				return true;
			else
				return false;
		},
		//判断是否只有一天的任务
		isToday:function(day,startTime,endTime){
			var t1 = new Date(Date.parse(day.replace('T',' ').replace(/-/g,"/")));
			var t2 = new Date(Date.parse(startTime.replace('T',' ').replace(/-/g,"/")));
			var t3 = new Date(Date.parse(endTime.replace('T',' ').replace(/-/g,"/")));
			var start=t2.format("dd");
			var end=t3.format("dd");
			var today=t1.format("dd");
			if(start==end&&end==today){
				return true;
			}else{
				return false;
			}
		},
		/**
		 * 判断是否是最后的结束时间，用于显示最后的进度箭头
		 */
		isLastHours:function(p,startTime,endTime){
			//var t1 = new Date(Date.parse(p1.replace(/-/g,"/")));
			var t3 = new Date(Date.parse(endTime.replace('T',' ').replace(/-/g,"/")));
			var end = t3.format("hh");
			if(p==end)
				return true;
			else
				return false;
		},
		isFirstDay:function(day,startTime,endTime){
			//var t1 = new Date(Date.parse(p1.replace(/-/g,"/")));
			var t3 = new Date(Date.parse(startTime.replace('T',' ').replace(/-/g,"/")));
			var end = t3.format("hh");
			
		},
		
		/**
		 * 判断同一天开始时间和结束时间
		 */
		isEndDate:function(t1,startTime,endTime){
			//var t1 = new Date(Date.parse(p1.replace(/-/g,"/")));
			var t2 = new Date(Date.parse(startTime.replace('T',' ').replace(/-/g,"/")));
			var t3 = new Date(Date.parse(endTime.replace('T',' ').replace(/-/g,"/")));
			var start=t2.format("hh");
			var end = t3.format("hh");
			//alert("time "+t1+",,,开始"+start+",,,结束"+end);
			if(t1>=start&&t1<=end){
				return true;
			}else{
				return false;
			}
		},
		/**
		 * 判断同一天开始时间和结束时间
		 */
		isStartAndEndFirst :function(t1,startTime,endTime){
			//var t1 = new Date(Date.parse(p1.replace(/-/g,"/")));
			var t2 = new Date(Date.parse(startTime.replace('T',' ').replace(/-/g,"/")));
			var t3 = new Date(Date.parse(endTime.replace('T',' ').replace(/-/g,"/")));
			var start=t2.format("hh");
			var end = t3.format("hh");
			//alert("time "+t1+",,,开始"+start+",,,结束"+end);
			if(t1>=start){
				return true;
			}else{
				return false;
			}
		},
		isManyTaskLast:function(t1,startTime,endTime){
			var t2 = new Date(Date.parse(startTime.replace('T',' ').replace(/-/g,"/")));
			var t3 = new Date(Date.parse(endTime.replace('T',' ').replace(/-/g,"/")));
			var start=t2.format("hh");
			var end = t3.format("hh");
			if(t1<=end){
				return true;
			}else{
				return false;
			}
			
			
		},
		//判断多天排班的第一天
		/**
		 * 多天值班任务单的判断
		 * return is  
		 * is==0 多天任务的第一天
		 * is==1 多天任务的中间任意一天
		 * is==2 多天任务中的最后一天
		 */
		isStartAndEnd:function(day,startTime,endTime){
			var t1 = new Date(Date.parse(day.replace('T',' ').replace(/-/g,"/")));
			var t2 = new Date(Date.parse(startTime.replace('T',' ').replace(/-/g,"/")));
			var t3 = new Date(Date.parse(endTime.replace('T',' ').replace(/-/g,"/")));
			var start=t2.format("dd");
			var end=t3.format("dd");
			var today=t1.format("dd");
			var is=0;
			if(start==today){
				return is;
			}else if(today>start&&today<end){
				return is=1;
			}else{
				return is=2;
			}
		},
		
		/**
		 * 显示请求详情
		 */
		showTip:function(ev,eno,etitle,estatus,eventCode,startTime,endTime){
			var left = wstuo.schedule.workforceMain.getMousePoint(ev).x;
			var top = wstuo.schedule.workforceMain.getMousePoint(ev).y;
			$('#showRequestInfo_div').css({
				'top':top-40,
				'left':left-230,
				'display':'block',
				'position':'absolute',
				'border':'1px solid #85BDE2'
			});
			$('#showRequestInfo_ecode').text(eventCode);
			$('#showRequestInfo_etitle').text(etitle);
			$('#showRequestInfo_estatus').text(estatus);
			$('#showRequestInfo_startTime').text(startTime);
			$('#showRequestInfo_endTime').text(endTime);
		},
		/**
		 * 隐藏请求详情
		 */
		hideTip:function(){
			$('#showRequestInfo_div').css({
				'display':'none'
			});
		},
		/**
		 * 显示任务详情
		 */
		showTaskTip:function(ev,title,location,introduction,taskStatus,startTime,endTime,allDay){
			var left = wstuo.schedule.workforceMain.getMousePoint(ev).x;
			var top = wstuo.schedule.workforceMain.getMousePoint(ev).y;
			$('#showWorkInfo_div').css({
				'top':top-40,
				'left':left-230,
				'display':'block',
				'position':'absolute',
				'border':'1px solid #bbb',
				'background-color':'#eee' 
			});
			$('#showWorkInfo_title').text(title);
			$('#showWorkInfo_location').text(location);
			$('#showWorkInfo_introduction').text(introduction);
			
			if(taskStatus=='0')
				taskStatus=i18n['title_newCreate']
			if(taskStatus=='1')	
				taskStatus=i18n['task_label_pending']
			if(taskStatus=='2')
				taskStatus=i18n['lable_complete']
			$('#showWorkInfo_taskStatus').text(taskStatus);
			$('#showWorkInfo_startTime').text(startTime);
			$('#showWorkInfo_endTime').text(endTime);
			if(allDay=='true')
				allDay = i18n['label_basicConfig_deafultCurrencyYes']
			else{
				allDay = i18n['label_basicConfig_deafultCurrencyNo']
			}
			$('#showWorkInfo_allDay').text(allDay);
		},
		/**
		 * 隐藏任务详情
		 */
		hideTaskTip:function(){
			$('#showWorkInfo_div').css({
				'display':'none' 
			});
		},
		/**
		 * 定义鼠标在视窗中的位置
		 */
		getMousePoint:function(ev){
			var point = {
				x:0,
				y:0
			};
		 
			// 如果浏览器支持 pageYOffset, 通过 pageXOffset 和 pageYOffset 获取页面和视窗之间的距离
			if(typeof window.pageYOffset != 'undefined') {
				point.x = window.pageXOffset;
				point.y = window.pageYOffset;
			}
			// 如果浏览器支持 compatMode, 并且指定了 DOCTYPE, 通过 documentElement 获取滚动距离作为页面和视窗间的距离
			// IE 中, 当页面指定 DOCTYPE, compatMode 的值是 CSS1Compat, 否则 compatMode 的值是 BackCompat
			else if(typeof document.compatMode != 'undefined' && document.compatMode != 'BackCompat') {
				point.x = document.documentElement.scrollLeft;
				point.y = document.documentElement.scrollTop;
			}
			// 如果浏览器支持 document.body, 可以通过 document.body 来获取滚动高度
			else if(typeof document.body != 'undefined') {
				point.x = document.body.scrollLeft;
				point.y = document.body.scrollTop;
			}
		 
			// 加上鼠标在视窗中的位置
			point.x += ev.clientX;
			point.y += ev.clientY;
		 
			// 返回鼠标在视窗中的位置
			return point;
		},
		/**
		 * 打开请求详细页面
		 */
		openReuqestDetail:function(eno){
			basics.tab.tabUtils.reOpenTab("request!requestDetails.action?eno="+eno,i18n["request_detail"]);
		},
		/**
		 * 上周
		 */
		prevWeek:function(){
			var datas =  basics.dateUtility.nextWeekMonth('WEEKS','w',_startDate);
			var _data = datas.split('&');
			
			_startDate = _data[0];
			_endDate = _data[1];
			wstuo.schedule.workforceMain.initSchedule(_startDate,_endDate,'week','');
		},
		/**
		 * 下周
		 */
		nextWeek:function(){
			var datas =  basics.dateUtility.nextWeekMonth('WEEKS','next',_startDate);
			var _data = datas.split('&');
			_startDate = _data[0];
			_endDate = _data[1];
			wstuo.schedule.workforceMain.initSchedule(_startDate,_endDate,'week','');
		},
		/**
		 * 本周
		 */
		thisWeek:function(){
			var nowTime = new Date().format("yyyy-MM-dd")
			_startDate =basics.dateUtility.showWeekFirstDay(nowTime).format("yyyy-MM-dd");
			_endDate =basics.dateUtility.showWeekLastDay(nowTime).format("yyyy-MM-dd");
			wstuo.schedule.workforceMain.initSchedule(_startDate,_endDate,'week','');
		},
		
		/**
		 * 上月
		 */
		prevMonth:function(){
			var datas =  basics.dateUtility.nextWeekMonth('MONTHS','m',_startDate);
			var _data = datas.split('&');
			
			_startDate = _data[0];
			_endDate = _data[1];
			wstuo.schedule.workforceMain.initMonthSchedule(_startDate,_endDate,'');
		},
		/**
		 * 下月
		 */
		nextMonth:function(){
			var datas =  basics.dateUtility.nextWeekMonth('MONTHS','next',_startDate);
			var _data = datas.split('&');
			_startDate = _data[0];
			_endDate = _data[1];
			wstuo.schedule.workforceMain.initMonthSchedule(_startDate,_endDate,'');
		},
		/**
		 * 本月
		 */
		thisMonth:function(){
			var nowTime = new Date().format("yyyy-MM-dd")
			_startDate =basics.dateUtility.showMonthFirstDay(nowTime).format("yyyy-MM-dd");
			_endDate =basics.dateUtility.showMonthLastDay(nowTime).format("yyyy-MM-dd");
			wstuo.schedule.workforceMain.initMonthSchedule(_startDate,_endDate,'');
		},
		
		 comptime:function() {
			 
		}
		,
		/**
		 * 搜索
		 */
		search:function(){
			if($('#workforceMain_search_form2').form('validate')){
				var _param = $('#workforceMain_search_form1,#workforceMain_search_form2').serialize();
				$("#show_workforce").show();
				$("#task_show").hide();
				wstuo.schedule.workforceMain.initMonthSchedule($('#workforceMain_search_startTime').val(),$('#workforceMain_search_endTime').val(),_param);
			}
		},
		openTaskUpdateWin1:function(taskId,start,end){
			itsm.portal.costCommon.scheduleShow3Init( 'true' );
			itsm.portal.costCommon.scheduleShow1( taskId ,start,end );
		},
		/** 
		 * 打开任务的编辑窗口
		 */
		openTaskUpdateWin:function(taskId){
			if( _currDay != '' && _currDay.length > 0){
				//var start = _currDay+" 00:00:00";var end = _currDay+" 23:59:59";
				wstuo.schedule.workforceMain.openTaskUpdateWin1(taskId,_currDay,_currDay);
			}
		},
		/**
		 * 日程表日期显示格式化，只显示月和日
		 */
		dateFormat:function(d){
			var _d = d.split('-');
			return _d[1]+'/'+_d[2];
		},
		/**
		 * 日程表日期显示格式化，只显示月和日
		 */
		yearFormat:function(d){
			var _d = d.split('-');
			return _d[0];
		},
		
		/**
		 * 根据是否是周六周日
		 */
		ifSatSun:function(date){
			var t1 = new Date(Date.parse(date.replace(/-/g,"/")));
			var y = t1.getDay() ; //返回0～6，表示星期日～星期六
		    if (y == 0){
		    	return true;
		    }
		    if (y == 6){
		    	return true;
		    }
		    
		    return false;
		},
		/**
		 * 判断精确到天是否是小于当前天
		 */
		dateLeToday:function(data){
			var nowTime = new Date().format("yyyy-MM-dd")
			var t1 = new Date(Date.parse(data.replace(/-/g,"/")));
			var d1 = t1.format("yyyy-MM-dd");
			if(nowTime<=d1)
				return true;
			else
				return false;
		},
		
		/**
		 * 显示长度
		 */
		showLengthFormat:function(value,length){
			if(value==null || value=='null' || value==''){
				return '-';
			}else{
				if(value!=null && value!=''){
					var _length = value.length;
					if(_length>length)
						return value.substring(0,length)+'...';
					else
						return value;
				}else{
					return value;
				}
			}
			
		},
		/**
		 * 根据模块或顾问进行排序
		 */
		scheduleSort:function(sidx){
			$('#workforceMain_sidx').val(sidx);
			var _param = $('#workforceMain_search_form1,#workforceMain_search_form2').serialize();
			wstuo.schedule.workforceMain.initMonthSchedule($('#workforceMain_search_startTime').val(),$('#workforceMain_search_endTime').val(),_param);
			if($('#workforceMain_sord').val()=='desc'){
				$('#workforceMain_sord').val('asc');
			}else{
				$('#workforceMain_sord').val('desc');
			}
		},
		/**
		 * 导出到excel
		 */
		exportScheduleExcel:function(){
			$('#workforceMain_search_form2').attr('action','tasks!exportTask.action');
			$("#workforceMain_search_form2").submit();
		},
		returnTaskIsDay:function(){
			$("#show_schedule").show();
			$("#task_show").hide();
			wstuo.schedule.workforceMain.search();
		},
		/**
		 * 初始化
		 */
		init:function(){
			$('#workforceMain_loading').hide();
			$('#workforceMain_content').show();
			
			//选择技术员
			$('#workforceMain_technician_name').click(function(){
				wstuo.tools.xssUtil.selectUser('#workforceMain_technician_name','#workforceMain_technician_id','','fullName',companyNo,function(){
				});
			});
			wstuo.schedule.workforceMain.comptime();
			wstuo.schedule.workforceMain.hideTaskTip();
			wstuo.schedule.workforceMain.hideTip();
			//搜索
			$('#workforceMain_search,#workforceMain_refresh').click(wstuo.schedule.workforceMain.search);
			
			_startDate = basics.dateUtility.showMonthFirstDay(_startDate);
			
			_endDate = basics.dateUtility.showMonthLastDay(_startDate);
			
			$('#workforceMain_search_startTime').val(_startDate);
			$('#workforceMain_search_endTime').val(_endDate);
			//初始化
			wstuo.schedule.workforceMain.initMonthSchedule(_startDate,_endDate,'taskQueryDto.startTime='+_startDate+'&taskQueryDto.endTime='+_endDate+'&sidx=owner'+'&taskQueryDto.typeDno=task_workforce');
			
			//返回以天为纬度的值班列表
			$('#back_Task').click(function(){//选择公司
				wstuo.schedule.workforceMain.returnTaskIsDay();
			});
			//根据滚动条显示
			$("#workforce_day_ticket").scroll(function() {
				$("#workforce_day").scrollLeft($("#workforce_day_ticket").scrollLeft());
				$("#workforce_day_technician").scrollTop($("#workforce_day_ticket").scrollTop());
				$("#workforce_day_ticket").css('height','415px');
			});
			$('#exportExcelTask').click(wstuo.schedule.workforceMain.exportScheduleExcel);
			//%y-%M-%d 代表当前年月日，{%y+1}是表达式，代表当前年+1
			commonWdatePicker({dateFmt:'yyyy-MM-dd',minDate:'%y-%M-01',maxDate:'{%y+1}-%M-01'},
			'#workforceMain_search_startTime');
			commonWdatePicker({dateFmt:'yyyy-MM-dd',minDate:'%y-%M-01',maxDate:'{%y+1}-%M-01'},
			'#workforceMain_search_endTime');
		}
	}
}();
$(function(){wstuo.schedule.workforceMain.init()});