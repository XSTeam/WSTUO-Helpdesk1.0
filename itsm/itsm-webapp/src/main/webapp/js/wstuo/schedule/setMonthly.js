$package('wstuo.schedule');
/**  
 * @author QXY  
 * @constructor WSTO
 * @description setMonthly
 * @date 2010-11-17
 */ 
wstuo.schedule.setMonthly = function(){
	
	return {
		/**
		 * 设置月份
		 */
		setmonthDay:function(div,select){
			$('#'+div+' input').click(function(){
				var oldValue = $('#'+select).val();
				var totalMonthly="";
				$('#'+select).html('');
				var strStart = "<option value='";
				var strMiddle = " '>";
				var strend = "</option>";	//下拉框html										
				$('#'+div+' input').each(function(){
					if($(this).is(':checked')){
						totalMonthly += $(this).val()+'-';
					}
				});
				var Otsuki = ['JAN','MAR','MAY','JUL','AUG','OCT','DEC'];  //大月份
				var Satsuki = ['APR','JUN','SEP','NOV'];			//小月份
				var status = 0;
				$.each(Satsuki,function(k,v){
					if(totalMonthly.indexOf(v)!=-1){
						status = 1;
						return ;
					}
				})
				if(totalMonthly.indexOf('FEB')!=-1){	//如果所选月份含有2月份
					for(var i=1;i<29;i++){
						$('#'+select).append(strStart+i+strMiddle+i+strend);
					}
				}else if(status ==1){			//小月份
					for(var i=1;i<31;i++){
						$('#'+select).append(strStart+i+strMiddle+i+strend);
					}
				}else{				//大月份
					for(var i=1;i<32;i++){
						$('#'+select).append(strStart+i+strMiddle+i+strend);
					}
				}
				$('#'+select).val(oldValue);
			});
		},
		setmonthDayForEdit:function(select,totalMonthly){
			var strStart = "<option value='";
			var strMiddle = " '>";
			var strend = "</option>";	//下拉框html
			var Satsuki = ['APR','JUN','SEP','NOV'];			//小月份
			var status = 0;
			$.each(Satsuki,function(k,v){
				if(totalMonthly.indexOf(v)!=-1){
					status = 1;
					return ;
				}
			});
			if(totalMonthly.indexOf('FEB')!=-1){	//如果所选月份含有2月份
				for(var i=1;i<29;i++){
					$('#'+select).append(strStart+i+strMiddle+i+strend);
				}
			}else if(status ==1){			//小月份
				for(var i=1;i<31;i++){
					$('#'+select).append(strStart+i+strMiddle+i+strend);
				}
			}else{				//大月份
				for(var i=1;i<32;i++){
					$('#'+select).append(strStart+i+strMiddle+i+strend);
				}
			}
		}
	}
	
	
}();