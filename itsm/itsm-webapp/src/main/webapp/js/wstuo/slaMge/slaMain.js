$package('wstuo.slaMge');
$import('wstuo.rules.ruleCM');
$import('wstuo.includes');
/**  
 * @author WSTUO 
 * @constructor WSTO
 * @description SLA管理主函数.

 */
wstuo.slaMge.slaMain=function(){
	 
	this.operation="";
	this.operationCounts="0";
	this.serviceReceiver={};
	
	return {
		
	
		
		/**
		 * @description 动作格式化
		 * @param cell 列显示值
		 * @param event 事件
		 * @param data 行数据
		 */
		slaActionFormatter:function(cell,event,data){
			return $('#contractGridFormatter').html();
//			return actionFormat('1','1')
//			.replace('[edit]','wstuo.slaMge.slaMain.editSlaOpenWindow()')
//			.replace('[delete]','wstuo.slaMge.slaMain.deleteSLAInline('+data.contractNo+')');
		},
	
		/**
		 * @description 系统数据显示格式化；
		 * @param cell 列显示值
		 * @param event 事件
		 * @param data 行数据
		 */
		dataFlagFormatter:function(cell,event,data){
			
			if(data.dataFlag==1){
				return "<span style='color:#ff0000'>[System]</span>&nbsp;"+data.contractName;
			}else{
				return data.contractName;
			}
		},
		/**
		 * @description 是否是默认数据显示格式化；
		 * @param cell 列显示值
		 * @param event 事件
		 * @param data 行数据
		 */
		isDefaultFormatter:function(cell,event,data){
			if(cell){
				return "<span style='color:#ff0000'>"+i18n.label_basicConfig_deafultCurrencyYes+"</span>";
			}else{
				return i18n.label_basicConfig_deafultCurrencyNo;
			}
		},
	
		/**
		 * @description 加载服务协议列表.
		 */
		showContractGrid:function(){
	
			var params = $.extend({},jqGridParams, {	
			url:'slaContractManage!find.action',
		/*	caption:i18n['caption_slaGrid'],*/
			colNames:[i18n['title_sla_name'],i18n['title_sla_org'],i18n['label_sla_isDefault'],i18n['title_sla_version'],i18n['title_sla_startTime'],i18n['title_sla_endTime'],i18n['common_action'],'','','','',''],
			colModel:[{name:'contractNamePanel',index:'contractName',width:120,align:'center',formatter:wstuo.slaMge.slaMain.dataFlagFormatter},
					  {name:'serviceOrgName',width:100,align:'center',sortable:false},
					  {name:'isDefault',width:60,align:'center',formatter:wstuo.slaMge.slaMain.isDefaultFormatter},
					  {name:'versionNumber',width:100,align:'center'},
					  {name:'beginTime',width:100,align:'center',formatter:timeFormatter},
					  {name:'endTime',width:100,align:'center',formatter:timeFormatter},
					  {name:'act',align:'center',formatter:function(cell,event,data){
                   	   		return $('#contractGridFormatter').html().replace(/{contractNo}/g,data.contractNo);
                      },sortable:false},
					  {name:'agreement' ,hidden:true},
					  {name:'rulePackageNo',hidden:true},
					  {name:'contractNo',hidden:true},
					  {name:'contractName',hidden:true},
					  {name:'dataFlag',hidden:true}
					  ],
			jsonReader: $.extend(jqGridJsonReader, {id: "contractNo"}),
			sortname:'contractNo',
			pager:'#contractGridPager'
			});
			$("#contractGrid").jqGrid(params);
			$("#contractGrid").navGrid('#contractGridPager',navGridParams);
			//列表操作项
			$("#t_contractGrid").css(jqGridTopStyles);
			$("#t_contractGrid").append($('#contractGridToolbar').html());
			//自适应宽度
			setGridWidth("#contractGrid",2);
		},
		
		
		
	
		
		/**
		 * @description 打开新增SLA窗口
		 */
		addSlaOpenWindow:function(){
			//清空数据
			$('#slaContract_contractNo').val("");
			$('#slaContract_contractName').val("");
			
			$('#slaContract_serviceOrgName').val("");
			$('#slaContract_serviceOrgNo').val("");
			
			$('#slaContract_versionNumber').val("");
			$('#slaContract_beginTime').val("");
			$('#slaContract_endTime').val("");
			$('#slaContract_agreement').val("");
			$('#slaContract_dataFlag').val("");
			$('#isDefaultSla').attr("checked",false);
			operation="save";
			
			windows('SLAOperationWindow',{title: i18n['label_sla_addSLA'],width:650,height:550});
			
			var grid_url="slaContractManage!findByOrgServices.action?contractNo=0";
			var serviceGrid_url="slaContractManage!findByServiceDir.action?contractNo=0";
			if(operationCounts=="0"){
	
				wstuo.slaMge.slaMain.showByServicesOrgGrid();
				wstuo.slaMge.slaMain.showServiceDirGrid();
				operationCounts="1";
				
			}
			$('#byServicesOrgGrid').jqGrid('setGridParam',{url:grid_url}).clearGridData().trigger('reloadGrid');
			$('#relatedServiceDirGrid').jqGrid('setGridParam',{url:serviceGrid_url}).clearGridData().trigger('reloadGrid');
		},
	
		/**
		 * @description 打开编辑SLA窗口.
		 */
		editSlaOpenWindow:function(){
			checkBeforeEditGrid('#contractGrid',function(rowData){
				if(operationCounts=="0"){
					operationCounts="1";
					wstuo.slaMge.slaMain.showByServicesOrgGrid();
					wstuo.slaMge.slaMain.showServiceDirGrid();
				}
				var url = 'slaContractManage!findSlaById.action?contractNo='+rowData.contractNo;
				$.post(url,function(res){
					operation="merge";
					$('#slaContract_contractNo').val(res.contractNo);
					$('#slaContract_contractName').val(res.contractName);
					$('#slaContract_versionNumber').val(res.versionNumber);
					if(res.beginTime!=null && res.beginTime!='null'){
						$('#slaContract_beginTime').val(timeFormatter(res.beginTime));
					}else{
						$('#slaContract_beginTime').val("");
					}
					if(res.endTime!=null && res.endTime!='null'){
						$('#slaContract_endTime').val(timeFormatter(res.endTime));
					}else{
						$('#slaContract_endTime').val("");
					}
					$('#slaContract_agreement').val(res.agreement);
					$('#slaContract_serviceOrgName').val(res.serviceOrgName);
					$('#slaContract_serviceOrgNo').val(res.serviceOrgNo);
					$('#slaContract_dataFlag').val(res.dataFlag);
					if(res.isDefault){
						$("#isDefaultSla").attr("checked",true);
					}else{
						$("#isDefaultSla").attr("checked",false);
					}
				
					windows('SLAOperationWindow',{width:650,height:550,title:i18n['label_sla_editSLA']});
					setTimeout(function(){
						//填充GRID
						var grid_url="slaContractManage!findByOrgServices.action?contractNo="+rowData.contractNo;
						$('#byServicesOrgGrid').jqGrid('setGridParam',{url:grid_url}).trigger('reloadGrid');
					},0);
					
					setTimeout(function(){
						//填充GRID
						var serviceGrid_url="slaContractManage!findByServiceDir.action?contractNo="+rowData.contractNo;
						$('#relatedServiceDirGrid').jqGrid('setGridParam',{url:serviceGrid_url}).trigger('reloadGrid');
					},0);
				});
				
			});
			
		},
		/**
		 * @description 打开编辑SLA窗口.
		 * @param contractNo SLA Id
		 */
		editSlaOpenWindow_aff:function(contractNo){
				if(operationCounts=="0"){
					operationCounts="1";
					wstuo.slaMge.slaMain.showByServicesOrgGrid();
					wstuo.slaMge.slaMain.showServiceDirGrid();
				}
				var url = 'slaContractManage!findSlaById.action?contractNo='+contractNo;
				$.post(url,function(res){
					operation="merge";
					$('#slaContract_contractNo').val(res.contractNo);
					$('#slaContract_contractName').val(res.contractName);
					$('#slaContract_versionNumber').val(res.versionNumber);
					if(res.beginTime!=null && res.beginTime!='null'){
						
						$('#slaContract_beginTime').val(timeFormatter(res.beginTime));
						
					}else{
						
						$('#slaContract_beginTime').val("");
					}
					
					if(res.endTime!=null && res.endTime!='null'){
						
						$('#slaContract_endTime').val(timeFormatter(res.endTime));
						
					}else{
						$('#slaContract_endTime').val("");
					}
					if(res.isDefault){
						$("#isDefaultSla").attr("checked",true);
					}else{
						$("#isDefaultSla").attr("checked",false);
					}
					$('#slaContract_agreement').val(res.agreement);
					$('#slaContract_serviceOrgName').val(res.serviceOrgName);
					$('#slaContract_serviceOrgNo').val(res.serviceOrgNo);
					$('#slaContract_dataFlag').val(res.dataFlag);
					
					windows('SLAOperationWindow',{width:650,height:550,title:i18n['label_sla_editSLA']});
					setTimeout(function(){
						//填充GRID
						var grid_url="slaContractManage!findByOrgServices.action?contractNo="+contractNo;
						$('#byServicesOrgGrid').jqGrid('setGridParam',{url:grid_url}).trigger('reloadGrid');
					},0);
					setTimeout(function(){
						//填充GRID
						var serviceGrid_url="slaContractManage!findByServiceDir.action?contractNo="+contractNo;
						$('#relatedServiceDirGrid').jqGrid('setGridParam',{url:serviceGrid_url}).trigger('reloadGrid');
					},0);
				});
		},
		
		/**
		 * @description 提交保存SLA.
		 */
		saveSla:function(){
			startProcess();
			var gridLength= $("#byServicesOrgGrid").jqGrid("getRowData").length;		
				if(gridLength>0){//判断被服务机构是否为空
				//清空数据
				var ids = $("#byServicesOrgGrid").getDataIDs();
				var serviceids = $("#relatedServiceDirGrid").getDataIDs();
				if($("#isDefaultSla").attr("checked")){
					$("#isDefaultSla").val(true);
				}else{
					$("#isDefaultSla").val(false);
				}
				$('#byServicesNosStr').val(ids);
				$('#servicesNosStr').val(serviceids);
				var contract = $('#SLAOperationForm').getForm();
				var url="slaContractManage!"+operation+".action";
				var msgStr=i18n["msg_addSuccessful"];
				
				if(operation=="merge"){msgStr=i18n["msg_editSuccessful"];}
				
				
				$.post(url,contract,function(){
					
					$("#contractGrid").trigger('reloadGrid');
					
					$('#SLAOperationWindow').dialog('close');
				
					//清空数据
					$('#slaContract_contractNo').val("");
					$('#slaContract_contractName').val("");
					
					$('#slaContract_serviceOrgName').val("");
					$('#slaContract_serviceOrgNo').val("");
					
					$('#slaContract_versionNumber').val("");
					$('#slaContract_beginTime').val("");
					$('#slaContract_endTime').val("");
					$('#slaContract_agreement').val("");
					
					var grid_url="slaContractManage!findByOrgServices.action?contractNo=0";
					
					$('#byServicesOrgGrid').jqGrid('setGridParam',{url:grid_url}).clearGridData();
					var servicegrid_url="slaContractManage!findByServiceDir.action?contractNo=0";
					
					$('#relatedServiceDirGrid').jqGrid('setGridParam',{url:servicegrid_url}).clearGridData();
					msgShow(msgStr,'show');
					endProcess();
				});
				}else{
					endProcess();
					msgAlert(i18n['ERROR_BYSERVICE_ORG_CAN_NOT_BE_NULL'],'info');
				}
	
		},
		
		
		
		/**
		 * @description 删除SLA.
		 */
		deleteSLA:function(){
			checkBeforeDeleteGrid('#contractGrid',function(rowIds){
				var arr = $("#contractGrid").jqGrid("getRowData",rowIds);
				if(arr.dataFlag === "1"){
					msgShow(i18n['msg_canNotDeleteSystemData'],'warning');
					return false;
				}
				var _param = $.param({'contractNos':rowIds},true);
				$.post("slaContractManage!delete.action",_param,function(data){
					if(data){
						$("#contractGrid").trigger('reloadGrid');
						msgShow(i18n['msg_deleteSuccessful'],'show');
					} 
				},"json");	
			});
			
			
		},
		/**
		 * @description 删除SLA.
		 * @param rowIds 选中要删除行的ID集合
		 */
		deleteSLA_aff:function(rowIds){
			msgConfirm(i18n['msg_msg'],'<br/>'+i18n['msg_confirmDelete'],function(){
				var _param = $.param({'contractNos':rowIds},true);
				$.post("slaContractManage!delete.action",_param,function(data){
					if(data){
						$("#contractGrid").trigger('reloadGrid');
						msgShow(i18n['msg_deleteSuccessful'],'show');
					}else{
						msgShow(i18n['msg_canNotDeleteSystemData'],'warning');
					}
				},"json");	
			});
		},

	
		/**
		 * @description 执行搜索.
		 */
		searchSLA:function(){
			var sdata = $('#searchContractDiv form').getForm();
			var postData = $("#contractGrid").jqGrid("getGridParam", "postData");       
			$.extend(postData, sdata);  //将postData中的查询参数覆盖为空值
			var _url = 'slaContractManage!find.action';
			$('#contractGrid').trigger('reloadGrid');
			$('#contractName').val("");
			$('#contractOrg').val("");
			$('#contractSlaSort').val("");
			sdata = $('#searchContractDiv form').getForm(); 
			$.extend(postData, sdata); 
		},
		
		/**
		 * @description 转到查看SLA详情页面.
		 */
		showSLADetail:function(){
			var gr = jQuery("#contractGrid").jqGrid('getGridParam','selrow');
			
			if(gr==null){
				msgAlert(i18n['msg_atLeastChooseOneData'],'info');
			}else{
				var row=$("#contractGrid").getRowData(gr);
				
				basics.index.initContent("slaContractManage!findSLAContractShowRules.action?contractNo="+row.contractNo);
				
			}
		},
	
		/**
		 * @description 选择服务机构.
		 */
		selectServiceOrg_OpenWindow:function(){
			
			wstuo.rules.ruleCM.selectServiceOrg('#index_selectServiceORG_window_tree','#index_selectServiceORG_window','#slaContract_serviceOrgNo','#slaContract_serviceOrgName');
		},
		
		/**
		 * @description 选择被服务机构.
		 */
		selectByServiceOrg:function(){
			
			$('#selectByServiceOrgTreeDiv').jstree({
				"json_data":{
				    ajax: {
				    	url : "organization!findAll.action?companyNo=-1",
				    	data:function(n){
					    	  return {'parentOrgNo':n.attr ? n.attr("orgNo").replace("node_",""):0};//types is in action
						},
				    	cache:false}
				},
				"plugins" : [ "themes", "json_data", "checkbox" ] 
				});			
		},
		
		/**
		 * @description 打开加入被服务机构窗口.
		 */
		selectByServiceOrg_OpenWindow:function(){
			
			windows('selectByServiceOrgDiv',{width:300,height:400});
			wstuo.slaMge.slaMain.selectByServiceOrg();
		},
		/**
		 * @description 选择服务目录.
		 */
		selectServiceDir:function(){
			
			$('#selectServiceDirTreeDiv').jstree({
				"json_data":{
				    ajax: {url : "event!getCategoryTree.action?num=0",
					      data:function(n){
					    	  return {'types': 'Service','parentEventId':n.attr ? n.attr("id").replace("node_",""):0};//types is in action
					      },
					      cache:false}
				},
				"plugins" : [ "themes", "json_data", "checkbox" ]
				});
		},
		
		/**
		 * @description 打开加入服务目录窗口.
		 */
		selectServiceDir_OpenWindow:function(){
			
			windows('selectServiceDirDiv',{width:250,height:400});
			wstuo.slaMge.slaMain.selectServiceDir();
		},
		/**
		 * @description 加载被服务机构.
		 */
		showByServicesOrgGrid:function(){
			
			var params = $.extend({},jqGridParams, {	
					url:'slaContractManage!findByOrgServices.action',
					/*caption:i18n['title_sla_byServiceOrg'],*/
					colNames:['ID',i18n['title_sla_orgName']],
					colModel:[{name:'orgNo',width:100,align:'center',sortable:false},
					          {name:'orgName',width:490,align:'left',sortable:false}
							  ],
					jsonReader: $.extend(jqGridJsonReader, {id:"orgNo"}),
					sortname:'orgNo',
					/*autowidth:true,*/
					rowNum:10000, 
				});
				$("#byServicesOrgGrid").jqGrid(params);
				$("#byServicesOrgGrid").navGrid('#byServicesOrgGridPager',navGridParams);
				//列表操作项
				$("#t_byServicesOrgGrid").css(jqGridTopStyles);
				$("#t_byServicesOrgGrid").append($('#byServicesOrgGridToolbar').html());
				
				//setGridWidth("#byServicesOrgGrid",15);
				
				$('#byServicesOrgGrid_add').click(wstuo.slaMge.slaMain.selectByServiceOrg_OpenWindow);
				$('#byServicesOrgGrid_remove').click(wstuo.slaMge.slaMain.deleteByServices);
				$('#selectByServiceOrg_getSelectedNodes').click(wstuo.slaMge.slaMain.getSelectedNodes);
		},
		/**
		 * @description 加载服务目录.
		 */
		showServiceDirGrid:function(){
			
			var params = $.extend({},jqGridParams, {	
				url:'slaContractManage!findByServiceDir.action',
				/*caption:i18n['label_knowledge_relatedService'],*/
				colNames:['ID',i18n['label_sla_serviceName']],
				colModel:[{name:'eventId',width:100,align:'center',sortable:false},
				          {name:'eventName',width:490,align:'left',sortable:false}
						  ],
				jsonReader: $.extend(jqGridJsonReader, {id: "eventId"}),
				sortname:'eventId',
/*				autowidth:false,
				width:'570',
				height:'180',*/
				rowNum:10000, 
				});
				$("#relatedServiceDirGrid").jqGrid(params);
				//setGridWidth("#relatedServiceDirGrid",15);
				$("#relatedServiceDirGrid").navGrid('#relatedServiceDirGridPager',navGridParams);
				//列表操作项
				$("#t_relatedServiceDirGrid").css(jqGridTopStyles);
				$("#t_relatedServiceDirGrid").append($('#relatedServiceDirGridToolbar').html());
				
				
				$('#relatedServiceDirGrid_add').click(wstuo.slaMge.slaMain.selectServiceDir_OpenWindow);
				$('#relatedServiceDirGrid_remove').click(wstuo.slaMge.slaMain.deleteServiceDirs);
				$('#selectServiceDir_getSelectedNodes').click(wstuo.slaMge.slaMain.getSelectedServiceDirNodes);
		},
		
		/**
		 * @description 获取已经选择的.
		 */
		getSelectedNodes:function(){
			
			$("#selectByServiceOrgTreeDiv").jstree("get_checked",false,true).each(function (i,n) { 
				 var node = jQuery(this); 
				 var orgType=node.attr('orgType').replace("Panel","");
				 var orgNo=node.attr('orgNo');
				 var orgName=node.attr('orgName');
				 if(orgNo==null){//校正orgNo
					 orgNo=1;
				 }
//				 var row=$("#byServicesOrgGrid").getRowData(orgNo);  //根据ID拿到一行的数据  判断是否已经存在
//				 if(row.orgNo==null){
//					 var dataRow = {orgNo:orgNo,orgName:orgName};
//					 $("#byServicesOrgGrid").jqGrid("addRowData",orgNo, dataRow, "first"); 
//				 }
				
				 wstuo.slaMge.slaMain.fillByServicesOrganization(orgNo,orgType);

				 //$('#byServicesOrgGrid').trigger('reloadGrid');
				 
			});
			
			$('#selectByServiceOrgDiv').dialog('close');
			
		},
		/**
		 * @description 获取已经选择的.
		 */
		getSelectedServiceDirNodes:function(){
			
			$("#selectServiceDirTreeDiv").jstree("get_checked",false,true).each(function (i,n) { 
				 var node = jQuery(this); 
				 var id=node.attr('id');
				 if(id==null){//校正orgNo
					 id=1;
				 }
				
				 wstuo.slaMge.slaMain.fillByServiceDirs(id);

				 //$('#byServicesOrgGrid').trigger('reloadGrid');
				 
			});
			
			$('#selectServiceDirDiv').dialog('close');
			
		},
		/**
		 * @description 填充被服务机构列表.
		 * @param orgNo 机构ID
		 * @param flag 机构类型标识(company,itosp,inner,service)
		 */
		fillByServicesOrganization:function(orgNo,flag){
			
			 var url="organization!findOrganizationsByType.action?orgNo="+orgNo+"&orgType="+flag;
			 
			 $.post(url,function(res){
					
					for(var i=0;i<res.length;i++){
						
						var row=$("#byServicesOrgGrid").getRowData(res[i].orgNo);  //根据ID拿到一行的数据  判断是否已经存在
						if(row.orgNo==null){
							
							var dataRow = {orgNo:res[i].orgNo,orgName:res[i].orgName};
							$("#byServicesOrgGrid").jqGrid("addRowData",res[i].orgNo, dataRow, "first"); 
						}
					}
					
					
				});
		},
		/**
		 * @description 填充被服务目录列表.
		 * @param id 选中的服务目录ID
		 */
		fillByServiceDirs:function(id){
			 var url="event!findSubCategorys.action?categoryId="+id;
			 $.post(url,function(res){
					
					for(var i=0;i<res.length;i++){
						var row=$("#relatedServiceDirGrid").getRowData(res[i].eventId);  //根据ID拿到一行的数据  判断是否已经存在
						if(row.eventId==null){
							
							var dataRow = {eventId:res[i].eventId,eventName:res[i].eventName}; 
							$("#relatedServiceDirGrid").jqGrid("addRowData",res[i].eventId, dataRow, "first"); 
						}
					}
				});
		},
		/**
		 * @description 移除服务机构.
		 */
		deleteByServices:function(){
			checkBeforeDeleteGrid('#byServicesOrgGrid',function(rowIds){
				for(var i=rowIds.length;i>=0;i--){
					$("#byServicesOrgGrid").delRowData(rowIds[i]);
				}
			});
		},
		/**
		 * @description 移除服务目录.
		 */
		deleteServiceDirs:function(){
			checkBeforeDeleteGrid('#relatedServiceDirGrid',function(rowIds){
				for(var i=rowIds.length;i>=0;i--){
					$("#relatedServiceDirGrid").delRowData(rowIds[i]);
				}
			});
		},
		/**
		 * @description 执行SLA导入
		 */
		doImport:function(){
			startProcess();
			$.ajaxFileUpload({
	            url:'slaContractManage!importSLA.action',
	            secureuri:false,
	            fileElementId:'importFile', 
	            dataType:'json',
	            success: function(data,status){
					$('#index_import_excel_window').dialog('close');
					$('#contractGrid').trigger('reloadGrid');
	            
	            	resetForm('#index_import_excel_window form');
	            	
	            	var msg="";
	            	if(data=="success"){
						msg=i18n['msg_dc_dataImportSuccessful'];
						endProcess();
					}else{
						msg=i18n["msg_dc_importFailure"];
						endProcess();
					}
	            	msgShow(msg,'show');
	            }
	      });
		},
		/**
		 * @description 添加多个规则
		 */
		addMultitermRule:function(){
			$('#rightop option').attr("selected", true);
			var strId = $('#rightop').val();
			var strText="";
			$('#rightop').each(function(){
				$(this).children("option").each(function(){
					strText+=$(this).text()+",";	
				})
			});
			var lan=strText.length;
			strText=strText.substring(0,lan-1);

			$('#addSlaRule_propertyValueid').val(strId);
			$('#addSLARule_propertyValue').val(strText);
			$('#editRule_propertyValue').val(strText);
			
			$('#multitermSLA').dialog('close');
		},
		
		/**
		 * @description 初始化
		 */
		init:function(){
			wstuo.includes.loadSelectCustomerIncludesFile();
			wstuo.includes.loadImportCsvIncludesFile();
			wstuo.includes.loadCategoryIncludesFile();
			//绑定日期控件
			DatePicker97(['#slaContract_beginTime','#slaContract_endTime']);
			
			$("#contarctManage_loading").hide();
			$("#contarctManage_content").show();
			
			wstuo.slaMge.slaMain.showContractGrid();	
			
			$('#contractGrid_add').click(wstuo.slaMge.slaMain.addSlaOpenWindow);
			$('#contractGrid_edit').click(wstuo.slaMge.slaMain.editSlaOpenWindow);
			$('#contractGrid_delete').click(wstuo.slaMge.slaMain.deleteSLA);
			$('#contractGrid_search').click(function(){
				windows('searchContractDiv',{width:350});
				//禁止回车事件
				$("#search_contractName").keydown(function (e) {
		            var curKey = e.which;
		            if (curKey == 13) {
		                return false;
		            }
		        });
			});
			$('#contractGrid_showSLADetail').click(wstuo.slaMge.slaMain.showSLADetail);
			$('#contractGrid_doSearch').click(wstuo.slaMge.slaMain.searchSLA);//搜索
			$('#contractGrid_doSave').click(wstuo.slaMge.slaMain.saveSla);
			$('#slaContract_serviceOrgName').click(wstuo.slaMge.slaMain.selectServiceOrg_OpenWindow);
			//导出
			$('#SLAExport').click(function(){
				$('#exportSLA_contractName').val($('#search_contractName').val());
				$('#exportSLAForm').submit();
			});
			//导入数据
			$('#SLAImport').click(function(){
				$('#index_import_href').attr('href',"../importFile/SLA.zip");
				windows('index_import_excel_window',{width:400});
				$("#index_import_confirm").unbind(); //清空事件      				
				$('#index_import_confirm').click(wstuo.slaMge.slaMain.doImport);
			});
			$('#multiterm_saveRuleBtn').click(wstuo.slaMge.slaMain.addMultitermRule);
		}
	}
 }();
 $(document).ready(wstuo.slaMge.slaMain.init);

