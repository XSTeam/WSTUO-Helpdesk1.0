$package('wstuo.slaMge');
/**  
 * @author WSTUO  
 * @constructor SLAServiceManage
 * @description SLA详细信息主函数.

 */
wstuo.slaMge.slaDetail_OverdueGrid = function(){
	this.OverdueGrid_operation;
	return {
		/**
		 * @description 格式化逾期前，逾期后.
		 * @param cell 列显示值
		 */
		formatBeforeOrAfterStr:function(cell){
			return cell=='逾期前升级'?i18n['label_sla_noticeBefore']:i18n['label_sla_noticeAfter'];
		},
		/**
		 * @description SL逾期通知技术员列表.
		 */
		showAutoOverdueGrid:function(){
			var params = $.extend({},jqGridParams, {	
				url:'promoteRule!findPromoteRulePage.action?contractNo='+_contractNo+"&ruleType=overdue",
				colNames:[i18n['title_customer_name'],i18n['sla_request_NotificationWay'],i18n['sla_request_due'],'','','','','','','','',''],
				colModel:[
						  {name:'ruleName',width:70,align:'center'},
						  {name:'beforeOrAfterStr',index:'beforeOrAfter',width:120,align:'center',formatter:wstuo.slaMge.slaDetail_OverdueGrid.formatBeforeOrAfterStr},
						  {name:'timeStr',index:'ruleTime',width:120,align:'center',formatter:wstuo.slaMge.slaDetail_updateGrid.timeStrFormatter},
						  {name:'beforeOrAfter',hidden:true},
						  {name:'ruleTime',hidden:true},
						  {name:'isIncludeHoliday',hidden:true},
						  {name:'assigneeNo',hidden:true},
						  {name:'assigneeName',hidden:true},
						  {name:'ruleNo',hidden:true},
						  {name:'updateLevelNo',hidden:true},
						  {name:'referType',hidden:true},
						  {name:'ruleOverdue',hidden:true}
						  ],
				jsonReader: $.extend(jqGridJsonReader, {id: "ruleNo"}),
				sortname:'ruleNo',
				pager:'#autoOverdueGridPager'
				});
				
				$("#autoOverdueGrid").jqGrid(params);
				$("#autoOverdueGrid").navGrid('#autoOverdueGridPager',navGridParams);
				//列表操作项
				$("#t_autoOverdueGrid").css(jqGridTopStyles);
				$("#t_autoOverdueGrid").append($('#autoOverdueGridToolbar').html());
				//自适应宽度
				setGridWidth("#autoOverdueGrid",15);
				slaDetailGrids.push('#autoOverdueGrid');
		},
		/**
		 * @description 清空表单.
		 */
		resetAutoOverdueFrom:function(){
			
			$('#overdueRule_ruleName').val('');
			$('#overdueRuleDay').val('0');
			$('#overdueRuleHour').val('0');
			$('#overdueRuleMinute').val('0');
			$('#promoteRule_assigneeName').val('');
			$('#promoteRule_assigneeNo').val('');
		},
		
		/**
		 * @description SLA逾期通知技术员 新增
		 */
		addAutoOverdue_OpenWindow:function(){
			//OverdueGrid_operation="savePromoteRule";
			//$('#autoOverdueWindow').window('open');
			//wstuo.slaMge.slaDetail_OverdueGrid.resetAutoOverdueFrom();
			OverdueGrid_operation = "savePromoteRule";
			wstuo.slaMge.slaDetail_OverdueGrid.resetAutoOverdueFrom();
			windows('autoOverdueWindow',{width:480});
		},
		
		
		
		
		/**
		 * @description SLA逾期列表编辑
		 * */
		editAutoOverdue_OpenWindow:function(){
			checkBeforeEditGrid('#autoOverdueGrid',function(rowData){			
				
				 wstuo.slaMge.slaDetail_OverdueGrid.resetAutoOverdueFrom();
				 var time=rowData.ruleTime;
				 $('#overdueRule_ruleNo').val(rowData.ruleNo);
				 $('#overdueRule_ruleName').val(rowData.ruleName);
				 $("input[name='promoteRuleDTO.beforeOrAfter']").val([rowData.beforeOrAfter]); 
				 				 
				 $('#overdueRuleDay,#overdueRuleHour,#overdueRuleMinute').val('0');
				
				 
				 if(time!=null&&time!='null'){
					 
					var day=Math.floor(time/86400);
					if(day>=1){
						 $('#overdueRuleDay').val(day);
					}
					
					var hour=Math.floor(time%86400/3600);					
					if(hour>=1){
						 $('#overdueRuleHour').val(hour);
					}
					
					var min=time%86400%3600/60;
					if(min>=1){
						$('#overdueRuleMinute').val(min);
					} 
				 }
				 
				 $("input[name='promoteRuleDTO.isIncludeHoliday']").val([rowData.isIncludeHoliday]);  
				 $("input[name='promoteRuleDTO.referType']").val([rowData.referType]); 
				 $("input[name='promoteRuleDTO.ruleOverdue']").val([rowData.referType]); 
				
				 $('#promoteRule_assigneeName').val(rowData.assigneeName);
				 $('#promoteRule_assigneeNo').val(rowData.assigneeNo);
				 $('#SLA_AutoOverdueLevel').val(rowData.OverdueLevelNo);

				 //OverdueGrid_operation="mergePromoteRule";
				 //$('#autoOverdueWindow').window('open');
				 OverdueGrid_operation="mergePromoteRule";
				 windows('autoOverdueWindow',{width:480});
			});
		},
		
		
		
		/**
		 * @description SLA逾期列表删除
		 * */
		deleteAutoOverdue:function(){
			checkBeforeDeleteGrid('#autoOverdueGrid',function(rowIds){
				var _param = $.param({'ruleNos':rowIds},true);
				$.post("promoteRule!deletePromoteRules.action",_param,function(){
					$("#autoOverdueGrid").trigger('reloadGrid');
					msgShow(i18n['msg_deleteSuccessful'],'show');
				},"json");
			});
		},

		
		/**
		 * @description SLA逾期列表添加通知
		 * */
		saveAutoOverdue:function(){
			var day=$('#overdueRuleDay').val();
			var hour=$('#overdueRuleHour').val();
			var minute=$('#overdueRuleMinute').val();
			if(day==null||day==''){
				day='0';
				$('#overdueRuleDay').val('0');
			}
			if(hour==null||hour==''){
				hour='0';
				$('#overdueRuleHour').val('0');
			}
			if(minute==null||minute==''){
				minute='0';
				$('#overdueRuleMinute').val('0');
			}
	
				
			if(day!='0'||hour!='0'||minute!='0'){
				
				var frm = $('#autoOverdueWindow form').serialize();
				var _url="promoteRule!"+OverdueGrid_operation+".action";
				startProcess();
				$.post(_url,frm, function(){
					$('#autoOverdueWindow').dialog('close');
					$('#autoOverdueGrid').trigger('reloadGrid');
					msgShow(i18n['msg_operationSuccessful'],'show');
					endProcess();
				});
			}else{
				msgAlert(i18n['NOT_CB_Empty'],'info');
			}
			
		},

		/**
		 * @description SLA逾期初始化方法
		 * */
		init:function(){
			
			wstuo.slaMge.slaDetail_OverdueGrid.showAutoOverdueGrid();
			
			$('#autoOverdueGrid_add').click(wstuo.slaMge.slaDetail_OverdueGrid.addAutoOverdue_OpenWindow);
			$('#autoOverdueGrid_edit').click(wstuo.slaMge.slaDetail_OverdueGrid.editAutoOverdue_OpenWindow);
			$('#autoOverdueGrid_delete').click(wstuo.slaMge.slaDetail_OverdueGrid.deleteAutoOverdue);
			$('#autoOverdueGrid_save').click(wstuo.slaMge.slaDetail_OverdueGrid.saveAutoOverdue);
		}
	};
	
 }();
 $(document).ready(wstuo.slaMge.slaDetail_OverdueGrid.init)

