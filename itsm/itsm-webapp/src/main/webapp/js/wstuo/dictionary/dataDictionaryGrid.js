$package('wstuo.dictionary');
/**  
 * @author QXY  
 * @constructor dictionary
 * @description 数据字典列表
 * @date 2010-11-17
 * @since version 1.0 
 */  
wstuo.dictionary.dataDictionaryGrid = function(){
	this.dcOperation='';
	/**
	 * 组装对应模块groupCode
	 * @param panelDIV 面板div
	 */
	function $groupCode(panelDIV) {
		return panelDIV;
	}
	return {
		/**
		 * @description 颜色格式化
		 * @param cell 当前列值
		 * @param event 事件
		 * @param data 行数据
		 * @private
		 */
		colorFormatter:function(cell,event,data){
			return "<span style='background-color:"+data.color+";font-weight: normal;font-size:99%;color: #000;' class='label'>"+data.dname+"</span>";
		
		},
		/**@description 类表菜单*/
		showDataDictionaryGrid:function(groupCode){
			var params = $.extend({},jqGridParams, {	
				url :'dataDictionaryItems!find.action?dataDictionaryQueryDto.groupCode='+groupCode,
				colNames:['ID',i18n['name'],'',i18n['description'],i18n['remark'],'','',''],
				colModel:[
				          	{name:'dcode',width:20,align:'center',sortable:true,hidden:true},
							{name:'dnameColor',width:40,align:'center',sortable:true,formatter:wstuo.dictionary.dataDictionaryGrid.colorFormatter},
							{name:'dname',hidden:true},
							{name:'description',align:'center',width:40,sortable:false},
							{name:'remark',align:'center',width:40,sortable:false},
							{name:'groupCode',hidden:true},	
							{name:'dno',hidden:true},
							{name:'color',hidden:true}
					    ],
				jsonReader : $.extend(jqGridJsonReader, {
					id : "dcode"
				}),
				sortname : 'dcode',
				pager : $groupCode('#dictionaryItemGridPager'),
				autowidth:true
			});
			$($groupCode('#dictionaryItemGrid')).jqGrid(params);
			$($groupCode('#dictionaryItemGrid')).navGrid($groupCode('#dictionaryItemGridPager'), navGridParams);
			//列表操作项
			$($groupCode('#t_dictionaryItemGrid')).css(jqGridTopStyles);
			if($($groupCode('#t_dictionaryItemGrid')).html()=="")
				$($groupCode('#t_dictionaryItemGrid')).html($('#dataDictionaryGridToolbar').html());
			//自适应宽度
			setGridWidth($groupCode('#dictionaryItemGrid'),10);
		},
		/**
		 * 保存数据字典
		 */
		addDictionaryItem:function(){
			//清空框内内容
			$($groupCode('#addEditDictionaryWindow')+' [name="dataDictionaryItemsDto.dname"]').val("");
			$($groupCode('#addEditDictionaryWindow')+' [name="dataDictionaryItemsDto.description"]').val("");
			$($groupCode('#addEditDictionaryWindow')+' [name="dataDictionaryItemsDto.remark"]').val("");
			$($groupCode('#addEditDictionaryWindow')+' [name="dataDictionaryItemsDto.dno"]').val("");
			$($groupCode('#addEditDictionaryWindow')+' [name="dataDictionaryItemsDto.dcode"]').val("");
			$("#color").val('#ffffff');$("#color").css({'background':'#ffffff'});
			windows('addEditDictionaryWindow',{width:450});
			dcOperation = 'save';
		},
		/**
		 * @description 保存数据字典.
		 * 
		 */
		saveDictionary:function(){		
			var ser = $('#addEditDictionaryWindow form').serialize();	
			var url = "dataDictionaryItems!"+dcOperation+".action";
			startProcess();
			$.post(url, ser, function(){
				$($groupCode('#addEditDictionaryWindow')).dialog('close');
				$($groupCode('#dictionaryItemGrid')).trigger('reloadGrid');			
				msgShow(i18n['saveSuccess'],'show');
				endProcess();
			});	
		},
		 /**
		  * @description 删除数据字典.
		  */
		delDictionary:function(){
			
			checkBeforeDeleteGrid($groupCode('#dictionaryItemGrid'),function(rowIds){				
				var _param = $.param({'dcode':rowIds},true);
				//判断是否为系统数据
				$.post("dataDictionaryItems!findByDcodeIsSystemData.action", _param, function(data){
					if(data==1){
						msgShow(i18n['label_Is_System_Data'],'show');
					}else{
						$.post("dataDictionaryItems!delete.action", _param, function(data){
						if(data){
							msgShow(i18n['msg_deleteSuccessful'],'show');
							$($groupCode('#dictionaryItemGrid')).jqGrid('setGridParam',{page:1}).trigger('reloadGrid'); 
							
						}else{
							msgAlert(i18n['systemDaQXYotDelete'],'info');
						}
					}, "json");		
					}
				});
			});
		},
		/**
		 * 打开搜索框
		 */
		openSearchWindow:function(){
			windows($groupCode('searchDataDictionaryDiv'),{width:380,modal: false});
		},
		 /**
		  * @description 编辑数据字典.
		  */
		editDictionary:function() {		
			checkBeforeEditGrid($groupCode('#dictionaryItemGrid'),function(data){
				$("#dcode").val(data.dcode);
				$("#dname").val(data.dname).focus();
				$("#description").val(data.description);
				$("#remark").val(data.remark);
				$("#groupCode").val(data.groupNo);
				$("#dno").val(data.dno);
				if(data.color==null || data.color==''){
					$("#color").val('#ffffff');
					$("#color").css({'background':'#ffffff'});
				}else{
					$("#color").val(data.color);
				    $("#color").css({'background':data.color});
				}
				dcOperation = 'merge';	
				windows($groupCode('#addEditDictionaryWindow').replace('#',''),{width:400});
			});
		},
		 /**
		  * @description 搜索
		  * */
		searchDictionary:function() {		
			var sdata = $('#searchDataDictionaryDiv form').getForm();
			/*var postData = $('#dictionaryItemGrid').jqGrid("getGridParam", "postData");  
			postData=sdata;*/
			var _url = 'dataDictionaryItems!find.action';//+$('#searchDataDictionaryDiv form').serialize();		
			$($groupCode('#dictionaryItemGrid')).jqGrid('setGridParam',{url:_url,page:1,postData:sdata}).trigger('reloadGrid'); 
			return false;
		},
		/**
		 * 导出数据到Excel
		 */
		exportView:function(){
			$($groupCode('#searchDataDictionaryDiv')+' form').submit();
		},
		/**
		 * 导入数据.
		 */
		importData:function(){
			if(language=="en_US"){
				$('#index_import_href').attr('href',"../importFile/en/DataDictionary.zip");
			}else{
				$('#index_import_href').attr('href',"../importFile/DataDictionary.zip");
			}
			windows('index_import_excel_window',{width:400});
			$('#index_import_confirm').unbind().click(wstuo.dictionary.dataDictionaryGrid.importExcel);
		},
		/**
		 * @description 导入数据.
		 */
		importExcel:function(){
			if($("#importFile").val().length>0){
				startProcess();
				$.ajaxFileUpload({
		            url:'dataDictionaryItems!importDataDictionaryItemData.action?dataDictionaryItemsDto.groupCode='+$("#groupCode").val(),
		            secureuri:false,
		            fileElementId:'importFile', 
		            dataType:'json',
		            success: function(data,status){
		            	if(data=="FileNotFound"){
							msgAlert(i18n['msg_dc_fileNotExists'],'error');
							endProcess();
						}else if(data=="IOError"){
							msgAlert(i18n['msg_dc_importFailure'],'error');
							endProcess();
						}else{
							$('#importDataDictionaryWindow').dialog('close');
							$($groupCode('#dictionaryItemGrid')).trigger('reloadGrid'); 
							msgAlert(data.replace('Total',i18n['opertionTotal']).replace('Insert',i18n['newAdd']).replace('Update',i18n['update']).replace('Failure',i18n['failure']),'show');
							endProcess();
						}
		            }
		        });
			}else{
				msgAlert(i18n['msg_dc_fileNull'],'info');
				endProcess();
			}
		},
		/**
		 * 初始化
		 * @private 
		 */
		init:function(){
			wstuo.dictionary.dataDictionaryGrid.showDataDictionaryGrid('requestStatus');//加载数据列表
			$("#groupCode,#searchItem_groupCode").val('requestStatus');
			$('#saveDataDictionaryBtn').click(wstuo.dictionary.dataDictionaryGrid.saveDictionary);//绑定事件
			$('#searchDataDictionaryBtn').click(wstuo.dictionary.dataDictionaryGrid.searchDictionary);//绑定事件
			$('#exportDataDc').click(wstuo.dictionary.dataDictionaryGrid.exportView);//导出
			$('#importDataDc').click(wstuo.dictionary.dataDictionaryGrid.importData);//导入	
		}
	}
}();
$(document).ready(wstuo.dictionary.dataDictionaryGrid.init);

