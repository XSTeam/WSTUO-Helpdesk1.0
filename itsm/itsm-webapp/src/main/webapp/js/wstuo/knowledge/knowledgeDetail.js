$package('wstuo.knowledge');
/**  
 * @author Van  
 * @constructor knowledgeDetail
 * @description 知识库详情
 * @date 2010-11-17
 * @since version 1.0 
 */
wstuo.knowledge.knowledgeDetail = function() 
{	

	return {
		/**
		 *@description 根据知识ID显示知识详细信息
		 *@param id 知识ID
		 */
		showKnowledgeDetail:function(id){
			
			var url="knowledgeInfo!showKnowledge.action?kid="+id;
			basics.index.initContent(url);
			//basics.tab.tabUtils.reOpenTab(url,i18n['knowledge']+i18n['details']);
			
		}
	}
	
}();



