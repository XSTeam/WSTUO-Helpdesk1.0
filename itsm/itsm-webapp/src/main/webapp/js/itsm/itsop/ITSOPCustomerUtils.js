$package('itsm.itsop');

/**  
 * @author Van  
 * @constructor WSTO
 * @description 选择关联请求公共方法.
 * @date 2011-05-28
 * @since version 1.0 
 */
itsm.itsop.ITSOPCustomerUtils = function() {
	
	this.index_loadITSOPCustomerGridFlag="0";
	this.index_selectITSOPUser_id;
	this.index_selectISSOPUser_name;

	//载入
	return {

		/**
		 * @description 选择关联请求Grid
		 */
		showGrid:function(){
			var params = $.extend({},jqGridParams, {
			url:'itsopUser!findITSOPUserPager.action',
			colNames:[i18n['common_name'],i18n['common_phone'],i18n['common_email'],i18n['common_address'],''],
			colModel:[
					  {name:'orgName',width:20,align:'center'},
					  {name:'officePhone',width:20,align:'center'},
					  {name:'email',width:30,align:'center'},
					  {name:'address',width:30,align:'center'},
					  {name:'orgNo',hidden:true}
					  ],
			jsonReader: $.extend(jqGridJsonReader, {id: "orgNo"}),
			sortname:'orgNo',
			pager:'#index_itsop_customer_grid_pager',
			multiselect:false,
			toolbar:false,
			ondblClickRow:itsm.itsop.ITSOPCustomerUtils.confirmSelect
			
			});
			$("#index_itsop_customer_grid").jqGrid(params);
			$("#index_itsop_customer_grid").navGrid('#index_itsop_customer_grid_pager',navGridParams);
			//列表操作项
			$("#t_index_itsop_customer_grid").css(jqGridTopStyles);

		},
		
		/**
		 * @description 打开选择外包客户窗口.
		 * @param p_id 用户id
		 * @param p_name 用户name
		 */
		openSelectWindow:function(p_id,p_name){
			index_selectITSOPUser_id=p_id;
			index_selectISSOPUser_name=p_name;

			windows('index_itsop_customer_window',{width:600,height:400});
			if(index_loadITSOPCustomerGridFlag=="0"){
				
				itsm.itsop.ITSOPCustomerUtils.showGrid();
				index_loadITSOPCustomerGridFlag="1";

			}else{
				$('#index_itsop_customer_grid').trigger('reloadGrid');
			}		

		},
		/**
		 * @description 选定选中的外包客户
		 * @param rowId grid列表id
		 */
		confirmSelect:function(rowId){
			var rowData=$('#index_itsop_customer_grid').getRowData(rowId);
			$(index_selectITSOPUser_id).val(rowData.orgNo);
			$(index_selectISSOPUser_name).val(rowData.orgName);
			
			$('#index_itsop_customer_window').dialog('close');
		}
		
	};
	
}();