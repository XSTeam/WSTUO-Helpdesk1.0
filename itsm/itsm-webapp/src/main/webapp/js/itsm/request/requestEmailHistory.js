$package('itsm.request');
/**  
 * @fileOverview 请求历史邮件主函数
 * @author QXY
 * @version 1.0  
 */  
 /**  
 * @author QXY  
 * @constructor 请求历史邮件主函数
 * @description 请求历史邮件主函数
 * @date 2011-05-09
 * @since version 1.0 
 */
itsm.request.requestEmailHistory=function(){
	
	return {

		/**
		 * @description 类型格式化.
		 * @param cellValue 列显示值
		 * @param options 操作项
		 */
		folderNameFormat:function(cellvalue, options){
			if(cellvalue=="INBOX"){
				return i18n.common_receive;
			}
				
			else{
				return i18n.common_sent;
			}
				
		},
		
		  /**
         * @description 发件人格式化
         * @param cellValue 列显示值
		 * @param options 操作项
         */
        FromUserFormatter:function(cellvalue,options){
        	if(cellvalue!=null && cellvalue!=='')
        		return (cellvalue.replace('<',' ')).replace('>',' ');
        	else
        		return '';
        },
        
        
        /**
         * @description 收件人格式化
         * @param cellValue 列显示值
		 * @param options 操作项
         */
        ToUserFormatter:function(cellvalue,options){
        	if(cellvalue!=null && cellvalue!=='')
        		return (cellvalue.replace('<',' ')).replace('>',' ');
        	else
        		return '';
        },
		
		
		 /**
         * @description 标题格式化
		 * @param cell 列显示值
		 * @param opt 操作项
		 * @param data 行对象
         */
        emailGridTitleFormatter: function (cell, opt, data) {
		
            return "<a href=javascript:itsm.request.requestEmailHistory.showEmailDetail('" + data.emailMessageId + "')>" + cell + "</a>";
        },

		/**
		 * @description 请求历史邮件
		 * @param div 历史邮件列表ID
		 * @param code 请求编号
		 */
		emailHistoryList:function(div,code,eno){
			var params = $.extend({},jqGridParams, {	
				url:'email!findEmailAboutRequest.action',
//				caption:i18n['caption_emailGrid'],
				colNames:[i18n.number,i18n.title_mailTitle,i18n.title_mailType,i18n.title_mailFromUser,i18n.title_mailToUser,i18n.title_sendTime,i18n.title_receviceTime,i18n.remark],
				postData:{'emailMessageQueryDto.moduleCode':code,'emailMessageQueryDto.moduleEno':eno},
				colModel:[
			   		{name:'emailMessageId',width:60,sortable:true},
			   		{name:'subject',width:260,formatter:this.emailGridTitleFormatter},
			   		{name:'folderName',width:160,formatter:this.folderNameFormat},
			   		{name:'fromUser',width:160,formatter:this.FromUserFormatter},
			   		{name:'toUser',width:160,sortable:false,formatter:this.ToUserFormatter},
			   		{name:'sendDate',width:200,formatter:timeFormatter},
			   		{name:'receiveDate',width:180,formatter:timeFormatter},
			   		{name:'keyword',width:260,formatter:itsm.request.requestEmailHistory.isOriginal}
			   	],
			   	toolbar: false,
			   	multiselect:false,
			   	ondblClickRow:function(rowId){itsm.request.requestEmailHistory.showEmailDetail(rowId);},
				jsonReader: $.extend(jqGridJsonReader, {id: "emailMessageId"}),
				sortname:'emailMessageId',
				pager:'#'+div+'Pager'
			});
			$("#"+div+"Grid").jqGrid(params);
			$("#"+div+"Grid").navGrid('#'+div+'Pager',navGridParams);
			setTimeout(function(){setGridWidth("#"+div+"Grid",9);},100);
		},
		
		/**
		 * @description 显示邮件详细信息
		 * @param id 请求ID 
		 */
		showEmailDetail:function(id){
			var url="email!showEmailDetail.action?eid="+id;
			basics.tab.tabUtils.refreshTab(i18n.common_email+i18n.details,url);		
		},
		/**
		 * @description 下载附件
		 * @param attachMentId 附件ID
		 */
		downloadAttachMent:function(attachMentId){
			
			var url="attachment!download.action?downloadAttachmentId="+attachMentId;
			window.open(url);
			//window.location.href=url;
		},
		/**
		 * @description 是否是原始邮件
		 * @param cell 列显示值
		 */
		isOriginal:function(cell){
			if(cell!=null && cell!=='')
				return "<span style='color:red'>"+i18n.email_original+"</span>";
			else
				return "";
		}

	};
	
}();
