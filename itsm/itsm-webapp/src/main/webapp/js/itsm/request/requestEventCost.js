$package("itsm.request");
/**  
 * @fileOverview 请求进展及成本主函数
 * @author QXY
 * @version 1.0  
 */  
 /**  
 * @author QXY  
 * @constructor 请求进展及成本主函数
 * @description 请求进展及成本主函数
 * @date 2011-05-09
 * @since version 1.0 
 */
itsm.request.requestEventCost = function() {
	this.ladeDefaultCurrencyFalg=true;
	return {
		
		/**
		 * @description 保存进展及成本
		 */
		saveEventCost:function(){
			startProcess();
			var requestEventCost=new eventCost($('#requestDetails_requestNo').val(),'request');			
			requestEventCost.saveEventCost();
		},
		
		/**
		 * @description 新增进展及成本
		 */
		addEventCost:function(){
			
			var requestEventCost=new eventCost($('#requestDetails_requestNo').val(),'request');	
			$('#index_timeCost_save').unbind('click');
			$('#index_timeCost_save').click(itsm.request.requestEventCost.saveEventCost);
			$('#index_timeCost_costTaskTitle').unbind('click');
			$('#index_timeCost_costTaskTitle').click(itsm.request.requestEventCost.selectRelatedEventTask);
			
			if(ladeDefaultCurrencyFalg)
			ladeDefaultCurrencyFalg=false;
			requestEventCost.addEventCost_win();
			$('#index_timeCost_responseTime').val(timeFormatter($('#requestDetail_responsesTime').val()));
		},
		
		/**
		 * @description 编辑进展及成本
		 */
		editEventCost:function(){
			var requestEventCost=new eventCost($('#requestDetails_requestNo').val(),'request');	
			$('#index_timeCost_save').unbind('click');
			$('#index_timeCost_save').click(itsm.request.requestEventCost.saveEventCost);
			$('#index_timeCost_costTaskTitle').unbind('click');
			$('#index_timeCost_costTaskTitle').click(itsm.request.requestEventCost.selectRelatedEventTask);
			if(ladeDefaultCurrencyFalg)
				ladeDefaultCurrencyFalg=false;
			requestEventCost.editEventCost_aff();
			$('#index_timeCost_responseTime').val(timeFormatter($('#requestDetail_responsesTime').val()));
		},
		/**
		 * @description 选中要编辑的进展及成本
		 * @param rowId 要编辑的行ID
		 */
		editEventCost_aff:function(rowId){
			var requestEventCost=new eventCost($('#requestDetails_requestNo').val(),'request');	
			$('#index_timeCost_save').unbind('click');
			$('#index_timeCost_save').click(itsm.request.requestEventCost.saveEventCost);
			$('#index_timeCost_costTaskTitle').unbind('click');
			$('#index_timeCost_costTaskTitle').click(itsm.request.requestEventCost.selectRelatedEventTask);
			if(ladeDefaultCurrencyFalg)
			ladeDefaultCurrencyFalg=false;
			requestEventCost.editEventCost_affs(rowId,requestEventCost);
		},
		/**
		 * @description 删除进展及成本
		 */
		deleteEventCost:function(){
			var requestEventCost=new eventCost($('#requestDetails_requestNo').val(),'request');	
			requestEventCost.deleteEventCost_aff();
		},
		/**
		 * @description 选中要删除的进展及成本
		 * @param rowId 要删除的行ID
		 */
		deleteEventCost_aff:function(rowId){
			var requestEventCost=new eventCost($('#requestDetails_requestNo').val(),'request');
			requestEventCost.deleteEventCost_affs(rowId,requestEventCost);
		},
		/**
		 * @description 搜索相关联的事件任务
		 */
		selectRelatedEventTask:function(){
			var requestEventTask=new eventTask($('#requestDetails_requestNo').val(),'request');	
			requestEventTask.relatedEventTaskGrid();
		},
		/**
		 * @description 加载货币单位
		 */
		loadCostCurrency:function(){
    		$.post('currency!findDefaultCurrency.action',function(data){
    			if(data!=null && data!==''){
    				$('#index_timeCost_perHourFees_currency').text(data);
    				$('#index_timeCost_skillFees_currency').text(data);
    				$('#index_timeCost_othersFees_currency').text(data);
    				$('#index_timeCost_totalFees_currency').text(data);
    			}else{
    				$('#index_timeCost_perHourFees_currency').text('RMB');
    				$('#index_timeCost_skillFees_currency').text('RMB');
    				$('#index_timeCost_othersFees_currency').text('RMB');
    				$('#index_timeCost_totalFees_currency').text('RMB');
    			}
    		});
    	},
	
    	/**
		 * @description 请求进展及成本初始化
		 */
		init:function() 
		{
		}
	};
}();
$(document).ready(itsm.request.requestEventCost.loadCostCurrency);