$package("itsm.request");
$import('wstuo.customForm.formControlImpl');
$import('itsm.itsop.selectCompany');
$import('wstuo.user.userUtil');
$import('wstuo.category.eventCategoryTree');
/**  
 * @author Wstuo  
 * @constructor requestCommon
 * @description 请求公共函数
 * @since version 1.0 
 */
itsm.request.requestCommon=function(){
	var loadAutoUserName=true;
	var loadAutoCompany=true;
	//载入
	return {
		
		/**
		 * 初始化默认表单
		 */
		initDefaultForm:function(htmlDivId){
			
			$("#"+htmlDivId+" .field_opt").remove();
			$("#"+htmlDivId+" .field_options").css('cursor','auto');
			var formCustomContents = $("#"+htmlDivId).html();
			formCustomContents = wstuo.customForm.formControlImpl.replaceSpecialAttr(formCustomContents,htmlDivId);
			$("#"+htmlDivId).html(formCustomContents);
			if(htmlDivId == "addRequest_formField" || htmlDivId == "addScheduledTaskRequest_formField" ){
				itsm.request.requestCommon.setDefaultParamValue(htmlDivId);
			}
			wstuo.customForm.formControlImpl.formCustomInit('#'+htmlDivId);
			
			/*var attrJson = wstuo.customForm.formControlImpl.setFormCustomContentJson("#"+htmlDivId);
						
			return common.security.base64Util.decode(attrJson);*/
		},
		/**
		 * 初始化默认表单
		 */
		initDefaultFormByAttr:function(htmlDivId){
			
			$("#"+htmlDivId+" .field_opt").remove();
			$("#"+htmlDivId+" .field_options").css('cursor','auto');
			var formCustomContents = $("#"+htmlDivId).html();
			formCustomContents = wstuo.customForm.formControlImpl.replaceSpecialAttr(formCustomContents,htmlDivId);
			$("#"+htmlDivId).html(formCustomContents);
			if(htmlDivId == "addRequest_formField" || htmlDivId == "addScheduledTaskRequest_formField" ){
				itsm.request.requestCommon.setDefaultParamValue(htmlDivId);
			}
		},
		setDefaultParamValue:function(htmlDivId){
			$("#"+htmlDivId+" #request_companyName").val(companyName);
			$("#"+htmlDivId+" #request_userName").val(fullName);
			$("#"+htmlDivId+" #request_companyNo").val(companyNo);
			$("#"+htmlDivId+" #request_userId").val(userId);
		},
		/**
		 * 选择公司
		 */
		chooseCompany :function(companyNo,companyName,event){
			var formId=$(event).closest('form').attr("id");
			if(formId)
				itsm.itsop.selectCompany.openSelectCompanyWin(
						'#'+formId+' '+companyNo,
						'#'+formId+' '+companyName,
						'#request_userName,#request_userId,#selectCreator,#addRequestUserId,#add_request_ref_ciname,#add_request_ref_ciid');
		},
		/**
		 * @description 选择请求者.
		 */
		selectRequestCreator:function(userName,userId,companyNo,event){
			var formId=$(event).closest('form').attr("id");
			if(formId)
				wstuo.user.userUtil.selectUser(
						'#'+formId+' '+userName,
						'#'+formId+' '+userId,
						'','fullName',
						$('#'+formId+' '+companyNo).val(),function(){});
		},
		/**
		 * @description 选择请求分类.
		 * @param showAttrId 扩展属性Id
		 * @param dtoName 实体对象名
		 */
		selectRequestCategory:function(categoryName,categoryNo,etitle,edesc,event){
			var formId=$(event).closest('form').attr("id");
			if(formId)
				wstuo.category.eventCategoryTree.showSelectTree(
						'#request_category_select_window',
                        '#request_category_select_tree',
                        'Request',
                        '#'+formId+' '+categoryName,
                        '#'+formId+' '+categoryNo,
                        '',
                        '',
                        null,
                        'requestDTO',function(formCustomId){
							if(defaultFormId!='edit'){
								if(defaultFormId!==0 && !formCustomId ){
									formCustomId=defaultFormId;
								}
								if(formCustomId){
									$("#addRequest_formField_row").show();
									itsm.request.addRequest.loadRequestFormHtmlByFormId(formCustomId,'addRequest_formField');
									$("#addRequest_formId").val(formCustomId);
								}else{
									$("#addRequest_formId").val(0);
									$("#addRequest_formField").empty();
									$("#addRequest_formField_row").hide();
								}
							}
						});
			
		},
		getFormAttributesValue:function(formId){
			$(formId+" :hidden").each(function(i, obj){
				var value='';
				var valuename='';
				var attrType=$(obj).attr("attrType");
				if(attrType==='checkbox'){
					$(formId+" input[name='"+$(obj).attr("id")+"']:checked").each(function(i, o){
					    value=value+","+$(o).val();
					    valuename=valuename+"，"+$(o).attr('title');
					});
					$(obj).val(value.substr(1)+"~"+valuename.substr(1));
				}else if(attrType==='radio'){
					var o=$(formId+" input[name='"+$(obj).attr("id")+"']:checked");
					$(obj).val(o.val()+"~"+o.attr('title'));
				}else if(attrType==='tree'){
					var val=$(formId+" #show_"+$(obj).attr("id")).val();
					$(obj).val($(obj).val()+"~"+val);
				}
			});
		},
		/**
		 * @description 选择位置.
		 * @param showAttrId 扩展属性Id
		 * @param dtoName 实体对象名
		 */
		selectRequestLocation:function(categoryName,categoryNo,event){
			var formId=$(event).closest('form').attr("id");
			if(formId)
				wstuo.category.eventCategoryTree.showSelectTree(
						'#ci_loc_select_window',
                        '#ci_loc_select_tree',
                        'Location',
                        '#'+formId+' '+categoryName,
                        '#'+formId+' '+categoryNo,
                        null,
                        null,
                        null,
                        'requestDTO');
		},
		
		/**
		 * 对旧表单的值进行赋值
		 * @param htmlDivId 表单Id
		 * @param data 数据源
		 */
		setRequestParamValueToOldForm:function(htmlDivId,data){
			$(htmlDivId+" #request_companyName").val(data.companyName);
			$(htmlDivId+" #request_companyNo").val(data.companyNo);
			$(htmlDivId+" #request_userName").val(data.createdByName);
			$(htmlDivId+" #request_userId").val(data.createdByNo);
			$(htmlDivId+" #request_edesc").val(data.edesc);
//			$(htmlDivId+" #request_locationName").val(data.locationName);
//			$(htmlDivId+" #request_locationNos").val(data.locationNos);
			
			if(htmlDivId=="#edit_basicInfo_field"){
				$(htmlDivId+" #editrequest_edesc").val(data.edesc);
			}
			$(htmlDivId+" #request_etitle").val(wstuo.tools.xssUtil.html_code(data.etitle));
			if(data.requestCategoryName!=null && data.requestCategoryName!=""){
				$(htmlDivId+" #request_categoryName").val(data.requestCategoryName);
			}else{
				$(htmlDivId+" #request_categoryName").val(data.ecategoryName);
			}
			if(data.requestCategoryNo!=null && data.requestCategoryNo!=""){
				$(htmlDivId+" #request_categoryNo").val(data.requestCategoryNo);
			}else{
				$(htmlDivId+" #request_categoryNo").val(data.ecategoryNo);
			}
		},
		/**
		 * 对旧表单的值进行赋值
		 * @param htmlDivId 表单Id
		 * @param data 数据源
		 */
		setRequestParamValueToOldFormByDetail:function(htmlDivId,data){
			$(htmlDivId+" #request_companyName").closest('.field,.field_lob').html(data.companyName);
			$(htmlDivId+" #request_userName").closest('.field,.field_lob').html(data.createdByName+'<a title="'+i18n.label_contactInfo+'" onclick="$(\'#requesterContactInfo_win\').dialog()"><img src="../images/icons/user.gif" style="vertical-align:middle;"></a>');
			$(htmlDivId+" #request_edesc").closest('.field_lob').html(data.edesc);
			$(htmlDivId+" #request_etitle").closest('.field,.field_lob').html(data.etitle);
			$(htmlDivId+" #request_categoryName").closest('.field,.field_lob').html(data.requestCategoryName);
			$(htmlDivId+" #request_locationName").closest('.field,.field_lob').html(data.locationName);
		},
		/**
		 * 自动补全
		 */
		bindAutoCompleteToRequest:function(htmlDivId){
			loadAutoUserName=true;//请求用户
			$('#'+htmlDivId+' #request_userName').focus(function(){
				if(loadAutoUserName){
					//请求用户
					autocomplete.bindAutoComplete('#'+htmlDivId+' #request_userName','com.wstuo.common.security.entity.User','fullName','fullName','userId','Long','#'+htmlDivId+' #request_userId','#'+htmlDivId+' #request_companyNo','true');
					loadAutoUserName=false;
				}
			});
			loadAutoCompany=true;//所属客户
			if(versionType==="ITSOP"){
				$('#'+htmlDivId+' #request_companyName').focus(function(){
					if(loadAutoCompany){
						loadAutoCompany=false;//所属客户
						autocomplete.bindAutoComplete('#'+htmlDivId+' #request_companyName','com.wstuo.common.security.entity.Organization','orgName','orgName','orgNo','Long','#'+htmlDivId+' #request_companyNo',userName,'false');
					}
				});
			}
		},
		changeCompany:function(htmlDivId){
			var temp_companyName = $(htmlDivId+' #request_companyName').val();
			$(htmlDivId+' #request_companyName').focus(function(){
				temp_companyName = $(htmlDivId+' #request_companyName').val();
			});
			$(htmlDivId+' #request_companyName').blur(function(){
				if(temp_companyName!==$(htmlDivId+' #request_companyName').val())
					$(htmlDivId+' #request_userName').val('');
			});
		},
		/**
		 * 给请求编辑form赋值
		 * @data 数据对象
		 * @formId 表单id
		 */
		setRequestFormValuesByScheduledTask:function(eventdata,formId,eventEavVals,eavNo,callback){
			itsm.request.requestCommon.setRequestParamValueToOldForm(formId,eventdata);
			//itsm.request.requestCommon.setRequestParamValueToOldFormByDataDictionaray(formId,eventdata,callback);
			itsm.request.requestCommon.setAttributesValue(eventdata,formId,eventEavVals,eavNo,callback);
		},
		/**
		 * 赋值扩展属性
		 */
		setAttributesValue:function(data,formId,callback){
			if(data!==null){
				var eventEavVals=data.attrVals;
				for(var o in eventEavVals){
					var value='';
					var attrName=o;
					var obj=$(formId+" :input[name*='"+attrName+"']");
					if(eventEavVals && eval('eventEavVals.'+attrName)!==undefined && eval('eventEavVals.'+attrName)!==''){
						value = eval('eventEavVals.'+attrName);
					}
					if(value==undefined || value==null)value='';
					
					if(obj.attr("attrType")=="select"){
						$(formId+" select[name*='"+attrName+"']").attr("val",value);
						//wstuo.dictionary.dataDictionaryUtil.setSelectOptionsByCode(obj.attr('attrdataDictionary'),formId+' #'+selectId,value);
					}if(obj.attr("attrType")=="tree"){
						obj.siblings(".form-control").val(value.substr(value.indexOf('~')+1));
						obj.val(value.substr(0,value.indexOf('~')));
					}else{
						obj.val(value);
					}
				}
				callback();
			}
		},
		/**
		 * 给请求编辑form赋值
		 * @data 数据对象
		 * @formId 表单id
		 */
		setRequestFormValues:function(eventdata,formId,callback){
			itsm.request.requestCommon.setRequestParamValueToOldForm("#edit_basicInfo_field",eventdata);
			//itsm.request.requestCommon.setRequestParamValueToOldFormByDataDictionaray(formId,eventdata,callback);
			itsm.request.requestCommon.setAttributesValue(eventdata,formId,callback);
		},
		/**
		 * 给请求详情form赋值
		 * @data 数据对象
		 * @formId 表单id
		 */
		setRequestFormValuesByDetail:function(eventdata,formId,eventType,eavId,callback){
			itsm.request.requestCommon.setRequestParamValueToOldFormByDetail(formId,eventdata);
			callback();
		},
		/**
		 * @description 根据扩展属性id查询数据字典名称
		 * @param attrValNo
		 * @param showAttrId 显示控件id
		 */
		showDataDicInfo:function(attrValNo,formId,attrName){
			$.post('dataDictionaryItems!findByDcode.action?groupNo='+attrValNo,function(res){
				$(formId+" :input[name*='"+attrName+"']").replaceWith(res.dname);
			});
		},
		setfieldOptionsLobCss:function(htmlDivId){
			var len=$(htmlDivId+" .field_options").not('.field_options_2Column,.field_options_lob').length;
			if(len%2!=0){
				$(htmlDivId).append('<div class="field_options"><div class="label"></div><div class="field"></div></div>');
			}
		},
		showFormBorder:function(isShowBorder,htmlDivId,isCssId,noCssId){
			if(isShowBorder==="1"){
				$(isCssId).attr("rel","stylesheet");
				$(noCssId).attr("rel","");
			}else{
				$(isCssId).attr("rel","");
				$(noCssId).attr("rel","stylesheet");
				$(htmlDivId+" :input").not(".control").css("margin-top","10px");
			}
		},
		removeRequiredField:function(page){
			if(page==="setting"){
				$("#request_userName").attr("class","control-img").removeAttr("required");
				$("#request_userName").parent().prev().children("div[class=required_div]").remove();
				$("#request_companyName").attr("class","control-img").removeAttr("required");
				$("#request_companyName").parent().prev().children("div[class=required_div]").remove();
			}
		}
	}
}();