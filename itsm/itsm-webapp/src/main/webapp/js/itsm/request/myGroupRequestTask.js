$package("itsm.request"); 
 /**  
 * @author QXY 
 * @constructor myGroupRequestTask
 * @description 属于当前用户所属组的请求.
 * @date 2011-02-26
 * @since version 1.0 
 */
itsm.request.myGroupRequestTask=function(){
	return {
		/**
		 * @description 数据列表行格式化.
		 */
		titleUrlFrm:function(cellvalue, options, rowObject){
			var executionId=rowObject.executionId;
			if(executionId.indexOf('Changes')>=0){
				return "<a href=javascript:basics.tab.tabUtils.reOpenTab('change!changeInfo.action?queryDTO.eno="+rowObject.variables.dto.eno+"&queryDTO.taskId="+rowObject.id+"&queryDTO.assignee="+rowObject.assignee+"','"+i18n.change_detail+"')>"+cellvalue+":"+rowObject.variables.dto.etitle+"</a>";
			}	
			else if(executionId.indexOf('request')>=0 || executionId.indexOf('Request')>=0){	
				return '<a href="JavaScript:itsm.request.myGroupRequestTask.requestDetails('+rowObject.variables.dto.eno+','+rowObject.id+')">'+cellvalue+":"+rowObject.variables.dto.etitle+'</a>';
			}else{
				return cellvalue;
			}
		},
		/**
		 * @description 转到请求详情页面.
		 * @param eno 编号eno
		 * @param taskId 任务Id
		 */
		requestDetails:function(eno,taskId){
			basics.tab.tabUtils.refreshTab(i18n.request_detail,'request!requestDetails.action?eno='+eno+'&taskId='+taskId);
			
		},
		/**
		 * @description 加载我们组的请求列表.
		 */
		myGroupRequestTaskGrid:function(){
			var params=$.extend({},jqGridParams,{
//				caption:i18n['caption_request_taskGrid'],
				url:'upload!showMyGroupTasks.action',
				colNames:['ID',i18n.common_name,i18n.title_request_requestTitle,i18n.common_desc,i18n.common_createTime,'eno'],
				colModel:[
	  					  {name:'id'},
						  {name:'name',width:150,formatter:this.titleUrlFrm},
						  {name:'variables.dto.etitle',hidden:true},
						  {name:'description',index:'description',width:100},
						  {name:'createTime',index:'createTime',width:100,formatter:timeFormatter},
						  {name:'variables.dto.eno',hidden:true}
					  ],
				toolbar: false,
				jsonReader: $.extend(jqGridJsonReader, {id: "id"}),
				sortname:'id',
				pager:'#MyGroupRequestTaskPager'
			});
			$("#MyGroupRequestTaskGrid").jqGrid(params);
			$("#MyGroupRequestTaskGrid").navGrid('#MyGroupRequestTaskPager',navGridParams);
			//自适应大小
			setGridWidth("#MyGroupRequestTaskGrid","regCenter",20);
		},
		/**
		 * 初始化
		 * @private
		 */
		init: function() {
			itsm.request.myGroupRequestTask.myGroupRequestTaskGrid();
		}
	};
}();
$(document).ready(itsm.request.myGroupRequestTask.init);