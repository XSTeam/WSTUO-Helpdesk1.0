$package("itsm.cim") 
$import('wstuo.includes');

 /**  
 * @author Wstuo  
 * @constructor Wstuo
 * @description 绑定一级菜单
 * @date2013-07-16
 * @since version 1.0 
 */
itsm.cim.cimTop = function() {
	
	//载入
	return {
		/**
		 * 绑定一级菜单
		 */
		menuClick:function(){
			
			//showLeftMenu('../pages/itsm/cim/leftMenu.jsp?random='+Math.random()*10+1,'leftMenu');
			basics.tab.tabUtils.addTab(i18n['ci_configureItemAdmin'],'../pages/itsm/cim/configureItem.jsp',function(){
				itsm.cim.cimTop.includesLoading();
			});
			
			$('#main_menu_tabs li').removeClass("tabs-selected");
			$('#cimTopMenu_li').addClass("tabs-selected");
		},
		includesLoading:function(){
			//配置项
			startLoading();
			setTimeout(function(){  
				endLoading();
			},800);
			wstuo.includes.loadSelectCIIncludesFile();
			wstuo.includes.loadSelectCustomerIncludesFile();
			wstuo.includes.loadCategoryIncludesFile();
			wstuo.includes.loadCustomFilterIncludesFile();
		}
	};
	
}();