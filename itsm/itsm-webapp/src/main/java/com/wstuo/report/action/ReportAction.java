package com.wstuo.report.action;
import com.opensymphony.xwork2.ActionSupport;
import java.util.List;

import javax.annotation.Resource;

import com.wstuo.itsm.request.service.IRequestService;

@SuppressWarnings("serial")
public class ReportAction extends ActionSupport{
	private List requestlist;
    @Resource
    private IRequestService requestService;

	public void setRequestService(IRequestService requestService) {
		this.requestService = requestService;
	}
    
	
	public List getRequestlist() {
		return requestlist;
	}


	public void setRequestlist(List requestlist) {
		this.requestlist = requestlist;
	}


	/**
	 * 统计请求分类报表
	 * @return
	 */
	public String requestCatagoryReport(){
		requestlist=requestService.requestCatagoryReport();
		return "requestReport";
	}
	/**
	 * 统计请求状态报表
	 * @return
	 */
	public String requestStatusReport(){
		requestlist=requestService.requestStatusReport();
		return "requestReport";
	}
	/**
	 * 统计SLA响应率报表
	 * @return
	 */
	public String requestSLAStatusReport(){
		requestlist=requestService.requestSLAStatusReport();
		return "requestReport";
	}
	/**
	 * 统计SLA完成率报表
	 * @return
	 */
	public String requestSLACompleteReport(){
		requestlist=requestService.requestSLACompleteReport();
		return "requestReport";
	}
	/**
	 * 技术员工作量统计
	 * @return
	 */
	public String engineerWorkloadReport(){
		requestlist=requestService.engineerWorkloadReport();
		return "requestReport";
	}
	/**
	 * 技术员忙碌程度统计
	 * @return
	 */
	public String workloadStatusStatistics(){
		requestlist=requestService.workloadStatusStatistics();
		return "requestReport";
	}
	/**
	 * 技术员处理请求耗时
	 * @return
	 */
	public String tctakeTimeperRequest(){
		requestlist=requestService.tctakeTimeperRequest();
		return "requestReport";
	}
	/**
	 * 技术员工时统计报表
	 * @return
	 */
	public String technicianCostTime(){
		requestlist=requestService.technicianCostTime();
		return "requestReport";
	}
}
