package com.wstuo.itsm.scheduled.service;

import java.io.InputStream;
import java.util.List;

import com.wstuo.common.scheduled.dto.SchedulePreviewDTO;
import com.wstuo.common.scheduled.dto.ScheduleQueryDTO;
import com.wstuo.common.scheduled.dto.ScheduleViewDTO;

/**
 * Schedule Service Interface Class
 * @author WSTUO
 *
 */
public interface IScheduleService {
	/**
	 * 获取行程显示数据
	 * @param qdto
	 * @param sidx
	 * @param sord
	 * @return ScheduleViewDTO
	 */
	ScheduleViewDTO findScheduleShowData(ScheduleQueryDTO qdto,String sidx,String sord);
	/**
	 * 当前行程视图导出
	 * @param qdto
	 * @param sidx
	 * @param sord
	 * @return InputStream
	 */
	InputStream exportScheduleData(ScheduleQueryDTO qdto,String sidx,String sord);
	
	/**
     * 人员行程主界面导出
     * @param qdto
     * @param sidx
     * @param sord
     * @return InputStream
     */
	InputStream exportScheduleView(ScheduleQueryDTO qdto,String sidx,String sord);
	
	/**
	 * 预览导出效果
	 * @param qdto
	 * @param sidx
	 * @param sord
	 * @return List<SchedulePreviewDTO> 
	 */
	List<SchedulePreviewDTO> exportSchedulePreview(ScheduleQueryDTO qdto,String sidx,String sord);
}
