package com.wstuo.common.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.wstuo.common.dto.AbstractValueObject;

/**
 * @version 1.00
 * @author wstuo.com
 */
@MappedSuperclass
public class BaseEntity<ID extends Serializable> extends
		AbstractValueObject {
	private static final long serialVersionUID = 1148750099142971324L;
	public static final Integer DELETED = 99;
	public static final Integer SYSTEM = 1;
	public static final Integer NORMAL = 0;
	public static final Integer TEST = 2;

	@Lob
	public String remark;
	@Temporal(TemporalType.TIMESTAMP)
	protected Date createTime = new Date();
	@Column(length = 60)
	protected String creator;
	@Temporal(TemporalType.TIMESTAMP)
	protected Date lastUpdateTime;
	@Column(length = 60)
	protected String lastUpdater;
	protected Integer optimisticLock;
	protected Long companyNo;
	protected Byte dataFlag = 0; // 0:normal, 1:system, 2:test,99:delete
	@Lob
	private String description;
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	public BaseEntity() {
	}

	public Byte getDataFlag() {
		return dataFlag;
	}

	public void setDataFlag(Byte dataFlag) {
		this.dataFlag = dataFlag;
	}

	/**
	 * @return the remark
	 */

	public String getRemark() {
		return remark;
	}

	/**
	 * @return the createTime
	 */

	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @return the creator
	 */
	public String getCreator() {
		return creator;
	}

	/**
	 * @return the lastUpdateTime
	 */

	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}

	/**
	 * @return the lastUpdater
	 */

	public String getLastUpdater() {
		return lastUpdater;
	}

	/**
	 * Adds optimistic locking capability.
	 * <p>
	 * The optimisticLock property will be mapped to the optimisticLock column,
	 * and the entity manager will use it to detect conflicting updates.
	 * 
	 * @return the optimisticLock
	 */
	public Integer getOptimisticLock() {
		return optimisticLock;
	}


	/**
	 * @param remark
	 *            - the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * @param createTime
	 *            - the createTime to set
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @param creator
	 *            - the creator to set
	 */
	public void setCreator(String creator) {
		this.creator = creator;
	}

	/**
	 * @param lastUpdateTime
	 *            - the lastUpdateTime to set
	 */
	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	/**
	 * @param lastUpdater
	 *            - the lastUpdater to set
	 */
	public void setLastUpdater(String lastUpdater) {
		this.lastUpdater = lastUpdater;
	}

	/**
	 * @param optimisticLock
	 *            the optimisticLock to set
	 */
	public void setOptimisticLock(Integer optimisticLock) {
		this.optimisticLock = optimisticLock;
	}

	public Long getCompanyNo() {
		return companyNo;
	}

	public void setCompanyNo(Long companyNo) {
		this.companyNo = companyNo;
	}
	/**
	 * 
	 * @return
	 */
	public String getIndexContent() {
		return "";
	}

	/**
	 * 给参数赋值的公共方法，其他子类有部分会重写此方法（如Request）
	 */
	public void updateProperty() {
	}

}
