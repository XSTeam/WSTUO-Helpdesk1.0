package com.wstuo.common.tools.dao;

import com.wstuo.common.tools.entity.EmailServer;
import com.wstuo.common.dao.IEntityDAO;

/**
 * 服务邮箱地址DAO接口
 * @author QXY
 *
 */
public interface IEmailServerDAO
    extends IEntityDAO<EmailServer>
{
}
