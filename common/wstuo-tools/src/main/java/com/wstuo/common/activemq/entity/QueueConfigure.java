package com.wstuo.common.activemq.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * 消息队列配置实体
 * @author WSTUO
 *
 */
@Entity
public class QueueConfigure {
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
	private Long qc_id;
	private String qc_serverAddress;
	private Long qc_port;
	private String qc_userName;
	private String qc_password;
	
	public Long getQc_id() {
		return qc_id;
	}
	public void setQc_id(Long qc_id) {
		this.qc_id = qc_id;
	}
	public String getQc_serverAddress() {
		return qc_serverAddress;
	}
	public void setQc_serverAddress(String qc_serverAddress) {
		this.qc_serverAddress = qc_serverAddress;
	}
	public Long getQc_port() {
		return qc_port;
	}
	public void setQc_port(Long qc_port) {
		this.qc_port = qc_port;
	}
	public String getQc_userName() {
		return qc_userName;
	}
	public void setQc_userName(String qc_userName) {
		this.qc_userName = qc_userName;
	}
	public String getQc_password() {
		return qc_password;
	}
	public void setQc_password(String qc_password) {
		this.qc_password = qc_password;
	}
	
}
