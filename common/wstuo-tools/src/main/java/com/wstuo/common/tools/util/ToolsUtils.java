package com.wstuo.common.tools.util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

public class ToolsUtils {
	final static Logger LOGGER = Logger.getLogger(ToolsUtils.class);
    private final static String FILE_FLAG = ".";
	
	/**
	 * 复制（上传）文件.
	 * 
	 * @param File
	 *            输入文件
	 * @param File
	 *            输出文件
	 */
	public static void copy(File src, File dst) {
		try {
			if (src != null && dst != null && src.exists()){
				FileUtils.copyFile(src, dst);	
			}
		} catch (IOException e) {
			LOGGER.error(e);
		}
	}
	
	/**
	 * 获取文件扩展名
	 * @param fileName
	 * @return
	 */
	public static String fileExtByFileName(String fileName) {
		String fileExt = null;
		int index = -1;
		//最末的点符号起码得大于0；因为如果该点的index是0，那么该文件压根没有文件名；
		if (fileName != null && ( index = fileName.lastIndexOf( FILE_FLAG ) ) > 0 ) {
			fileExt = fileName.substring(index + 1, fileName.length());
		}else{
			fileExt = fileName;
		}
		return fileExt;
	}
}
