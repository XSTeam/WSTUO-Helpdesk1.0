package com.wstuo.common.tools.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

import com.wstuo.common.config.attachment.entity.Attachment;
import com.wstuo.common.entity.BaseEntity;

/**
 * 邮件消息实体
 * 
 * @author Administrator
 * 
 */
@SuppressWarnings({ "serial", "rawtypes" })
@Entity
public class EmailMessage extends BaseEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long emailMessageId;
	private String subject; // 邮件主题
	@Lob
	private String content; // 邮件内容
	private String mailPath; // 邮件存储路径（用来保存邮件在本地)
	private String affachmentPath; // 附件的路邮件
	private String picPath; // 图片路径
	@OneToMany(fetch = FetchType.LAZY)
	private List<Attachment> attachment; // 附件
	private String folderName; // 邮件类型--发邮件or接收
	// 接收
	private String fromUser; // 发件邮件
	private Date receiveDate; // 接收日期
	// 发邮件
	@Lob
	private String toUser; // 接收邮件
	private String bcc; // 密邮件
	private String cc; // 抄邮件
	private Date sendDate; // 发邮件日期
	private Boolean isToRequest = false;
	private String keyword;
	private String moduleCode;// 关联的模块编号

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public Long getEmailMessageId() {
		return emailMessageId;
	}

	public void setEmailMessageId(Long emailMessageId) {
		this.emailMessageId = emailMessageId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getMailPath() {
		return mailPath;
	}

	public void setMailPath(String mailPath) {
		this.mailPath = mailPath;
	}

	public String getAffachmentPath() {
		return affachmentPath;
	}

	public void setAffachmentPath(String affachmentPath) {
		this.affachmentPath = affachmentPath;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public List<Attachment> getAttachment() {
		return attachment;
	}

	public void setAttachment(List<Attachment> attachment) {
		this.attachment = attachment;
	}

	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public String getFromUser() {
		return fromUser;
	}

	public void setFromUser(String fromUser) {
		this.fromUser = fromUser;
	}

	public Date getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(Date receiveDate) {
		this.receiveDate = receiveDate;
	}

	public String getToUser() {
		return toUser;
	}

	public void setToUser(String toUser) {
		this.toUser = toUser;
	}

	public String getBcc() {
		return bcc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public Boolean getIsToRequest() {
		return isToRequest;
	}

	public void setIsToRequest(Boolean isToRequest) {
		this.isToRequest = isToRequest;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	@Override
	public String toString() {
		return "EmailMessage [emailMessageId=" + emailMessageId + ", subject="
				+ subject + ", content=" + content + ", mailPath=" + mailPath
				+ ", affachmentPath=" + affachmentPath + ", picPath=" + picPath
				+ ", attachment=" + attachment + ", folderName=" + folderName
				+ ", fromUser=" + fromUser + ", receiveDate=" + receiveDate
				+ ", toUser=" + toUser + ", bcc=" + bcc + ", cc=" + cc
				+ ", sendDate=" + sendDate + ", isToRequest=" + isToRequest
				+ ", keyword=" + keyword + ", moduleCode=" + moduleCode + "]";
	}
}
