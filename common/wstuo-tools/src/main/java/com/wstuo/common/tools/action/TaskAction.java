package com.wstuo.common.tools.action;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;


import com.wstuo.common.tools.dto.ScheduleJsonDTO;
import com.wstuo.common.tools.dto.TaskDTO;
import com.wstuo.common.tools.dto.TaskViewDTO;
import com.wstuo.common.tools.entity.Task;
import com.wstuo.common.tools.dto.TaskQueryDTO;
import com.wstuo.common.tools.service.ITaskService;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;
import com.wstuo.common.security.action.SupplierAction;
/**
 * 任务ACTION类
 * @author WSTUO
 *
 */
@SuppressWarnings( "serial" )
public class TaskAction extends SupplierAction {

	@Autowired
	private ITaskService taskService;
	
	private TaskDTO taskDto;
	private TaskQueryDTO taskQueryDto=new TaskQueryDTO();
	private PageDTO taskList;
	private List<Task> taskLists;
	private List<TaskDTO> taskDTOLists = new ArrayList<TaskDTO>();
	private Long[] taskIds;
	private int page = 1;
    private int rows = 10;
    private String sidx;
    private String sord;
    private boolean result;
    private List<ScheduleJsonDTO> scheduleDTO;
    private Long taskId;
    private InputStream exportStream;
    private String fileName="";
    private Long start;
    private Long end;
    private TaskViewDTO taskViewDTO=new TaskViewDTO();
    
	public List<TaskDTO> getTaskDTOLists() {
		return taskDTOLists;
	}
	public void setTaskDTOLists(List<TaskDTO> taskDTOLists) {
		this.taskDTOLists = taskDTOLists;
	}
	public TaskViewDTO getTaskViewDTO() {
		return taskViewDTO;
	}
	public void setTaskViewDTO(TaskViewDTO taskViewDTO) {
		this.taskViewDTO = taskViewDTO;
	}
	public Long getStart() {
		return start;
	}
	public void setStart(Long start) {
		this.start = start;
	}
	public Long getEnd() {
		return end;
	}
	public void setEnd(Long end) {
		this.end = end;
	}
	public InputStream getExportStream() {
		return exportStream;
	}
	public void setExportStream(InputStream exportStream) {
		this.exportStream = exportStream;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public TaskDTO getTaskDto() {
		return taskDto;
	}
	public void setTaskDto(TaskDTO taskDto) {
		this.taskDto = taskDto;
	}
	public TaskQueryDTO getTaskQueryDto() {
		return taskQueryDto;
	}
	public void setTaskQueryDto(TaskQueryDTO taskQueryDto) {
		this.taskQueryDto = taskQueryDto;
	}
	public PageDTO getTaskList() {
		return taskList;
	}
	public void setTaskList(PageDTO taskList) {
		this.taskList = taskList;
	}
	
	public Long[] getTaskIds() {
		return taskIds;
	}
	public void setTaskIds(Long[] taskIds) {
		this.taskIds = taskIds;
	}
	
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public List<Task> getTaskLists() {
		return taskLists;
	}
	public void setTaskLists(List<Task> taskLists) {
		this.taskLists = taskLists;
	}
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public List<ScheduleJsonDTO> getScheduleDTO() {
		return scheduleDTO;
	}
	public void setScheduleDTO(List<ScheduleJsonDTO> scheduleDTO) {
		this.scheduleDTO = scheduleDTO;
	}
	
	public Long getTaskId() {
		return taskId;
	}
	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	/**
	 * 分页查询任务
	 * @return String
	 */
	public String findPagerTask(){
		int start = ( page - 1 ) * rows;
		taskList =taskService.findPagerTask(taskQueryDto,start,rows,sidx,sord);
		taskList.setPage( page );
		taskList.setRows( rows );
        return SUCCESS;
	}
	/**
	 * 查找最近任务
	 * @return
	 */
	public String findMyTaskForRecently(){
		taskDTOLists = taskService.findMyTaskForRecently( taskQueryDto );
        return "taskDTOLists";
	}
	/**
	 * 查找最近任务（首页面板中的查询需要的都是pageDTO）
	 * @return
	 */
	public String findMyTaskForRecently1(){
		taskList = new PageDTO();
		taskDTOLists = taskService.findMyTaskForRecently( taskQueryDto );
		taskList.setData( taskDTOLists);
		taskList.setTotalSize(taskDTOLists.size());
        return SUCCESS;
	}
	/**
	 * 添加任务
	 * @return String
	 */
	public String saveTask(){
		taskIds = taskService.saveTask(taskDto);
		return "taskIds";
	}
	/**
	 * 编辑任务
	 * @return String
	 */
	public String editTask(){
		taskIds = taskService.editTask(taskDto);
		return "taskIds";
	}
	/**
	 * 删除任务
	 * @return String
	 */
	public String deleteTask(){
		try{
			taskService.deleteTask(taskIds);
		}catch(Exception ex){
			throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE",ex);
		}
		return SUCCESS;
	}
	public String deleteScheduleTask(){
		try{
			taskService.deleteScheduleTask( taskDto );
		}catch(Exception ex){
			throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE",ex);
		}
		return SUCCESS;
	}
	
	/**
	 * 查询全部任务
	 * @return String
	 */
	public String findAllTask(){
		taskLists=taskService.findAllTask(taskQueryDto);
		return "taskLists";
	}
	/**
	 * 查询指定时间段任务
	 * @return String
	 */
	public String timeConflict(){
		result=taskService.timeConflict(taskDto);
		return "result";
	}
	
	/**
	 * 查询所有行程任务
	 * @return List<ScheduleJsonDTO>
	 */
	public String findAllTaskShowSchedue(){
		if (start != null && end != null ) {
			taskQueryDto.setStartTime( new Date(start * 1000) );
			taskQueryDto.setEndTime( new Date(end * 1000) );
		}
		scheduleDTO = taskService.findAllTaskShowSchedue(taskQueryDto);
		return "scheduleDTO";
	}
	/**
	 * 根据ID查询任务详细
	 * @return taskDto
	 */
	public String findByTaskId(){
		taskDto=taskService.findById(taskId);
		return "taskDto";
		
	}
	/**
	 * 查询排班任务
	 * @return
	 */
	public String findWrokForce(){
		if (start != null && end != null && taskQueryDto != null) {
			taskQueryDto.setStartTime( new Date(start * 1000) );
			taskQueryDto.setEndTime( new Date(end * 1000) );
		}
		taskViewDTO=taskService.findWorkForceShowData(taskQueryDto, sidx, sord);
		return "taskViewDTO";
	}
	
	/**
	 * 编辑任务
	 * @return null
	 */
	public String editTaskDetail(){
		taskService.editTaskDetail(taskDto);
		return SUCCESS;
	}
	
	/**
	 * 关闭任务
	 * @return null
	 */
	public String closedTaskDetail(){
		taskService.closedTaskDetail(taskDto);
		return SUCCESS;
	}
	
	/**
	 * 添加任务备注
	 * @return null
	 */
	public String remarkTaskDetail(){
		taskService.remarkTaskDetail(taskDto);
		return SUCCESS;
	}
	
	/**
	 * 判断任务ID是否存在
	 * @return String
	 */
	public String findTaskByIds(){
		result = taskService.findTaskByIds(taskIds);
		return "result";
	}
	
	/**
	 * 查询面板任务
	 * @return taskDto
	 */
	public String findPortalById(){
		taskDto=taskService.findPortalById(taskId);
		return "taskDto";
	}
	
	/**
	 * 导出任务成
	 * @return exportStream
	 */
	public String exportTask(){
		fileName="Task.csv";
		exportStream = taskService.exportTask(taskQueryDto);	
    	return "exportFileSuccessful";
	}
	
	/**
	 * 任务读取时间默认值
	 * @return taskDto
	 */
	public String taskRealTimeDefaultValue(){
		//taskDto=taskService.taskRealTimeDefaultValue(taskId);
		taskDto=taskService.taskRealTimeDefaultValue(taskDto);
		return "taskDto";
	}
	
	public String taskCostByTaskId(){
		taskDto = taskService.findTaskCostWith(taskDto);
    	return "taskDto";
	}
	public String taskCostByTaskId1(){
		taskDto = taskService.findTaskCost(taskDto);
    	return "taskDto";
	}
	
}
