package com.wstuo.common.tools.service;

import java.io.UnsupportedEncodingException;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import com.wstuo.common.tools.dto.EmailConnectionDTO;
import com.wstuo.common.tools.dto.EmailServerDTO;
import com.wstuo.common.tools.dto.SMSAccountDTO;

/**
 * 邮件密码加密
 * 
 * @author lenovo
 * 
 */
public class BaseTools implements IBaseTools {
	private static final Logger LOGGER = Logger.getLogger(BaseTools.class ); 
	/**
	 * 使用Base64进行解码
	 * 
	 * @param in
	 *            需要解码的字符串
	 * @return 返回解码后的字符串，如果解码失败，返回 in
	 */
	public String base64Decode(String in) {
		Base64 base64 = new Base64();
		String out = in;
		if (in != null) {
			try {
				out = new String(base64.decode(in.getBytes()), "utf-8");
			} catch (UnsupportedEncodingException e) {
				LOGGER.error(e.getMessage());
			}
		}

		return out;
	}

	/**
	 * 使用Base64进行编码
	 * 
	 * @param in
	 *            需要编码的字符串
	 * @return 返回编码后的字符串，如果编码失败，返回 in
	 */
	public String base64Encode(String in) {
		Base64 base64 = new Base64();
		String out = in;
		if (in != null) {
			try {
				out = new String(base64.encode(in.getBytes()), "utf-8");
			} catch (UnsupportedEncodingException e) {
				LOGGER.error(e.getMessage());
			}
		}
		return out;
	}

	/**
	 * 使用Base64编码EmailConnectionDTO的密码属性
	 * 
	 * @param dto
	 */
	public void encodeDTO(EmailConnectionDTO dto) {
		if (dto != null) {
			dto.setPassword(base64Encode(dto.getPassword()));
		}
	}

	/**
	 * 使用Base64解码EmailConnectionDTO的密码属性
	 * 
	 * @param dto
	 */
	public void decodeDTO(EmailConnectionDTO dto) {
		if (dto != null) {
			dto.setPassword(base64Decode(dto.getPassword()));
		}
	}

	/**
	 * 使用Base64编码EmailServerDTO的密码属性
	 * 
	 * @param dto
	 */
	public void encodeDTO(EmailServerDTO dto) {
		if (dto != null) {
			dto.setExchangePassword(base64Encode(dto.getExchangePassword()));
			dto.setPassword(base64Encode(dto.getPassword()));
		}
	}

	/**
	 * 使用Base64解码EmailServerDTO的密码属性
	 * 
	 * @param dto
	 */
	public void decodeDTO(EmailServerDTO dto) {
		if (dto != null) {
			dto.setExchangePassword(base64Decode(dto.getExchangePassword()));
			dto.setPassword(base64Decode(dto.getPassword()));
		}
	}

	
	/**
	 * 使用Base64编码SMSAccountDTO的密码属性
	 * 
	 * @param dto
	 */
	public void encodeDTO(SMSAccountDTO dto) {
		if (dto != null) {
			dto.setPwd( base64Encode( dto.getPwd() ) );
		}
	}

	/**
	 * 使用Base64解码SMSAccountDTO的密码属性
	 * 
	 * @param dto
	 */
	public void decodeDTO(SMSAccountDTO dto) {
		if (dto != null) {
			dto.setPwd( base64Decode( dto.getPwd() ) );
		}
	}
}
