package com.wstuo.common.noticeRule.dto;

import javax.persistence.Column;

import com.wstuo.common.dto.BaseDTO;


/**
 * 通知规则信息DTO类
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class NoticeRuleDTO extends BaseDTO {
	//规则ID
	private Long noticeRuleId;
	//通知规则编码
	private String noticeRuleNo;
	//通知方式
	private String noticeWay;
	//规则种类
	private String noticeRuleType;
	//规则名称
	private String noticeRuleName;
	//使用状态
	private Boolean useStatus;
	//通知用户
	private String technician;
	//邮件内容模板
	private String emailTemp;
	//邮件标题模板
	private String emailTitleTemp;
	//要另外通知的电邮地址
	private String emailAddress;
	//邮件通知
	private Boolean smsNotice;
	private String technicianOrEmail;
	//IM通知
	private Boolean imNotice;
	//邮箱通知
	private Boolean mailNotice;
	//推送通知
	private Boolean pushNotice;
	//所属模块(request,problem,change,release,cim)
	private String module;
	//短信模板
	private String smsTemp;
	//IM消息模板
	private String imTemp;
	//消息推送模板
	private String pushTemp;
	
	public String getPushTemp() {
		return pushTemp;
	}
	public void setPushTemp(String pushTemp) {
		this.pushTemp = pushTemp;
	}
	public Boolean getPushNotice() {
		return pushNotice;
	}
	public void setPushNotice(Boolean pushNotice) {
		this.pushNotice = pushNotice;
	}
	public Boolean getMailNotice() {
		return mailNotice;
	}
	public void setMailNotice(Boolean mailNotice) {
		this.mailNotice = mailNotice;
	}
	public String getTechnicianOrEmail() {
		return technicianOrEmail;
	}
	public void setTechnicianOrEmail(String technicianOrEmail) {
		this.technicianOrEmail = technicianOrEmail;
	}
	private String url;
	
	public Boolean getSmsNotice() {
		return smsNotice;
	}
	public void setSmsNotice(Boolean smsNotice) {
		this.smsNotice = smsNotice;
	}
	public NoticeRuleDTO(Long noticeRuleId,String noticeRuleName,String noticeRuleNo){
		this.noticeRuleId=noticeRuleId;
		this.emailTemp="email.ftl";
		this.noticeRuleName=noticeRuleName;
		this.noticeRuleNo=noticeRuleNo;
		this.useStatus=true;

	}
	public NoticeRuleDTO(){

	}
	public Long getNoticeRuleId() {
		return noticeRuleId;
	}
	public void setNoticeRuleId(Long noticeRuleId) {
		this.noticeRuleId = noticeRuleId;
	}
	public String getNoticeRuleNo() {
		return noticeRuleNo;
	}
	public void setNoticeRuleNo(String noticeRuleNo) {
		this.noticeRuleNo = noticeRuleNo;
	}

	public String getNoticeWay() {
		return noticeWay;
	}
	public void setNoticeWay(String noticeWay) {
		this.noticeWay = noticeWay;
	}
	public String getNoticeRuleType() {
		return noticeRuleType;
	}
	public void setNoticeRuleType(String noticeRuleType) {
		this.noticeRuleType = noticeRuleType;
	}
	public String getNoticeRuleName() {
		return noticeRuleName;
	}
	public void setNoticeRuleName(String noticeRuleName) {
		this.noticeRuleName = noticeRuleName;
	}
	public Boolean getUseStatus() {
		return useStatus;
	}
	public void setUseStatus(Boolean useStatus) {
		this.useStatus = useStatus;
	}
	public String getTechnician() {
		return technician;
	}
	public void setTechnician(String technician) {
		this.technician = technician;
	}
	public String getEmailTemp() {
		return emailTemp;
	}
	public void setEmailTemp(String emailTemp) {
		this.emailTemp = emailTemp;
	}
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getEmailTitleTemp() {
		return emailTitleTemp;
	}
	public void setEmailTitleTemp(String emailTitleTemp) {
		this.emailTitleTemp = emailTitleTemp;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Boolean getImNotice() {
		return imNotice;
	}
	public void setImNotice(Boolean imNotice) {
		this.imNotice = imNotice;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getSmsTemp() {
		return smsTemp;
	}
	public void setSmsTemp(String smsTemp) {
		this.smsTemp = smsTemp;
	}
	public String getImTemp() {
		return imTemp;
	}
	public void setImTemp(String imTemp) {
		this.imTemp = imTemp;
	}
	
}
