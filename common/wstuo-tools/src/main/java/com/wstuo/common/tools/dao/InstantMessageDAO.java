package com.wstuo.common.tools.dao;


import com.wstuo.common.tools.dto.InstantMessageQueryDTO;
import com.wstuo.common.tools.entity.InstantMessage;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.DaoUtils;
import com.wstuo.common.util.StringUtils;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.springframework.orm.hibernate3.HibernateCallback;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * 即时信息DAO类
 * 
 * @author QXY date 2010-10-11
 */
public class InstantMessageDAO extends BaseDAOImplHibernate<InstantMessage> implements IInstantMessageDAO {
	/**
	 * 查询全部即时信息
	 * 
	 * @return List<InstantMessage>
	 */
	@SuppressWarnings("unchecked")
	public List<InstantMessage> findInstantMessage() {
		String hql = "from InstantMessage";

		return getHibernateTemplate().find(hql);
	}

	/**
	 * 即时信息分页查询
	 * 
	 * @param instantmessageQueryDTO
	 * @param start
	 * @param limit
	 * @param sidx
	 * @param sord
	 * @return PageDTO
	 */
	public PageDTO findPager(InstantMessageQueryDTO instantmessageQueryDTO, int start, int limit, String sidx, String sord) {
		DetachedCriteria dc = DetachedCriteria.forClass(InstantMessage.class);

		if (instantmessageQueryDTO != null) {
			
			if (StringUtils.hasText(instantmessageQueryDTO.getReceiveUsers())
					&& StringUtils.hasText(instantmessageQueryDTO.getSendUsers())) {
				dc.add(Restrictions.or(Restrictions.eq("receiveUsers", instantmessageQueryDTO.getReceiveUsers()), 
						Restrictions.eq("sendUsers", instantmessageQueryDTO.getSendUsers())));
			}else if (StringUtils.hasText(instantmessageQueryDTO.getReceiveUsers())) {
				dc.add(Restrictions.eq("receiveUsers", instantmessageQueryDTO.getReceiveUsers()));
			}else if (StringUtils.hasText(instantmessageQueryDTO.getSendUsers())) {
				dc.add(Restrictions.eq("sendUsers", instantmessageQueryDTO.getSendUsers()));
			}
			if (StringUtils.hasText(instantmessageQueryDTO.getImType())) {

				dc.add(Restrictions.eq("imType", instantmessageQueryDTO.getImType()));
			}
			if (StringUtils.hasText(instantmessageQueryDTO.getStatus())) {

				dc.add(Restrictions.eq("status", instantmessageQueryDTO.getStatus()));
			}

			if (StringUtils.hasText(instantmessageQueryDTO.getTitle())) {

				dc.add(Restrictions.like("title", instantmessageQueryDTO.getTitle(), MatchMode.ANYWHERE));
			}

			if (StringUtils.hasText(instantmessageQueryDTO.getContent())) {

				dc.add(Restrictions.like("content", instantmessageQueryDTO.getContent(), MatchMode.ANYWHERE));
			}

			

			// 时间搜索
			if (instantmessageQueryDTO.getSendSatrt() != null && instantmessageQueryDTO.getSendEnd() == null) {
				dc.add(Restrictions.ge("sendTime", instantmessageQueryDTO.getSendSatrt()));
			}
			if (instantmessageQueryDTO.getSendSatrt() == null && instantmessageQueryDTO.getSendEnd() != null) {
				Calendar endTimeCl = new GregorianCalendar();
				endTimeCl.setTime(instantmessageQueryDTO.getSendEnd());
				endTimeCl.set(Calendar.DATE, endTimeCl.get(Calendar.DATE) + 1);
				dc.add(Restrictions.le("sendTime", endTimeCl.getTime()));
			}
			if (instantmessageQueryDTO.getSendSatrt() != null && instantmessageQueryDTO.getSendEnd() != null) {
				Calendar endTimeCl = new GregorianCalendar();
				endTimeCl.setTime(instantmessageQueryDTO.getSendEnd());
				endTimeCl.set(Calendar.DATE, endTimeCl.get(Calendar.DATE) + 1);

				dc.add(Restrictions.and(Restrictions.le("sendTime", endTimeCl.getTime()), Restrictions.ge("sendTime", instantmessageQueryDTO.getSendSatrt())));
			}

			// 发送全名搜索
			if (StringUtils.hasText(instantmessageQueryDTO.getSenderFullName()))
				dc.add(Restrictions.eq("senderFullName", instantmessageQueryDTO.getSenderFullName()));
			// 接收全名搜索
			if (StringUtils.hasText(instantmessageQueryDTO.getReceiverFullName()))
				dc.add(Restrictions.eq("receiverFullName", instantmessageQueryDTO.getReceiverFullName()));

		}
		// 排序
		dc = DaoUtils.orderBy(sidx, sord, dc);
		//根据状态排序后再根据时间排序
		if("status".equals(sidx)){
			dc.addOrder(Order.desc("sendTime"));
		}
		return super.findPageByCriteria(dc, start, limit);
	}

	/**
	 * find 根据关键字查询
	 * 
	 * @param keyWord
	 * @return List<InstantMessage>
	 */
	@SuppressWarnings("unchecked")
	public List<InstantMessage> findByKeyWord(String keyWord) {
		final DetachedCriteria dc = DetachedCriteria.forClass(InstantMessage.class);

		dc.add(Restrictions.or(Restrictions.like("title", keyWord, MatchMode.ANYWHERE), Restrictions.or(Restrictions.like("sendUsers", keyWord, MatchMode.ANYWHERE), Restrictions.like("status", keyWord, MatchMode.ANYWHERE))));

		List<InstantMessage> students = getHibernateTemplate().executeFind(new HibernateCallback<List<InstantMessage>>() {
			public List<InstantMessage> doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria criteria = dc.getExecutableCriteria(session);

				return criteria.list();
			}
		});

		return students;
	}

	/**
	 * 统计未读的即时信息
	 * 
	 * @return int
	 */
	@SuppressWarnings("unchecked")
	public int findUnread() {
		String hql = "from InstantMessage where status='0'";
		List<InstantMessage> list = getHibernateTemplate().find(hql);
		int sum = list.size();

		return sum;
	}
}
