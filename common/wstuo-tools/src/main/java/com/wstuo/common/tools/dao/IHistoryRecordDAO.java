package com.wstuo.common.tools.dao;

import java.util.List;

import com.wstuo.common.tools.dto.HistoryRecordDTO;
import com.wstuo.common.tools.entity.HistoryRecord;
import com.wstuo.common.dao.IEntityDAO;

/**
 * 历史记录DAO接口类
 * @author WSTUO
 *
 */
public interface IHistoryRecordDAO extends IEntityDAO<HistoryRecord> {
	/**
	 * 查询全部
	 * @param dto HistoryRecordDTO
	 * @return List<HistoryRecordDTO>
	 */
	public List<HistoryRecord> findAllHistoryRecord(HistoryRecordDTO dto); 
	
	/**
	 * 删除根据Eno历史记录
	 * @param eno
	 * @param eventType
	 */
	void deleteHistoryRecordByEno(Long[] eno,String eventType); 
}
