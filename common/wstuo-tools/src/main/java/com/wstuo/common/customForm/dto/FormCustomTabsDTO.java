package com.wstuo.common.customForm.dto;

import org.apache.commons.beanutils.BeanUtils;

import com.wstuo.common.exception.ApplicationException;

public class FormCustomTabsDTO {
	private Long tabId;	
	private String tabName;	
	private String tabAttrNos;
	private String tabAttrsConent;
	private String tabAttrsDecode;
	private String tabAttrsConentDecode;
	
	public String getTabAttrsConentDecode() {
		return tabAttrsConentDecode;
	}

	public void setTabAttrsConentDecode(String tabAttrsConentDecode) {
		this.tabAttrsConentDecode = tabAttrsConentDecode;
	}

	public String getTabAttrsDecode() {
		return tabAttrsDecode;
	}

	public void setTabAttrsDecode(String tabAttrsDecode) {
		this.tabAttrsDecode = tabAttrsDecode;
	}

	public String getTabAttrsConent() {
		return tabAttrsConent;
	}

	public void setTabAttrsConent(String tabAttrsConent) {
		this.tabAttrsConent = tabAttrsConent;
	}
	public Long getTabId() {
		return tabId;
	}
	public void setTabId(Long tabId) {
		this.tabId = tabId;
	}
	public String getTabName() {
		return tabName;
	}
	public void setTabName(String tabName) {
		this.tabName = tabName;
	}
	public String getTabAttrNos() {
		return tabAttrNos;
	}
	public void setTabAttrNos(String tabAttrNos) {
		this.tabAttrNos = tabAttrNos;
	}	
	/**
	 * Transform dto to entity.
	 * 
	 * @param dto
	 *            DTO object
	 * @param entity
	 *            domain entity object
	 */
	public static void dto2entity(Object dto, Object entity) {

		try {
			if (dto != null) {
				BeanUtils.copyProperties(entity, dto);
			}
		} catch (Exception ex) {

			throw new ApplicationException(ex);
		}
	}

	/**
	 * Transform entity to dto.
	 * 
	 * @param entity
	 *            doamin entity object
	 * @param dto
	 *            dto object
	 */
	public static void entity2dto(Object entity, Object dto) {

		try {
			if (entity != null) {
				BeanUtils.copyProperties(dto, entity);
			}
		} catch (Exception ex) {
			throw new ApplicationException(ex);
		}
	}
}
