package com.wstuo.common.tools.dao;

import java.io.File;
import java.io.FileInputStream;

import org.apache.log4j.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.tools.dto.ExportInfoDTO;
import com.wstuo.common.tools.entity.ExportInfo;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.DaoUtils;

/**
 * 下载信息DAO
 * @author WSTUO
 *
 */
public class ExportInfoDAO extends BaseDAOImplHibernate<ExportInfo> implements IExportInfoDAO{
	private static final Logger LOGGER = Logger.getLogger(ExportInfoDAO.class ); 
	/**
	 * 分页查询下载信息
	 * @param dto
	 * @param start
	 * @param limit
	 * @param sidx
	 * @param sord
	 * @return PageDTO
	 */
	public PageDTO findExportInfoPager(ExportInfoDTO dto,int start, int limit,String sord,String sidx) {
        DetachedCriteria dc = DetachedCriteria.forClass(ExportInfo.class);
        if (dto != null && dto.getCompanyNo() !=null) {
        	dc.add(Restrictions.eq("companyNo", dto.getCompanyNo()));
        }
        dc = DaoUtils.orderBy(sidx, sord, dc);
        return super.findPageByCriteria(dc, start, limit);
    }
	/**  
     * 获得文件大小  
     * @param urlFile
     * @return 文件大小  
     */  
    public long getFileLength(String urlFile) {   
        long result = 0;   
        File file = new File(urlFile); 
        FileInputStream fis=null;
        try{
        	fis=new FileInputStream(file);
        	result=fis.available()/1000;
        }catch (Exception e) {
			// TODO: handle exception
        	LOGGER.error(e.getMessage());
		}
        return result;   
    } 
}
