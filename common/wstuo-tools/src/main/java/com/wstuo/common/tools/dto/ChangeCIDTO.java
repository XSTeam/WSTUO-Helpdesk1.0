package com.wstuo.common.tools.dto;

import java.io.Serializable;

/**
 * 变更配置项DTO
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class ChangeCIDTO implements Serializable{
	private String changeCiName;
	private Long changeCiNo;
	public String getChangeCiName() {
		return changeCiName;
	}
	public void setChangeCiName(String changeCiName) {
		this.changeCiName = changeCiName;
	}
	public Long getChangeCiNo() {
		return changeCiNo;
	}
	public void setChangeCiNo(Long changeCiNo) {
		this.changeCiNo = changeCiNo;
	}
}
