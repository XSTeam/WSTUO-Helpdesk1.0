package com.wstuo.common.tools.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * FileInfoDTO
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class FileInfoDTO extends BaseDTO {

	private String err = "";
    private String msg = "";
    private String pre = "";
    private Long fileSize;
    
    
    public FileInfoDTO() {
        super();
    }

    public String getErr() {

        return err;
    }

    public void setErr(String err) {

        this.err = err;
    }

    public String getMsg() {

        return msg;
    }

    public void setMsg(String msg) {

        this.msg = msg;
    }

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	public String getPre() {
		return pre;
	}

	public void setPre(String pre) {
		this.pre = pre;
	}

	@Override
	public String toString() {
		return "FileInfoDTO [err=" + err + ", msg=" + msg + ", pre=" + pre
				+ ", fileSize=" + fileSize + "]";
	}
	
}