package com.wstuo.common.tools.sms;
import com.wstuo.common.tools.dto.SMSMessageDTO;



/**
 * 短信发送连接构造接口
 * @author QXY
 *
 */
public interface ISMSSender {
	
	
	
	/**
	 * 发送信息.
	 * @param mobile 手机号码列表
	 * @param content 短信内容
	 * @param isSystem 发送方式,true为系统发送，false为手动发送
	 * @param saveHistory 是否保存历史记录，true保存，false不保存
	 * @return SMSMessageDTO
	 */
	SMSMessageDTO SendSms(String postUrl,String mobile, String content,boolean isSystem,boolean saveHistory);

	/**
	 * 查询账户信息.
	 * @return result
	 */
	SMSMessageDTO queryMoney(String postUrl);
	
	/**
	 * 查询服务器状态.
	 * @return true连接成功，false连接失败
	 */
	Boolean testConnection(String postUrl);
}
