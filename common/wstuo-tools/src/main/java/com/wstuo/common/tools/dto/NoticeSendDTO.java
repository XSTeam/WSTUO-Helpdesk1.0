package com.wstuo.common.tools.dto;

import java.util.HashSet;
import java.util.Set;

import com.wstuo.common.dto.AbstractValueObject;

/**
 * 通知发送DTO
 * @author QXY
 *
 */
@SuppressWarnings({ "serial", "rawtypes" })
public class NoticeSendDTO extends AbstractValueObject{
	//租户标识
	private String tenantId;
	//通知类型
	private String sendType;
	//模板路径
	private String templatePath;
	//模板编号
	private String noticeRuleNo;
	//模块
	private String module;
	//模板变量值
	private Object variables;
	//通知地址
	private Set<String> sendTo = new HashSet<String>();

	public String getNoticeRuleNo() {
		return noticeRuleNo;
	}

	public void setNoticeRuleNo(String noticeRuleNo) {
		this.noticeRuleNo = noticeRuleNo;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getTemplatePath() {
		return templatePath;
	}

	public void setTemplatePath(String templatePath) {
		this.templatePath = templatePath;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getSendType() {
		return sendType;
	}

	public void setSendType(String sendType) {
		this.sendType = sendType;
	}

	public Object getVariables() {
		return variables;
	}

	public void setVariables(Object variables) {
		this.variables = variables;
	}

	public Set<String> getSendTo() {
		return sendTo;
	}

	public void setSendTo(Set<String> sendTo) {
		this.sendTo = sendTo;
	}

	
}
