package com.wstuo.common.tools.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.tools.dao.IPagesSetDAO;
import com.wstuo.common.tools.entity.PagesSet;
import com.wstuo.common.util.StringUtils;

/**
 * 页面设置Service
 * @author Candy
 *
 */
public class PagesSetSerice implements IPagesSetSerice{
	@Autowired
	private IPagesSetDAO pagesSetdao;
	private PagesSet pagesSet=new PagesSet();


	public PagesSet getPagesSet() {
		return pagesSet;
	}
	public void setPagesSet(PagesSet pagesSet) {
		this.pagesSet = pagesSet;
	}
	
	@Transactional
	public void updatePagesSet(PagesSet entity) {
		
		List<PagesSet> list= pagesSetdao.findAll();
		if(list.size()>0){
			pagesSet=list.get(list.size()-1);
			if(StringUtils.hasText(entity.getPaName()))
				pagesSet.setPaName(entity.getPaName());
			else
				pagesSet.setPaName("ITIL WSTUO");
			if(StringUtils.hasText(entity.getImgname()))
				pagesSet.setImgname(entity.getImgname());
			pagesSetdao.update(pagesSet);
		}else{
			entity.setId(1l);
			if(entity.getPaName()==null){
				entity.setPaName("ITIL WSTUO");
			}if(entity.getImgname()==null){
				entity.setImgname("logo.png");
			}
			pagesSetdao.save(entity);
		}
	}
	public PagesSet showPagesSet(){
		List<PagesSet> list= pagesSetdao.findAll();
		if(list.size()!=0)
			pagesSet=list.get(list.size()-1);

		return pagesSet;
	}
}
