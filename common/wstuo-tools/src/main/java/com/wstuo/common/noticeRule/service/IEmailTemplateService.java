package com.wstuo.common.noticeRule.service;

public interface IEmailTemplateService {
	
	final String TEMPLATEDIRNAME = "emailTemplates";
	
	String template(Object object, String templateFileDir,
			String templateFile,String tenantId);
	
	/**
	 * 从基础数据的邮件模板文件复制到租户下的模板文件
	 * @param destDir
	 */
	void copyEmailTemplate(String destDir);
}
