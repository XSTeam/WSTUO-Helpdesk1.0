package com.wstuo.common.tools.dto;

import java.util.Date;

import com.wstuo.common.dto.BaseDTO;

/**
 * 回访列表DTO
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class UserReturnVisitGridDTO extends BaseDTO {
	private Long visitId;
	private Long satisfaction;
	private Long state;
	private Date returnVisitSubmitTime;
	private Date returnVisitTime ;
	private Long returnVisitMode;
	private String returnVisitUser;
	private String returnVisitSubmitUser;
	private Long eno;
	public Long getVisitId() {
		return visitId;
	}
	public void setVisitId(Long visitId) {
		this.visitId = visitId;
	}
	public Long getSatisfaction() {
		return satisfaction;
	}
	public void setSatisfaction(Long satisfaction) {
		this.satisfaction = satisfaction;
	}
	public Long getState() {
		return state;
	}
	public void setState(Long state) {
		this.state = state;
	}
	public Date getReturnVisitSubmitTime() {
		return returnVisitSubmitTime;
	}
	public void setReturnVisitSubmitTime(Date returnVisitSubmitTime) {
		this.returnVisitSubmitTime = returnVisitSubmitTime;
	}
	public Date getReturnVisitTime() {
		return returnVisitTime;
	}
	public void setReturnVisitTime(Date returnVisitTime) {
		this.returnVisitTime = returnVisitTime;
	}

	public Long getReturnVisitMode() {
		return returnVisitMode;
	}
	public void setReturnVisitMode(Long returnVisitMode) {
		this.returnVisitMode = returnVisitMode;
	}
	public String getReturnVisitUser() {
		return returnVisitUser;
	}
	public void setReturnVisitUser(String returnVisitUser) {
		this.returnVisitUser = returnVisitUser;
	}
	public String getReturnVisitSubmitUser() {
		return returnVisitSubmitUser;
	}
	public void setReturnVisitSubmitUser(String returnVisitSubmitUser) {
		this.returnVisitSubmitUser = returnVisitSubmitUser;
	}
	public Long getEno() {
		return eno;
	}
	public void setEno(Long eno) {
		this.eno = eno;
	}
	
}
