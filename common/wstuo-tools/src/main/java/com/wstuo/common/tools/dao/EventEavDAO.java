package com.wstuo.common.tools.dao;

import com.wstuo.common.tools.entity.EventEav;
import com.wstuo.common.dao.BaseDAOImplHibernate;

/**
 * Event Eav DAO Class
 * @author WSTUO
 *
 */
public class EventEavDAO extends BaseDAOImplHibernate<EventEav> implements IEventEavDAO {

}
