package com.wstuo.common.tools.dto;

import java.util.List;
import java.util.Map;

import com.wstuo.common.tools.entity.ExportInfo;
import com.wstuo.common.dto.PageDTO;

/**
 * 导出页面DTO
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class ExportPageDTO extends PageDTO{
	private String fileName;
	private String mqTitle;//MQ执行的主题
    private String mqContent;//MQ执行的内容
    private String mqCreator;//MQ创建者
    private Map<String, String> knowledgeServices;
    private String fileFormat;//文件格式
    private ExportInfo entity;

    private String lang;  //i18n国际化参数
    
    private List<ChangeCIDTO> ciNameList;
	private String tenantId;
	private String path;
	



	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public List<ChangeCIDTO> getCiNameList() {
		return ciNameList;
	}

	public void setCiNameList(List<ChangeCIDTO> ciNameList) {
		this.ciNameList = ciNameList;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public ExportInfo getEntity() {
		return entity;
	}

	public void setEntity(ExportInfo entity) {
		this.entity = entity;
	}

	public String getFileFormat() {
		return fileFormat;
	}

	public void setFileFormat(String fileFormat) {
		this.fileFormat = fileFormat;
	}

	public Map<String, String> getKnowledgeServices() {
		return knowledgeServices;
	}

	public void setKnowledgeServices(Map<String, String> knowledgeServices) {
		this.knowledgeServices = knowledgeServices;
	}

	public String getMqTitle() {
		return mqTitle;
	}

	public void setMqTitle(String mqTitle) {
		this.mqTitle = mqTitle;
	}

	public String getMqContent() {
		return mqContent;
	}

	public void setMqContent(String mqContent) {
		this.mqContent = mqContent;
	}

	public String getMqCreator() {
		return mqCreator;
	}

	public void setMqCreator(String mqCreator) {
		this.mqCreator = mqCreator;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
