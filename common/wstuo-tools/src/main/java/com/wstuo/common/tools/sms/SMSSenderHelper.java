package com.wstuo.common.tools.sms;

import org.springframework.beans.factory.annotation.Autowired;

import com.wstuo.common.tools.dao.ISMSAccountDAO;
import com.wstuo.common.tools.dto.MessageConsumerDTO;
import com.wstuo.common.tools.dto.SMSAccountDTO;
import com.wstuo.common.tools.dto.SMSMessageDTO;
import com.wstuo.common.tools.dto.SMSsendDTO;
import com.wstuo.common.tools.entity.SMSAccount;
import com.wstuo.common.exception.ApplicationException;

/**
 * 短信发送初始化
 * @author QXY
 *
 */
public class SMSSenderHelper {

	
	@Autowired
	private ISMSAccountDAO smsAccountDAO;
	@Autowired
	private PanZhiSMSHelper panZhiSMSHelper;
	@Autowired
	private WanCiSMSHelper wanCiSMSHelper;
	@Autowired
	private OtherSMSHelper otherSMSHelper;
	
	private ISMSSender smsSender;
	
	private String sendURL;
	private String queryURL;
	private String userName;
	
	/**
	 * 初始化账户信息.
	 */
	public void initSMSAccount(){
		
		SMSAccount account=smsAccountDAO.getSmsAccount();
		
		if(account!=null){
			//初始化账户信息
			userName=account.getUserName();
			String smsInstance=account.getSmsInstance();
			String password=account.getPwd();
			
			if(smsInstance.equals("WanCiSMSHelper")){
				sendURL="http://service.winic.org/sys_port/gateway/?id="+userName+"&pwd="+password+"&content={content}&to={to}";
				queryURL="http://service.winic.org:8009/webservice/public/remoney.asp?uid="+userName+"&pwd="+password+"";
				smsSender=wanCiSMSHelper;
			}else if(smsInstance.equals("PanZhiSMSHelper")){
				sendURL="http://59.42.247.51/http.php?act={action}&orgid="+account.getOrgId()+"&username="+userName+"&passwd="+password+"&msg={msg}&destnumbers={phoneNumbers}";
				queryURL=sendURL;
				smsSender=panZhiSMSHelper;
			}else if(smsInstance.equals("OtherSMSHelper")){
				smsSender=otherSMSHelper;
			}
		}else{
			throw new ApplicationException("ERROR_SMS_ACCOUNT");
		}
	}
	
	/**
	 * 发送信息.
	 * @param mobile 手机号码列表
	 * @param content 短信内容
	 * @param isSystem 发送方式,true为系统发送，false为手动发送
	 * @param saveHistory 是否保存历史记录，true保存，false不保存
	 * @return SMSMessageDTO
	 */
	public SMSMessageDTO SendSms(String mobile, String content,boolean isSystem,boolean saveHistory){
		initSMSAccount();
		return smsSender.SendSms(sendURL,mobile,content, isSystem, saveHistory);
	}

	/**
	 * 发送信息.
	 * @param noticeSendDTO 
	 * @return SMSMessageDTO
	 */
	public void sendSms(MessageConsumerDTO messageConsumerDTO){
		SMSsendDTO dto=new SMSsendDTO();
		for (String to: messageConsumerDTO.getTo()) {
			dto.setMobile(to);
			dto.setContent(messageConsumerDTO.getContent());
			sendSms(dto.getMobile(), dto.getContent(), true, true);
		}
	}
	/**
	 * 发送信息.
	 * @param mobile 手机号码列表
	 * @param content 短信内容
	 * @param isSystem 发送方式,true为系统发送，false为手动发送
	 * @param saveHistory 是否保存历史记录，true保存，false不保存
	 * @return SMSMessageDTO
	 */
	public SMSMessageDTO sendSms(String mobile, String content,boolean isSystem,boolean saveHistory){
		initSMSAccount();
		return smsSender.SendSms(sendURL,mobile,content, isSystem, saveHistory);
	}
	
	/**
	 * 查询账户信息.
	 * @return result
	 */
	public SMSMessageDTO queryMoney(){
		
		initSMSAccount();
		
		SMSMessageDTO dto= smsSender.queryMoney(queryURL);
		dto.setId(userName);
		return dto;
		
		
	}

	/**
	 * 查询服务器状态.
	 * @return true连接成功，false连接失败
	 */
	public Boolean testConnection(SMSAccountDTO smsAccountDTO){
		String postUrl="";
		if("WanCiSMSHelper".equals(smsAccountDTO.getSmsInstance())){
			smsSender=wanCiSMSHelper;
		    postUrl="http://service.winic.org:8009/webservice/public/remoney.asp?uid="+smsAccountDTO.getUserName()+"&pwd="+smsAccountDTO.getPwd();
		}else if("PanZhiSMSHelper".equals(smsAccountDTO.getSmsInstance())){
			smsSender=panZhiSMSHelper;
			postUrl="http://59.42.247.51/http.php?act=busytest&orgid="+smsAccountDTO.getOrgId()+"&username="+smsAccountDTO.getUserName()+"&passwd="+smsAccountDTO.getPwd();
		}else if("OtherSMSHelper".equals(smsAccountDTO.getSmsInstance())){
			smsSender=otherSMSHelper;
		}
		if(smsSender!=null){
			return smsSender.testConnection(postUrl);
		}else{
			return false;
		}
		
	}
	
}
