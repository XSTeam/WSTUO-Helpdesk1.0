package com.wstuo.common.customForm.dao;

import java.util.List;

import com.wstuo.common.customForm.entity.Field;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

/**
 * 自定义字段DAO接口类
 * @author QXY
 *
 */
public interface IFieldDAO extends IEntityDAO<Field>{
	
	PageDTO findPageField(Field field,int start,int limit,String sidx,String  sord); 

	int addField(String table,String name ,String type);
	
	int delField(String table,String name);
	
	PageDTO findByCustom(String table,int start,int limit,String sidx,String  sord);
	
	List<Field> findListField(Field field ,Boolean export);
}
