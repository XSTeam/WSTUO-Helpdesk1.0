package com.wstuo.common.tools.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;


import com.wstuo.common.entity.BaseEntity;

/**
 * 用户回访信息Entity
 * @author WSTUO
 *
 */
@SuppressWarnings({ "serial", "rawtypes" })
@Entity
@Table(name="userreturnvisit")
public class UserReturnVisit extends BaseEntity {
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	private Long visitId;
	@Column(nullable=false)
	private Long eno;
	@Column(nullable=false)
	private String entityType;
	@Column(nullable=true)
	private Long satisfaction;//满意度,非常满意(5)、满意(4)、一般(3)、不满意(2)、非常不满意(1)
	@Lob
	@Column(nullable=true)
	private String returnVisitDetail;//回访详细
	@Column(nullable=false)
	private Long state;//回访状态,待复(0)、已复(1)
	@Column(nullable=true)
	private Date returnVisitSubmitTime;//回访发出时间
	@Column(nullable=true)
	private Date returnVisitTime ;//回访时间  
	@Column(nullable=true)
	private Long returnVisitMode;//回访方式,电话(0)、邮件(1)、其他(2)
	@Column(nullable=true)
	private String returnVisitUser;//回访用户
	@Column(nullable=true)
	private String returnVisitSubmitUser;//提出回访用户
	@Column(nullable=true)
	private String returnVisitTechnicianName;  //保存请求操作的技术员
	
	
	public String getReturnVisitTechnicianName() {
		return returnVisitTechnicianName;
	}
	public void setReturnVisitTechnicianName(String returnVisitTechnicianName) {
		this.returnVisitTechnicianName = returnVisitTechnicianName;
	}
	public Long getVisitId() {
		return visitId;
	}
	public void setVisitId(Long visitId) {
		this.visitId = visitId;
	}
	public Long getEno() {
		return eno;
	}
	public void setEno(Long eno) {
		this.eno = eno;
	}
	public String getEntityType() {
		return entityType;
	}
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}
	public Long getSatisfaction() {
		return satisfaction;
	}
	public void setSatisfaction(Long satisfaction) {
		this.satisfaction = satisfaction;
	}
	public String getReturnVisitDetail() {
		return returnVisitDetail;
	}
	public void setReturnVisitDetail(String returnVisitDetail) {
		this.returnVisitDetail = returnVisitDetail;
	}
	public Long getState() {
		return state;
	}
	public void setState(Long state) {
		this.state = state;
	}
	public Date getReturnVisitSubmitTime() {
		return returnVisitSubmitTime;
	}
	public void setReturnVisitSubmitTime(Date returnVisitSubmitTime) {
		this.returnVisitSubmitTime = returnVisitSubmitTime;
	}
	public Date getReturnVisitTime() {
		return returnVisitTime;
	}
	public void setReturnVisitTime(Date returnVisitTime) {
		this.returnVisitTime = returnVisitTime;
	}
	public Long getReturnVisitMode() {
		return returnVisitMode;
	}
	public void setReturnVisitMode(Long returnVisitMode) {
		this.returnVisitMode = returnVisitMode;
	}
	public String getReturnVisitUser() {
		return returnVisitUser;
	}
	public void setReturnVisitUser(String returnVisitUser) {
		this.returnVisitUser = returnVisitUser;
	}
	public String getReturnVisitSubmitUser() {
		return returnVisitSubmitUser;
	}
	public void setReturnVisitSubmitUser(String returnVisitSubmitUser) {
		this.returnVisitSubmitUser = returnVisitSubmitUser;
	}



}
