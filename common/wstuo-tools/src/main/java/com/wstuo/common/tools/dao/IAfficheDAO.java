package com.wstuo.common.tools.dao;


import com.wstuo.common.tools.dto.AfficheQueryDTO;
import com.wstuo.common.tools.entity.AfficheInfo;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

import java.util.List;


/**
 * 公告管理DAO接口
 * @author QXY
 */
public interface IAfficheDAO
    extends IEntityDAO<AfficheInfo>
{
	/**
	 * 公告分页查询
	 * @param dto
	 * @param start
	 * @param limit
	 * @param sidx
	 * @param sord
	 * @return PageDTO
	 */
    PageDTO findPager( AfficheQueryDTO dto, int start, int limit,String sidx,String sord );
    /**
     * 根据公告标题查询
     * @param affTitle 公告标题
	 * @return List<AfficheInfo>
     * date 2010/9/10
     * **/
    public List<AfficheInfo> findByName( String affTitle );
}
