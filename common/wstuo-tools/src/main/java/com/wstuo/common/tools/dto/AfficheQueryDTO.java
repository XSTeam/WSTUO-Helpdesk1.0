package com.wstuo.common.tools.dto;

import com.wstuo.common.dto.AbstractValueObject;

import java.util.Date;

/**
 * 公告属性查询DTO
 * 
 * @author brain date 2010/9/10
 * **/
@SuppressWarnings("serial")
public class AfficheQueryDTO extends AbstractValueObject {
	/*
	 * Notice No. *
	 */
	private Long affId;

	/*
	 * Notice Title
	 * 
	 * *
	 */
	private String affTitle;

	/*
	 * Notice of the time initiated *
	 */
	private Date affStart;

	/*
	 * Notice the end of time *
	 */
	private Date affEnd;

	/*
	 * 用户过滤的条 *
	 */
	private String selectVaule;

	/**
	 * Visible state 
	 **/
	private Integer visibleState = 0;
	/**
	 * 所属客户 
	 */
	private Long companyNo;

	private String queryFlag;

	public String getQueryFlag() {
		return queryFlag;
	}

	public void setQueryFlag(String queryFlag) {
		this.queryFlag = queryFlag;
	}

	public String getSelectVaule() {
		return selectVaule;
	}

	public void setSelectVaule(String selectVaule) {
		this.selectVaule = selectVaule;
	}

	public Long getAffId() {
		return affId;
	}

	public void setAffId(Long affId) {
		this.affId = affId;
	}

	public String getAffTitle() {
		return affTitle;
	}

	public void setAffTitle(String affTitle) {
		this.affTitle = affTitle;
	}

	public Date getAffstart() {
		return affStart;
	}

	public void setAffstart(Date affstart) {
		this.affStart = affstart;
	}

	public Date getAffEnd() {
		return affEnd;
	}

	public void setAffEnd(Date affEnd) {
		this.affEnd = affEnd;
	}

	public Date getAffStart() {
		return affStart;
	}

	public void setAffStart(Date affStart) {
		this.affStart = affStart;
	}

	public Integer getVisibleState() {
		return visibleState;
	}

	public void setVisibleState(Integer visibleState) {
		this.visibleState = visibleState;
	}

	public Long getCompanyNo() {
		return companyNo;
	}

	public void setCompanyNo(Long companyNo) {
		this.companyNo = companyNo;
	}

}
