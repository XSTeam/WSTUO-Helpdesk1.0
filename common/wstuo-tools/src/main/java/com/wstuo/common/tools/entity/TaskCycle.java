package com.wstuo.common.tools.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.wstuo.common.tools.util.ToolsConstant;

@Entity
public class TaskCycle {

	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
	private Long cid;
	private String type;//Week or No //任务类型
	private String cycleType = ToolsConstant.TASK_TYPE_CYCLE_END_NO;//COUNT or DATE or NO //结束类型
	private Date startDate;
	private Date endDate;
	private Integer cycleCount;
	private String dateStr;
	
	public Long getCid() {
		return cid;
	}
	public void setCid(Long cid) {
		this.cid = cid;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getStartDate() {
		return startDate;
	}
	/**
	 * 循环任务的开始时间
	 * @param startDate
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	/**
	 * 循环任务的结束时间
	 * @param startDate
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getDateStr() {
		return dateStr;
	}
	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
	}
	public Integer getCycleCount() {
		return cycleCount;
	}
	public void setCycleCount(Integer cycleCount) {
		if (cycleCount == null || cycleCount < 1) {
			cycleCount = 1;
		}
		this.cycleCount = cycleCount;
	}
	public String getCycleType() {
		return cycleType;
	}
	public void setCycleType(String cycleType) {
		if (cycleType == null ) {
			cycleType = ToolsConstant.TASK_TYPE_CYCLE_END_NO;
		}
		this.cycleType = cycleType;
	}
	
	//================
	public int[] getDateIndexVerify() {
		int[] indexs = null;
		if (this.dateStr != null && this.dateStr.matches("(\\d{1,2};)+")) {
			String[] dates = this.dateStr.split(ToolsConstant.TASK_TIME_SEPARATED) ;
			int len = dates.length;
			indexs = new int[len];
			for (int i = 0; i < len; i++) {
				indexs[i] = Integer.parseInt(dates[i]);
			}
		}
		return indexs;
	}
	public String[] getDateStrVerify() {
		String[] dates = null;
		if (this.dateStr != null && this.dateStr.matches("(\\d{1,2};)+")) {
			dates = this.dateStr.split(ToolsConstant.TASK_TIME_SEPARATED);
		}
		return dates;
	}
	@Override
	public String toString() {
		return "TaskCycle [cid=" + cid + ", type=" + type + ", cycleType="
				+ cycleType + ", startDate=" + startDate + ", endDate="
				+ endDate + ", cycleCount=" + cycleCount + ", dateStr="
				+ dateStr + "]";
	}
}
