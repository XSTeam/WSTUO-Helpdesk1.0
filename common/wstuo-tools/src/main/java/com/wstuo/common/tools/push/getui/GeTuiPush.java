package com.wstuo.common.tools.push.getui;
/**
 * 封装个推需要的应用参数；
 * http://www.oschina.net/question/1782938_231949
 * @author gs60
 *
 */
public class GeTuiPush {
	private String appId = "KIHuHao8My8xE0LXgdeRt2";
	private String appSecret = "04X6eOK7sfA3r5jNkoAmP6";
	private String appkey = "PWNxdxkBue5xQTOTtpdnk4";
	private String master = "1gKYaUy9Lm6GGRNBCRPygA";
	private String host = "http://sdk.open.api.igexin.com/apiex.htm";
	
	public GeTuiPush() {
		super();
	}
	public GeTuiPush(String appId, String appSecret, String appkey,
			String master, String host) {
		super();
		this.appId = appId;
		this.appSecret = appSecret;
		this.appkey = appkey;
		this.master = master;
		this.host = host;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getAppSecret() {
		return appSecret;
	}
	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}
	public String getAppkey() {
		return appkey;
	}
	public void setAppkey(String appkey) {
		this.appkey = appkey;
	}
	public String getMaster() {
		return master;
	}
	public void setMaster(String master) {
		this.master = master;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	@Override
	public String toString() {
		return "GeTuiPush [appId=" + appId + ", appSecret=" + appSecret
				+ ", appkey=" + appkey + ", master=" + master + ", host="
				+ host + "]";
	}
	
}
