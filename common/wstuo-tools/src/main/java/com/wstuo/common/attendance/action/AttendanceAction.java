package com.wstuo.common.attendance.action;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.wstuo.common.tools.dto.TaskDTO;
import com.wstuo.common.tools.service.ITaskService;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.action.SupplierAction;
import com.wstuo.common.security.dto.UserQueryDTO;
import com.wstuo.common.security.service.IUserInfoService;

/**
 * 排班
 * @author gs60
 *
 */
public class AttendanceAction extends SupplierAction {
	@Autowired
	private IUserInfoService userInfoService;
	@Autowired
	private ITaskService taskService;
	private UserQueryDTO userQueryDto = new UserQueryDTO();
	private String sord;
	private String sidx;
	private int id;
	private int page = 1;
	private int rows = 10;
	private String state;
	private PageDTO users;

	private TaskDTO taskDto = new TaskDTO();
	private static final long serialVersionUID = 1251123386467486468L;

	/**
	 * search user
	 * 此方法原本在userAction中，由于排班任务的查询需要，拷贝过来进行修改
	 * @return String
	 */
	public String find() {
		int start = (page - 1) * rows;
		userQueryDto.setStart(start);
		userQueryDto.setLimit(rows);
		if ("false".equals(state)) {
			userQueryDto.setUserState(false);
		} else if ("true".equals(state)) {
			userQueryDto.setUserState(true);
		} else {
			userQueryDto.setUserState(null);
		}
		if(userQueryDto.getAttendance() != null && userQueryDto.getAttendance()){
			List<String> userNames = taskService.attendanceTaskByTimeRange(taskDto);
			if ( userNames != null && userNames.size() > 0) {
				userQueryDto.setLoginNames( userNames.toArray(new String[userNames.size()]) );
				users = userInfoService.findPagerUser(userQueryDto, sord, sidx);
			}else{
				users = new PageDTO();
				users.setTotalSize( 0 );
			}
		}else{
			users = userInfoService.findPagerUser(userQueryDto, sord, sidx);
		}
		
		if (users != null) {
			users.setPage(page);
			users.setRows(rows);
		}
		return SUCCESS;
	}

	public UserQueryDTO getUserQueryDto() {
		return userQueryDto;
	}

	public void setUserQueryDto(UserQueryDTO userQueryDto) {
		this.userQueryDto = userQueryDto;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public PageDTO getUsers() {
		return users;
	}

	public void setUsers(PageDTO users) {
		this.users = users;
	}

	public TaskDTO getTaskDto() {
		return taskDto;
	}

	public void setTaskDto(TaskDTO taskDto) {
		this.taskDto = taskDto;
	}
	
}
