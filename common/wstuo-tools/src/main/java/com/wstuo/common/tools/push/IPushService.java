package com.wstuo.common.tools.push;


import java.util.Set;

import com.wstuo.common.tools.dto.PushMessageDTO;

public interface IPushService {

	void pushMessage(PushMessageDTO pushMessageDTO);
	public void pushMessage( Set<PushMessageDTO> dtos );
}
