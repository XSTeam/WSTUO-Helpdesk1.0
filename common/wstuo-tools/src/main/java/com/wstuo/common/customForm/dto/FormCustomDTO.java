package com.wstuo.common.customForm.dto;

import java.util.List;

import com.wstuo.common.dto.BaseDTO;

@SuppressWarnings("serial")
public class FormCustomDTO extends BaseDTO {

	private Long formCustomId;
	private String formCustomName;
	private String formCustomContents;
	private Long eventId;
	private String eventName;
	private int isTemplate;
	private String isShowBorder;
	private Boolean isDefault = false;//是否默认
	private String type;//模块类型 request ci
	private Long ciCategoryNo;
	private Long[] tabsIds;
	private List<FormCustomTabsDTO> formCustomTabsDtos;
	
	
	public List<FormCustomTabsDTO> getFormCustomTabsDtos() {
		return formCustomTabsDtos;
	}

	public void setFormCustomTabsDtos(List<FormCustomTabsDTO> formCustomTabsDtos) {
		this.formCustomTabsDtos = formCustomTabsDtos;
	}

	public Long[] getTabsIds() {
		return tabsIds;
	}

	public void setTabsIds(Long[] tabsIds) {
		this.tabsIds = tabsIds;
	}

	public Long getCiCategoryNo() {
		return ciCategoryNo;
	}

	public void setCiCategoryNo(Long ciCategoryNo) {
		this.ciCategoryNo = ciCategoryNo;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	
	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}


	public Long getFormCustomId() {
		return formCustomId;
	}

	public void setFormCustomId(Long formCustomId) {
		this.formCustomId = formCustomId;
	}

	public String getFormCustomName() {
		return formCustomName;
	}

	public void setFormCustomName(String formCustomName) {
		this.formCustomName = formCustomName;
	}

	public String getFormCustomContents() {
		return formCustomContents;
	}

	public void setFormCustomContents(String formCustomContents) {
		this.formCustomContents = formCustomContents;
	}

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public int getIsTemplate() {
		return isTemplate;
	}

	public void setIsTemplate(int isTemplate) {
		this.isTemplate = isTemplate;
	}

	public String getIsShowBorder() {
		return isShowBorder;
	}

	public void setIsShowBorder(String isShowBorder) {
		this.isShowBorder = isShowBorder;
	}

}
