package com.wstuo.common.tools.push.getui;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.gexin.rp.sdk.base.IPushResult;
import com.gexin.rp.sdk.base.ITemplate;
import com.gexin.rp.sdk.base.impl.ListMessage;
import com.gexin.rp.sdk.base.impl.SingleMessage;
import com.gexin.rp.sdk.base.impl.Target;
import com.gexin.rp.sdk.http.IGtPush;
import com.gexin.rp.sdk.template.TransmissionTemplate;
import com.wstuo.common.tools.dto.PushMessageDTO;
import com.wstuo.common.util.StringUtils;
/**
 * 个推业务类；
 * @author gs60
 *
 */
public class GeTuiService {
	private static JsonConfig jsonConfig = new JsonConfig();
	private final static Logger LOGGER = Logger
			.getLogger(GeTuiService.class);
	/**
	 * APNS发送内容提示,这里的内容长度改变时，APNS消息内容可允许长度也会改变
	 */
	private static final String APNS_ACTION_LOC_KEY = "查看";
	/** 消息内容中的默认标题，（主要用在android，iOS使用的是APNS消息，标题不在这里设置）*/
	private final static String DEFAULT_TITLE = "WSTUO";
	/**
	 * 测试发现最大汉字数量在58。PushInfo中默认大约占用82=(3*27.33)个字节
	 */
	private static final int APNS_CONTENT_LEN = 58 - APNS_ACTION_LOC_KEY.length();
	@Autowired
	private GeTuiPush geTuiPush;
	//private static IGtPush push = null;
	/**
	 * 消息推送主方法
	 * @param dto
	 */
	public void pushMessage( Set<PushMessageDTO> dtos) {
		try {
			IGtPush push = new IGtPush( geTuiPush.getHost() , geTuiPush.getAppkey() , geTuiPush.getMaster() );
			boolean connState = push.connect();
			for (PushMessageDTO dto : dtos) {
				Target target = pushTarget( dto.getAccept() );
				TransmissionTemplate template = transmissionTemplate( dto );
				SingleMessage message = singleMessage(template);
		        IPushResult ret = push.pushMessageToSingle(message, target);
				//System.out.println("GeTuiService.pushMessage(5)" + pushResultToString(ret) );
				LOGGER.debug(ret);
			}
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}
	public void pushMessage( PushMessageDTO dto) {
		List<String> clientId = new ArrayList<String>();
		clientId.add( dto.getAccept() );
        List<Target> targets = pushTarget( clientId );
		
		try {
			IGtPush push = new IGtPush( geTuiPush.getHost() , geTuiPush.getAppkey() , geTuiPush.getMaster() );
	        push.connect();
			TransmissionTemplate template = transmissionTemplate( dto );
	        ListMessage message = listMessage( template );
	        String contentId = push.getContentId( message );
	        IPushResult ret1 = push.pushMessageToList(contentId, targets);
			LOGGER.debug(ret1);
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}
	
	/**
	 * 个推针对推送列表消息生成，及相关参数配置；
	 * @param template
	 * @return
	 */
	public ListMessage listMessage(ITemplate template) {
		ListMessage message = new ListMessage();
		message.setData(template);
	    message.setOffline(true);//用户当前不在线时，是否离线存储，可选，默认不存储
		message.setOfflineExpireTime(72 * 3600 * 1000);		//离线有效时间，单位为毫秒，可选

	    //判断是否客户端是否wifi环境下推送，1为在WIFI环境下，0为不限制网络环境。
	    message.setPushNetWorkType(0);
	    return message;
	}
	public SingleMessage singleMessage(ITemplate template) {
		SingleMessage message = new SingleMessage();
		message.setData(template);
	    message.setOffline(true);//用户当前不在线时，是否离线存储，可选，默认不存储
		message.setOfflineExpireTime(72 * 3600 * 1000);		//离线有效时间，单位为毫秒，可选

	    //判断是否客户端是否wifi环境下推送，1为在WIFI环境下，0为不限制网络环境。
	    message.setPushNetWorkType(0);
	    return message;
	}
	/**
	 * 生成消息推送模板；
	 * @param content
	 * @return
	 */
	public TransmissionTemplate transmissionTemplate (  PushMessageDTO dto ) {
		jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
		String content =JSONObject.fromObject(dto.messageContent(DEFAULT_TITLE), jsonConfig).toString();
	    TransmissionTemplate template = new TransmissionTemplate();
	    template.setAppId( geTuiPush.getAppId() );
	    template.setAppkey( geTuiPush.getAppkey() );
	    // 透传消息设置，1为强制启动应用，客户端接收到消息后就会立即启动应用；2为等待应用启动
	    template.setTransmissionType(2);
	    template.setTransmissionContent( content );
	    /**ios独有的通知参数 */
	    setAPNSParam(dto, template ,"");//content
	    return template;
	}
	/**
	 * 生成推送目标；
	 * @param clientIds
	 * @return
	 */
	public List<Target> pushTarget(List<String> clientIds) {
		List<Target> targets = new ArrayList<Target>();
		if (clientIds != null) {
			for (String cid : clientIds) {
				if ( StringUtils.hasText( cid ) ) {
					Target target = new Target();
					target.setAppId( geTuiPush.getAppId() );
					target.setClientId( cid );
					targets.add(target);
				}
			}
		}
		return targets;
	}
	/**
	 * 生成推送目标；
	 * @param clientIds
	 * @return
	 */
	public Target pushTarget(String clientId) {
		Target target = null;
		if (clientId != null && clientId.length() > 0) {
			target = new Target();
			target.setAppId( geTuiPush.getAppId() );
			target.setClientId( clientId );
		}
		return target;
	}
	/**
	 * iOS需要，iOS强制要求的APNS参数；（注意：iOS APNS消息内容大小限制为256Byte，pushInfo大小不能超过这个值；）
	 * 测试得出统计的是“ASCII字符数”：一个汉字在消息中占用3个字节，一个数字、字母是1个字节
	 * APNS发送内容只有标题，其他数据放在template.setTransmissionContent( content );
	 * @param0 actionLocKey 显示在锁屏界面。”滑动来“查看xxx字样旁边
	 * @param1 badge 桌面图标上红点显示的数字；
	 * @param2 message 消息内容主体；
	 * @param3 sound 音效
	 * @param4 payload 个推的透传消息
	 * @param5 locKey 通知栏上的标题
	 * @param6 locArgs 未使用
	 * @param7 launchImage 运行的图片，使用默认；
	 * @param dto
	 * @see <a href="http://docs.getui.com/pages/viewpage.action?pageId=1213594">查看个推详细介绍</a>
	 */
	public void setAPNSParam(PushMessageDTO dto,TransmissionTemplate template,String message) {
	    //template.setPushInfo(actionLocKey 0, badge 1, message 2, sound 3,
		//payload 4, locKey 5, locArgs 6, launchImage 7 );
		try {
			String locKey = dto.getContent();
			int badge = dto.getCount() ;
			template.setPushInfo(APNS_ACTION_LOC_KEY,
					badge, message, null,"" , APNSContent(locKey,APNS_CONTENT_LEN - 5) 
					, dto.getEno()+"" , null);
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}
	/**
	 * APNS发送内容只有内容，其他数据放在template.setTransmissionContent( content );，
	 * */
	public String APNSContent(String content,int len){
		content += "";
		return (content.length() > len ? content.substring( 0, len  ) : content) ;
	}
	
	public String pushResultToString( IPushResult pushResult) {
		StringBuffer result = new StringBuffer();
		if (pushResult != null) {
			result.append( "任务：" + pushResult.getTaskId() );
			result.append( ",消息：" + pushResult.getMessageId() );
			result.append( ",状态：" + pushResult.getResultCode() );
		}
		return result.toString();
	}
}
