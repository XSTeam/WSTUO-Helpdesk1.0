package com.wstuo.common.tools.service;

import java.io.File;
import java.util.List;

import com.wstuo.common.tools.dto.EmailConnectionDTO;
import com.wstuo.common.tools.dto.EmailDTO;
import com.wstuo.common.tools.dto.EmailMessageDTO;
import com.wstuo.common.tools.dto.EmailMessageQueryDTO;
import com.wstuo.common.tools.dto.EmailServerDTO;
import com.wstuo.common.tools.dto.MessageConsumerDTO;
import com.wstuo.common.tools.entity.EmailMessage;
import com.wstuo.common.dto.PageDTO;

/**
 * 邮件Service
 * @author QXY
 **/
public interface IEmailService
{

	/**
    * yyyyMMdd
    */
   public final static String FILENAME_PATTERN = "yyyyMMdd";
	
	public Boolean scanEmailMessagesByUserService(EmailConnectionDTO emailConnectionDto);
	
    /**
     * 发送邮件连接创建
     */
    public void mailSmtpConnection();

    /**
         * 接收邮件
         * @param dto
         * @return EmailDTO[]
         */
    public PageDTO getEamilMessage( EmailConnectionDTO dto, int start, int limit );

    /**
         * 发送邮件测试
         * @param dto
         * @return void
         */
    public boolean sendEmailConnTest( final EmailConnectionDTO dto );

    /**
         * 接收邮件测试
         * @param dto
         * @return void
         */
    public boolean receiveEmailConnTest( final EmailConnectionDTO dto );

    /**
         * 邮件测试
         * @param dto
         * @return void
         */
    public int emailConnTest( final EmailConnectionDTO dto );

    /**
     * 扫描邮箱收件信息并保存入数据库
     * @param dto EmailConnectionDTO
     * @return EmailDTO[]
     */
    public boolean saveReceiveMessages( EmailConnectionDTO dto );

    /**
     * 将entity EmailMessage保存入数据库
     * @param emailMessage
     * @return Long
     */
    public Long saveEmailMessages( EmailMessage emailMessage );

    /**
     * 邮件发送
     * @param dto
     * @return boolean
     * @throws Exception
     */
    public boolean sendMail( EmailDTO dto )
                     throws Exception;
    
    void sendMail(MessageConsumerDTO messageConsumerDTO);

    /**
     * 邮件分页查询
     * @param dto
     * @param start
     * @param limit
     * @param sidx
     * @param sord
     * @return PageDTO
     */
    public PageDTO getEmailMessage( EmailMessageQueryDTO dto, int start, int limit, String sidx, String sord );

    /**
     * 根据ID查找邮件信息.
     * @param id Long 编号
     * @return EmailMessageDTO 邮件信息DTO�?
     */
    public EmailMessageDTO findById( Long id );
    /**
     * 删除邮件信息
     * @param ids
     */
    void deleteEmailMessage(Long[] ids);
    
    /**
     * 根据登录帐号发送邮件
     * @param loginName
     * @param title
     * @param content
     * @return boolean
     */
    boolean sendMailByLoginName( String loginName,String title,String content);
    
    
    /**
     * SMTP Connection
     * @param emailConnectionDto
     */
    void mailSmtpConnection(final EmailConnectionDTO emailConnectionDto);
    
    /**
     * 发送邮件
     * @param dto
     * @param emailConnectionDto
     * @return boolean
     */
    boolean sendMail( EmailDTO dto,EmailConnectionDTO emailConnectionDto);
    
    /**
     * 发送邮件
     * @param dto
     * @param emailConnectionDto
     * @param type
     * @return boolean
     */
    boolean sendMail(EmailDTO dto,EmailConnectionDTO emailConnectionDto,String type);
    
    /**
	 * 接收邮件
	 * @param dto 连接邮件服务器信息
	 * @return 返回邮件集合
	 */
    List<EmailMessageDTO> receiveMail();
    
    /**
     * 邮件发送并保存到数据库中作为已发送的邮件
     * @param dto
     */
    boolean sendMail(EmailDTO dto,List<File> files);
    
    /**
     * 赋值EmailConnectionDTO
     * @param emailConnectionDto
     * @return EmailConnectionDTO
     */
    EmailConnectionDTO assignmentEmailServerDTO(EmailConnectionDTO emailConnectionDto);
    
    
    /**
     * 连接测试，包含普通邮件及Exchange邮件的测试方法；
     * @param emailConnectionDto
     * @return
     */
    int testEmailConn(EmailConnectionDTO emailConnDto,EmailServerDTO emailServerDTO);
    boolean testEmail(EmailServerDTO emailServerDTO,boolean isSend);
}
