package com.wstuo.common.noticeRule.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * 用户联系信息
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class UserContactInfoDTO extends BaseDTO{
	private String loginName;
	private String email;
	private String mobile;
	private String userName;
	private String remark;
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
}
