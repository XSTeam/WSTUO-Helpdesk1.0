package com.wstuo.common.scheduledTask.dao;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.scheduledTask.dto.ScheduledTaskQueryDTO;
import com.wstuo.common.scheduledTask.entity.ScheduledTask;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.StringUtils;

/**
 * 定期任务DAO类
 * @author WSTUO
 *
 */
public class ScheduledTaskDAO extends BaseDAOImplHibernate<ScheduledTask> implements IScheduledTaskDAO {
	/**
	 * 分页查询
	 * @param queryDTO
	 * @return PageDTO
	 */ 
	public PageDTO findPageScheduledTask(ScheduledTaskQueryDTO queryDTO){
		final DetachedCriteria dc = DetachedCriteria.forClass( ScheduledTask.class );
        int start = 0;
        int limit = 0;
        if(queryDTO!=null){
        	start=queryDTO.getStart();
        	limit=queryDTO.getLimit();
        	//dj.add(Restrictions.eq("scheduledTaskId", queryDTO.getScheduledTaskId()));
        	if(queryDTO.getKeywork()!=null)
        		dc.add(Restrictions.like("requestDtoJson", queryDTO.getKeywork(), MatchMode.ANYWHERE));
        	if(queryDTO.getCompanyNo()!=null)
        		dc.add(Restrictions.eq("companyNo", queryDTO.getCompanyNo()));
            if(StringUtils.hasText(queryDTO.getScheduledTaskType()))
            	dc.add(Restrictions.eq("scheduledTaskType",queryDTO.getScheduledTaskType()));
            if(StringUtils.hasText(queryDTO.getTimeType()))
            	dc.add(Restrictions.eq("timeType",queryDTO.getTimeType()));
        	
           //创建时间搜索
            if (queryDTO.getStartCreateTime() != null && queryDTO.getEndCreateTime()==null) {
                dc.add(Restrictions.ge("createTime",queryDTO.getStartCreateTime()));
            }
            if (queryDTO.getStartCreateTime() == null && queryDTO.getEndCreateTime()!=null) {
            	Calendar endTimeCl=new GregorianCalendar();
            	endTimeCl.setTime(queryDTO.getEndCreateTime());
            	endTimeCl.set(Calendar.DATE, endTimeCl.get(Calendar.DATE)+1);
                dc.add(Restrictions.le("createTime",endTimeCl.getTime()));
            }
            if (queryDTO.getStartCreateTime() != null && queryDTO.getEndCreateTime()!=null) {
            	Calendar endTimeCl=new GregorianCalendar();
            	endTimeCl.setTime(queryDTO.getEndCreateTime());
            	endTimeCl.set(Calendar.DATE, endTimeCl.get(Calendar.DATE)+1);
  
                dc.add(Restrictions.and(Restrictions.le("createTime",endTimeCl.getTime()),Restrictions.ge("createTime", queryDTO.getStartCreateTime())));
            }
            
            //更新时间搜索
            if (queryDTO.getStartUpdateTime() != null && queryDTO.getEndUpdateTime()==null) {
                dc.add(Restrictions.ge("lastUpdateTime",queryDTO.getStartUpdateTime()));
            }
            if (queryDTO.getStartUpdateTime() == null && queryDTO.getEndUpdateTime()!=null) {
            	Calendar endTimeCl=new GregorianCalendar();
            	endTimeCl.setTime(queryDTO.getEndUpdateTime());
            	endTimeCl.set(Calendar.DATE, endTimeCl.get(Calendar.DATE)+1);
                dc.add(Restrictions.le("lastUpdateTime",endTimeCl.getTime()));
            }
            if (queryDTO.getStartUpdateTime() != null && queryDTO.getEndUpdateTime()!=null) {
            	Calendar endTimeCl=new GregorianCalendar();
            	endTimeCl.setTime(queryDTO.getEndUpdateTime());
            	endTimeCl.set(Calendar.DATE, endTimeCl.get(Calendar.DATE)+1);
  
                dc.add(Restrictions.and(Restrictions.le("lastUpdateTime",endTimeCl.getTime()),Restrictions.ge("lastUpdateTime", queryDTO.getStartUpdateTime())));
            }
            
            
            //开始、结束时间搜索
            if (queryDTO.getTaskDate() != null && queryDTO.getTaskEndDate()==null) {
                dc.add(Restrictions.ge("taskDate", queryDTO.getTaskDate()));
            }
            if (queryDTO.getTaskDate() == null && queryDTO.getTaskEndDate()!=null) {

            	Calendar endTimeCl=new GregorianCalendar();
            	endTimeCl.setTime(queryDTO.getTaskEndDate());
            	endTimeCl.set(Calendar.DATE, endTimeCl.get(Calendar.DATE)+1);
  
                dc.add(Restrictions.le("taskEndDate",endTimeCl.getTime()));
            }
            
            if (queryDTO.getTaskDate() != null && queryDTO.getTaskEndDate()!=null) {

            	
            	Calendar endTimeCl=new GregorianCalendar();
            	endTimeCl.setTime(queryDTO.getTaskEndDate());
            	endTimeCl.set(Calendar.DATE, endTimeCl.get(Calendar.DATE)+1);
  
                dc.add(Restrictions.and(Restrictions.le("taskDate",endTimeCl.getTime()),Restrictions.ge("taskEndDate", queryDTO.getTaskDate())));
            }
        }
        //排除报表中的定时任务
        dc.add(Restrictions.not(Restrictions.eq("beanId", "autoSendReportByReportJob")));
        //排序
        if(queryDTO!=null && StringUtils.hasText(queryDTO.getSord())&&StringUtils.hasText(queryDTO.getSidx())){
            if(queryDTO.getSord().equals("desc"))
            	dc.addOrder(Order.desc(queryDTO.getSidx()));
            else
            	dc.addOrder(Order.asc(queryDTO.getSidx()));
        }
        return super.findPageByCriteria(dc, start,limit);
	}
	
	/**
	 * 查询关联表单的数量
	 * @param formId
	 * @return
	 */
	public Integer findScheduledTaskNumByFormId(Long[] formIds){
		Integer num = 0;
		DetachedCriteria dc = DetachedCriteria.forClass(ScheduledTask.class);
		if(formIds!=null && formIds.length >0 ){
			dc.add(Restrictions.in("formId", formIds));
		}
		List list = super.getHibernateTemplate().findByCriteria(dc);
		if(list!=null){
			num = list.size();
		}
		return num;
	}
}
