package com.wstuo.common.tools.action;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;


import com.wstuo.common.tools.dto.InstantMessageDTO;
import com.wstuo.common.tools.dto.InstantMessageQueryDTO;
import com.wstuo.common.tools.service.IInstantMessageService;
import com.wstuo.common.dto.PageDTO;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * InstantMessage Action class
 * 
 * @author Jet date 2010-10-7
 * @version 0.1
 */
@SuppressWarnings("serial")
public class InstantMessageAction extends ActionSupport {

	/**
	 * back stage service interface
	 */
	@Autowired
	private IInstantMessageService instantmessageService;

	/**
	 * add del update DTO
	 */
	private InstantMessageDTO imDTO;

	/**
	 * back stage find DTO
	 */
	private List<InstantMessageDTO> listIM;

	/**
	 * back stage pagination
	 */
	private PageDTO pageDTO;

	/**
	 * back stage QueryDTO
	 */
	private InstantMessageQueryDTO manageQueryDTO;

	/**
	 * delete id
	 */
	private Long[] ids;

	private String[] receiveUsers;
	/**
	 * come into vogue
	 */
	private int page = 1;

	/**
	 * total
	 */
	private int rows = 10;
	private int unreadCount = 0;

	private String sidx;
	private String sord;

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public int getUnreadCount() {
		return unreadCount;
	}

	public void setUnreadCount(int unreadCount) {
		this.unreadCount = unreadCount;
	}

	public PageDTO getPageDTO() {
		return pageDTO;
	}

	public void setPageDTO(PageDTO pageDTO) {
		this.pageDTO = pageDTO;
	}

	public List<InstantMessageDTO> getListIM() {
		return listIM;
	}

	public void setListIM(List<InstantMessageDTO> listIM) {
		this.listIM = listIM;
	}

	public InstantMessageDTO getImDTO() {
		return imDTO;
	}

	public void setImDTO(InstantMessageDTO imDTO) {
		this.imDTO = imDTO;
	}

	public Long[] getIds() {
		return ids;
	}

	public void setIds(Long[] ids) {
		this.ids = ids;
	}

	public InstantMessageQueryDTO getManageQueryDTO() {
		return manageQueryDTO;
	}

	public void setManageQueryDTO(InstantMessageQueryDTO manageQueryDTO) {
		this.manageQueryDTO = manageQueryDTO;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public String[] getReceiveUsers() {
		return receiveUsers;
	}

	public void setReceiveUsers(String[] receiveUsers) {
		this.receiveUsers = receiveUsers;
	}

	/**
	 * this is save data method
	 * 
	 * @return String
	 */
	public String save() {
		return instantmessageService.saveInstantMessage(receiveUsers, imDTO);
	}

	/**
	 * this is update method
	 * 
	 * @return String
	 */
	public String update() {
		instantmessageService.updateInstantMessage(imDTO);
		return Action.SUCCESS;
	}

	/**
	 * this is del data method
	 * 
	 * @return String
	 */
	public String delete() {
		instantmessageService.removeInstantMessage(ids);
		return Action.SUCCESS;
	}

	/**
	 * this is Paginating method
	 * 
	 * @return String
	 */
	public String findPager() {
		int start = (page - 1) * rows;
		pageDTO = instantmessageService.findPagerIM(manageQueryDTO, start, rows, sidx, sord);
		pageDTO.setPage(page);
		pageDTO.setRows(rows);
		return Action.SUCCESS;
	}

	/**
	 * findUnread
	 * 
	 * @return String
	 */
	public String findUnread() {
		this.unreadCount = instantmessageService.findUnreadIM();
		return "unreadCount";
	}

	/**
	 * 回复
	 * 
	 * @return String
	 */
	public String reply() {
		instantmessageService.updateInstantMessage(ids, receiveUsers, imDTO);
		return Action.SUCCESS;
	}

	/**
	 * 根据ID查IM消息
	 * 
	 * @return imDTO
	 */
	public String findInstantmessageById() {

		imDTO = instantmessageService.findInstantMessageDTOById(imDTO.getImId());
		return "imDTO";

	}
    public String fundIMList(){
		int start = ( page - 1 ) * rows;
		pageDTO = instantmessageService.findPagerIM( manageQueryDTO, start, rows,sidx,sord );
		pageDTO.setPage( page );
		pageDTO.setRows( rows );
		return "pageDTO";
   }
}
