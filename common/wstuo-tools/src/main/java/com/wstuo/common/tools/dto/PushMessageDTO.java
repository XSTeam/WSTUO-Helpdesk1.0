package com.wstuo.common.tools.dto;
import java.util.Date;

import com.wstuo.common.security.entity.User;
import com.wstuo.common.security.entity.UserInfoClient;

/**
 * 消息推送DTO
 * @author gs60
 * 此DTO的属性名不能随意修改，Android端的通知消息也是使用的这个DTO；
 */
public class PushMessageDTO {

	
	private Date pushDate = new Date();
	private String timeStamp;
	private String title;
	private String content;
	private String logo;
	private String accept;
	private Integer count;
	private String eno;
	private Boolean state = true;
	public String getTitle() {
		return title;
	}
	public String getTitle(String title) {
		if (this.title == null) {
			setTitle(title);
		}
		return getTitle();
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	
	public String getAccept() {
		return accept;
	}
	public void setAccept(String accept) {
		this.accept = accept;
	}
	public PushMessageDTO() {
		super();
	}
	
	@Override
	public String toString() {
		return "PushMessageDTO  timeStamp="
				+ timeStamp + ", title=" + title + ", content=" + content
				+ ", logo=" + logo + ", accept=" + accept + ", count=" + count
				+ ", eno=" + eno + ", state=" + state + "]";
	}
	/**
	 * 去掉不用推送到客户端的字段；
	 */
	public PushMessageDTO messageContent(String defaultTitle) {
		PushMessageDTO dto = new PushMessageDTO();
		dto.setTitle( this.getTitle(defaultTitle) );
		dto.setContent( this.getContent() );
		dto.setEno( this.getEno() );
		dto.setPushDate( this.getPushDate() );
		dto.setTimeStamp( this.getPushDate().getTime()+"" );
		dto.setPushDate( null );
		return dto;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public String getEno() {
		return eno;
	}
	public void setEno(String eno) {
		this.eno = eno;
	}
	public Date getPushDate() {
		return pushDate;
	}
	public void setPushDate(Date pushDate) {
		this.pushDate = pushDate;
	}
	public Boolean getState() {
		return state;
	}
	public void setState(Boolean state) {
		this.state = state;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	public static PushMessageDTO user2PushMessageDTO(User user){
		PushMessageDTO dto = null;
		UserInfoClient client = null;
		if (user != null && ( client = user.getUserInfoClient() ) != null) {
			dto = new PushMessageDTO();
			dto.setAccept( client.getMoblieClientId());
			dto.setCount( client.getMessageCount() );
		}
		return dto;
	}
	
}
