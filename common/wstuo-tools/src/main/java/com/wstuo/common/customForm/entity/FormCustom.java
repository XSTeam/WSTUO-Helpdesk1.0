package com.wstuo.common.customForm.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

import com.wstuo.common.entity.BaseEntity;

/**
 * 自定义表单模块
 * 
 * @author Wstuo
 * 
 */
@Entity
public class FormCustom extends BaseEntity{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long formCustomId;
	private String formCustomName;
	@Lob
	private String formCustomContents;
	private int isTemplate;
	private String isShowBorder;
	private Boolean isDefault = false;//是否默认
	private String type;//模块类型 request ci
	private Long ciCategoryNo;
	@OneToMany
	private List<FormCustomTabs> formCustomTabs;
	
	public List<FormCustomTabs> getFormCustomTabs() {
		return formCustomTabs;
	}

	public void setFormCustomTabs(List<FormCustomTabs> formCustomTabs) {
		this.formCustomTabs = formCustomTabs;
	}

	public Long getCiCategoryNo() {
		return ciCategoryNo;
	}

	public void setCiCategoryNo(Long ciCategoryNo) {
		this.ciCategoryNo = ciCategoryNo;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public Long getFormCustomId() {
		return formCustomId;
	}

	public void setFormCustomId(Long formCustomId) {
		this.formCustomId = formCustomId;
	}

	public String getFormCustomName() {
		return formCustomName;
	}

	public void setFormCustomName(String formCustomName) {
		this.formCustomName = formCustomName;
	}

	public String getFormCustomContents() {
		return formCustomContents;
	}

	public void setFormCustomContents(String formCustomContents) {
		this.formCustomContents = formCustomContents;
	}

	public int getIsTemplate() {
		return isTemplate;
	}

	public void setIsTemplate(int isTemplate) {
		this.isTemplate = isTemplate;
	}

	public String getIsShowBorder() {
		return isShowBorder;
	}

	public void setIsShowBorder(String isShowBorder) {
		this.isShowBorder = isShowBorder;
	}

}
