package com.wstuo.common.customForm.dao;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

import com.wstuo.common.customForm.entity.Field;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.DaoUtils;
import com.wstuo.common.util.StringUtils;

/**
 * 自定义字段DAO类
 * @author WSTUO
 *
 */
public class FieldDAO extends BaseDAOImplHibernate<Field> implements IFieldDAO{

	public int addField(String table, String name,String type) {
		String hql="alter table cu_"+table+" add "+name+" VARCHAR(255)";
		return getHibernateTemplate().excuteSQL(hql);
	}

	public int delField(String table, String name) {
		String hql="alter table cu_"+table+" drop COLUMN "+name;
		return getHibernateTemplate().excuteSQL(hql);
	}

	public PageDTO findByCustom(String table,int start,int limit,String sidx,String  sord) {
		String hql="select * from cu_"+table+"";
		SQLQuery query = super.getSession().createSQLQuery(hql);  
		List<Field> list=super.findBy("module", table);
        if (list != null) {
           for (Field field : list) {
	            //query.setParameter(i, values[i]);
	            query.addScalar(field.getName());
           } 
        }
        query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        query.setFirstResult(start);
        query.setMaxResults(limit);
        PageDTO pageDTO = new PageDTO();
        pageDTO.setTotalSize(findCount(table));
		pageDTO.setData(query.list());
        return pageDTO;
	}
	
	public int findCount(String table){
		String hql = "select count(*) from cu_"+table;  
		SQLQuery query = super.getSession().createSQLQuery(hql);  
		return ((Integer)query.uniqueResult()).intValue();
	}

	public PageDTO findPageField(Field field,int start,int limit,String sidx,String  sord) {
		DetachedCriteria dc = DetachedCriteria.forClass( Field.class );
        if(field!=null){
        	if(StringUtils.hasText(field.getFieldName()))
        		dc.add(Restrictions.like("fieldName", field.getFieldName(), MatchMode.ANYWHERE));
        	if(StringUtils.hasText(field.getModule()))
        		dc.add(Restrictions.eq("module", field.getModule()));
            if(StringUtils.hasText(field.getType()))
            	dc.add(Restrictions.eq("type",field.getType()));
        }
        // 排序
     	dc = DaoUtils.orderBy(sidx, sord, dc);
        return super.findPageByCriteria(dc, start,limit);
	}
	
	public List<Field> findListField(Field field ,Boolean export) {
		DetachedCriteria dc = DetachedCriteria.forClass( Field.class );
        if(field!=null){
        	if(StringUtils.hasText(field.getFieldName()))
        		dc.add(Restrictions.like("fieldName", field.getFieldName(), MatchMode.ANYWHERE));
        	if(StringUtils.hasText(field.getModule()))
        		dc.add(Restrictions.eq("module", field.getModule()));
            if(StringUtils.hasText(field.getType()))
            	dc.add(Restrictions.eq("type",field.getType()));
            
            if(export!=null)
            	dc.add(Restrictions.eq("export",export));
        }
        // 排序
        return  super.getHibernateTemplate().findByCriteria(dc);
	}
}
