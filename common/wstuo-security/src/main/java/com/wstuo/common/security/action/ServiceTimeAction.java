package com.wstuo.common.security.action;

import com.opensymphony.xwork2.ActionSupport;

import com.wstuo.common.security.dto.ServiceTimeDTO;
import com.wstuo.common.security.service.IServiceTimeService;

/**
 * 服务时间Action
 * 
 * @author QXY
 * 
 */
@SuppressWarnings("serial")
public class ServiceTimeAction extends ActionSupport {
	private IServiceTimeService serviceTimeService;
	private ServiceTimeDTO serviceTimeDTO = new ServiceTimeDTO();
	private Integer effect = 1;
	public IServiceTimeService getServiceTimeService() {
		return serviceTimeService;
	}

	public void setServiceTimeService(IServiceTimeService serviceTimeService) {
		this.serviceTimeService = serviceTimeService;
	}

	public ServiceTimeDTO getServiceTimeDTO() {
		return serviceTimeDTO;
	}

	public void setServiceTimeDTO(ServiceTimeDTO serviceTimeDTO) {
		this.serviceTimeDTO = serviceTimeDTO;
	}

	public Integer getEffect() {
		return effect;
	}

	public void setEffect(Integer effect) {
		this.effect = effect;
	}

	/**
	 * findByOrgNo
	 * 
	 * @return String
	 */
	public String findByOrgNo() {
		Long orgNo = serviceTimeDTO.getOrgNo();
		if (orgNo != null) {
			serviceTimeDTO = serviceTimeService.findServiceTimeByOrgNo(orgNo);
			return "singleServiceTime";
		} else {
			effect = -1;
			return SUCCESS;
		}
	}

	/**
	 * saveOrgUpdate
	 * 
	 * @return String
	 */
	public String saveOrgUpdate() {
		serviceTimeService.saveOrUpdateServiceTime(serviceTimeDTO);
		return SUCCESS;
	}
}
