package com.wstuo.common.acl.dto;

import java.util.Map;
import java.util.Set;

/**
 * Entries DTO CLASS
 * @author Administrator
 */
public class EntriesDTO {
	private Set<String> entries_sid;
	private Map<String,Set<Integer>> entries_permission_mask;
	private Map<String,String> entries_sid_name;
	public Set<String> getEntries_sid() {
		return entries_sid;
	}
	public void setEntries_sid(Set<String> entries_sid) {
		this.entries_sid = entries_sid;
	}
	public Map<String, Set<Integer>> getEntries_permission_mask() {
		return entries_permission_mask;
	}
	public void setEntries_permission_mask(
			Map<String, Set<Integer>> entries_permission_mask) {
		this.entries_permission_mask = entries_permission_mask;
	}
	public Map<String, String> getEntries_sid_name() {
		return entries_sid_name;
	}
	public void setEntries_sid_name(Map<String, String> entries_sid_name) {
		this.entries_sid_name = entries_sid_name;
	}
	
}
