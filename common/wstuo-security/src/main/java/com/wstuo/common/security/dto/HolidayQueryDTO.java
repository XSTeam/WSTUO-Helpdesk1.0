package com.wstuo.common.security.dto;

import java.util.Date;

import com.wstuo.common.dto.AbstractValueObject;

/**
 * class HolidayQueryDTO
 * 
 * @author will
 * 
 */
public class HolidayQueryDTO extends AbstractValueObject {
	private Long orgNo;
	private Integer start;
	private Integer limit;
	private Date hdate;
	private Date startTime;
	private Date endTime;
	private Long companyNo;

	public Long getCompanyNo() {
		return companyNo;
	}

	public void setCompanyNo(Long companyNo) {
		this.companyNo = companyNo;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Date getHdate() {
		return hdate;
	}

	public void setHdate(Date hdate) {
		this.hdate = hdate;
	}

	public HolidayQueryDTO() {
		super();
	}

	public Long getOrgNo() {
		return orgNo;
	}

	public void setOrgNo(Long orgNo) {
		this.orgNo = orgNo;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	
}
