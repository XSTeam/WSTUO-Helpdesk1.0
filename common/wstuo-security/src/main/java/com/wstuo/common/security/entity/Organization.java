package com.wstuo.common.security.entity;

import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.wstuo.common.entity.BaseEntity;



/**
 * Entity class Organization.
 */
@SuppressWarnings( {"serial",
    "rawtypes"
} )
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Inheritance( strategy = InheritanceType.JOINED )
public class Organization
    extends BaseEntity
{
	@Id
    private Long orgNo;
    private String orgName;
    private String address;
    private String email;
    private String officePhone;
    private String officeFax;
    @OneToMany
    @JoinColumn( name = "orgNo" )
    private Set<User> users;
    @ManyToOne
    @JoinColumn( name = "parentOrgNo" )
    private Organization parentOrg;
    @OneToMany( mappedBy = "parentOrg", cascade = CascadeType.REMOVE )
    private Set<Organization> suborgs;
    @ManyToOne
    @JoinColumn( name = "personInChargeNo" )
    private User personInCharge;
    @ManyToMany(fetch=FetchType.EAGER)
    private Set<Role> orgRoles;
    @OneToOne(mappedBy="organization",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
    private ServiceTime serviceTime;
    private String orgType;
    private String path;//路径 
    private String pathAlias;//路径
    public String getOrgType() {
		return orgType;
	}

	public void setOrgType(String orgType) {
		this.orgType = orgType;
	}

	public User getPersonInCharge(  )
    {
        return personInCharge;
    }

    public void setPersonInCharge( User personInCharge )
    {
        this.personInCharge = personInCharge;
    }

    /**
    * get method for orgNo.
    * @return orgNo
    */
    public Long getOrgNo(  )
    {
        return orgNo;
    }

    /**
     * set method for orgNo.
     * @param orgNo Long
     */
    public void setOrgNo( Long orgNo )
    {
        this.orgNo = orgNo;
    }

    /**
     * get method for orgName.
     * @return orgName
     */
    public String getOrgName(  )
    {
        return orgName;
    }

    /**
     * set method for orgName.
     * @param orgName String
     */
    public void setOrgName( String orgName )
    {
        this.orgName = orgName;
    }

    /**
     * get method for address.
     * @return address
     */
    public String getAddress(  )
    {
        return address;
    }

    /**
     * set method for address.
     * @param address String
     */
    public void setAddress( String address )
    {
        this.address = address;
    }

    /**
     * get method for email.
     * @return email
     */
    public String getEmail(  )
    {
        return email;
    }

    /**
     * set method for email.
     * @param email String
     */
    public void setEmail( String email )
    {
        this.email = email;
    }

    /**
     * get method for officePhone.
     * @return officePhone
     */
    public String getOfficePhone(  )
    {
        return officePhone;
    }

    /**
     * set method for officePhone.
     * @param officePhone String
     */
    public void setOfficePhone( String officePhone )
    {
        this.officePhone = officePhone;
    }

    /**
     * get method for getOfficeFax.
     * @return getOfficeFax
     */
    public String getOfficeFax(  )
    {
        return officeFax;
    }

    /**
     * set method for officeFax.
     * @param officeFax String
     */
    public void setOfficeFax( String officeFax )
    {
        this.officeFax = officeFax;
    }

    /**
     * get method for users.
     * @return users
     */
    public Set<User> getUsers(  )
    {
        return users;
    }

    /**
     * get method for users.
     * @param users Set User
     */
    public void setUsers( Set<User> users )
    {
        this.users = users;
    }

    public Organization getParentOrg(  )
    {
        return parentOrg;
    }

    public void setParentOrg( Organization parentOrg )
    {
        this.parentOrg = parentOrg;
    }

    public Set<Organization> getSuborgs(  )
    {
        return suborgs;
    }

    public void setSuborgs( Set<Organization> suborgs )
    {
        this.suborgs = suborgs;
    }

    public Set<Role> getOrgRoles(  ){
    	
        return orgRoles;
    }

    public void setOrgRoles( Set<Role> orgRoles )
    {
        this.orgRoles = orgRoles;
    }
    
    public ServiceTime getServiceTime(  )
    {
        return serviceTime;
    }

    public void setServiceTime( ServiceTime serviceTime )
    {
        this.serviceTime = serviceTime;
    }

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getPathAlias() {
		return pathAlias;
	}

	public void setPathAlias(String pathAlias) {
		this.pathAlias = pathAlias;
	}
}
