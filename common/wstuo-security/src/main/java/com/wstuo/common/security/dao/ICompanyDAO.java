package com.wstuo.common.security.dao;

import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.security.entity.Company;

import java.util.List;

/**
 * 公司信息DAO接口.
 * @author will
 */
public interface ICompanyDAO
    extends IEntityDAO<Company>
{
    /**
     * 查找顶级公司信息.
     * @return 公司集合List<Company>
     */
    List<Company> findATree(  );

    /**
     * 查找公司信息.
     * @return 公司信息 Company
     */
    Company findCompany(Long companyNo);
    
    Company merge(Company entity);
}
