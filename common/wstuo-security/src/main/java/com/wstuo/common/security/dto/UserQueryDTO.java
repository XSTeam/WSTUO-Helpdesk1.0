package com.wstuo.common.security.dto;

import java.io.File;

import com.wstuo.common.dto.BaseDTO;

/**
 * User Query DTO
 * 
 */
@SuppressWarnings("serial")
public class UserQueryDTO extends BaseDTO {
	private String loginName;
	private String lastName;
	private String firstName;
	private Long orgNo;
	private String email;
	private String mobilePhone;
	private String officePhone;
	private String description;
	private String remark;
	private Integer start;
	private Integer limit = 10;
	private Long[] orgNos;
	private Long roleNo;
	private String roleCode;
	private File importFile;
	private String orgType;
	private String fullName;
	private String technicalGroupName;
	private Long technicalGroupId;
	private String belongsGroup;
	private Long[] belongsGroupIds;
	private Boolean userState;// 状态

	private Boolean attendance;// 值班状态
	private String[] loginNames;// 值班状态

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Boolean getUserState() {
		return userState;
	}

	public void setUserState(Boolean userState) {
		this.userState = userState;
	}

	public Long getTechnicalGroupId() {
		return technicalGroupId;
	}

	public void setTechnicalGroupId(Long technicalGroupId) {
		this.technicalGroupId = technicalGroupId;
	}

	public String getTechnicalGroupName() {
		return technicalGroupName;
	}

	public void setTechnicalGroupName(String technicalGroupName) {
		this.technicalGroupName = technicalGroupName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Long getOrgNo() {
		return orgNo;
	}

	public void setOrgNo(Long orgNo) {
		this.orgNo = orgNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getOfficePhone() {
		return officePhone;
	}

	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long[] getOrgNos() {
		return orgNos;
	}

	public void setOrgNos(Long[] orgNos) {
		this.orgNos = orgNos;
	}

	public Long getRoleNo() {
		return roleNo;
	}

	public void setRoleNo(Long roleNo) {
		this.roleNo = roleNo;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public File getImportFile() {
		return importFile;
	}

	public void setImportFile(File importFile) {
		this.importFile = importFile;
	}

	public String getOrgType() {
		return orgType;
	}

	public void setOrgType(String orgType) {
		this.orgType = orgType;
	}

	public String getBelongsGroup() {
		return belongsGroup;
	}

	public void setBelongsGroup(String belongsGroup) {
		this.belongsGroup = belongsGroup;
	}

	public Long[] getBelongsGroupIds() {
		return belongsGroupIds;
	}

	public void setBelongsGroupIds(Long[] belongsGroupIds) {
		this.belongsGroupIds = belongsGroupIds;
	}

	public Boolean getAttendance() {
		return attendance;
	}

	public void setAttendance(Boolean attendance) {
		this.attendance = attendance;
	}

	public String[] getLoginNames() {
		return loginNames;
	}

	public void setLoginNames(String[] loginNames) {
		this.loginNames = loginNames;
	}
}
