package com.wstuo.common.acl.dao;

import java.util.List;
import java.util.Set;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.acl.entity.ACL_Entry;
import com.wstuo.common.dao.BaseDAOImplHibernate;

/**
 * ACL DAO 类
 * @author Administrator
 *
 */
public class AclsDAO extends BaseDAOImplHibernate<ACL_Entry> implements IAclsDAO{
	/**
	 * 查询资源权限
	 * @param sid 角色
	 * @param aclClass 类
	 * @param permissions 操作权限
	 * @param object_id_identity 资源ID
	 * @return all acl entry
	 */
	public List<ACL_Entry> findAcl(Set<String> sid, String aclClass,
			Integer[] permissions, Long[] object_id_identity) {
		// TODO Auto-generated method stub
		final DetachedCriteria dc = DetachedCriteria.forClass(ACL_Entry.class);
		dc.createAlias("sid", "acl_sid");
		if(sid!=null && sid.size()>0){
			dc.add(Restrictions.in("acl_sid.sid", sid));
		}
		dc.createAlias("acl_object_identity", "acl_object_identity");
		
		if(object_id_identity!=null && object_id_identity.length>0){
			dc.add(Restrictions.in("acl_object_identity.object_id_identity", object_id_identity));
		}
		dc.createAlias("acl_object_identity.object_id_class", "object_id_class");
		dc.add(Restrictions.eq("object_id_class.aclclass", aclClass));
		
		//要查询的权限
		if(permissions!=null && permissions.length>0){
			dc.add(Restrictions.in("mask", permissions));
		}
		return super.getHibernateTemplate().findByCriteria(dc);
	}


}
