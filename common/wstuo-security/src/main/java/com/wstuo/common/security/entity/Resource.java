package com.wstuo.common.security.entity;

import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.wstuo.common.entity.BaseEntity;

/**
 * Entity class Resource.
 * 
 */
@SuppressWarnings({"serial", "rawtypes"})
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Table(name = "o_resource")
@Inheritance(strategy = InheritanceType.JOINED)
public class Resource extends BaseEntity {
	@Id
	private Long resNo;
	@Column(unique = true)
	private String resCode;
	private String resName;
	private String resIcon;
	private Integer sequence = 0;
	private String resUrl;
	@ManyToMany(mappedBy = "resources",fetch = FetchType.LAZY)
	private Set<Role> roles;
	@Transient
	private String[] roleCodes;

	public Long getResNo() {
		return resNo;
	}

	public void setResNo(Long resNo) {
		this.resNo = resNo;
	}
	
	@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
	public Set<Role> getRoles() {
		return roles;
	}
	
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	public String getResCode() {
		return resCode;
	}

	public void setResCode(String resCode) {
		this.resCode = resCode;
	}

	public String getResName() {
		return resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	public String getResIcon() {
		return resIcon;
	}

	public void setResIcon(String resIcon) {
		this.resIcon = resIcon;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public String getResUrl() {
		return resUrl;
	}

	public void setResUrl(String resUrl) {
		this.resUrl = resUrl;
	}
	
	public String[] getRoleCodes(){
		/*if (roleCodes == null) {
			roleCodes = new String[roles.size()];
			int i=0;
			for(Role role : roles){
				roleCodes[i++] = role.getRoleCode();
			} 
		}*/
		return roleCodes;
	}
	
}
