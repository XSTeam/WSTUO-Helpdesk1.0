package com.wstuo.common.security.service;

import com.wstuo.common.security.dto.OrganizationDTO;
import com.wstuo.common.security.entity.OrganizationInner;

/**
 * 内部机构业务类接口
 *
 */
public interface IOrganizationInnerService
{
    /**
     * 保存内部机构.
     * @param dto 机构DTO：OrganizationDTO
     */
    void saveOrganizationInner( OrganizationDTO dto );

    /**
     * 修改内部机构.
     * @param dto 机构DTO：OrganizationDTO
     */
    void mergeOrganizationInner( OrganizationDTO dto );

    /**
     * 根据删除机构.
     * @param orgNo 机构编号：Long orgNo
     */
    void deleteOrganizationInner( Long orgNo );

    /**
     * 根据编号查找机构.
     * @param id 机构编号：Long id
     * @return OrganizationInner
     */
    OrganizationInner findOrganizationInnerById( Long id );
    
    /**
     * 根据编号查找机构DTO.
     * @param id 机构编号：Long id
     * @return OrganizationDTO
     */
    OrganizationDTO findById(Long id);
}
