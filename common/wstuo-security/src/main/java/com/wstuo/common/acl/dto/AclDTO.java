package com.wstuo.common.acl.dto;

/**
 * ACL DTO CLASS
 * @author Administrator
 *
 */
public class AclDTO {
	private String owner_principal;//创建账号
	private String owner_principalName;//创建账号姓名
	private Long objectIdentity_identifier;//资源标识
	private String objectIdentity_type;//资源类型
	private EntriesDTO entries;
	
	
	public String getOwner_principal() {
		return owner_principal;
	}
	public void setOwner_principal(String owner_principal) {
		this.owner_principal = owner_principal;
	}
	public String getOwner_principalName() {
		return owner_principalName;
	}
	public void setOwner_principalName(String owner_principalName) {
		this.owner_principalName = owner_principalName;
	}
	public Long getObjectIdentity_identifier() {
		return objectIdentity_identifier;
	}
	public void setObjectIdentity_identifier(Long objectIdentity_identifier) {
		this.objectIdentity_identifier = objectIdentity_identifier;
	}
	public String getObjectIdentity_type() {
		return objectIdentity_type;
	}
	public void setObjectIdentity_type(String objectIdentity_type) {
		this.objectIdentity_type = objectIdentity_type;
	}
	public EntriesDTO getEntries() {
		return entries;
	}
	public void setEntries(EntriesDTO entries) {
		this.entries = entries;
	}
	
	
}
