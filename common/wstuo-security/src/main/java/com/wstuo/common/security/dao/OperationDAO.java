package com.wstuo.common.security.dao;

import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dto.OperationQueryDTO;
import com.wstuo.common.security.entity.Operation;
import com.wstuo.common.util.StringUtils;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 操作DAO.
 * 
 * @author will
 */
@SuppressWarnings("unchecked")
public class OperationDAO extends BaseDAOImplHibernate<Operation> implements IOperationDAO {

	@Autowired
	private IResourceDAO resourceDAO;

	/**
	 * 分页查找功能操作.
	 * 
	 * @param qdto
	 *            查询DTO OperationQueryDTO
	 * @return 分页数据 PageDTO
	 */
	public PageDTO findPager(OperationQueryDTO qdto) {
		final DetachedCriteria dc = DetachedCriteria.forClass(Operation.class);
		int start = 0;
		int limit = 10;

		if (qdto != null) {
			start = qdto.getStart();
			limit = qdto.getLimit();

			if (qdto.getFunctionNo() != null && qdto.getFunctionNo() != -1) {
				dc.createAlias("function", "func");
				dc.add(Restrictions.eq("func.resNo", qdto.getFunctionNo()));
			}

			if (qdto.getFunctionNo() == null && StringUtils.hasText(qdto.getFunctionResCode())) {
				dc.createAlias("function", "func");
				dc.add(Restrictions.eq("func.resCode", qdto.getFunctionResCode()));
			}

			if (StringUtils.hasText(qdto.getResName())) {
				dc.add(Restrictions.like("resName", qdto.getResName(), MatchMode.ANYWHERE));
			}

			if (StringUtils.hasText(qdto.getResCode())) {
				dc.add(Restrictions.like("resCode", qdto.getResCode(), MatchMode.ANYWHERE));
			}

			if (StringUtils.hasText(qdto.getResUrl())) {
				dc.add(Restrictions.like("resUrl", qdto.getResUrl(), MatchMode.ANYWHERE));
			}
			// 排序
			if (StringUtils.hasText(qdto.getSord()) && StringUtils.hasText(qdto.getSidx())) {
				if ("desc".equals(qdto.getSord())) {
					dc.addOrder(Order.desc(qdto.getSidx()));
				} else {
					dc.addOrder(Order.asc(qdto.getSidx()));
				}
			} else {
				dc.addOrder(Order.desc("resNo"));
			}
		}
		return super.findPageByCriteria(dc, start, limit,"resNo");
	}

	/**
	 * 验证是否存重复的操作.
	 * 
	 * @param qdto
	 *            查询DTO OperationQueryDTO
	 * @return boolean
	 */
	public Boolean findExist(OperationQueryDTO qdto) {
		boolean result = false;
		if (qdto.getResName() != null) {
			List<Object> obj = super.getHibernateTemplate().find(" from Operation op where op.resName='" + qdto.getResName() + "'");

			if (!obj.isEmpty()) {
				result = true;
			}
		} else if (qdto.getResCode() != null) {
			List<Object> obj = super.getHibernateTemplate().find(" from Operation op where op.resCode='" + qdto.getResCode() + "'");

			if (!obj.isEmpty()) {
				result = true;
			}
		} else if (qdto.getResUrl() != null) {
			List<Object> obj = super.getHibernateTemplate().find(" from Operation op where op.resUrl='" + qdto.getResUrl() + "'");

			if (!obj.isEmpty()) {
				result = true;
			}
		}

		return result;
	}

	/**
	 * 取得下一个编号.
	 * 
	 * @return long
	 */
	public Long getNextResNo() {

		Long v = 1L;
		final String hql = "select max(o.resNo) from Resource o";
		List list = getHibernateTemplate().find(hql);

		if (list != null && list.size() > 0) {
			Number n = (Number) list.get(0);
			if (n != null) {
				v = n.longValue();
			}
		}
		return v;
	}

	/**
	 * 重写SAVE方法.
	 * 
	 * @param entity
	 */
	@Override
	public void save(Operation entity) {

		if (entity.getResNo() == null || entity.getResNo() == 0) {
			entity.setResNo(resourceDAO.getLatesResNo());
		} else {

			if (entity.getResNo() > resourceDAO.getLatesResNo()) {

				resourceDAO.setLatesResNo(entity.getResNo());
			}

		}
		resourceDAO.increment();// 编号添加一个
		super.save(entity);

	}

	/**
	 * 重写merge方法
	 * 
	 * @param entity
	 * @return Operation
	 */
	@Override
	public Operation merge(Operation entity) {

		if (entity.getResNo() != null) {
			if (entity.getResNo() >= resourceDAO.getLatesResNo()) {
				resourceDAO.setLatesResNo(entity.getResNo() + 1);
			}
		} else {
			resourceDAO.increment();// 自增1.
		}

		return super.merge(entity);
	}

}
