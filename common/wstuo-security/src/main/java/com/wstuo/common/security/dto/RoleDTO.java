package com.wstuo.common.security.dto;


import com.wstuo.common.dto.BaseDTO;

import org.springframework.security.core.GrantedAuthority;


/**
 *
 * Role DTO
 */
@SuppressWarnings("serial")
public class RoleDTO extends BaseDTO implements GrantedAuthority {
    private Long roleId;
    private String authority;
    private String roleName;
    private String roleCode;
    private Boolean roleState = false;
    private String description;
    private String remark;

    public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public String getAuthority() {
		return authority;
	}
	public void setAuthority(String authority) {
		this.authority = authority;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	public Boolean getRoleState() {
		return roleState;
	}
	public void setRoleState(Boolean roleState) {
		this.roleState = roleState;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public RoleDTO() {
        super();
    }
	public RoleDTO(long roleId, String roleName, String roleCode,
			Boolean roleState, String description, String remark) {
		this.roleId = roleId;
		this.roleName = roleName;
		this.roleCode = roleCode;
		this.roleState = roleState;
		this.description = description;
		this.remark = remark;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authority == null) ? 0 : authority.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((remark == null) ? 0 : remark.hashCode());
		result = prime * result
				+ ((roleCode == null) ? 0 : roleCode.hashCode());
		result = prime * result + ((roleId == null) ? 0 : roleId.hashCode());
		result = prime * result
				+ ((roleName == null) ? 0 : roleName.hashCode());
		result = prime * result
				+ ((roleState == null) ? 0 : roleState.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RoleDTO other = (RoleDTO) obj;
		if (authority == null) {
			if (other.authority != null)
				return false;
		} else if (!authority.equals(other.authority))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (remark == null) {
			if (other.remark != null)
				return false;
		} else if (!remark.equals(other.remark))
			return false;
		if (roleCode == null) {
			if (other.roleCode != null)
				return false;
		} else if (!roleCode.equals(other.roleCode))
			return false;
		if (roleId == null) {
			if (other.roleId != null)
				return false;
		} else if (!roleId.equals(other.roleId))
			return false;
		if (roleName == null) {
			if (other.roleName != null)
				return false;
		} else if (!roleName.equals(other.roleName))
			return false;
		if (roleState == null) {
			if (other.roleState != null)
				return false;
		} else if (!roleState.equals(other.roleState))
			return false;
		return true;
	}
    
    
    


    
}