package com.wstuo.common.security.service;

import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dto.FunctionDTO;
import com.wstuo.common.security.dto.FunctionQueryDTO;
import com.wstuo.common.security.dto.FunctionTreeDTO;
import com.wstuo.common.security.entity.Function;

import java.io.InputStream;
import java.util.List;

/**
 * 功能业务类接口
 */
public interface IFunctionService
{
    /**
     * 分页查询功能.
     * @param functionQueryDTO 分页查询DTO：FunctionQueryDTO
     * @return 分页数据：PageDTO
     */
    PageDTO findPagerFunction( FunctionQueryDTO functionQueryDTO );

    /**
     * 查找全部功能
     * @return 功能集合 List<FunctionDTO>
     */
    List<FunctionDTO> findAllFunction(  );

    /**
     * 查找功能(树结构)
     * @return 树形集合：List<FunctionTreeDTO>
     */
    List<FunctionTreeDTO> findFunctionTreeDtos(String resCode);

    /**
     * 新增功能.
     * @param functionDto 功能DTO:FunctionDTO
     */
    void addFunction( FunctionDTO functionDto );
    
    /**
     * 添加默认操作
     * @param functionDto
     */
    void addDefaultOperations(FunctionDTO functionDto);
    /**
     * 修改功能.
     * @param functionDto 功能DTO：FunctionDTO
     */
    void upadteFunction( FunctionDTO functionDto );

    /**
     * 根据编号删除功能.
     * @param resNo 功能编号：Long resNo
     * @return boolean 值为true时删除成功，否则失败
     */
    boolean deleteFunction( Long resNo );

    /**
     * 批量删除功能.
     * @param resNos 功能编号数组：Long[] resNos
     * @return boolean 值为true时删除成功，否则失败
     */
    boolean deleteFunction( Long[] resNos );

    /**
     * 拷贝功能.
     * @param resNo 当前功能ID：Long resNo
     * @param parentFunctionNo 拷贝到的父节点编号：Long parentFunctionNo
     * @return FunctionDTO
     */
    FunctionDTO copyFunction( Long resNo, Long parentFunctionNo );

    /**
     * 剪切（更改父节点)
     * @param resNo 当前功能ID：Long resNo
     * @param parentFunctionNo 拷贝到的父节点编号：Long parentFunctionNo
     */
    void cutFunction( Long resNo, Long parentFunctionNo );

    /**
     * 验证功能是否已经存在.
     * @param qdto 功能查询DTO:FunctionQueryDTO
     * @return boolean true为存在，反之不存在
     */
    Boolean findFunctionExist( FunctionQueryDTO qdto );
    
    /**
     * 根据ID获取function
     * @param resNo
     * @return FunctionDTO
     */
    FunctionDTO findById( Long resNo );
    
    /**
     * 导出CSV
     * @param qdto
     * @return InputStream
     */
    InputStream exportFunction(FunctionQueryDTO qdto);
    
    /**
     * 导入CSV
     * @param filePath
     * @return String
     */
    String importFunction(String filePath);
    
    /**
     * 根据编号字符查找功能.
     * @param functionNoStr
     * @return Function
     */
    Function getFunctionByFunctionNoStr(String functionNoStr);
}
