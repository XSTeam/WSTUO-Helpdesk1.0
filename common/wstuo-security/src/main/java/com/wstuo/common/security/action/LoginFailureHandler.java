package com.wstuo.common.security.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户验证失败回调
 * @author Administrator
 *
 */
public class LoginFailureHandler implements AuthenticationFailureHandler {
	@Transactional
	public void onAuthenticationFailure(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException exception) throws IOException,
			ServletException{
		String url=request.getContextPath();
		String loginModel=request.getParameter("j_loginModel");
		if("EndUserWeb".equals(loginModel)){
			url=url+"/tickets/login.jsp?error=true";
		}else if("wap".equals(loginModel)){
			url=url+"/wap/login.jsp?error=true";
		}else{
			url =url + "/pages/login.jsp?error=true" ;
		}
		
		response.sendRedirect(url);
	}
}
