package com.wstuo.common.bpm.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.bpm.dao.IFlowActivityDAO;
import com.wstuo.common.bpm.entity.FlowActivity;
import com.wstuo.common.dao.BaseDAOImplHibernate;
/**
 * Flow Activity DAO Class
 * @author wst
 */
public class FlowActivityDAO extends BaseDAOImplHibernate<FlowActivity> implements IFlowActivityDAO {
	/**
	 * 根据DeploymentId、activityType、activityName查询流程活动
	 * @param processDefinitionId 流程布署ID
	 * @param activityType 流程活动类型
	 * @param activityName 流程活动名称
	 * @return FlowActivity
	 */
	public FlowActivity findFlowActivityByDeploymentId(String processDefinitionId,String[] activityType,String activityName){
		final DetachedCriteria dc = DetachedCriteria.forClass( FlowActivity.class );
		FlowActivity fActivity = null;
		dc.createAlias( "flowProperty", "fp" );
		dc.add( Restrictions.eq("fp.processDefinitionId",processDefinitionId));
		if(activityType!=null){
			dc.add( Restrictions.in("activityType",activityType));
		}
		dc.add( Restrictions.eq("activityName",activityName));
		List<FlowActivity> fas = super.getHibernateTemplate().findByCriteria(dc);
		if(fas!=null && fas.size()!=0)
			fActivity = fas.get(0);
		return fActivity;
	}
}
