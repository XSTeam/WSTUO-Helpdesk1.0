package com.wstuo.common.security.service;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dto.CompanyDTO;
import com.wstuo.common.security.dto.EditUserPasswordDTO;
import com.wstuo.common.security.dto.LDAPAuthenticationSettingDTO;
import com.wstuo.common.security.dto.UserDTO;
import com.wstuo.common.security.dto.UserDetailDTO;
import com.wstuo.common.security.dto.UserQueryDTO;
import com.wstuo.common.security.dto.UserRoleDTO;
import com.wstuo.common.security.entity.Role;
import com.wstuo.common.security.entity.User;
import com.wstuo.common.sso.dto.SSOUserDTO;

/**
 * 
 * 用户 Service Interface class
 */
public interface IUserInfoService {
	/**
	 * save user
	 * 
	 * @param userDto
	 */
	void saveUser(UserDTO userDto);

	/**
	 * search user
	 * 
	 * @param userQueryDto
	 * @param sord
	 * @param sidx
	 * @return PageDTO
	 */
	PageDTO findPagerUser(UserQueryDTO userQueryDto, String sord, String sidx);

	/**
	 * delete User
	 * 
	 * @param userId
	 */
	boolean delUser(Long[] userId);

	/**
	 * edit User
	 * 
	 * @param userDto
	 */
	void editUser(UserDTO userDto);

	/**
	 * find all user
	 * 
	 * @return List<User>
	 */
	List<User> findAllUser();

	/**
	 * findById(int id)
	 * 
	 * @param id
	 * @return User
	 */
	User findUserById(long id);

	/**
	 * getUserRole(long id)
	 * 
	 * @param id
	 * @return List<UserRoleDTO>
	 */
	List<UserRoleDTO> getUserRoleByUserId(long id);

	/**
	 * mergeAll(Collection<User> users)
	 * 
	 * @param ids
	 * @param roleIds
	 */
	void mergeAllUser(Long[] ids, Long[] roleIds);

	/**
	 * getRoleByIds
	 * 
	 * @param roleIds
	 * @return Set<Role>
	 */
	Set<Role> getRoleByIds(Long[] roleIds);

	/**
	 * userExist
	 * 
	 * @return boolean
	 */
	boolean userExist(String loginName);

	Set<Long> autoVerification(String fullName);

	/**
	 * 批量修改用户机构
	 * 
	 * @param ids
	 * @param orgNo
	 * @return boolean
	 */
	boolean mergeByOrg(String str, long orgNo);

	/**
	 * 根据登录名获取用户信�?
	 * 
	 * @param uname
	 * @return UserDTO
	 */
	UserDTO findUserByName(String uname);

	/**
	 * 判断用户是否禁用
	 * 
	 * @param uname
	 * @param pwd
	 * @return String
	 */
	String findDisable(String uname, String pwd);

	/**
	 * 导出数据字典到EXCEL.
	 * 
	 * @return Boolean true为导出成功，false为导出失败.
	 */
	InputStream exportUserItems(UserQueryDTO userQueryDTO, String start, String limit);

	/**
	 * 从EXCEL文件导入数据.
	 * 
	 * @param importFile
	 * @return String
	 */
	String importUserItems(File importFile);

	/**
	 * 修改用户密码
	 * 
	 * @param dto
	 * @return boolean
	 */
	boolean resetPassword(EditUserPasswordDTO dto);

	/**
	 * 根据ID获取UserDTO
	 * 
	 * @param id
	 *            long id
	 * @return UserDTO
	 */
	UserDTO findById(Long id);

	Boolean findUserByPassword(String password);

	/**
	 * 获取用户所在的公司信息
	 * 
	 * @param loginName
	 * @return CompanyDTO
	 */
	CompanyDTO findUserCompany(String loginName);

	/**
	 *  根据登录名获取对应的分类ID
	 * 
	 * @param loginName
	 * @return Long[]
	 */
	Long[] findCategoryNosByLoginName(String loginName, String resCode);

	/**
	 * 根据UserId或loginName获取用户详细信息
	 * 
	 * @param userId
	 * @param loginName
	 * @return UserDetailDTO
	 */
	UserDetailDTO findUserByUserIdAndLogName(Long userId, String loginName);

	/**
	 * 用户登录.
	 * 
	 * @param userName
	 * @param password
	 * @return boolean
	 */
	Boolean userLogin(String userName, String password);

	/**
	 * 根据邮件查询用户
	 * 
	 * @param emails
	 * @return Map<String, String>
	 */
	Map<String, String> findUserByEmail(String[] emails);

	/**
	 * 获取当前登录用户
	 * 
	 * @return UserDTO
	 */
	UserDTO getCurrentLoginUser();

	/**
	 * 根据用户名获取详细信息.
	 * 
	 * @param loginName
	 * @return UserDTO
	 */
	UserDTO getUserDetailByLoginName(String loginName);

	/**
	 * 根据电话、手机号码查询所有用户
	 * 
	 * @param qdto
	 * @return List<UserDTO>
	 */
	List<UserDTO> findUserByNumber(final UserQueryDTO qdto);

	/**
	 * 自动登录
	 * 
	 * @param requestUserName
	 * @param domainName
	 * @return LDAPAuthenticationSettingDTO
	 */
	LDAPAuthenticationSettingDTO autoLogin(String requestUserName, String domainName);

	/**
	 * 统计已经使用的技术员数量.
	 * 
	 * @return int
	 */
	int countTechnician();

	/**
	 * LDAP验证并把用户加入到系统.
	 * 
	 * @param userName
	 * @param password
	 * @return boolean
	 */
	boolean ldapAuthAndUpdate(String userName, String password);

	/**
	 * 判断相关资源权限在当前用户是否可以访问
	 * 
	 * @param userName
	 * @param res
	 * @return Map<String,Object>
	 */
	Map<String, Object> findResAuthByUserName(String userName, String[] res);

	/**
	 * 判断资源权限在当前用户是否可以访问
	 * 
	 * @param userName
	 * @param res
	 * @return boolean
	 */
	boolean isAllowAccessResByCurrentUser(String res);

	/**
	 * findUserByName
	 * 
	 * @param fullNames
	 * @return boolean
	 */
	Boolean findUserByFullNames(String[] fullNames);

	/**
	 * 根据登录名判断是否存在 findUserByLoginNames
	 * 
	 * @param loginNames
	 * @return boolean
	 */
	Boolean findUserByLoginNames(String[] loginNames);
	
	Boolean findUserByLoginName(String loginName) ;

	/**
	 * 注册用户激活
	 * 
	 * @param eamil
	 * @param password
	 * @param remark
	 * @return String
	 */
	String userActivation(String loginName, String password, String remark);
	/**
	 * Kerberos用户添加
	 * 
	 * @param eamil
	 * @param password
	 * @return String
	 */
	String userKerberos(String loginName, String password);

	/**
	 * 状态修改
	 * 
	 * @param dataFlag
	 * @param email
	 * @return String
	 */
	String updateOnLineRegisterStatus(String dataFlag, String loginName);

	/**
	 * 根据打过来的电话去查询用户
	 * 
	 * @return UserDTO
	 */
	UserDTO updateUserByPhone(UserDTO dto);
	/**
	 * 根据用户名修改用户的手机号码
	 */
	boolean findUserUpdatePhone(UserDTO dto);

	UserDTO saveUserByPhone(UserDTO dto);
	/**
	 * 根据id查询用户
	 * 
	 * @param dto
	 *            UserDTO
	 * @return UserDTO
	 */
	UserDTO findUserById(UserDTO dto);
	/**
	 * 生成随机数
	 * 
	 * @param dto
	 * @return UserDTO
	 */
	UserDTO setUserRandomId(UserDTO dto);
	/**
	 * 根据随机数查询用户
	 * 
	 * @param dto
	 * @return UserDTO
	 */
	UserDTO findUserByRandomNum(UserDTO dto);

	/**
	 * 根据用户ID判断用户是否为非终端用户
	 * 
	 * @param loginName
	 * @return true 为非终端用户 false 为终端用户
	 */
	boolean checkNonTerminal(String loginName);

	/**
	 * 检验用户是否存在.
	 */
	boolean vaildateUserByIds(String[] userNames);
	/**
	 * 查询我的所属技术组
	 * 
	 * @param loginName
	 * @return
	 */
	Long[] findMyRelatedTechnologyGroup(String loginName);
	
	/**
	 * 根据登录名查找用户
	 * 
	 * @param loginNames
	 * @return List<User>
	 */
	List<User> findUsersByname(String loginNames);
	/**
	 * 根据登录名处理对应的客户端绑定与解除绑定；
	 * userName 为null ，clientId不为null，那么就是解绑所有包含这个ClientId的用户；
	 * userName 不为null ，clientId为null，那么解绑该用户；
	 * userName 不为null ，clientId不为null，那么就是绑定；
	 * @param loginNames
	 * @param cilentId
	 */
	String pushUserBind(String userName,String clientId);
	/**消息数量修改
	 * @return messageCount
	 */
	int pushCount(String userName,int count);
	
	
	/**
	 * 获取变量指派人
	 * @param assignee 指派人
	 * @param variablesAssigneeType 变量类型
	 * @param createdByNo 请求人
	 * @param technicianNo  指派技术员
	 * @param variablesAssigneeGroupNo  指派组
	 * @return User
	 */
	User setVariablesAssignee(User assignee ,String variablesAssigneeType,Long createdByNo,Long technicianNo,Long variablesAssigneeGroupNo);
	/**
	 * 免登录用户新增
	 * @param loginName 验证人
	 */
	void userSsoAdd(String loginName,String password);
	
	/**
	 * 修改终端用户页面风格
	 * @param dto
	 * @return
	 */
	int changeEndUserStyle(UserDTO dto);
	
	/**
	 * 同步北塔用户信息
	 * @param btuDto
	 */
	void synchronizeBtUser(SSOUserDTO btuDto);
	/**
	 * 创建租户用户
	 * @param userDTO
	 */
	void createTenantUser(UserDTO userDTO);
}
