package com.wstuo.common.security.dao;

import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dto.HolidayQueryDTO;
import com.wstuo.common.security.entity.Holiday;
import com.wstuo.common.util.DaoUtils;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * 节假日DAO.
 * @author will
 */
@SuppressWarnings( "unchecked" )
public class HolidayDAO
    extends BaseDAOImplHibernate<Holiday>
    implements IHolidayDAO
{
    /**
     * 分页查找节假日功能
     * @param qdto 查询DTO HolidayQueryDTO
     * @param sidx
     * @param sord
     * @return 分页数据 PageDTO
     */
    public PageDTO findHolidayPage( HolidayQueryDTO qdto,String sidx, String sord )
    {
        DetachedCriteria dc = DetachedCriteria.forClass( Holiday.class );
        int start = 0;
        int limit = 0;

        if ( qdto != null )
        {
            start = qdto.getStart(  );
            limit = qdto.getLimit(  );
            
            if ( qdto.getOrgNo(  ) != null )
            {
                dc.createAlias( "organization", "os" );
                dc.add( Restrictions.eq( "os.orgNo",
                                         qdto.getOrgNo(  ) ) );
            }
          //时间搜索
            if (qdto.getStartTime() != null && qdto.getEndTime()==null) {
                dc.add(Restrictions.ge("hdate", qdto.getStartTime()));
            }
            if (qdto.getStartTime() == null && qdto.getEndTime()!=null) {
            	Calendar endTimeCl=new GregorianCalendar();
            	endTimeCl.setTime(qdto.getEndTime());
            	endTimeCl.set(Calendar.DATE, endTimeCl.get(Calendar.DATE)+1);
                dc.add(Restrictions.le("hdate",endTimeCl.getTime()));
            }
            if (qdto.getStartTime() != null && qdto.getEndTime()!=null) {
            	Calendar endTimeCl=new GregorianCalendar();
            	endTimeCl.setTime(qdto.getEndTime());
            	endTimeCl.set(Calendar.DATE, endTimeCl.get(Calendar.DATE)+1);
  
                dc.add(Restrictions.and(Restrictions.le("hdate",endTimeCl.getTime()),Restrictions.ge("hdate", qdto.getStartTime())));
            }
            if(null != qdto.getCompanyNo()){   
            	dc.add(Restrictions.eq("companyNo", qdto.getCompanyNo()));
            }
        }
        //排序
        dc = DaoUtils.orderBy(sidx, sord, dc);
        
        //dc.addOrder(Order.desc("hid"));

        return super.findPageByCriteria( dc, start, limit );
    }

    /**
     * 根据机构查询节假日列表
     * @param  servicesNo 机构编号 Long
     * @return 节假日列表 List<Holiday>
     */
    public List<Holiday> findHolidayByServicesNo( Long servicesNo )
    {
        return super.getHibernateTemplate(  ).find( " from Holiday hd where hd.organization.orgNo=" + servicesNo +" order by hd.hid desc");
    }
    
    
    /**
     * 根据机构和日期查询节假日列表
     * @param servicesNo 机构编号 Long
     * @param hdate
     * @return 节假日列表 List<Holiday>
     */
    public Holiday findHolidayByServicesNoAndHdate( Long servicesNo,Date hdate )
    {
    	Holiday holiday = null;
    	final DetachedCriteria dc = DetachedCriteria.forClass( Holiday.class );
    	 int start = 0;
         int limit = 0;
    	
    	dc.createAlias( "organization", "os" );
        dc.add( Restrictions.eq( "os.orgNo",servicesNo));
        dc.add( Restrictions.eq( "hdate",hdate));
        //排序
        dc.addOrder(Order.desc("hid"));
    	List<Holiday> list=super.findPageByCriteria( dc, start, limit ).getData();
    	if(list!=null&&list.size()!=0){
    		holiday = list.get(0);
    	}
    	return holiday;
    }
    
    
}
