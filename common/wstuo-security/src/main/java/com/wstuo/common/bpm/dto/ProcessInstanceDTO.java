package com.wstuo.common.bpm.dto;

/**
 * 流程实例信息DTO
 * @author wstuo
 *
 */
public class ProcessInstanceDTO
{
    /**
     * id
     */
    private String id;

    /**
     * process id
     */
    private String processDefinitionId;
    private String key;
    private String state;

    /**
     * priority
     */
    private int priority;

    public String getId(  )
    {
        return id;
    }

    public void setId( String id )
    {
        this.id = id;
    }

    public String getProcessDefinitionId(  )
    {
        return processDefinitionId;
    }

    public void setProcessDefinitionId( String processDefinitionId )
    {
        this.processDefinitionId = processDefinitionId;
    }

    public String getKey(  )
    {
        return key;
    }

    public void setKey( String key )
    {
        this.key = key;
    }

    public String getState(  )
    {
        return state;
    }

    public void setState( String state )
    {
        this.state = state;
    }

    public int getPriority(  )
    {
        return priority;
    }

    public void setPriority( int priority )
    {
        this.priority = priority;
    }
}
