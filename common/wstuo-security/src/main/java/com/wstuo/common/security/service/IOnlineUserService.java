package com.wstuo.common.security.service;

import java.util.List;

import com.wstuo.common.security.dto.OnlineUserDTO;

/**
 * 用户在线状况统计
 */
public interface IOnlineUserService {
    
    /**
     * 设置租户和对应的在线用户的集合
     * @param tenantId 租户ID
     * @param userName 用户名
     * @param isTerminal 是否技术员
     */
    public void setOnlineUsersMap(String tenantId, String userName, boolean isTerminal);
 
    /**
     * 根据租户id获取在线的用户
     * @param tenantId
     * @return
     */
    public List<OnlineUserDTO> getUsersByTenantId (String tenantId);
    

    /**
     * 根据租户id统计在线用户数
     * @param tenantId
     * @return {总数, 技术员数, 终端数}
     */
    public int[] countUserByTenantId (String tenantId);
    
    /**
    * 从租户和对应的在线用户的集合中移除
    * @param tenantId
    * @param userName
    */
   public void setUserOffline(String tenantId, String userName);
   
   
   
}
