package com.wstuo.common.security.service;

import java.util.Comparator;

import com.wstuo.common.security.entity.Role;

/**
 * 角色比较排序
 * @author Administrator
 *
 */
@SuppressWarnings("rawtypes")
public class ComparatorRole implements Comparator {
	public int compare(Object arg0, Object arg1) {
		  Role role=(Role)arg0;
		  Role role1=(Role)arg1;

		  int flag=role.getRoleCode().compareTo(role1.getRoleCode());
		  if(flag==0){
			  flag= role.getRoleName().compareTo(role1.getRoleName());
		  }
		  return flag;
	}
}
