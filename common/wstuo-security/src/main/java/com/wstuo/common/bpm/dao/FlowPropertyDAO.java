package com.wstuo.common.bpm.dao;

import com.wstuo.common.bpm.dao.IFlowPropertyDAO;
import com.wstuo.common.bpm.entity.FlowProperty;
import com.wstuo.common.dao.BaseDAOImplHibernate;

/**
 * Flow Property DAO Class
 * @author wst
 *
 */
public class FlowPropertyDAO extends BaseDAOImplHibernate<FlowProperty> implements IFlowPropertyDAO {
	
}
