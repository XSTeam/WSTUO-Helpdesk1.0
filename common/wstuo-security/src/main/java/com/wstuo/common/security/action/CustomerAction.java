package com.wstuo.common.security.action;

import java.io.InputStream;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;

import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;
import com.wstuo.common.security.dto.CustomerDTO;
import com.wstuo.common.security.dto.CustomerQueryDTO;
import com.wstuo.common.security.service.ICustomerService;

/**
 * 客户管理Action类
 * 
 * @author will
 */
@SuppressWarnings("serial")
public class CustomerAction extends ActionSupport {

	private static final Logger LOGGER = Logger.getLogger(CustomerAction.class);
	@Autowired
	private ICustomerService customerService;
	private PageDTO customers;
	private CustomerQueryDTO queryDto = new CustomerQueryDTO();
	private CustomerDTO customerDto = new CustomerDTO();
	private int page = 1;
	private int rows = 10;
	private Long[] orgNos;
	private Long orgNo;
	private String effect;
	private InputStream exportStream;
	private String fileName = "";

	private String sidx;
	private String sord;
	private Long companyNo;

	public Long getCompanyNo() {
		return companyNo;
	}

	public void setCompanyNo(Long companyNo) {
		this.companyNo = companyNo;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}

	public InputStream getExportStream() {
		return exportStream;
	}

	public void setExportStream(InputStream exportStream) {
		this.exportStream = exportStream;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public ICustomerService getCustomerService() {
		return customerService;
	}

	public void setCustomerService(ICustomerService customerService) {
		this.customerService = customerService;
	}

	public PageDTO getCustomers() {
		return customers;
	}

	public void setCustomers(PageDTO customers) {
		this.customers = customers;
	}

	public CustomerQueryDTO getQueryDto() {
		return queryDto;
	}

	public void setQueryDto(CustomerQueryDTO queryDto) {
		this.queryDto = queryDto;
	}

	public CustomerDTO getCustomerDto() {
		return customerDto;
	}

	public void setCustomerDto(CustomerDTO customerDto) {
		this.customerDto = customerDto;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public Long[] getOrgNos() {
		return orgNos;
	}

	public void setOrgNos(Long[] orgNos) {
		this.orgNos = orgNos;
	}

	public Long getOrgNo() {
		return orgNo;
	}

	public void setOrgNo(Long orgNo) {
		this.orgNo = orgNo;
	}

	/**
	 * 根据公司信息查找客户列表（查找所有客户）.
	 * 
	 * @return String
	 */
	public String findByCompany() {
		int start = (page - 1) * rows;
		queryDto.setStart(start);
		queryDto.setLimit(rows);
		if (companyNo != null) {
			queryDto.setCompanyNo(companyNo);
		}
		customers = customerService.findPagerCustomer(queryDto, sidx, sord);
		customers.setPage(page);
		customers.setRows(rows);
		return SUCCESS;
	}

	/**
	 * 新增客户.
	 * 
	 * @return String
	 */
	public String save() {
		if (customerDto.getCompanyNo() == null) {
			customerDto.setCompanyNo(companyNo);
		}
		customerService.saveCustomer(customerDto);
		return SUCCESS;
	}

	/**
	 * 编辑客户.
	 * 
	 * @return String
	 */
	public String merge() {
		if (companyNo != null) {
			customerDto.setCompanyNo(companyNo);
		}
		customerService.mergeCustomer(customerDto);
		return SUCCESS;
	}

	/**
	 * 批量删除客户.
	 * 
	 * @return String
	 */
	public String deleteCustomers() {
		try {
			customerService.deleteCustomer(orgNos);
		} catch (Exception ex) {
			throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE", ex.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * 删除客户（单个）.
	 * 
	 * @return String
	 */
	public String deleteCustomer() {
		customerService.deleteCustomer(orgNo);
		return SUCCESS;
	}

	/**
	 * 导出数据.
	 * 
	 * @return export inputStream
	 */
	public String exportCustomer() {
		fileName = new Date().getTime() + ".xls";
		queryDto.setStart(0);
		queryDto.setLimit(10000);
		try {
			// 设置好要传入的groupNo
			exportStream = customerService.exportCustomerItems(queryDto, sidx, sord);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
		return "exportFileSuccessful";
	}

	/**
	 * 导入数据
	 * 
	 * @return import result
	 */
	public String importCustomerData() {
		effect = customerService.importCustomerItems(queryDto) + "";
		return "importResult";
	}

}
