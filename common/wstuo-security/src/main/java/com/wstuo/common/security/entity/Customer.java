package com.wstuo.common.security.entity;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * Entity class Customer.
 */
@SuppressWarnings( "serial" )
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Customer
    extends OrganizationOuter{
	
	
    @Transient
    public String getOrgType() {
		return "customer";
	}

}
