package com.wstuo.common.security.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.bpm.api.IBPS;
import com.wstuo.common.exception.ApplicationException;
import com.wstuo.common.security.dao.IOrganizationDAO;
import com.wstuo.common.security.dao.IOrganizationInnerDAO;
import com.wstuo.common.security.dao.IRoleDAO;
import com.wstuo.common.security.dao.IUserDAO;
import com.wstuo.common.security.dto.OrganizationDTO;
import com.wstuo.common.security.entity.Organization;
import com.wstuo.common.security.entity.OrganizationInner;
import com.wstuo.common.security.entity.Role;
import com.wstuo.common.util.AppliactionBaseListener;

/**
 * 内部机构业务类
 * 
 */
public class OrganizationInnerService implements IOrganizationInnerService {
	@Autowired
	private IOrganizationInnerDAO organizationInnerDAO;
	@Autowired
	private IOrganizationDAO organizationDAO;
	@Autowired
	private IUserDAO userDAO;
	@Autowired
	private IRoleDAO roleDAO;

	/**
	 * 保存内部机构.
	 * 
	 * @param dto
	 *            机构DTO：OrganizationDTO
	 */
	@Transactional()
	public void saveOrganizationInner(OrganizationDTO dto) {
		OrganizationInner inner = new OrganizationInner();
		inner.setOrgName(dto.getOrgName());
		inner.setOfficeFax(dto.getOfficeFax());
		inner.setOfficePhone(dto.getOfficePhone());
		inner.setEmail(dto.getEmail());
		inner.setAddress(dto.getAddress());
		inner.setOrgType("inner");
		if (dto.getParentOrgNo() != null) {
			Organization org = organizationDAO.findById(dto.getParentOrgNo());
			if (org != null) {
				if (org.getOrgType().equals("inner") || org.getOrgType().equals("company") || org.getOrgType().equals("itsop")) {
					inner.setParentOrg(org);
				} else {
					throw new ApplicationException("ERROR_PARENT_MUST_BE_INNER_OR_COMPANY\n");
				}
			}
		}

		// 关联负责
		if (dto.getPersonInChargeName() != null) {
			inner.setPersonInCharge(userDAO.findUniqueBy("fullName", dto.getPersonInChargeName()));
		}

		// 关联负责
		if (dto.getPersonInChargeNo() != null) {
			inner.setPersonInCharge(userDAO.findById(dto.getPersonInChargeNo()));
		}
		if(null != dto.getCompanyNo()){
			inner.setCompanyNo(dto.getCompanyNo());
		}
		organizationInnerDAO.save(inner);
		if(inner.getParentOrg()!=null){
			inner.setPath(inner.getParentOrg().getPath()+inner.getOrgNo()+"/");
		}else{
			inner.setPath(inner.getOrgNo()+"/");
		}
		dto.setOrgNo(inner.getOrgNo());

		String parentOrgName = "";

		if (inner.getParentOrg() != null) {
			parentOrgName = "Group_" + inner.getParentOrg().getOrgNo();
		}
		ApplicationContext ctx = AppliactionBaseListener.ctx;
		if(ctx!=null){
			IBPS bps = (IBPS) ctx.getBean("bps");
			bps.createGroup("Group_" + inner.getOrgNo(), parentOrgName);
		}
	}

	/**
	 * 修改内部机构.
	 * 
	 * @param dto
	 *            机构DTO：OrganizationDTO
	 */
	@Transactional()
	public void mergeOrganizationInner(OrganizationDTO dto) {
		OrganizationInner inner = organizationInnerDAO.findById(dto.getOrgNo());
		inner.setOrgName(dto.getOrgName());
		inner.setOfficeFax(dto.getOfficeFax());
		inner.setOfficePhone(dto.getOfficePhone());
		inner.setEmail(dto.getEmail());
		inner.setAddress(dto.getAddress());
		inner.setOrgType("inner");
		if (dto.getParentOrgNo() != -1 && !dto.getOrgNo().equals(dto.getParentOrgNo()) ) {
			inner.setParentOrg(organizationDAO.findById(dto.getParentOrgNo()));

		} else {

			throw new ApplicationException("ERROR_PARENT_ORG_IS_SELF");
		}

		// 关联负责
		if (dto.getPersonInChargeName() != null) {
			inner.setPersonInCharge(userDAO.findUniqueBy("fullName", dto.getPersonInChargeName()));
		}

		// 关联负责
		if (dto.getPersonInChargeNo() != null) {
			inner.setPersonInCharge(userDAO.findById(dto.getPersonInChargeNo()));
		}

		// 机构角色
		Set<Role> roles = new HashSet<Role>();
		if (dto.getOrgRole() != null) {

			for (Long id : dto.getOrgRole()) {
				Role role = roleDAO.findById(id);
				roles.add(role);
			}
		}
		inner.setOrgRoles(roles);

		organizationInnerDAO.merge(inner);
		if(inner.getParentOrg()!=null){
			inner.setPath(inner.getParentOrg().getPath()+inner.getOrgNo()+"/");
		}else{
			inner.setPath(inner.getOrgNo()+"/");
		}
	}

	/**
	 * 根据删除机构.
	 * 
	 * @param orgNo
	 *            机构编号：Long orgNo
	 */
	@Transactional()
	public void deleteOrganizationInner(Long orgNo) {
		organizationInnerDAO.delete(organizationInnerDAO.findById(orgNo));
	}

	/**
	 * 根据编号查找机构.
	 * 
	 * @param id
	 *            机构编号：Long id
	 * @return OrganizationInner
	 */
	public OrganizationInner findOrganizationInnerById(Long id) {
		return organizationInnerDAO.findById(id);
	}

	/**
	 * 根据编号查找机构DTO.
	 * 
	 * @param id
	 *            机构编号：Long id
	 * @return OrganizationDTO
	 */
	public OrganizationDTO findById(Long id) {
		OrganizationDTO dto = new OrganizationDTO();
		OrganizationInner entity = organizationInnerDAO.findById(id);
		if (entity != null) {
			OrganizationDTO.entity2dto(entity, dto);
			if (entity.getParentOrg() != null) {
				dto.setParentOrgNo(entity.getParentOrg().getOrgNo());
			}
		}
		return dto;

	}

}
