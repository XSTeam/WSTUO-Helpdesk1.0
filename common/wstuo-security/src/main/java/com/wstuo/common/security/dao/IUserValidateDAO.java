package com.wstuo.common.security.dao;

import java.util.List;

import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.security.entity.UserValidate;

public interface IUserValidateDAO extends IEntityDAO<UserValidate>
{
	/**
     * 用户登录.
     * @param userName
     * @param password
     * @param tanentId
     * @return boolean
     */
	Boolean userLogin(String userName, String password) ;
	
	/**
	 * 获取所有tenantid
	 * @return
	 */
	List<String> findAllTenantId();
}
