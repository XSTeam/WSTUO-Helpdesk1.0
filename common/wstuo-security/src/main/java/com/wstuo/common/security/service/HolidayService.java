package com.wstuo.common.security.service;

import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;
import com.wstuo.common.security.dao.IHolidayDAO;
import com.wstuo.common.security.dao.IOrganizationDAO;
import com.wstuo.common.security.dto.HolidayDTO;
import com.wstuo.common.security.dto.HolidayQueryDTO;
import com.wstuo.common.security.entity.Holiday;
import com.wstuo.common.security.entity.Organization;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * 节假日业务类.
 */
@SuppressWarnings("unchecked")
public class HolidayService implements IHolidayService {
	@Autowired
	private IHolidayDAO holidayDAO;
	@Autowired
	private IOrganizationDAO organizationDAO;

	/**
	 * 分页查找节假日
	 * 
	 * @param qdto
	 *            分页查询DTO：HolidayQueryDTO
	 * @param sidx
	 * @param sord
	 * @return 分页数据：PageDTO
	 */
	@Transactional
	public PageDTO findHolidayPager(HolidayQueryDTO qdto, String sidx, String sord) {
		PageDTO p = holidayDAO.findHolidayPage(qdto, sidx, sord);

		List<Holiday> entities = (List<Holiday>) p.getData();

		List<HolidayDTO> dtos = new ArrayList<HolidayDTO>();

		for (Holiday ho : entities) {
			HolidayDTO dto = new HolidayDTO();
			HolidayDTO.entity2dto(ho, dto);
			// add mars
			if (ho.getOrganization() != null) {
				dto.setOrgNo(ho.getOrganization().getOrgNo());
				dto.setOrgName(ho.getOrganization().getOrgName());
				dto.setOrgType(ho.getOrganization().getOrgType());
			}
			dtos.add(dto);
		}

		p.setData(dtos);

		return p;
	}

	/**
	 * 新增节假日
	 * 
	 * @param holidayDTO
	 *            节假日DTO：HolidayDTO
	 * @return Long[]
	 */
	@Transactional
	public Long[] addOrEditHoliday(HolidayDTO holidayDTO) {
		HolidayQueryDTO qdto = new HolidayQueryDTO();
		qdto.setStartTime(holidayDTO.getHdate());
		qdto.setEndTime(holidayDTO.getHedate());
		qdto.setOrgNo(holidayDTO.getOrgNo());
		qdto.setStart(0);
		qdto.setLimit(1000);
		PageDTO pageDto = holidayDAO.findHolidayPage(qdto, null, null);

		Long startTime = holidayDTO.getHdate().getTime();
		Long endTime = holidayDTO.getHedate().getTime();
		Long days = (endTime - startTime) / (1000 * 3600 * 24);
		int betweenDay = days.intValue() + 1;

		Long[] holidayIds = new Long[betweenDay];
		if (pageDto.getData().size() > 0 && holidayDTO.getHid() == null) {
			throw new ApplicationException("ERROR_HOLIDAY_HAVE", "ERROR_HOLIDAY_HAVE");
		} else {
			Organization os = organizationDAO.findById(holidayDTO.getOrgNo());

			Calendar cl = new GregorianCalendar();
			cl.setTimeInMillis(startTime);

			for (int i = 0; i < betweenDay; i++) {

				Calendar hiloday = new GregorianCalendar(cl.get(Calendar.YEAR), cl.get(Calendar.MONTH), cl.get(Calendar.DATE) + i);

				Holiday holiday = null;
				if (holidayDTO.getHid() != null) {// 编辑
					holiday = holidayDAO.findById(holidayDTO.getHid());
				}
				if (holiday == null) {// 是否重复
					holiday = holidayDAO.findHolidayByServicesNoAndHdate(holidayDTO.getOrgNo(), hiloday.getTime());
				}

				if (holiday == null) {
					holiday = new Holiday();
					holiday.setHdate(hiloday.getTime());
				}
				if(null != holidayDTO.getCompanyNo()){
					holiday.setCompanyNo(holidayDTO.getCompanyNo());
				}
				holiday.setHdesc(holidayDTO.getHdesc());
				holiday.setOrganization(os);
				holidayDAO.merge(holiday);
				holidayIds[i] = holiday.getHid();

			}

		}

		return holidayIds;
	}

	/**
	 * 批量删除节假日
	 * 
	 * @param hids
	 *            节假日编号数据：Long[] hids
	 */
	@Transactional
	public void deteleHoliday(Long[] hids) {
		holidayDAO.deleteByIds(hids);
	}

	/**
	 * 根据ID获取节假日DTO
	 * 
	 * @param hid
	 *            节假日编号数据：Long hid
	 * @return HolidayDTO
	 */
	@Transactional
	public HolidayDTO findById(Long hid) {
		HolidayDTO dto = new HolidayDTO();
		Holiday entity = holidayDAO.findById(hid);
		if (entity != null) {
			HolidayDTO.entity2dto(entity, dto);
			if (entity.getOrganization() != null) {
				dto.setOrgNo(entity.getOrganization().getOrgNo());
			}
		}
		return dto;

	}

}
