package com.wstuo.common.security.dao;


import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.security.entity.UserValidate;

public class UserValidateDAO extends BaseDAOImplHibernate<UserValidate> implements IUserValidateDAO {
	/**
     * 用户登录.
     * @param userName
     * @param password
     * @return boolean
     */
	public Boolean userLogin(String userName, String password) {
		boolean result = false; 
		final DetachedCriteria dc = DetachedCriteria.forClass(UserValidate.class);
		dc.add(Restrictions.eq("loginName", userName).ignoreCase());
		dc.add(Restrictions.eq("password", password));

		List list=super.getHibernateTemplate().findByCriteria(dc);
		if ( list!= null && list.size() > 0) {
			result = true;
		}
		
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<String> findAllTenantId() {
		final DetachedCriteria dc = DetachedCriteria.forClass(UserValidate.class);
		
		dc.setProjection( Projections.alias( Projections.groupProperty("tenantId"), "tenantId" ));
		
		return super.getHibernateTemplate().findByCriteria(dc);
	}
}
