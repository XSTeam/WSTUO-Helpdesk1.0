package com.wstuo.common.security.entity;

import javax.persistence.Entity;
import javax.persistence.Transient;

/**
 * Entity class OrganizationOuter.
 */
@Entity
public class OrganizationOuter
    extends Organization
{
	
	@Transient
    public String getOrgType() {
		return "outer";
	}

   
}
