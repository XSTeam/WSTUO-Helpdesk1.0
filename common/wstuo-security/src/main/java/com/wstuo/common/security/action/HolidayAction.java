package com.wstuo.common.security.action;

import com.opensymphony.xwork2.ActionSupport;

import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.dto.HolidayDTO;
import com.wstuo.common.security.dto.HolidayQueryDTO;
import com.wstuo.common.security.service.IHolidayService;

/**
 * 节假日Action类
 * 
 * @author QXY 修改时间2011-02-10
 */
@SuppressWarnings("serial")
public class HolidayAction extends ActionSupport {
	private IHolidayService holidayService;
	private HolidayQueryDTO holidayQueryDTO = new HolidayQueryDTO();
	private HolidayDTO holidayDTO = new HolidayDTO();
	private int page = 1;
	private int rows = 10;
	private PageDTO holidays;
	private Long[] ids;
	private String sidx;
	private String sord;

	public IHolidayService getHolidayService() {
		return holidayService;
	}

	public void setHolidayService(IHolidayService holidayService) {
		this.holidayService = holidayService;
	}

	public HolidayQueryDTO getHolidayQueryDTO() {
		return holidayQueryDTO;
	}

	public void setHolidayQueryDTO(HolidayQueryDTO holidayQueryDTO) {
		this.holidayQueryDTO = holidayQueryDTO;
	}

	public HolidayDTO getHolidayDTO() {
		return holidayDTO;
	}

	public void setHolidayDTO(HolidayDTO holidayDTO) {
		this.holidayDTO = holidayDTO;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public PageDTO getHolidays() {
		return holidays;
	}

	public void setHolidays(PageDTO holidays) {
		this.holidays = holidays;
	}

	public Long[] getIds() {
		return ids;
	}

	public void setIds(Long[] ids) {
		this.ids = ids;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	/**
	 * 分页查找节假日列表
	 * 
	 * @return String
	 */
	public String findHolidayPage() {
		int start = (page - 1) * rows;
		holidayQueryDTO.setStart(start);
		holidayQueryDTO.setLimit(rows);
		holidays = holidayService.findHolidayPager(holidayQueryDTO, sidx, sord);
		holidays.setPage(page);
		holidays.setRows(rows);

		return "holidays";
	}

	/**
	 * 新增节假日
	 * 
	 * @return String
	 */
	public String addHoliday() {
		ids = holidayService.addOrEditHoliday(holidayDTO);

		return SUCCESS;
	}

	/**
	 * 修改节假日
	 * 
	 * @return String
	 */
	public String updateHoliday() {
		holidayService.addOrEditHoliday(holidayDTO);

		return SUCCESS;
	}

	/**
	 * 删除节假日
	 * 
	 * @return String
	 */
	public String deleteHoliday() {
		holidayService.deteleHoliday(ids);

		return SUCCESS;
	}
}
