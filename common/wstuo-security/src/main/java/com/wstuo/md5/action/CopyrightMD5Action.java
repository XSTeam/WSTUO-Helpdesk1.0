package com.wstuo.md5.action;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.sha.JDKMessageDigest;
import com.wstuo.md5.service.CopyrightMD5Service;
import com.wstuo.md5.service.ICopyrightMD5Service;

/**
 * 版权MD5加密Action类
 * @author Administrator
 *
 */
@SuppressWarnings("serial")
public class CopyrightMD5Action extends ActionSupport{
	private String resultString;
	
	private String copyrightCode;
	private String code="72dc46b3f93de9f38c532dde9b20568658c4d263";//加密后字符
	
	
	public String getCopyrightCode() {
		return copyrightCode;
	}

	public void setCopyrightCode(String copyrightCode) {
		this.copyrightCode = copyrightCode;
	}

	public String getResultString() {
		return resultString;
	}

	public void setResultString(String resultString) {
		this.resultString = resultString;
	}

	/**
	 * 调用判断版权是否被修改
	 * @return md5 加密字段串
	 */
	public String JudgeMD5save(){
		ICopyrightMD5Service icopyrightMD5=new CopyrightMD5Service();
		resultString=icopyrightMD5.JudgeMD5save();
		return "resultString";
	}
	
	/**
	 * 版权信息
	 * @return
	 */
	public String copyright(){
		JDKMessageDigest sha = new JDKMessageDigest();
		if(code.equals(sha.Encryption(copyrightCode))){
			ICopyrightMD5Service icopyrightMD5=new CopyrightMD5Service();
			resultString=icopyrightMD5.JudgeMD5save();
		}else{
			resultString="failure";
		}
		return "resultString";
	}
	
	
	
	
	
	
	
}
