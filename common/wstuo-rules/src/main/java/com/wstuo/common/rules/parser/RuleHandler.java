package com.wstuo.common.rules.parser;

import com.wstuo.common.rules.entity.Rule;
import com.wstuo.common.rules.entity.RuleAction;
import com.wstuo.common.rules.entity.RuleConstraint;
import com.wstuo.common.rules.entity.RulePackage;
import com.wstuo.common.rules.entity.RulePattern;

import java.util.ArrayList;
import java.util.List;

/**
 * RuleHandler
 * @author QXY
 *
 */
public class RuleHandler
    implements IRuleHandler
{
    private RulePackage rulePackage = new RulePackage(  );
    private List<Rule> rules = new ArrayList<Rule>(  );
    private List<RuleConstraint> constraints = new ArrayList<RuleConstraint>(  );
    private List<RuleAction> actions = new ArrayList<RuleAction>(  );
    private StringBuffer buffer = new StringBuffer(  );

    public RulePackage onPackage( String packageName )
    {
        rulePackage.setPackageName( packageName );

        return rulePackage;
    }

    public RulePackage onImport( String imports )
    {
        rulePackage.setImports( buffer.append( imports ).append( "\r\n" ).toString(  ) );

        return rulePackage;
    }

    public Rule onRule( String ruleName, String dialect )
    {
        Rule rule = new Rule(  );

        rule.setRuleName( ruleName );
        rule.setDialect( dialect );

        rules.add( rule );
        rulePackage.setRules( rules );

        return rule;
    }

    public RulePattern onPattern( String patternBinding, String patternType )
    {
        RulePattern pattern = new RulePattern(  );

        pattern.setPatternBinding( patternBinding );
        pattern.setPatternType( patternType );

        List<Rule> rules = rulePackage.getRules(  );

        for ( Rule rule : rules )
        {
            rule.setCondition( pattern );
        }

        return pattern;
    }

    public RuleConstraint onConstraint( String propertyName, String propertyValue, Boolean propertyType, int sequence,
                                        String andOr )
    {
        RuleConstraint constraint = new RuleConstraint(  );

        constraint.setPropertyName( propertyName );
        constraint.setPropertyValue( propertyValue );
        constraint.setPropertyType( propertyType );
        constraint.setSequence( sequence );
        constraint.setAndOr( andOr );
        constraints.add( constraint );

        List<Rule> rules = rulePackage.getRules(  );

        for ( Rule rule : rules )
        {
            RulePattern pattern = rule.getCondition(  );

            pattern.setConstraints( constraints );
        }

        return constraint;
    }

    public RuleAction onAction( String propertyName, String givenValue, int sequence, String andOr )
    {
        RuleAction action = new RuleAction(  );

        action.setPropertyName( propertyName );
        action.setGivenValue( givenValue );
        action.setSequence( sequence );
        action.setAndOr( andOr );
        actions.add( action );

        List<Rule> rules = rulePackage.getRules();

        for ( Rule rule : rules )
        {
            rule.setActions( actions );
        }

        return action;
    }
}
