package com.wstuo.common.rules.dao;

import com.wstuo.common.rules.dto.RulePackageDTO;
import com.wstuo.common.rules.entity.RulePackage;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

import java.util.List;

/**
 * interface of rule package dao
 * @author <a href="mailto:376890523@qq.com">jeff</a>
 * @version 0.1         2010.9.28
 * */
public interface IRulePackageDAO extends IEntityDAO<RulePackage>{
	
	/**
	 * 查询所有规则包
	 * @return List<RulePackage>
	 */
    List<RulePackage> findRules(  );
    
    /**
     * 分页查询规则包
     * @param packageDTO
     * @param sidx
     * @param sord
     * @param start
     * @param rows
     * @return PageDTO
     */
    PageDTO findPager(RulePackageDTO packageDTO,String sidx,String sord,int start,int rows);
    
}
