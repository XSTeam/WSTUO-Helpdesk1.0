package com.wstuo.common.rules.action;

import com.opensymphony.xwork2.ActionSupport;

import com.wstuo.common.rules.dto.RulePackageDTO;
import com.wstuo.common.rules.service.IRulePackageService;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 规则包Action
 * 
 * @author QXY
 * 
 */
@SuppressWarnings("serial")
public class RulePackageAction extends ActionSupport {
	@Autowired
	private IRulePackageService rulePackageService;
	private RulePackageDTO rulePackageDto = new RulePackageDTO();
	private List<RulePackageDTO> rulePackages;
	private int page = 1;
	private int rows = 10;
	private Long[] ids;
	private String sidx;
	private String sord;
	private PageDTO pageDto = new PageDTO();
	private String addpackageName;
	private Boolean result = false;

	public Boolean getResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}

	public String getAddpackageName() {
		return addpackageName;
	}

	public void setAddpackageName(String addpackageName) {
		this.addpackageName = addpackageName;
	}

	public PageDTO getPageDto() {
		return pageDto;
	}

	public void setPageDto(PageDTO pageDto) {
		this.pageDto = pageDto;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public Long[] getIds() {
		return ids;
	}

	public void setIds(Long[] ids) {
		this.ids = ids;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public RulePackageDTO getRulePackageDto() {
		return rulePackageDto;
	}

	public void setRulePackageDto(RulePackageDTO rulePackageDto) {
		this.rulePackageDto = rulePackageDto;
	}

	public List<RulePackageDTO> getRulePackages() {
		return rulePackages;
	}

	public void setRulePackages(List<RulePackageDTO> rulePackages) {
		this.rulePackages = rulePackages;
	}

	/**
	 * 查询
	 * 
	 * @return rulePackages(集合)
	 */
	public String findRulePackage() {
		// 调用方法找出集合中的数据
		rulePackages = rulePackageService.findRulePackages();
		return "listRulePackages";
	}

	/**
	 * 软件分页查询
	 * 
	 * @return PageDTO
	 */
	public String findRulePackageByPage() {
		int start = (page - 1) * rows;
		pageDto = rulePackageService.findRulePackageByPage(rulePackageDto, sidx, sord, start, rows);
		pageDto.setPage(page);
		pageDto.setRows(rows);
		return SUCCESS;
	}

	/**
	 * 保存
	 * 
	 * @return save Result
	 */
	public String saveRulePackage() {
		rulePackageService.rulePackageSave(rulePackageDto);
		return SUCCESS;
	}

	/**
	 * 更新
	 * 
	 * @return updae result
	 */
	public String editRulePackage() {
		rulePackageService.editRulePackage(rulePackageDto);
		return SUCCESS;
	}

	/**
	 * 删除
	 * 
	 * @return delete result
	 */
	public String deleteRulePackage() {
		try {
			rulePackageService.deleteRulePackage(ids);
		} catch (Exception e) {
			throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE", e);
		}
		return SUCCESS;
	}

	/**
	 * 查询
	 * 
	 * @return rulePackages(集合)
	 */
	public String findListRulePackage() {
		// 调用方法找出集合中的数据
		rulePackages = rulePackageService.findRulePackages();

		return "rulePackages";
	}

	/**
	 * 查询
	 * 
	 * @return rulePackages(集合)
	 */
	public String findRulePackageBoolean() {
		// 调用方法进行判断
		result = rulePackageService.findRulePackageBoolean(addpackageName);
		return "result";
	}
}
