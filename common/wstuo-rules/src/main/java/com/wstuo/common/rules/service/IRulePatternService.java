package com.wstuo.common.rules.service;

import com.wstuo.common.rules.dto.RulePatternDTO;

import java.util.List;

/**
 * interface of RulePatternService
 * @author <a href="mailto:376890523@qq.com">jeff</a>
 * @version 0.1  2010-9-29
 * */
public interface IRulePatternService
{
    /**
     *                 --------------save,remove,update Method--------------
     * */

    /**
    * save rulePattern
    * @param dto*/
    public void saveRulePattern( RulePatternDTO dto );

    /**
    * remove rulePattern
    * @param no*/
    public void removeRulePattern( Long no );

    /**
    * remove rulePatterns
    * @param nos*/
    public void removeRulePatterns( Long[] nos );

    /**
    * merge rulePattern
    * @param dto
    * @return RulePatternDTO*/
    public RulePatternDTO mergeRulePattern( RulePatternDTO dto );

    /**
    * merge rulePatterns
    * @param dtos*/
    public void mergeAllRulePattern( List<RulePatternDTO> dtos );

    /**
     *                 --------------find Method--------------
     * */

    /**
    * find all rulePattern
    * @return List<RulePatternDTO>*/
    public List<RulePatternDTO> findRulePatterns(  );

    /**
     * find rulePattern by id
     * @param no
     * @return RulePatternDTO*/
    public RulePatternDTO findRulePatternById( Long no );

    /**
     * find rulePattern by ruleNo
     * @param ruleNo
     * @return RulePatternDTO*/
    public RulePatternDTO findRulePatternByRuleNo( Long ruleNo );
}
