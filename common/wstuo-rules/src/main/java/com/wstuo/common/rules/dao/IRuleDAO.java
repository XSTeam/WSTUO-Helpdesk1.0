package com.wstuo.common.rules.dao;

import java.util.List;

import com.wstuo.common.rules.dto.RuleQueryDTO;
import com.wstuo.common.rules.entity.Rule;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

/**
 * interface of rule dao
 * @author <a href="mailto:376890523@qq.com">jeff</a>
 * @version 0.1         2010.9.28
 * */
public interface IRuleDAO extends IEntityDAO<Rule>{
	/**
	 * 分页查询规则
	 * @param qdto
	 * @param start
	 * @param limit
	 * @param sidx
	 * @param sord
	 * @return PageDTO
	 */
    PageDTO findPager( final RuleQueryDTO qdto, int start, int limit, String sidx, String sord );
    
    /**
     * 根据SLA ID查询规则列表
     * @param id SLAId
     * @return list rule
     */
    List<Rule> findRuleList(Long id);
    
    /**
     * 根据规则名称查询
     * @param id
     * @param ruleName
     * @return List<Rule>
     */
    List<Rule> existRuleName(Long id,String ruleName);
}
