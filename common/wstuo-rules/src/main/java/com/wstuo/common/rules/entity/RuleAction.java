package com.wstuo.common.rules.entity;

import com.wstuo.common.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * bean of action
 * 
 * @author <a href="mailto:376890523@qq.com">jeff</a>
 * @version 0.1 2010.9.28
 * */
@SuppressWarnings({ "serial", "rawtypes" })
@Entity
public class RuleAction extends BaseEntity {
	/**
	 * Action no
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long actionNo;

	/**
	 * action name
	 */
	private String actionName;

	/**
	 * property name
	 */
	private String propertyName;

	/**
	 * given value
	 */
	private String givenValue;

	private String givenName;

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	/**
	 * sequence
	 */
	private Integer sequence=0;

	/**
	 * and ,or
	 */
	private String andOr;
	private String dataType;

	/**
	 * rule
	 */
	@ManyToOne
	@JoinColumn(name = "ruleNo")
	private Rule rule;

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public Rule getRule() {
		return rule;
	}

	public void setRule(Rule rule) {
		this.rule = rule;
	}

	public Long getActionNo() {
		return actionNo;
	}

	public void setActionNo(Long actionNo) {
		this.actionNo = actionNo;
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	public String getAndOr() {
		return andOr;
	}

	public void setAndOr(String andOr) {
		this.andOr = andOr;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getGivenValue() {
		return givenValue;
	}

	public void setGivenValue(String givenValue) {
		this.givenValue = givenValue;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	/**
	 * toString Method
	 */
	public String toString() {
		StringBuffer buffer = new StringBuffer();

		buffer.append(propertyName);
		buffer.append("==\"");
		buffer.append(givenValue);
		buffer.append("\"");
		buffer.append(andOr);

		return buffer.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((actionName == null) ? 0 : actionName.hashCode());
		result = prime * result
				+ ((actionNo == null) ? 0 : actionNo.hashCode());
		result = prime * result + ((andOr == null) ? 0 : andOr.hashCode());
		result = prime * result
				+ ((dataType == null) ? 0 : dataType.hashCode());
		result = prime * result
				+ ((givenName == null) ? 0 : givenName.hashCode());
		result = prime * result
				+ ((givenValue == null) ? 0 : givenValue.hashCode());
		result = prime * result
				+ ((propertyName == null) ? 0 : propertyName.hashCode());
		result = prime * result + ((rule == null) ? 0 : rule.hashCode());
		result = prime * result
				+ ((sequence == null) ? 0 : sequence.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RuleAction other = (RuleAction) obj;
		if (actionName == null) {
			if (other.actionName != null)
				return false;
		} else if (!actionName.equals(other.actionName))
			return false;
		if (actionNo == null) {
			if (other.actionNo != null)
				return false;
		} else if (!actionNo.equals(other.actionNo))
			return false;
		if (andOr == null) {
			if (other.andOr != null)
				return false;
		} else if (!andOr.equals(other.andOr))
			return false;
		if (dataType == null) {
			if (other.dataType != null)
				return false;
		} else if (!dataType.equals(other.dataType))
			return false;
		if (givenName == null) {
			if (other.givenName != null)
				return false;
		} else if (!givenName.equals(other.givenName))
			return false;
		if (givenValue == null) {
			if (other.givenValue != null)
				return false;
		} else if (!givenValue.equals(other.givenValue))
			return false;
		if (propertyName == null) {
			if (other.propertyName != null)
				return false;
		} else if (!propertyName.equals(other.propertyName))
			return false;
		if (rule == null) {
			if (other.rule != null)
				return false;
		} else if (!rule.equals(other.rule))
			return false;
		if (sequence == null) {
			if (other.sequence != null)
				return false;
		} else if (!sequence.equals(other.sequence))
			return false;
		return true;
	}
}
