package com.wstuo.common.priorityMatrix.service;

import com.wstuo.common.priorityMatrix.dto.PriorityMatrixStatusDTO;

/**
 * 优先级矩阵状态Service接口类
 * @author WSTUO
 *
 */
public interface IPriorityMatrixStatusService {
	/**
	 * 保存优先级状态
	 * @param dto 优先级矩阵DTO参数
	 */
	void savePriorityMatrixStatus(PriorityMatrixStatusDTO dto);
	
	/**
	 * 查询优先级状态
	 * @return PriorityMatrixStatusDTO
	 */
	PriorityMatrixStatusDTO findPriorityMatrixStatus();
}
