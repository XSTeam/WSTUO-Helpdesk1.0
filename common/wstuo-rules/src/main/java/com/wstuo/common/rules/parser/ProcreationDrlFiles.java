package com.wstuo.common.rules.parser;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.wstuo.common.rules.drool.RuleFileName;
import com.wstuo.common.rules.drool.RulePathType;
import com.wstuo.common.rules.entity.Rule;
import com.wstuo.common.rules.entity.RulePackage;
import com.wstuo.common.exception.ApplicationException;
import com.wstuo.common.security.utils.AppConfigUtils;



/**
 * 打印规则文件
 * @author QXY
 * 修改时间 2011-01-26
 */
public class ProcreationDrlFiles{
	final static Logger LOGGER = Logger.getLogger(ProcreationDrlFiles.class);
	
    /**
     * 打印规则文件.
     * @param rule
     */
	@Transactional
    public void printDrlFiles( Rule rule ){
    	//调用方法打印
    	printDrlFiles(rule.getRulePackage());
    }
	
    /**
     * 打印规则文件.
     * @param rulePackage
     */
    @Transactional
    public void printDrlFiles( RulePackage rulePackage ){
    	String RULE_PATH =  AppConfigUtils.getInstance().getDroolsFilePath();
    	Boolean deleteFile=false;
    	//判断包下是否还存在规则
    	if(rulePackage!=null && rulePackage.getRules()!=null && rulePackage.getRules().size()>0){
    		for(Rule r:rulePackage.getRules()){
    			if(r.getDataFlag()!=99){
    				deleteFile=true;
    				break;
    			}
    		}
    	}
    	OutputStreamWriter out = null;
        try{
        	if(rulePackage!=null){
        		String fielName =  rulePackage.getLocation();
            	String module = "";
            	if(RuleFileName.RequestRule.getValue().equals(fielName) || RuleFileName.PrioritymatrixRule.getValue().equals(fielName)){
            		module = RulePathType.Request.getValue();
            	}else if(RuleFileName.MailRequestRule.getValue().equals(fielName)){
            		module = RulePathType.RequestMail.getValue();
            	}else if(RuleFileName.ChangeApprovalRule.getValue().equals(fielName)){
            		module = RulePathType.Change.getValue();
            	}else if("requestProce".equals(rulePackage.getFlagName())){
            		module = RulePathType.RequestWorkFlow.getValue();
            	}else if("changeProce".equals(rulePackage.getFlagName())){
            		module = RulePathType.ChangeWorkFlow.getValue();
            	}else{
            		module = RulePathType.Sla.getValue();
            	}
                String text = rulePackage.toString();
                File folder = new File( RULE_PATH +"/"+module );
                //如果文件夹不存在就创建文件夹
                if (!folder.exists()) {
                	folder.mkdirs();
                }
                //读取配置文件
                String loactionUrl = RULE_PATH +"/"+module+"/"+fielName;
                File ruleFile = new File( loactionUrl );
                //删除文件
                if ( ruleFile.exists()){
                	ruleFile.delete();
                }
                if(deleteFile){//存在规则才写入
    	            //写入文件
    	            out = new OutputStreamWriter( new FileOutputStream( loactionUrl ));
    	            out.write(text);
                }
        	}
        } catch ( Exception ex ){
        	//抛出异常
        	throw new ApplicationException("ERROR_RULES_ONPRINTDRLFILES \n"+ex,ex);
        }finally{
        	 try {
        		 if(out!=null){
        			 out.flush();
        			 out.close();
        		 }
			} catch (IOException e) {
				LOGGER.error(e);
			}
        }
    }
    @Transactional
    public void removeFile(String rulePackageloaction){
    	String RULE_PATH =  AppConfigUtils.getInstance().getDroolsFilePath();
    	//读取配置文件
        String loaction = RULE_PATH +"/"+rulePackageloaction;
        File frlFile = new File(loaction);
        //删除文件
        if ( frlFile.exists()){
            frlFile.delete();
        }
    }
}
