package com.wstuo.common.rules.dao;

import com.wstuo.common.rules.entity.RuleAction;
import com.wstuo.common.dao.BaseDAOImplHibernate;

/**
 * dao of rule action
 * @author <a href="mailto:376890523@qq.com">jeff</a>
 * @version 0.1         2010.9.28
 * */
public class RuleActionDAO
    extends BaseDAOImplHibernate<RuleAction>
    implements IRuleActionDAO
{
}
