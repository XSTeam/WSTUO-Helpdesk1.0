package com.wstuo.common.priorityMatrix.dao;

import com.wstuo.common.priorityMatrix.entity.PriorityMatrixStatus;
import com.wstuo.common.dao.IEntityDAO;

/**
 * 优先级矩阵状态DAO类
 * @author WSTUO
 *
 */
public interface IPriorityMatrixStatusDAO extends IEntityDAO<PriorityMatrixStatus> {

}
