package com.wstuo.common.rules.dao;

import com.wstuo.common.rules.entity.RuleAction;
import com.wstuo.common.dao.IEntityDAO;

/**
 * interface of rule action dao
 * @author <a href="mailto:376890523@qq.com">jeff</a>
 * @version 0.1         2010.9.28
 * */
public interface IRuleActionDAO
    extends IEntityDAO<RuleAction>
{
}
