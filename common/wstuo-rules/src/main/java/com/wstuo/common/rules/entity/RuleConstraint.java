package com.wstuo.common.rules.entity;

import com.wstuo.common.entity.BaseEntity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

/**
 * bean of constraint
 * 
 * @author <a href="mailto:376890523@qq.com">jeff</a>
 * @version 0.1 2010.9.28
 * */
@SuppressWarnings({ "serial", "rawtypes" })
@Entity
public class RuleConstraint extends BaseEntity {
	/**
	 * Constraint no
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long conNo;

	/**
	 * and ,or
	 */
	private String andOr;

	/**
	 * property name
	 */
	private String propertyName;

	/**
	 * whether String
	 */
	private Boolean propertyType = false;

	/**
	 * operator
	 */
	private String operator;

	/**
	 * property value
	 */
	@Lob
	private String propertyValue;
	@Lob
	private String propertyValueName;

	public String getPropertyValueName() {
		return propertyValueName;
	}

	public void setPropertyValueName(String propertyValueName) {
		this.propertyValueName = propertyValueName;
	}

	/**
	 * sequence
	 */
	private Integer sequence=0;

	/**
	 * RulePattern
	 */
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "rulePatternNo")
	private RulePattern pattern;
	private String dataType;

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public RulePattern getPattern() {
		return pattern;
	}

	public void setPattern(RulePattern pattern) {
		this.pattern = pattern;
	}

	public Long getConNo() {
		return conNo;
	}

	public void setConNo(Long conNo) {
		this.conNo = conNo;
	}

	public String getAndOr() {
		return andOr;
	}

	public void setAndOr(String andOr) {
		this.andOr = andOr;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getPropertyValue() {
		return propertyValue;
	}

	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public Boolean getPropertyType() {
		return propertyType;
	}

	public void setPropertyType(Boolean propertyType) {
		this.propertyType = propertyType;
	}

	/**
	 * toString Method
	 */
	public String toString() {
		StringBuffer buffer = new StringBuffer();

		if (propertyType == true) {
			buffer.append(propertyName);
			buffer.append("==\"");
			buffer.append(propertyValue);
			buffer.append(andOr);
			buffer.append("\"");
		} else {
			buffer.append(propertyName);
			buffer.append("==");
			buffer.append(propertyValue);
			buffer.append(andOr);
		}

		return buffer.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((andOr == null) ? 0 : andOr.hashCode());
		result = prime * result + ((conNo == null) ? 0 : conNo.hashCode());
		result = prime * result
				+ ((dataType == null) ? 0 : dataType.hashCode());
		result = prime * result
				+ ((operator == null) ? 0 : operator.hashCode());
		result = prime * result + ((pattern == null) ? 0 : pattern.hashCode());
		result = prime * result
				+ ((propertyName == null) ? 0 : propertyName.hashCode());
		result = prime * result
				+ ((propertyType == null) ? 0 : propertyType.hashCode());
		result = prime * result
				+ ((propertyValue == null) ? 0 : propertyValue.hashCode());
		result = prime
				* result
				+ ((propertyValueName == null) ? 0 : propertyValueName
						.hashCode());
		result = prime * result
				+ ((sequence == null) ? 0 : sequence.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RuleConstraint other = (RuleConstraint) obj;
		if (andOr == null) {
			if (other.andOr != null)
				return false;
		} else if (!andOr.equals(other.andOr))
			return false;
		if (conNo == null) {
			if (other.conNo != null)
				return false;
		} else if (!conNo.equals(other.conNo))
			return false;
		if (dataType == null) {
			if (other.dataType != null)
				return false;
		} else if (!dataType.equals(other.dataType))
			return false;
		if (operator == null) {
			if (other.operator != null)
				return false;
		} else if (!operator.equals(other.operator))
			return false;
		if (pattern == null) {
			if (other.pattern != null)
				return false;
		} else if (!pattern.equals(other.pattern))
			return false;
		if (propertyName == null) {
			if (other.propertyName != null)
				return false;
		} else if (!propertyName.equals(other.propertyName))
			return false;
		if (propertyType == null) {
			if (other.propertyType != null)
				return false;
		} else if (!propertyType.equals(other.propertyType))
			return false;
		if (propertyValue == null) {
			if (other.propertyValue != null)
				return false;
		} else if (!propertyValue.equals(other.propertyValue))
			return false;
		if (propertyValueName == null) {
			if (other.propertyValueName != null)
				return false;
		} else if (!propertyValueName.equals(other.propertyValueName))
			return false;
		if (sequence == null) {
			if (other.sequence != null)
				return false;
		} else if (!sequence.equals(other.sequence))
			return false;
		return true;
	}
}
