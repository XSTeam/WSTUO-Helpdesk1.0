package com.wstuo.common.priorityMatrix.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.priorityMatrix.dto.PriorityMatrixDTO;
import com.wstuo.common.priorityMatrix.entity.PriorityMatrix;
import com.wstuo.common.dao.BaseDAOImplHibernate;

/**
 * 优先级矩阵DAO类
 * @author WSTUO
 *
 */
public class PriorityMatrixDAO extends BaseDAOImplHibernate<PriorityMatrix> implements IPriorityMatrixDAO {
	/**
	 * 根据紧急度、影响范围查找优先级矩阵
	 * @param dto PriorityMatrixDTO
	 * @return PriorityMatrix 返回优先级矩阵结果
	 */
	public PriorityMatrix findPriorityMatrixByUrgencyAffect(PriorityMatrixDTO dto){
		final DetachedCriteria dc = DetachedCriteria.forClass(PriorityMatrix.class);
		PriorityMatrix entity=new PriorityMatrix(); 
		if(dto!=null){
			if(dto.getUrgencyId()!=null)
				dc.add(Restrictions.eq("urgency.dcode", dto.getUrgencyId()));
			if(dto.getAffectId()!=null)
				dc.add(Restrictions.eq("affect.dcode", dto.getAffectId()));
			
			List<PriorityMatrix> pms = super.getHibernateTemplate().findByCriteria(dc);
			if(pms.size()>0){
				entity=pms.get(0);
			}
		}
		return entity;
	};
}
