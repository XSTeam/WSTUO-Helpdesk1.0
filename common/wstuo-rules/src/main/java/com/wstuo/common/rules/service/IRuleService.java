package com.wstuo.common.rules.service;

import com.wstuo.common.rules.dto.RuleDTO;
import com.wstuo.common.rules.dto.RuleQueryDTO;
import com.wstuo.common.rules.entity.Rule;
import com.wstuo.common.dto.PageDTO;

import java.io.InputStream;
import java.util.List;

/**
 * interface of RuleService
 * @author <a href="mailto:376890523@qq.com">jeff</a>
 * @version 0.1  2010-9-28
 * */
public interface IRuleService
{
    /**
     * save rule
     * @param dto*/
    void saveRule( RuleDTO dto );

    /**
     * remove rule
     * @param no*/
    boolean removeRule( Long no );

    /**
     * remove rules
     * @param nos*/
    boolean removeRules( Long[] nos );

    /**
     * merge rule
     * @param dto
     * @return RuleDTO*/
    RuleDTO mergeRule( RuleDTO dto );

    /**
     * merge rules
     * @param dtos*/
    void mergeAllRule( List<RuleDTO> dtos );

    /**
     * save rule
     * @param rule*/
    Long saveRuleEntity( Rule rule );

    /**
     * merge rule entity
     * @param rule
     * @return RuleDTO*/
    Rule mergeRuleEntity( Rule rule );

    /**
     *                 --------------find Method--------------
     * */

    /**
     * find rule by page query
     * @param qdto
	 * @param start
	 * @param limit
     * @return PageDTO*/
    PageDTO findRuleByPager( RuleQueryDTO qdto,
    		int start, int limit, String sidx, String sord);

    /**
     * find rule by id
     * @param no
     * @return RuleDTO*/
    RuleDTO findRuleDTOById( Long no );

    /**
     * find rule by id
     * @param no
     * @return Rule*/
    Rule findRuleById( Long no );
    
    /**
     * 读取规则文件
     * @param drlFilePath
     * @param rulePackageNo
     * @return import result long
     */
    Long importRules(String drlFilePath,Long rulePackageNo);
    
    /**
     * 导出规则文件.
     * @param rulePackageNo
     * @return InputStream
     */
    InputStream exportRules(Long rulePackageNo);
    
    /**
     * entity to dto
     * @param entity
     * @param dto
     */
    void entity2dto( Rule entity, RuleDTO dto );
    
    /**
     * print drl rule file
     * @param rulePackageNo
     */
    void printDrlFiles(Long rulePackageNo);

    /**
     * print drl rule file
     * @param rule
     */
	void printDrlFiles(Rule rule);
    
	/**
	 * create default rule data
	 */
	void createDefalutRule();
	/**
	 * 查询集合
	 * @return List<RuleDTO>
	 */
	List<RuleDTO> findByRuleSelect();
	
	/**
	 * 根据ID查询规则包是否存在
	 * @param id
	 * @return boolean
	 */
	Boolean findRulePackageBoolean(Long id);
	
	/**
	 * 判断规则名称是否已存在
	 * @param ruleName
	 * @param rulePackageNo
	 * @return boolean
	 */
	boolean existRuleName(String ruleName,Long rulePackageNo);
	
	/**
	 * 读取
	 * @param drlFilePath
	 * @return file to string
	 */
	String readDrlFile(String drlFilePath);
	
}
