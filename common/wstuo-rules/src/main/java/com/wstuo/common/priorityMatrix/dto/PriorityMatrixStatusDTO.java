package com.wstuo.common.priorityMatrix.dto;

import com.wstuo.common.dto.BaseDTO;

/**
 * 优先级矩阵状态DTO类
 * @author Administrator
 *
 */
@SuppressWarnings("serial")
public class PriorityMatrixStatusDTO extends BaseDTO {
	private Long id;
	private String priorityMatrixStatus;//状态
	private String type;//类型
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPriorityMatrixStatus() {
		return priorityMatrixStatus;
	}
	public void setPriorityMatrixStatus(String priorityMatrixStatus) {
		this.priorityMatrixStatus = priorityMatrixStatus;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
