package com.wstuo.common.jbpm.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.wstuo.common.bpm.api.IBPS;
import com.wstuo.common.jbpm.IJbpmFacade;

/**
 * business process user or group implement class
 * @author wstuo
 *
 */
public class JbpmBPS implements IBPS {
	@Autowired
	private IJbpmFacade jbpmFacade;
	public void createGroup(String groupName, String parentGroupId) {
		// TODO Auto-generated method stub
		jbpmFacade.createGroup(groupName, parentGroupId);
	}

	public void createUser(String id, String familyName, String givenName,
			String businessEmail) {
		// TODO Auto-generated method stub
		jbpmFacade.createUser(id, familyName, givenName, businessEmail);
	}

	public void modifyUser(String id, String familyName, String givenName,
			String businessEmail) {
		// TODO Auto-generated method stub
		jbpmFacade.modifyUser(id, familyName, givenName, businessEmail);
	}

	public void createMembership(String groupId, String parentGroupId, String[] userIds) {
		// TODO Auto-generated method stub
		jbpmFacade.createMembership(groupId, parentGroupId, userIds);
	}

}
