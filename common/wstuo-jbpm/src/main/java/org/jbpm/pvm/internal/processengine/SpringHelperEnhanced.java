package org.jbpm.pvm.internal.processengine;

import org.hibernate.SessionFactory;
import org.jbpm.api.ProcessEngine;
import org.jbpm.pvm.internal.cfg.ConfigurationImpl;

/**
 * 
 * <p>SpringHelper Enhanced</p>
 * 
 * <p>Inject sessionFactory</P>
 * 
 * @author wstuo
 *
 */
public class SpringHelperEnhanced extends SpringHelper {  
       
    protected SessionFactory sessionFactory;  
       
    public void setSessionFactory(SessionFactory sessionFactory) {  
        this.sessionFactory = sessionFactory;  
    }  
       
    public ProcessEngine createProcessEngine() {  
        processEngine = new ConfigurationImpl()
                        .springInitiated(applicationContext)
                        .setResource(jbpmCfg)  
                        .setHibernateSessionFactory(sessionFactory)
                        .buildProcessEngine();  
        return processEngine;
    }  
       
}  