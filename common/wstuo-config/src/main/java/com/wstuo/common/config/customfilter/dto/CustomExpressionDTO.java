package com.wstuo.common.config.customfilter.dto;



import com.wstuo.common.dto.BaseDTO;
/**
 * DTO of CustomExpression
 * @author Will
 * */
@SuppressWarnings("serial")
public class CustomExpressionDTO extends BaseDTO{
	
	private Long expId;
	
	private String propDisplayName;//变量显示
	
	private String propName;//变量名称
	
	private String displayOperator;//操作显示
	
	private String operator;//操作
	
	private String propValue;//值
	
	private String propDisplayValue;//显示值
	
	private String joinType;//连接类别---and 还是or
	
	private Long filterId;//关联Filter
	
	private String propType;//变量类型int String time.....

	
	public Long getExpId() {
		return expId;
	}

	public void setExpId(Long expId) {
		this.expId = expId;
	}

	public String getPropName() {
		return propName;
	}

	public void setPropName(String propName) {
		this.propName = propName;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getPropValue() {
		return propValue;
	}

	public void setPropValue(String propValue) {
		this.propValue = propValue;
	}

	public String getJoinType() {
		return joinType;
	}

	public void setJoinType(String joinType) {
		this.joinType = joinType;
	}

	public Long getFilterId() {
		return filterId;
	}

	public void setFilterId(Long filterId) {
		this.filterId = filterId;
	}
	
	public String getPropType() {
		return propType;
	}

	public void setPropType(String propType) {
		this.propType = propType;
	}

	public String getPropDisplayValue() {
		return propDisplayValue;
	}

	public void setPropDisplayValue(String propDisplayValue) {
		this.propDisplayValue = propDisplayValue;
	}
	

	public String getPropDisplayName() {
		return propDisplayName;
	}

	public void setPropDisplayName(String propDisplayName) {
		this.propDisplayName = propDisplayName;
	}

	public String getDisplayOperator() {
		return displayOperator;
	}

	public void setDisplayOperator(String displayOperator) {
		this.displayOperator = displayOperator;
	}

	public CustomExpressionDTO(){
		
	}

	public CustomExpressionDTO(Long expId, String propDisplayName,
			String propName, String displayOperator, String operator,
			String propValue, String propDisplayValue, String joinType,
			Long filterId, String propType) {
		super();
		this.expId = expId;
		this.propDisplayName = propDisplayName;
		this.propName = propName;
		this.displayOperator = displayOperator;
		this.operator = operator;
		this.propValue = propValue;
		this.propDisplayValue = propDisplayValue;
		this.joinType = joinType;
		this.filterId = filterId;
		this.propType = propType;
	}

	
	
}
