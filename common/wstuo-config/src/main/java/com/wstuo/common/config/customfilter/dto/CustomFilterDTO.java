package com.wstuo.common.config.customfilter.dto;



import java.util.List;

import org.apache.log4j.Logger;

import com.wstuo.common.dto.BaseDTO;

/**
 * DTO of CustomFilter
 * @author will
 *
 */
@SuppressWarnings("serial")
public class CustomFilterDTO extends BaseDTO{
	private static final Logger LOGGER = Logger.getLogger(CustomFilterDTO.class );    
	private Long filterId;
	
	private String filterName;//过滤器名称
	
	private String filterDesc;//过滤器描述
	
	private String entityClass;//过滤器查询的类
	
	private Long userId;//创建用户ID
	
	private String userName;//创建用户登录名
	
	private String filterCategory;//过滤器类别
	
	private List<CustomExpressionDTO> expressions;
	
	private Byte dataFlag;
	
	private String share;//是否共享
	
	private Long [] shareGroupIds;//分享到组
	private String shareGroupIdString;
	
	

	public Long getFilterId() {
		return filterId;
	}

	public void setFilterId(Long filterId) {
		this.filterId = filterId;
	}

	public String getFilterName() {
		return filterName;
	}

	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}

	public String getFilterDesc() {
		return filterDesc;
	}

	public void setFilterDesc(String filterDesc) {
		this.filterDesc = filterDesc;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFilterCategory() {
		return filterCategory;
	}

	public void setFilterCategory(String filterCategory) {
		this.filterCategory = filterCategory;
	}

	public List<CustomExpressionDTO> getExpressions() {
		return expressions;
	}

	public void setExpressions(List<CustomExpressionDTO> expressions) {
		this.expressions = expressions;
	}
	
	public Byte getDataFlag() {
		return dataFlag;
	}

	public void setDataFlag(Byte dataFlag) {
		this.dataFlag = dataFlag;
	}

	public String getEntityClass() {
		return entityClass;
	}

	public void setEntityClass(String entityClass) {
		this.entityClass = entityClass;
	}

	public CustomFilterDTO(){
		
	}
	

	public String getShare() {
		return share;
	}

	public void setShare(String share) {
		this.share = share;
	}

	public CustomFilterDTO(Long filterId, String filterName, String filterDesc,
			String entityClass, Long userId, String userName,
			String filterCategory, List<CustomExpressionDTO> expressions) {
		super();
		this.filterId = filterId;
		this.filterName = filterName;
		this.filterDesc = filterDesc;
		this.entityClass = entityClass;
		this.userId = userId;
		this.userName = userName;
		this.filterCategory = filterCategory;
		this.expressions = expressions;
	}

	public Long[] getShareGroupIds() {
		
		
		
		if(shareGroupIdString!=null && shareGroupIdString.length()>0){
			
			try{
				
				String [] arrString=shareGroupIdString.split(",");
				Long [] arrLong=new Long[arrString.length];
				
				int i=0;
				
				
				for(String s:arrString){
					arrLong[i]=Long.parseLong(s);
					i++;
				}
				
				return arrLong;
				
				
				
			}
			catch(Exception ex){
				
				LOGGER.error("getShareGroupId", ex);
			}
			
			
		}

		
		return shareGroupIds;
	}

	public void setShareGroupIds(Long[] shareGroupIds) {
		this.shareGroupIds = shareGroupIds;
	}

	public String getShareGroupIdString() {
		return shareGroupIdString;
	}

	public void setShareGroupIdString(String shareGroupIdString) {
		this.shareGroupIdString = shareGroupIdString;
	}
	
	
	
}
