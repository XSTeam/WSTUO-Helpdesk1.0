package com.wstuo.common.config.historyData.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Column;

import com.wstuo.common.entity.BaseEntity;

/**
 * 历史数据实体
 * @author Will
 *
 */

@SuppressWarnings("serial")
@Entity
@Table(name="T_HistoryData")
public class HistoryData extends BaseEntity<Long> {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long historyNo; //历史数据编号
	@Column(length=128,nullable=false)
	private String entityName; //实体名称
	@Column(nullable=true)
	private Long entityId;//实体ID引用 
	@Column(nullable=true)
	private String entityTitle;//数据标题
	@Lob
	private String jsonData;//实体数据的JSON格式保存

	public Long getHistoryNo() {
		return historyNo;
	}
	public void setHistoryNo(Long historyNo) {
		this.historyNo = historyNo;
	}
	public String getEntityName() {
		return entityName;
	}
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	
	public String getEntityTitle() {
		return entityTitle;
	}
	public void setEntityTitle(String entityTitle) {
		this.entityTitle = entityTitle;
	}
	public String getJsonData() {
		return jsonData;
	}
	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}
	
}

