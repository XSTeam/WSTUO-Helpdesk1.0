package com.wstuo.common.config.server.dao;

import com.wstuo.common.config.server.entity.ServerUrl;
import com.wstuo.common.dao.BaseDAOImplHibernate;
/**
 * ServerUrl DAO class
 * @author will
 *
 */
public class ServerUrlDAO extends BaseDAOImplHibernate<ServerUrl> implements IServerUrlDAO {

}
