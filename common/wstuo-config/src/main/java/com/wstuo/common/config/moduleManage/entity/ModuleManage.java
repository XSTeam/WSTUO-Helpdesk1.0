package com.wstuo.common.config.moduleManage.entity;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Column;
/**
 * 模块属性
 * @author QXY
 *
 */
@Entity
public class ModuleManage {
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
	private Long moduleId;
	@Column(unique=true)
	private String moduleName;//模块名称
	private Date createTime=new Date();//日期
	private String version="1.0";//版本
	private Long showSort;//显示顺序
	private String createName;//作者
	private String title; //标题
	@Lob
	private String description;//描述
	protected Byte dataFlag = 0; // 0:normal, 1:system, 2:test,99:delete
	private String pid;
	
	
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public Long getModuleId() {
		return moduleId;
	}
	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public Long getShowSort() {
		return showSort;
	}
	public void setShowSort(Long showSort) {
		this.showSort = showSort;
	}
	public String getCreateName() {
		return createName;
	}
	public void setCreateName(String createName) {
		this.createName = createName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Byte getDataFlag() {
		return dataFlag;
	}
	public void setDataFlag(Byte dataFlag) {
		this.dataFlag = dataFlag;
	}
	
	
	
}
