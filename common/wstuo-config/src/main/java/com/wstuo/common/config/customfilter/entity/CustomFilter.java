package com.wstuo.common.config.customfilter.entity;

import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.wstuo.common.entity.BaseEntity;
import com.wstuo.common.security.entity.Organization;
import com.wstuo.common.security.entity.User;

/**
 * entity of CustomFilter
 * @author Mars
 * */
@SuppressWarnings( {"serial",
    "rawtypes"
} )
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CustomFilter extends BaseEntity{
	
	@Id
    @GeneratedValue( strategy = GenerationType.AUTO )
	private Long filterId;
	@Column(nullable=false)
	private String filterName;//过滤器名称
	private String filterDesc;//过滤器描述
	private String entityClass;
	@ManyToOne
	private User filterOwner;//过滤器创建者
	@Column(name="o_share")
	private String share;//是否共享
	@ManyToMany
	private List<Organization> shareGroups;
	@OneToMany( cascade={CascadeType.REMOVE, CascadeType.REFRESH})
	private List<CustomExpression> expressions;//过滤器表达式
	private String filterCategory;//过滤器类别
	
	public CustomFilter(){
		super();
	}
	
	public Long getFilterId() {
		return filterId;
	}

	public void setFilterId(Long filterId) {
		this.filterId = filterId;
	}

	public String getFilterName() {
		return filterName;
	}

	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}

	public String getFilterDesc() {
		return filterDesc;
	}

	public void setFilterDesc(String filterDesc) {
		this.filterDesc = filterDesc;
	}


	public List<CustomExpression> getExpressions() {
		return expressions;
	}

	public void setExpressions(List<CustomExpression> expressions) {
		this.expressions = expressions;
	}

	public String getFilterCategory() {
		return filterCategory;
	}

	public void setFilterCategory(String filterCategory) {
		this.filterCategory = filterCategory;
	}

	public String getEntityClass() {
		return entityClass;
	}

	public void setEntityClass(String entityClass) {
		this.entityClass = entityClass;
	}

	public User getFilterOwner() {
		return filterOwner;
	}

	public void setFilterOwner(User filterOwner) {
		this.filterOwner = filterOwner;
	}

	
	public String getShare() {
		return share;
	}

	public void setShare(String share) {
		this.share = share;
	}

	public List<Organization> getShareGroups() {
		return shareGroups;
	}

	public void setShareGroups(List<Organization> shareGroups) {
		this.shareGroups = shareGroups;
	}
}
