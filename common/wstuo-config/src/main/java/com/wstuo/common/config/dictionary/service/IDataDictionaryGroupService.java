package com.wstuo.common.config.dictionary.service;

import com.wstuo.common.config.dictionary.dto.DataDictionaryGroupDTO;
import com.wstuo.common.config.dictionary.dto.DataDictionaryGroupQueryDTO;
import com.wstuo.common.dto.PageDTO;

import java.io.File;
import java.io.InputStream;
import java.util.List;

/**
 * 数据字典分组业务类接口
 * @author QXY
 */
public interface IDataDictionaryGroupService
{
	/**
	 * 分页查询字典分组.
	 * @param redto 查询DTO
	 * @return PageDTO 分页数据
	 */
	PageDTO findDictionaryGroupByPage(DataDictionaryGroupQueryDTO redto);
	
    /**
     * 保存字典分组.
     * @param dto 数据字典数据
     */
    void saveDataDictionaryGroup( DataDictionaryGroupDTO dto );

    /**
     * 删除字典分组.
     * @param no 分组编号
     */
    void removeDataDictionaryGroup( Long no );

    /**
     * 删除数据字典分组.
     * @param nos 字典分组编号.
     */
    void removeDataDictionaryGroups( Long[] nos );

    /**
     * 修改字典分组.
     * @param dto DTO
     * @return DataDictionaryGroupDTO 
     */
    DataDictionaryGroupDTO mergeDataDictionaryGroup( DataDictionaryGroupDTO dto );

    /**
     * 修改字典分组.
     * @param dto DTO
     */
    void updateDataDictionaryGroup( DataDictionaryGroupDTO dto );

    /**
     * 批量修改.
     * @param dtos 字典分组DTO集合
     */
    void mergeAllDataDictionaryGroup( List<DataDictionaryGroupDTO> dtos );

    /**
     * 查找数据字典分组.
     * @return List<DataDictionaryGroupDTO> 字典分组集合
     */
    List<DataDictionaryGroupDTO> findAllDataDictionaryGroup(  );

    /**
     * 根据ID查找数据字典分组.
     * @param no 字典分组编号
     */
    DataDictionaryGroupDTO findDataDictionaryGroupById( Long no );

    /**
     * 根据名称查找字典列表.
     * @param name 名称
     * @return List<DataDictionaryGroupDTO> 字典分组集合
     */
    List<DataDictionaryGroupDTO> findDataDictionaryGroupByName( String name );
    
    /**
     * 导入数据字典
     * @param importFile 导入文件流
     * @return String
     */
    String importDataDictionaryGroup(File importFile);
    
    /**
     * 导出数据
     * @param redto
     * @return InputStream
     */
    InputStream exportDataDictionaryGroup(DataDictionaryGroupQueryDTO redto);
    
    List<DataDictionaryGroupDTO> findAllDataDictionaryGroupByType(String groupType) ;
}
