package com.wstuo.common.config.attachment.service;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import com.wstuo.common.config.attachment.dto.AttachmentDTO;
import com.wstuo.common.config.attachment.dto.AttachmentDownloadDTO;
import com.wstuo.common.config.attachment.entity.Attachment;
import com.wstuo.common.dto.PageDTO;

/**
 * 附件Service类接
 * @author QXY
 *  date 2011-02-11
 */
public interface IAttachmentService {
	/**
	 * 根据附件编号查询附件信息
	 * @param attachmentId
	 */
	Attachment findAttachmentById(Long attachmentId);
	/**
	 * 保存附件.
	 * @param dto 附件DTO
	 */
	void save(AttachmentDTO dto);
	
	/**
	 * 删除附件.
	 * @param id 附件编号.
	 */
	void delete(Long id);
	
	/**
	 * 批量删除附件.
	 * @param ids 附件编号数组.
	 */
	void delete(Long[] ids);
	

    /**
     * 保存附件并返回LIST
     * @param attachmentStr
     * @param type
     * @return  List<Attachment>
     */
    List<Attachment> saveAttachment(String attachmentStr,String type);
    /**
     * 查询附件列表
     * @param qdto
     * @param start
     * @param limit
     * @param sidx
     * @param sord
     * @return PageDTO
     */
    PageDTO findByPager(AttachmentDTO qdto,int start, int limit,String sidx , String sord);
    /**
     * 获取图片流
     * @param aid
     * @return InputStream
     */
    InputStream getImageStream(Long aid);

   /**
    * 删除附件（含返回值）
    * @param id
    * @return boolean
    */
	boolean deleteReturn(Long id);
	/**
     * 根据文件后缀获取ContentType
     * @param fileExt
     */
    String switchContentType(String fileExt);
    /**
	 * 附件下载
	 * @param attachment
	 * @return AttachmentDownloadDTO
	 */
    public AttachmentDownloadDTO downloadAttachment(Attachment attachment);
    
    /**
	 * 删除附件
	 * @param downloadAttachmentId
	 */
	public void deleteAttachment(Long downloadAttachmentId);
	
	/**
	 * 判断文件是否存在
	 * @param imagePath
	 * @return boolean
	 */
	public boolean isFileExit(String imagePath);
	/**
	 * 对比列表，移除重复的.
	 * 
	 * @param attachments
	 * @param newAttachments
	 */
	List<Attachment> removeDuplicateAttachment(List<Attachment> attachments, List<Attachment> newAttachments);
	
	File readFileByUrl(String fileUrl);
}
