package com.wstuo.common.config.updatelevel.service;

import com.wstuo.common.config.updatelevel.dto.UpdateLevelDTO;
import com.wstuo.common.config.updatelevel.dto.UpdateLevelQueryDTO;
import com.wstuo.common.config.updatelevel.entity.UpdateLevel;
import com.wstuo.common.dto.PageDTO;

/**
 * IUpdateLevelService interface class
 * @author QXY
 *
 */
public interface IUpdateLevelService {
	/**
	 * 分页查询升级级别
	 * @param qdto
	 * @return PageDTO
	 */
	PageDTO findPagerUpdateLevel(UpdateLevelQueryDTO qdto);
	/**
	 * 保存升级级别
	 * @param dto
	 */
	void saveUpdateLevel(UpdateLevelDTO dto);
	/**
	 * 更新升级级别
	 * @param dto
	 */
	void mergeUpdateLevel(UpdateLevelDTO dto);
	/**
	 * 删除升级级别
	 * @param ulIds
	 * @return boolean
	 */
	boolean deleteUpdateLevel(Long[] ulIds);
	
	/**
	 * 根据ID查找升级级别.
	 * @param id
	 * @return UpdateLevel
	 */
	UpdateLevel findById(Long id);
}
