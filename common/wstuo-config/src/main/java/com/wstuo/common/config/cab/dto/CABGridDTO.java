package com.wstuo.common.config.cab.dto;

/**
 * CAB列表DTO
 * @author WSTUO
 *
 */
public class CABGridDTO {
	private Long cabId;
	private String cabName;
	private String cabDesc;
	public Long getCabId() {
		return cabId;
	}
	public void setCabId(Long cabId) {
		this.cabId = cabId;
	}
	public String getCabName() {
		return cabName;
	}
	public void setCabName(String cabName) {
		this.cabName = cabName;
	}
	public String getCabDesc() {
		return cabDesc;
	}
	public void setCabDesc(String cabDesc) {
		this.cabDesc = cabDesc;
	}
	
}
