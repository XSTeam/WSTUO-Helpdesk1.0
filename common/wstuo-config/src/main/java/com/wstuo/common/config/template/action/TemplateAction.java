package com.wstuo.common.config.template.action;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.config.template.dto.TemplateDTO;
import com.wstuo.common.config.template.dto.TemplateRequestDTO;
import com.wstuo.common.config.template.entity.Template;
import com.wstuo.common.config.template.service.ITemplateService;
import com.wstuo.common.dto.PageDTO;


/**
 * 内容模板Action
 * @author WSTUO
 *
 */
@SuppressWarnings("serial")
public class TemplateAction extends ActionSupport{
	/**
	 * 内容模板DTO
	 */
	private TemplateDTO templateDTO;
	
	private List<TemplateDTO> templateDTOList;
	/**
	 * 内容模板业务接口
	 */
	@Autowired
	private ITemplateService templateService;
	/**
	 * 页数
	 */
	private int page = 1;
	/**
	 * 记录数
	 */
    private int rows = 10;
    /**
     * 排序规则
     */
    private String sord;
    /**
     * 排序的属性
     */
    private String sidx;
    /**
     * 分页DTO
     */
    private PageDTO templates;
    /**
     * 请求模板DTO
     */
	private TemplateRequestDTO templateRequestDTO;
	/**
	 * 内容模板实体
	 */
    private List<Template> lis;
    /**
     * 模板ID数组
     */
	private Long[] templateIds;

	public List<TemplateDTO> getTemplateDTOList() {
		return templateDTOList;
	}
	public void setTemplateDTOList(List<TemplateDTO> templateDTOList) {
		this.templateDTOList = templateDTOList;
	}
	public TemplateDTO getTemplateDTO() {
		return templateDTO;
	}
	public void setTemplateDTO(TemplateDTO templateDTO) {
		this.templateDTO = templateDTO;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public PageDTO getTemplates() {
		return templates;
	}
	public void setTemplates(PageDTO templates) {
		this.templates = templates;
	}
	public TemplateRequestDTO getTemplateRequestDTO() {
		return templateRequestDTO;
	}
	public void setTemplateRequestDTO(TemplateRequestDTO templateRequestDTO) {
		this.templateRequestDTO = templateRequestDTO;
	}
	public List<Template> getLis() {
		return lis;
	}
	public void setLis(List<Template> lis) {
		this.lis = lis;
	}
	public Long[] getTemplateIds() {
		return templateIds;
	}
	public void setTemplateIds(Long[] templateIds) {
		this.templateIds = templateIds;
	}

	/**
	 * 获取所有内容模板
	 * @return 内容模板分页DTO
	 */
	public String findPager(){
		int start = (page - 1) * rows;
		templates=templateService.findTemplatePager(templateDTO, start, rows, sord, sidx);
		templates.setRows(rows);
		templates.setPage(page);
		return "pageDTO";
	}
	
	/**
	 * 根据内容模板类型查询
	 * @return  内容模板实体
	 */
	public String findByTemplateType(){
		lis=templateService.findByTemplateType(templateDTO.getTemplateType());
		return "templateList";
	}
	
	/**
	 * 根据内容模板ID查询出内容详细信息
	 * @return 内容模板实体
	 */
	public String findByTemplateId(){
		templateDTO=templateService.findByTemplateId(templateDTO.getTemplateId());
		return "templateDTO";
	}
	
	/**
	 * 根据ID删除内容模板
	 * @return 无
	 */
	public String delTemplate(){
		templateService.delTemplate(templateIds);
		return SUCCESS;
	}
	
	/**
	 * 编辑模板名称 
	 * @return 无
	 */
	public String editTemplateName(){
		templateService.editTemplateName(templateDTO);
		return SUCCESS;
	}
	
	public String findAllTemplate(){
		templateDTOList = templateService.findAllTemplate(templateDTO);
		return "findAllTemplate";
	}
	
}
