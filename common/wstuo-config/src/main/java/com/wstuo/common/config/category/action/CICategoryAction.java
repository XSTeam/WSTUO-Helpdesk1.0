/**
*author:Eileen
*date:010-9-14
*description:
*/
package com.wstuo.common.config.category.action;


import java.io.File;
import java.io.InputStream;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.config.category.dto.CategoryDTO;
import com.wstuo.common.config.category.dto.CategoryTreeViewDTO;
import com.wstuo.common.config.category.service.ICICategoryService;
import com.wstuo.common.exception.ApplicationException;
import com.wstuo.common.security.service.IRoleService;

/**
 * 配置项分类Action类
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class CICategoryAction extends ActionSupport {

    @Autowired
    private ICICategoryService cicategoryService;
    @Autowired
    private IRoleService roleService;
    private CategoryDTO categoryDTO;
    private CategoryTreeViewDTO configurationItemsTreeDto;
    private File upload;
    private String uploadFileName;
    private Long categoryNo;
    private String effect;
    private InputStream exportStream;
    private String fileName="";
    private File importFile;
    private Long cno;
    private Boolean findAll=true;//是否查询全部
    private String userName;
    private Boolean simpleDto=false;
    private List<CategoryDTO> categorys;
    private Long categourType;
    private Long[] categoryEavs;
    private Long[] eavNos;
    
    
    
	public Long[] getEavNos() {
		return eavNos;
	}

	public void setEavNos(Long[] eavNos) {
		this.eavNos = eavNos;
	}

	public Long[] getCategoryEavs() {
		return categoryEavs;
	}

	public void setCategoryEavs(Long[] categoryEavs) {
		this.categoryEavs = categoryEavs;
	}

	public Long getCategourType() {
		return categourType;
	}

	public void setCategourType(Long categourType) {
		this.categourType = categourType;
	}
	private String msg;
    
    public CategoryDTO getCategoryDTO() {
		return categoryDTO;
	}

	public void setCategoryDTO(CategoryDTO categoryDTO) {
		this.categoryDTO = categoryDTO;
	}

	public CategoryTreeViewDTO getConfigurationItemsTreeDto() {
		return configurationItemsTreeDto;
	}

	public void setConfigurationItemsTreeDto(CategoryTreeViewDTO configurationItemsTreeDto) {
		this.configurationItemsTreeDto = configurationItemsTreeDto;
	}

	public File getUpload() {
		return upload;
	}

	public void setUpload(File upload) {
		this.upload = upload;
	}

	public String getUploadFileName() {
		return uploadFileName;
	}

	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}

	public Long getCategoryNo() {
		return categoryNo;
	}

	public void setCategoryNo(Long categoryNo) {
		this.categoryNo = categoryNo;
	}

	public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}

	public InputStream getExportStream() {
		return exportStream;
	}

	public void setExportStream(InputStream exportStream) {
		this.exportStream = exportStream;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public File getImportFile() {
		return importFile;
	}

	public void setImportFile(File importFile) {
		this.importFile = importFile;
	}

	public Long getCno() {
		return cno;
	}

	public void setCno(Long cno) {
		this.cno = cno;
	}

	public Boolean getFindAll() {
		return findAll;
	}

	public void setFindAll(Boolean findAll) {
		this.findAll = findAll;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Boolean getSimpleDto() {
		return simpleDto;
	}

	public void setSimpleDto(Boolean simpleDto) {
		this.simpleDto = simpleDto;
	}

	public List<CategoryDTO> getCategorys() {
		return categorys;
	}

	public void setCategorys(List<CategoryDTO> categorys) {
		this.categorys = categorys;
	}

	public Long[] getCnos() {
		return cnos;
	}

	public void setCnos(Long[] cnos) {
		this.cnos = cnos;
	}

	public Long getParentEventId() {
		return parentEventId;
	}

	public void setParentEventId(Long parentEventId) {
		this.parentEventId = parentEventId;
	}

	public List<CategoryTreeViewDTO> getConfigurationItemsTreeDtoList() {
		return configurationItemsTreeDtoList;
	}
	private Long cnos[];
    private Long parentEventId;
    private List<CategoryTreeViewDTO> configurationItemsTreeDtoList;
   

	public void setConfigurationItemsTreeDtoList(
			List<CategoryTreeViewDTO> configurationItemsTreeDtoList) {
		this.configurationItemsTreeDtoList = configurationItemsTreeDtoList;
	}
	
	

	public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * view configuration category tree
     */
    public String getConfigurationCategoryTree() {
    	
    	if(parentEventId==null || parentEventId ==0){
    		configurationItemsTreeDto=cicategoryService.findCICategoryTree("CICategory", findAll, userName,simpleDto,parentEventId);
    		return SUCCESS;
    	}else{
    		configurationItemsTreeDtoList = cicategoryService.findCICategoryTreeSub("CICategory", findAll, userName,simpleDto,parentEventId);
    		return "configurationItemsTreeDtoList";
    	}
    	
    }

    /**
     * save configuration category tree
     */
    public String save() {

        cicategoryService.saveCICategory(categoryDTO);

        return SUCCESS;
    }
    
    /**
     * 判断分类是否存在
     * @return
     */
    public String isCategoryExisted() {
        boolean con = cicategoryService.isCategoryExisted(categoryDTO);
        msg = con ? "isExist" : "";
        return "msg";
    }
    
    public String isCategoryExistdOnEdit() {
        boolean con = cicategoryService.isCategoryExistdOnEdit(categoryDTO);
        msg = con ? "isExist" : "";
        return "msg";
    }
    

    /**
     * remove configuration category tree
     */
    public String remove() {

    	try{
    		
	    	roleService.refreshRole("CONFIGUREITEMCATEGORY_RES"+categoryDTO.getCno());
	        cicategoryService.removeCICategory(categoryDTO.getCno());

    	}catch(Exception ex){
    		
    		throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE",ex);
    	}
    	
        return SUCCESS;
    }

    /**
     * update configuration category tree
     */
    public String update() {

        cicategoryService.updateCICategory(categoryDTO);

        return SUCCESS;
    }

    /**
     * change configuration category tree
     */
    public String changeParent() {

        cicategoryService.changeCICategory(categoryDTO);

        return SUCCESS;
    }
    /**
     * copy configuration category tree
     */
    public String copyCICategory() {

        cno=cicategoryService.copyCICategory(categoryDTO);

        return SUCCESS;
    }

    /**
     * 根据配置项分类编号查询分类信息
     */
    public String findCICategoryById(){
    	categoryDTO=cicategoryService.findCICategoryById(categoryNo);
    	return "configurationItemsDto";
    }
    
    /**
     * 导出配置项分类
     */
    public String exportCiCategory() {
    	fileName="ConfigureItemCategory.csv";
        exportStream = cicategoryService.exportCiCategory("CICategory");
    	return "exportFileSuccessful";
    }
    /**
     * 导入配置项分类
     */
    public String importCiCategory(){
    	
    	effect=cicategoryService.importCiCategory(importFile);
    	return "importResult";
    }
    
    /**
     * 根据分类编号查询子节点
     */
    public String findSubCategorys(){
    	categorys=cicategoryService.findSubCategorys(cno);
    	return "categorys";
    }
    
    public String findSubCategorysByResType(){
    	categorys=cicategoryService.findSubCategorysByResType(cno);
    	return "categorys";
    }
    /**
     * 根据数组查询节点数据 
     * */
    public String findSubCategoryArray(){
    	categorys=cicategoryService.findSubCategorysArray(cnos);
    	
    	return "categorys";
    }
    
    public String findByCategoryId(){
    	categourType=cicategoryService.findCategoryId(categoryNo);
    	return "categourType";
    }
    /**
	 * 删除扩展属性
	 * @return String
	 */
    public String deleteEavEntity() {
    	cicategoryService.deleteEavEntity(eavNos);
    	return SUCCESS;
    }
    

}