package com.wstuo.common.config.authorization.action;

import org.springframework.beans.factory.annotation.Autowired;

import com.wstuo.common.config.authorization.dto.AuthorizationDTO;
import com.wstuo.common.config.authorization.entity.Authorization;
import com.wstuo.common.config.authorization.service.IAuthorizationService;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.security.action.SupplierAction;

/**
 * 授权管理
 * @author liufei
 */
@SuppressWarnings("serial")
public class AuthorizationAction extends SupplierAction{
	@Autowired
	private IAuthorizationService authorizationService; 
	private AuthorizationDTO authdto;
	private Long aid;
	private Long[] ids;
	private String atype="";
	private Authorization entity;
	private PageDTO authList;
	private int page = 1;
    private int rows = 10;
    private String sidx;
    

	public Authorization getEntity() {
		return entity;
	}
	public void setEntity(Authorization entity) {
		this.entity = entity;
	}
	public String getAtype() {
		return atype;
	}
	public void setAtype(String atype) {
		this.atype = atype;
	}
	public Long[] getIds() {
		return ids;
	}
	public void setIds(Long[] ids) {
		this.ids = ids;
	}
	public PageDTO getAuthList() {
		return authList;
	}
	public void setAuthList(PageDTO authList) {
		this.authList = authList;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	private String sord;
	
	
	
	public AuthorizationDTO getAuthdto() {
		return authdto;
	}
	public void setAuthdto(AuthorizationDTO authdto) {
		this.authdto = authdto;
	}
	public Long getAid() {
		return aid;
	}
	public void setAid(Long aid) {
		this.aid = aid;
	}
	
	
	/**
	 * 分页查询任务
	 * @return String
	 */
	public String findPagerAuthorization(){
		int start = ( page - 1 ) * rows;
		authList =authorizationService.findPagerAuthorization(authdto,start,rows,sidx,sord);
		authList.setPage( page );
		authList.setRows( rows );
        return SUCCESS;
	}
	
	/**
	 * 授权信息保存
	 * @return String
	 * */
	public String AuthorizationSave(){
		authorizationService.Save(authdto);
		
		return SUCCESS;
	}
	/**
	 * 授权信息删除
	 * @return String
	 * */
	public String AuthorizationDelete(){
		authorizationService.delete(ids);
		return SUCCESS;
	}
	/**
	 * 授权信息编辑
	 * */
	public String AuthorizationUpdate(){
		authorizationService.update(authdto);
		
		return SUCCESS;
	}
	/**
	 * 更具类型查询授权信息
	 * */
	public String findByAuthType(){
		entity=authorizationService.findByAuthType(atype);

		return "authorization";
	}
}
