package com.wstuo.common.config.dictionary.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.config.dictionary.dto.DataDictionaryGroupQueryDTO;
import com.wstuo.common.config.dictionary.entity.DataDictionaryGroup;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.StringUtils;

/**
 * 数据字典分组DAO
 * @author QXY
 */
public class DataDictionaryGroupDAO
    extends BaseDAOImplHibernate<DataDictionaryGroup>
    implements IDataDictionaryGroupDAO
{
   
	
	/**
	 * 分页查询字典分组.
	 * @param rqdto 查询DTO
	 * @return PageDTO 分页数据
	 */
    public PageDTO findpager( DataDictionaryGroupQueryDTO rqdto )
    {
    	
        final DetachedCriteria dc = DetachedCriteria.forClass( DataDictionaryGroup.class );
        
        int start = 0;
        int limit = 0;

        if ( rqdto != null )
        {
            start = rqdto.getStart(  );
            limit = rqdto.getLimit(  );
            if (rqdto.getGroupNo() !=null && rqdto.getGroupNo()!=0 && StringUtils.hasText(rqdto.getGroupNo().toString())){
            	dc.add( Restrictions.eq( "groupNo",rqdto.getGroupNo()));
            }
            

	            if ( StringUtils.hasText( rqdto.getGroupName() ) )
	            {
	                dc.add( Restrictions.like( "groupName",
	                                           rqdto.getGroupName(  ),
	                                           MatchMode.ANYWHERE ) );
	            }
	            if ( StringUtils.hasText( rqdto.getGroupCode() ) )
	            {
	                dc.add( Restrictions.like( "groupCode",
	                                           rqdto.getGroupCode(  ),
	                                           MatchMode.ANYWHERE ) );
	            }
	            if ( StringUtils.hasText( rqdto.getGroupCode() ) )
	            {
	                dc.add( Restrictions.eq( "groupType",
	                                           rqdto.getGroupType()) );
	            }
	            //排序
	            if(StringUtils.hasText(rqdto.getSord())&&StringUtils.hasText(rqdto.getSidx())){
	                if(rqdto.getSord().equals("desc"))
	                	dc.addOrder(Order.desc(rqdto.getSidx()));
	                else
	                	dc.addOrder(Order.asc(rqdto.getSidx()));
	            }else{
	            	
	            	dc.addOrder(Order.desc("groupNo"));
	            }
	            
        }
        return super.findPageByCriteria( dc, start, limit );
    }
    
    

    /**
     * 查询数据字典分组.
     * @return List<DataDictionaryGroup> 数据字典集合.
     */
    @SuppressWarnings( "unchecked" )
    public List<DataDictionaryGroup> findDataDictionaryGroups(  )
    {
        String hql = "from DataDictionaryGroup";

        return getHibernateTemplate(  ).find( hql );
    }
}
