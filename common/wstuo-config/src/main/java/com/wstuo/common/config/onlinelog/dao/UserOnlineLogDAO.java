package com.wstuo.common.config.onlinelog.dao;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.config.onlinelog.dto.UserOnlineLogQueryDTO;
import com.wstuo.common.config.onlinelog.entity.UserOnlineLog;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.StringUtils;


/**
 * 用户在线日志DAO类.
 * @author QXY date 2011-02-11
 *
 */
public class UserOnlineLogDAO extends BaseDAOImplHibernate<UserOnlineLog>
    implements IUserOnlineLogDAO {

   
	/**
	 * 根据会话ID查找在线日志
	 * @param sid 会话编号
	 * @return UserOnlineLog
	 */
    public UserOnlineLog findBySessionId(String sid) {

        return super.findUniqueBy("sessionId", sid);
    }

    /**
     * 分页查找用户在线日志
     * @param useronlinelogQueryDTO 查询DTO
     * @param start 开始数据行
     * @param limit 每页数据条数
     * @return PageDTO 分页数据
     */
    public PageDTO findPager(UserOnlineLogQueryDTO useronlinelogQueryDTO,
        int start, int limit) {

        final DetachedCriteria dc = DetachedCriteria.forClass(UserOnlineLog.class);

        
        if (useronlinelogQueryDTO != null) {

            if (useronlinelogQueryDTO.getOnlineTimeStart() != null) {

                dc.add(Restrictions.gt("onlineTime",
                        useronlinelogQueryDTO.getOnlineTimeStart()));
            }

            if (useronlinelogQueryDTO.getOnlineTimeEnd() != null) {

                dc.add(Restrictions.lt("onlineTime",
                        useronlinelogQueryDTO.getOnlineTimeEnd()));
            }

            if (useronlinelogQueryDTO.getOfflineTimeStart() != null) {

                dc.add(Restrictions.gt("offlineTime",
                        useronlinelogQueryDTO.getOfflineTimeStart()));
            }

            if (useronlinelogQueryDTO.getOfflineTimeEnd() != null) {

                dc.add(Restrictions.lt("offlineTime",
                        useronlinelogQueryDTO.getOfflineTimeEnd()));
            }

            if (StringUtils.hasText(useronlinelogQueryDTO.getUserName())) {
                dc.add(Restrictions.like("userName",
                        useronlinelogQueryDTO.getUserName(),
                        MatchMode.ANYWHERE));
            }
            
            if(useronlinelogQueryDTO.getCompanyNo()!=null){
            	  dc.add(Restrictions.eq("companyNo", useronlinelogQueryDTO.getCompanyNo()));
            }
            //查询在线用户
            if(useronlinelogQueryDTO.getIsSearchOffLine()){
            	dc.add(Restrictions.and(Restrictions.isNotNull("onlineTime"), Restrictions.isNull("offlineTime")));
            }
            
            //排序
            if(StringUtils.hasText(useronlinelogQueryDTO.getSord())&&StringUtils.hasText(useronlinelogQueryDTO.getSidx())){
                if(useronlinelogQueryDTO.getSord().equals("desc"))
                	dc.addOrder(Order.desc(useronlinelogQueryDTO.getSidx()));
                else
                	dc.addOrder(Order.asc(useronlinelogQueryDTO.getSidx()));
            }else{
            	
            	dc.addOrder( Order.desc( "offlineTime" ) );
            }
            
        }
        
        return super.findPageByCriteria(dc, start, limit);
    }
}