package com.wstuo.common.config.onlinelog.dto;

import com.wstuo.common.dto.AbstractValueObject;

import java.util.Date;

/***
 * UserOnlineLog QueryDTO class.
 * 
 * @author spring date 2010-10-11
 */
public class UserOnlineLogQueryDTO extends AbstractValueObject {
	/**
	 * userName
	 */
	private String userName;

	/**
	 * OnlineTime Start
	 */
	private Date onlineTimeStart;

	/**
	 * OfflineTime End
	 */
	private Date onlineTimeEnd;

	/**
	 * OfflineTime Start
	 */
	private Date offlineTimeStart;

	/**
	 * OfflineTime Start
	 */
	private Date offlineTimeEnd;

	/**
	 * start
	 */
	private Integer start;

	/**
	 * limit
	 */
	private Integer limit;

	private Long companyNo;

	private String sidx;
	private String sord;

	private String userFullName;

	private Boolean isSearchOffLine = false;
	
	public Boolean getIsSearchOffLine() {
		return isSearchOffLine;
	}

	public void setIsSearchOffLine(Boolean isSearchOffLine) {
		this.isSearchOffLine = isSearchOffLine;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public Long getCompanyNo() {
		return companyNo;
	}

	public void setCompanyNo(Long companyNo) {
		this.companyNo = companyNo;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getOnlineTimeStart() {
		return onlineTimeStart;
	}

	public void setOnlineTimeStart(Date onlineTimeStart) {
		this.onlineTimeStart = onlineTimeStart;
	}

	public Date getOnlineTimeEnd() {
		return onlineTimeEnd;
	}

	public void setOnlineTimeEnd(Date onlineTimeEnd) {
		this.onlineTimeEnd = onlineTimeEnd;
	}

	public Date getOfflineTimeStart() {
		return offlineTimeStart;
	}

	public void setOfflineTimeStart(Date offlineTimeStart) {
		this.offlineTimeStart = offlineTimeStart;
	}

	public Date getOfflineTimeEnd() {
		return offlineTimeEnd;
	}

	public void setOfflineTimeEnd(Date offlineTimeEnd) {
		this.offlineTimeEnd = offlineTimeEnd;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public String getUserFullName() {
		return userFullName;
	}

	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}

}
