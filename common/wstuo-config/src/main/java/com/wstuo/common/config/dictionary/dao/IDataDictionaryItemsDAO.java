package com.wstuo.common.config.dictionary.dao;

import java.util.List;


import com.wstuo.common.config.dictionary.dto.DataDictionaryQueryDTO;
import com.wstuo.common.config.dictionary.entity.DataDictionaryItems;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

/**
 * 数据字典DAO
 * @author QXY
 */
public interface IDataDictionaryItemsDAO
    extends IEntityDAO<DataDictionaryItems>{
	
	/**
	 * 分页查询数据字典信息.
	 * @param dataDictionaryQueryDTO 查询DTO�?
	 * @param start 开始数
	 * @param limit 每页数据条数
	 * @return PageDTO 分页数据
	 */
    PageDTO findPager( final DataDictionaryQueryDTO dataDictionaryQueryDTO,
    		int start, int limit , String sidx, String sord);
    /**
	 * 根据分组搜索Items
	 * @param groupCode
	 * @return List<DataDictionaryItems>
	 */
	List<DataDictionaryItems> findItemByGroupCode(String groupCode);
	
	
	/**
	 * 根据CODE和Name查看搜索
	 * @param groupCode
	 * @param name
	 * @return List<DataDictionaryItems>
	 */
	List<DataDictionaryItems> findItemByGroupCodeAndName(String groupCode,String name);
	
	/**
	 * 根据分组编码查询唯一的对象，如果未查询到返回null；
	 * @param groupName
	 * @param name
	 * @return
	 */
	DataDictionaryItems findUniqueByGroupCodeAndName(String groupCode,String name);
}
