package com.wstuo.common.config.systeminfo.dao;

import com.wstuo.common.config.systeminfo.entity.SystemInfo;
import com.wstuo.common.dao.IEntityDAO;

/**
 * SystemInfo DAO interface class
 * @author Will
 *
 */
public interface ISystemInfoDAO extends IEntityDAO<SystemInfo>{

}
