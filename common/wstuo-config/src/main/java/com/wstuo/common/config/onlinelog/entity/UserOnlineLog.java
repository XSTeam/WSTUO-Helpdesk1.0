package com.wstuo.common.config.onlinelog.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


/***
 * UserOnlineLog class.
 *
 * @author spring date 2010-10-11
 */
@Entity
public class UserOnlineLog { /**
     * UserOnlineLog ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * UserOnlineLog session ID
     */
    private String sessionId;

    /**
     * UserOnlineLog User
     */
    private String userName;

    /**
     *UserOnlineLog OnlineTime
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date onlineTime;

    /**
     *UserOnlineLog OfflineTime
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date offlineTime;

    /**
     * UserOnlineLog Period
     */
    @Transient
    private long period;

    /**
     * UserOnlineLog IP
     */
    private String ip;
    
    /**
     * UserOnlineLog CompanyNo
     */
    private Long companyNo;
    
    
    private String userFullName;
    
    
    public Long getCompanyNo() {
		return companyNo;
	}

	public void setCompanyNo(Long companyNo) {
		this.companyNo = companyNo;
	}

	public String getSessionId() {

        return sessionId;
    }

    public void setSessionId(String sessionId) {

        this.sessionId = sessionId;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

 
    public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getOnlineTime() {

        return onlineTime;
    }

    public void setOnlineTime(Date onlineTime) {

        this.onlineTime = onlineTime;
    }

    public Date getOfflineTime() {

        return offlineTime;
    }

    public void setOfflineTime(Date offlineTime) {

        this.offlineTime = offlineTime;
    }

    public long getPeriod() {
    	
         return period;
    }

    public void setPeriod(long period) {

        this.period = period;
    }

    public String getIp() {

        return ip;
    }

    public void setIp(String ip) {

        this.ip = ip;
    }

	public String getUserFullName() {
		return userFullName;
	}

	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}
	
	@Override  
	public int hashCode(){  
	    final int prime = 31;
	    int result = 1;
	    result = prime * result + ((sessionId == null) ? 0 : sessionId.hashCode());
	    result = prime * result + ((userName == null) ? 0 : userName.hashCode()); 
	    return result;
	}  
	@Override  
	public boolean equals(Object obj){
	    boolean result=true;
        if(this == obj)
            return result;
        if(obj == null)
            result=false;
        if(getClass() != obj.getClass())
            result=false;
        final UserOnlineLog other = (UserOnlineLog)obj;
        if(!sessionId.equals(other.sessionId)){
           result=false;
        }
        if(!userName.equals(other.userName)){
            result=false;
        }
        return result ;
	}
	

	@Override
	public String toString() {
		return "UserOnlineLog [id=" + id + ", sessionId=" + sessionId
				+ ", userName=" + userName + ", onlineTime=" + onlineTime
				+ ", offlineTime=" + offlineTime + ", period=" + period
				+ ", ip=" + ip + ", companyNo=" + companyNo + ", userFullName="
				+ userFullName + "]";
	}
}