package com.wstuo.common.config.onlinelog.dao;


import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.wstuo.common.config.onlinelog.dto.UserErrLogDTO;
import com.wstuo.common.config.onlinelog.entity.UserErrLog;
import com.wstuo.common.dao.BaseDAOImplHibernate;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.util.DaoUtils;

/**
 * 用户操作异常DAO类
 * @author WSTUO
 *
 */
public class UserErrLogDAO extends BaseDAOImplHibernate<UserErrLog> implements IUserErrLogDAO {
	/**
	 * 分页查询用户操作异常
	 * @param userErrLogDTO
	 * @param start
	 * @param limit
	 * @return PageDTO
	 */
	public PageDTO findUserErrLogPager(final UserErrLogDTO userErrLogDTO,int start, int limit,String sidx,String sord){
		DetachedCriteria dc = DetachedCriteria.forClass(UserErrLog.class);
		if(userErrLogDTO!=null){
			if(userErrLogDTO.getErrMsg()!=null){
				dc.add(Restrictions.like("errMsg", userErrLogDTO.getErrMsg(),MatchMode.ANYWHERE));
			}
			
			if(userErrLogDTO.getErrCause()!=null){
				dc.add(Restrictions.like("errCause", userErrLogDTO.getErrCause(),MatchMode.ANYWHERE));
			}
			
			if(userErrLogDTO.getUserName()!=null){
				
				dc.add(Restrictions.like("userName", userErrLogDTO.getUserName(),MatchMode.ANYWHERE));
			}
		}
		//排序
        dc = DaoUtils.orderBy(sidx, sord, dc);
		return super.findPageByCriteria(dc, start, limit);
	};
	
}
