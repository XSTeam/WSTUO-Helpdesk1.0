package com.wstuo.common.config.category.entity;

import com.wstuo.common.entity.BaseEntity;

import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * Category Entity
 * @author QXY
 *
 */
@SuppressWarnings( {"serial","rawtypes"} )
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Inheritance( strategy = InheritanceType.SINGLE_TABLE )
public class Category extends BaseEntity{
	@Id
    private Long cno;
    private String cname;
    @ManyToOne()
    private Category parent;
    @OneToMany( mappedBy = "parent", fetch = FetchType.EAGER)
    private List<Category> children;
    private String iconPath;
    private Long categoryType;//存放扩展属性ID
    private String categoryRoot;
    private String categoryCodeRule;//配置项分类编号规则
    private String path;//路径
    private String pathAlias;//路径
    public Long getCno(  )
    {
        return cno;
    }

    public void setCno( Long cno )
    {
        this.cno = cno;
    }

    public String getCname(  )
    {
        return cname;
    }

    public void setCname( String cname )
    {
        this.cname = cname;
    }

    public String getIconPath(  )
    {
        return iconPath;
    }

    public void setIconPath( String iconPath )
    {
        this.iconPath = iconPath;
    }

    public Category getParent(  )
    {
        return parent;
    }

    public void setParent( Category parent )
    {
        this.parent = parent;
    }

    public List<Category> getChildren(  )
    {
        return children;
    }

    public void setChildren( List<Category> children )
    {
        this.children = children;
    }

	public Long getCategoryType() {
		return categoryType;
	}

	public void setCategoryType(Long categoryType) {
		this.categoryType = categoryType;
	}

	public String getCategoryRoot() {
		return categoryRoot;
	}

	public void setCategoryRoot(String categoryRoot) {
		this.categoryRoot = categoryRoot;
	}
    
	public String getCategoryCodeRule() {
		return categoryCodeRule;
	}

	public void setCategoryCodeRule(String categoryCodeRule) {
		this.categoryCodeRule = categoryCodeRule;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getPathAlias() {
		return pathAlias;
	}

	public void setPathAlias(String pathAlias) {
		this.pathAlias = pathAlias;
	}
    
}
