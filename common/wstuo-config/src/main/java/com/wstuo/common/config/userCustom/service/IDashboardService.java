package com.wstuo.common.config.userCustom.service;

import java.io.File;
import java.util.List;
import com.wstuo.common.config.userCustom.dto.DashboardDTO;
import com.wstuo.common.config.userCustom.dto.DashboardQueryDTO;
import com.wstuo.common.dto.PageDTO;

/**
 * 面板Service 接口类
 * @author WSTUO
 *
 */
public interface IDashboardService {
	/**
	 * 查询全部面板信息
	 * @return List<DashboardDTO>
	 */
	List<DashboardDTO> findDashboardAll();
	
	/**
	 * 分页查询
	 * @param queryDTO
	 * @return PageDTO
	 */
	PageDTO findPageDashboard(DashboardQueryDTO queryDTO);
	
	/**
	 * 添加面板
	 * @param dto
	 */
	void saveDashboard(final DashboardDTO dto);
	/**
	 * 编辑面板
	 * @param dto
	 */
	void editDashboard(final DashboardDTO dto);
	/**
	 * 删除面板
	 * @param ids
	 */
	void deteleDashboard(final Long[] ids);
	
	/**
	 * 面板模块导入
	 * @param importFile
	 * @return String
	 */
	String importDashboard(File importFile);
}
