package com.wstuo.common.config.backup.action;

import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.config.backup.service.IBuckUpFileService;
import com.wstuo.common.dto.PageDTO;
/**
 * 备份文件.
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class BuckUpFileAction extends ActionSupport{
	private PageDTO backupFiles=new PageDTO();
	private String effect;
	private String [] fileNames;
	private String fileName;
	@Autowired
	private IBuckUpFileService buckUpFileService;
	private InputStream downloadStream; //the input stream of the download file.
	
	public InputStream getDownloadStream() {
		return downloadStream;
	}

	public void setDownloadStream(InputStream downloadStream) {
		this.downloadStream = downloadStream;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String[] getFileNames() {
		return fileNames;
	}

	public void setFileNames(String[] fileNames) {
		this.fileNames = fileNames;
	}

	public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}


	public PageDTO getBackupFiles() {
		return backupFiles;
	}

	public void setBackupFiles(PageDTO backupFiles) {
		this.backupFiles = backupFiles;
	}

	/**
	 * 取得备份文件列表.
	 */
	public String showAllBackFiles(){
		backupFiles = buckUpFileService.showAllBackFiles();
		return "showAllBackkUpFiles";
	}

	/**
	 * 删除备份.
	 */
	public String deleteBackup(){
		buckUpFileService.deleteBackup(fileNames);
		return "effect";
	}
	
	/**
	 * 创建备份.
	 */
	public String createBackup(){
		effect = buckUpFileService.createBackup();
		return "effect";
	}
	
	/**
	 * 还原数据库.
	 */
	public String restoreBackup(){
		buckUpFileService.restoreBackup(fileName);
		return "effect";
	}
	
	/**
	 * 下载备份文件.
	 */
	public String download(){
		downloadStream = buckUpFileService.download(fileName);
		return "downloadSuccess";
		
	}
	
	
}
