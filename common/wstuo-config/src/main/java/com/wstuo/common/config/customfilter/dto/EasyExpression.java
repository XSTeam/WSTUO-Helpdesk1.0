package com.wstuo.common.config.customfilter.dto;

import org.hibernate.criterion.SimpleExpression;

public class EasyExpression extends SimpleExpression {

	public EasyExpression(String propertyName, Object value, String op) {
		super(propertyName, value, op);
	}

	public EasyExpression(String propertyName, Object value, String op,
			boolean ignoreCase) {
		super(propertyName, value, op, ignoreCase);
	}

}
