package com.wstuo.itsm.knowledge.action;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.wstuo.common.config.customfilter.dto.KeyTransferDTO;
import com.wstuo.common.config.template.dto.TemplateDTO;
import com.wstuo.common.config.template.service.ITemplateService;
import com.wstuo.itsm.knowledge.dto.CommentDTO;
import com.wstuo.itsm.knowledge.dto.KnowledgeCountResultDTO;
import com.wstuo.itsm.knowledge.dto.KnowledgeDTO;
import com.wstuo.itsm.knowledge.dto.KnowledgeQueryDTO;
import com.wstuo.itsm.knowledge.entity.KnowledgeInfo;
import com.wstuo.itsm.knowledge.service.IKnowledgeService;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;
import com.wstuo.common.util.StringUtils;


/**
 * 知识库Action类.
 * @author QXY
 */
@SuppressWarnings({"serial" })
public class KnowledgeAction extends ActionSupport {
    @Autowired
    private IKnowledgeService knowledgeService;
    @Autowired
    private ITemplateService templateService;
    private static final Logger LOGGER = Logger.getLogger(KnowledgeAction.class ); 
    private KnowledgeDTO knowledgeDto = new KnowledgeDTO();
    private KnowledgeQueryDTO knowledgeQueryDto = new KnowledgeQueryDTO();
    private List<KnowledgeDTO> list = new ArrayList<KnowledgeDTO>();
    private Long[] nos;
    private PageDTO knowledges;
    private int page = 1;
    private int rows = 10;
    private Long kid;
    private Long aid;
    private Long rid;
    private String flag;
    private int effect;
    private String title;
    private String content;
    private String sidx;
    private String sord;
    private File importFile;
    private String filePath;
    private String effects="";
    private Long [] historyDataNos;
    private String loginName;
    private String fileName;
    private KeyTransferDTO ktd = new KeyTransferDTO();//报表数据传递
    private TemplateDTO templateDTO;
    private KnowledgeCountResultDTO kdto=new KnowledgeCountResultDTO();
    
    private List<CommentDTO> listCommentDTO=new ArrayList<CommentDTO>();
    
	public List<CommentDTO> getListCommentDTO() {
		return listCommentDTO;
	}

	public void setListCommentDTO(List<CommentDTO> listCommentDTO) {
		this.listCommentDTO = listCommentDTO;
	}

	public KnowledgeCountResultDTO getKdto() {
		return kdto;
	}

	public void setKdto(KnowledgeCountResultDTO kdto) {
		this.kdto = kdto;
	}
	public KeyTransferDTO getKtd() {
		return ktd;
	}
	public void setKtd(KeyTransferDTO ktd) {
		this.ktd = ktd;
	}
	public String getEffects() {
		return effects;
	}
	public void setEffects(String effects) {
		this.effects = effects;
	}
	public File getImportFile() {
		return importFile;
	}
	public void setImportFile(File importFile) {
		this.importFile = importFile;
	}
	public Long[] getHistoryDataNos() {
		return historyDataNos;
	}
	public void setHistoryDataNos(Long[] historyDataNos) {
		this.historyDataNos = historyDataNos;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
    public Long getRid() {
		return rid;
	}
	public void setRid(Long rid) {
		this.rid = rid;
	}
	public Long getAid() {
        return aid;
    }
    public void setAid(Long aid) {
        this.aid = aid;
    }
    public Long getKid() {
        return kid;
    }
    public void setKid(Long kid) {
        this.kid = kid;
    }
    public KnowledgeQueryDTO getKnowledgeQueryDto() {
        return knowledgeQueryDto;
    }
    public void setKnowledgeQueryDto(KnowledgeQueryDTO knowledgeQueryDto) {
        this.knowledgeQueryDto = knowledgeQueryDto;
    }
    public PageDTO getKnowledges() {
        return knowledges;
    }
    public void setKnowledges(PageDTO knowledges) {
        this.knowledges = knowledges;
    }
    public int getPage() {
        return page;
    }
    public void setPage(int page) {
        this.page = page;
    }
    public int getRows() {
        return rows;
    }
    public void setRows(int rows) {
        this.rows = rows;
    }
    public Long[] getNos() {
        return nos;
    }
    public void setNos(Long[] nos) {
        this.nos = nos;
    }
    public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public KnowledgeDTO getKnowledgeDto() {
        return knowledgeDto;
    }
    public void setKnowledgeDto(KnowledgeDTO knowledgeDto) {
        this.knowledgeDto = knowledgeDto;
    }
    public List<KnowledgeDTO> getList() {
        return list;
    }	
	public int getEffect() {
		return effect;
	}
	public void setEffect(int effect) {
		this.effect = effect;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
    public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public void setList(List<KnowledgeDTO> list) {
		this.list = list;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public TemplateDTO getTemplateDTO() {
		return templateDTO;
	}
	public void setTemplateDTO(TemplateDTO templateDTO) {
		this.templateDTO = templateDTO;
	}
	/**
	 * 根据知识库编号来查询知识库
	 */
	public String findKnowledgeById() {
        knowledgeDto = knowledgeService.findById(kid);
        return "knowledgeDto";
    }
	/**
     * 分页查询所有知识.
     * @return SUCCESS
     */ 
	public String findAllKnowledges() {
        int start = (page - 1) * rows;
        knowledges = knowledgeService.findKnowledgesByPages(knowledgeQueryDto,start,rows,sidx,sord);
        if(knowledges!=null){
	        knowledges.setPage(page);
	        knowledges.setRows(rows);
        }
        return SUCCESS;
    }
    
    /**
	 * 自定义分页查询问题列表.
	 * 
	 */
	public String findKnowledgesPagerByCustomFilter(){
        int start = (page - 1) * rows;
        knowledgeQueryDto.setStart(start);
        knowledgeQueryDto.setLimit(rows);
        knowledges = knowledgeService.findKnowledgesPagerByCustomtFilter(knowledgeQueryDto,sidx,sord);
        knowledges.setPage(page);
        knowledges.setRows(rows);
        return SUCCESS;
	}

    /**
     * 保存知识信息.
     * @return SUCCESS
     */
	public String saveKnowledgeInfo() {
        knowledgeService.saveKnowledgeInfo(knowledgeDto);
        kid= knowledgeDto.getKid();
        return "kid";
    }

    /**
     * 删除知识信息.
     * @return SUCCESS
     */
    public String removeKnowledgeItems() {
    	try{
    		knowledgeService.removeKnowledgeItems(nos);
    	}catch(Exception ex){
    		throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE",ex);
    	}
        return SUCCESS;
    }

    /**
     * 修改知识信息.
     * @return SUCCESS
     */
    public String updateKnowledgeInfo() {
        knowledgeService.updateKnowledgeInfo(knowledgeDto);
        return SUCCESS;
    }
    /**
     * 修改知识信息.
     * @return SUCCESS
     */
    public String editKnowledgeInfo() {
        knowledgeService.updateKnowledgeInfo(knowledgeDto);
        return SUCCESS;
    }

    /**
     * 查找最新知识信息.
     * @return SUCCESS
     */
    public String findNewKnowledge() {
        list = knowledgeService.findNewKnowledge(loginName);
        return "simpleList";
    }

    /**
     * 查找热门知识.
     * @return SUCCESS
     */
    public String findPopularKnowledge() {
        list = knowledgeService.findPopularKnowledge(loginName);
        return "simpleList";
    }

    /**
     * 全文检索.
     * @return SUCCESS
     */
	public String fullSearch() {
		int start = (page - 1) * rows;
        knowledges = knowledgeService.fullSearch(knowledgeQueryDto,start,rows,sidx,sord);
        if(knowledges!=null){
	        knowledges.setPage(page);
	        knowledges.setRows(rows);
        }
        return SUCCESS;
    }

    /**
     * 删除附件.
     * @return SUCCESS
     */
    public String deleteAttachment() {
        knowledgeService.deleteAttachment(kid, aid);
        return SUCCESS;
    }
    
    
    
    /**
     * 查看知识详细信息.
     * @return String
     */
    public String showKnowledge(){
    	knowledgeDto = knowledgeService.findById(kid);
    	return "knowledgeDetails";
    }
    
    /**
     * 查看知识详细信息.wap端
     * @return String
     */
    public String showKnowledgeDetail(){
    	knowledgeDto = knowledgeService.findById(kid);
    	return "showKnowledgeDetails";
    }
    
    /**
     * 导出数据到Excel.
     * 
     */
    public String exportKnowledgeFromExcel(){
    	try{
    		knowledgeService.exportFindData(knowledgeQueryDto,sidx,sord);
    	}catch(Exception ex){
    		if(ex.getMessage().indexOf("JMS")!=-1){
        		throw new ApplicationException("label_expoort_error_jmsException");
        	}
    		LOGGER.error("export Knowledge From Excel",ex);
		}
    	return SUCCESS;
    }
    /**
     * 导出搜索结果列表
     */
    public String exportKnowledgeByFind(){
    	fileName="knowledge.csv";
    	try{
    		if (knowledgeQueryDto != null && knowledgeQueryDto.getFilterId() != null) {
    			/*fullTextQueryDto.setQueryString("");
    		}
    		if(fullTextQueryDto!=null && StringUtils.hasText(fullTextQueryDto.getQueryString())){
    			if(StringUtils.hasText(fullTextQueryDto.getAlias())){
    				fullTextQueryDto.setAlias("KnowledgeInfo");
    			}
    			fullTextQueryDto.setSortField(sidx);
    			fullTextQueryDto.setSortType(sord);
    			knowledgeService.exportFindDataFind(fullTextQueryDto);	
    		}else{*/
    			knowledgeService.exportFindData(knowledgeQueryDto,sidx,sord);
    		}
    		
    	}catch(Exception ex){
    		ex.printStackTrace();
    		if(ex.getMessage().indexOf("JMS")!=-1){
        		throw new ApplicationException("label_expoort_error_jmsException");
        	}
    		LOGGER.error("export Knowledge By Find",ex);
			
		}
    	return "exportFileSuccessful";
    }
    
    /**
     * 从Excel导入数据.
     */
    public String importKnowledgeFromExcel(){
    	try {
    		effects=knowledgeService.importKnowledge(importFile);
    	} catch (Exception e) {
    		effects="ERROR_CSV_FILE_IO";
    	}
    	return "importResult";
    	
    }
    
    
	/**
	 * 恢复数据.
	 */
	public String restoreData(){

		knowledgeService.restoreData(historyDataNos);
		return SUCCESS;
	}
	
	/**
	 * 审核知识.
	 */
	public String passKW(){
		knowledgeService.passKW(knowledgeDto);
		return SUCCESS;
	}
	/**
	 * 审核结果.
	 */
	public String appResult(){
		knowledgeDto=knowledgeService.appResult(kid);
		return "knowledgeDto";
	}
	/**
	 * 统计知识.
	 */
	public String countKnowledge(){
		kdto=knowledgeService.countKnowledge(knowledgeQueryDto);
		return "result";
	}
	/**
	 * 保存知识库模板
	 * 
	 */
	public String saveKnowledgeTemplate(){
		templateService.saveTemplate(knowledgeDto , templateDTO);
		return SUCCESS;
	}
	/**
	 *find dynamic Knowledge
	 */
	public String findKnowledgesByCrosstabCell(){
    	int start = (page - 1) * rows; 
    	knowledges = knowledgeService.findKnowledgesByCrosstabCell(ktd,start,rows,sidx,sord);
    	knowledges.setPage(page);
        knowledges.setRows(rows);
    	return SUCCESS; 
    }
	/**
	 * 重建知识库索引
	 * @return
	 *//*
	public String reindex(){
		knowledgeService.reindexKnowledgeInfo();
    	return SUCCESS; 
    }*/
	
	/**
	 * 根据标题查询知识
	 * @return
	 */
	public String findKnowledgeByTitle(){
		knowledgeDto = knowledgeService.findUniqueBy("title", knowledgeDto.getTitle());
		return "knowledgeDto"; 
	}
	/**
	 * 根据id 查询知识库评价
	 * @return
	 */
	public String findByIdCommentKnowledge(){
	    listCommentDTO =knowledgeService.findByIdCommentKnowledge(kid);
		return "findByIdCommentKnowledge";
	}
	
	public String saveCommentKnowledge(){
		knowledgeService.saveStars(knowledgeDto);
		return SUCCESS;
	}
	
}