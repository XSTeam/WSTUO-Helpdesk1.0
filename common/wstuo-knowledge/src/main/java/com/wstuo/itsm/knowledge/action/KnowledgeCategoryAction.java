/**
*author:Eileen
*date:010-9-10
*description:
*/
package com.wstuo.itsm.knowledge.action;

import com.opensymphony.xwork2.ActionSupport;

import com.wstuo.common.config.category.dto.CategoryDTO;
import com.wstuo.common.config.category.dto.CategoryTreeViewDTO;
import com.wstuo.itsm.knowledge.service.IKnowledgeCategoryService;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 知识库分类的Action类
 * @author QXY
 *
 */
@SuppressWarnings("serial")
public class KnowledgeCategoryAction extends ActionSupport {

    @Autowired
    private IKnowledgeCategoryService knowledgecategoryService;
    private CategoryDTO knowledgeDto;
    private CategoryTreeViewDTO knowledgeTreeDto;

    public CategoryDTO getKnowledgeDto() {

        return knowledgeDto;
    }

    public void setKnowledgeDto(CategoryDTO knowledgeDto) {

        this.knowledgeDto = knowledgeDto;
    }

    public CategoryTreeViewDTO getKnowledgeTreeDto() {

        return knowledgeTreeDto;
    }

    public void setKnowledgeTreeDto(CategoryTreeViewDTO knowledgeTreeDto) {

        this.knowledgeTreeDto = knowledgeTreeDto;
    }

    /**
    * view knowledge category tree
    * @return SUCCESS
    */
    public String getKnowledgeTree() {

        knowledgeTreeDto = knowledgecategoryService.findKnowledgeCategoryTreeDtos("KnowledgeCategory").get(0);

        return SUCCESS;
    }

    /**
     * add knowledge tree
     * @return SUCCESS
     */
    public String save() {

        knowledgecategoryService.saveKnowledgeCategory(knowledgeDto);

        return SUCCESS;
    }

    /**
     * update knowledge tree
     * @return SUCCESS
     */
    public String update() {

        knowledgecategoryService.updateKnowledgeCategory(knowledgeDto);

        return SUCCESS;
    }

    /**
     * change Parent
     * @return SUCCESS
     */
    public String changeParent() {

        knowledgecategoryService.changeKnowledgeCategory(knowledgeDto);

        return SUCCESS;
    }

    /**
     * remove knowledge tree
     * @return SUCCESS
     */
    public String remove() {

        knowledgecategoryService.removeKnowledgeCategory(knowledgeDto.getCno());

        return SUCCESS;
    }
}