package com.wstuo.itsm.knowledge.service;

import java.io.File;
import java.util.List;

import com.wstuo.common.config.customfilter.dto.KeyTransferDTO;
import com.wstuo.itsm.knowledge.dto.CommentDTO;
import com.wstuo.itsm.knowledge.dto.KnowledgeCountResultDTO;
import com.wstuo.itsm.knowledge.dto.KnowledgeDTO;
import com.wstuo.itsm.knowledge.dto.KnowledgeQueryDTO;
import com.wstuo.itsm.knowledge.entity.KnowledgeInfo;
import com.wstuo.common.tools.dto.ExportQueryDTO;
import com.wstuo.common.dto.PageDTO;


/**
 * 知识库Service接口类.
 * @author QXY
 */
public interface IKnowledgeService {

	public static final String ENTITYCLASS="com.wstuo.itsm.knowledge.entity.KnowledgeInfo";
	public static final String FILTERCATEGORY="knowledgeInfo";
	public static final String MODULECATEGORY="KnowledgeInfo";
	/**
	 * 保存知识信息.
	 * @param dto 知识库DTO类
	 */
    void saveKnowledgeInfo(KnowledgeDTO dto);
    
    /**
     * 自定义分页查询知识库列表.
     * @param qdto
     * @param sidx
     * @param sord
     * @return PageDTO
     */
	PageDTO findKnowledgesPagerByCustomtFilter(KnowledgeQueryDTO qdto,String sidx,String sord);

    /**
     * 删除知识库信息.
     * @param nos 知识编号数组.
     */
    void removeKnowledgeItems(Long[] nos);

    /**
     * 修改知识库信息.
     * @param dto 知识库DTO类
     */
    void updateKnowledgeInfo(KnowledgeDTO dto);

    /**
     * 查找最新知识.
     * @return List<KnowledgeDTO> 最新知识集合.
     */
    List<KnowledgeDTO> findNewKnowledge(String loginName);

    /**
     * 查找热门知识.
     * @return List<KnowledgeDTO> 热门知识集合.
     */
    List<KnowledgeDTO> findPopularKnowledge(String loginName);

    /**
     * 根据ID查找知识信息.
     * @param id 编号
     * @return KnowledgeDTO 知识库DTO类
     */
    KnowledgeDTO findById(Long id);

    /**
     * 分页查找知识信息.
     * @param dto 查询DTO
     * @param start 起始行
     * @param limit 结束行
     * @return PageDTO 分页数据
     */
    PageDTO findKnowledgesByPages(KnowledgeQueryDTO dto, int start, int limit,String sidx,String sord);

    /**
     * 删除附件.
     * @param kid 知识编号.
     * @param aid 附件编号.
     */
    void deleteAttachment(Long kid,Long aid);
    /**
     * 导出知识库
     * @param dto
     */
    void exportKnowledges(ExportQueryDTO dto);
   /**
    * 导出搜索的知识
    * @param dto
    * @param start
    * @param limit
    * @param sidx
    * @param sord
    */
    void exportFindData(KnowledgeQueryDTO dto,String sidx,String sord);
    /**
     * 导出过滤器搜索结果
     * @param queryDTO
     * @param page
     * @param rows
     */
  // exportFindDataFind(FullTextQueryDTO<KnowledgeInfo> queryDTO);
    /**
     * 导入知识库
     * @param importFile
     * 
     */
    String importKnowledge(File importFile);
    /**
     * 恢复数据
     * @param historyDataNos
     */
    void restoreData(Long [] historyDataNos);
    
    /**
	 * 分页全文检索.
	 * @param queryDTO
	 * @param page
	 * @param rows
	 * 
	 */
    PageDTO fullSearch(KnowledgeQueryDTO queryDTO,int start,int limit, String sidx, String sord);
    
    
    /**
     * 批量审核知识.
     * @param dto
     */
    void passKW(KnowledgeDTO dto);
    
    /**
     * 按状态统计知识数量.
     * @param flag
     */
    KnowledgeCountResultDTO countKnowledge(KnowledgeQueryDTO dto);
    
    /**
     * 审核结果
     * @param kid
     */
    KnowledgeDTO appResult(Long kid);
    /**
     * find dynamic Knowledge
     * @param rows
     * @param start
     * @param sidx
     * @param sord
     */
    PageDTO findKnowledgesByCrosstabCell(KeyTransferDTO ktd, int start, int rows, String sidx, String sord);
    /**
	 * 全文检索知识库的公共方法
	 */
	//List<KnowledgeInfo> fullTextSearch(FullTextQueryDTO<KnowledgeInfo> queryDTO,int pageNumber, int pageSize );
	/**
	 * 重建知识库索引
	 * @return
	 */
	//void reindexKnowledgeInfo();
	
	KnowledgeDTO findUniqueBy(String propertyName, Object value);
	
    List<CommentDTO> findByIdCommentKnowledge(Long kid);
    
    void saveStars(KnowledgeDTO dto);
}