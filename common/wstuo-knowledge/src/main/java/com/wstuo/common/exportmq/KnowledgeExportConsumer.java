package com.wstuo.common.exportmq;

import org.springframework.beans.factory.annotation.Autowired;

import com.wstuo.common.activemq.consumer.IMessageConsumer;
import com.wstuo.common.tools.dto.ExportQueryDTO;
import com.wstuo.itsm.knowledge.service.IKnowledgeService;
/**
 * MQ知识库导出
 * @author will
 *
 */
public class KnowledgeExportConsumer implements IMessageConsumer{

    @Autowired
    private IKnowledgeService knowledgeService;
	@Override
	public void messageConsumer(Object obj) {
		ExportQueryDTO dto=(ExportQueryDTO) obj;
		knowledgeService.exportKnowledges(dto);
	}

}
