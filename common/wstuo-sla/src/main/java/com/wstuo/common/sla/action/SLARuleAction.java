package com.wstuo.common.sla.action;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.opensymphony.xwork2.ActionSupport;

import com.wstuo.common.rules.entity.RuleConstraint;
import com.wstuo.common.sla.comparator.RuleConstraintComparator;
import com.wstuo.common.sla.dto.SLARuleDTO;
import com.wstuo.common.sla.dto.SLARuleQueryDTO;
import com.wstuo.common.sla.entity.SLARule;
import com.wstuo.common.sla.service.ISLARuleService;
import com.wstuo.common.dto.PageDTO;
import com.wstuo.common.exception.ApplicationException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * SLA规则Action类
 * @author QXY
 */
@SuppressWarnings("serial")
public class SLARuleAction extends ActionSupport {


	/**
	 * SLARuleAction错误日志
	 */
    public static final Logger LOGGER = Logger.getLogger(SLARuleAction.class);
    /**
     * SLA规则物业类接口
     */
    @Autowired
    private ISLARuleService slaRuleService;
    /**
     * 页数
     */
    private int page = 1;
    /**
     * 记录数
     */
    private int rows = 10;
    /**
     * 分页DTO
     */
    private PageDTO pageDTO;
    /**
     * sla规则DTO
     */
    private SLARuleDTO slaRuleDTO = new SLARuleDTO();
    
    private List<SLARuleDTO> slaRuleDTOList = new ArrayList<SLARuleDTO>();
    /**
     * sla规则查询DTO
     */
    private SLARuleQueryDTO slaRuleQueryDTO = new SLARuleQueryDTO();
    /**
     * sla规则实体
     */
    private SLARule rule = new SLARule();
    /**
     * sla协议No
     */
    private Long contractNo;
    /**
     * 规则NO
     */
    private Long ruleNo;
    /**
     * 规则No数组
     */
    private Long[] ruleNos;
    /**
     * 规则文件
     */
    private String ruleJson;
    /**
     * 结果
     */
	private String effect;
	 /**
	  * 导入文件
	  */
	private File importFile;
	 /**
	  * 导出文件
	  */
	private InputStream exportFile;
	 /**
	  * 排序规则
	  */
	private String sord;
	 /**
	  * 排序的属性
	  */
	private String sidx;
	 /**
	  * 返回结果
	  */
	private boolean result;
    
    public List<SLARuleDTO> getSlaRuleDTOList() {
		return slaRuleDTOList;
	}

	public void setSlaRuleDTOList(List<SLARuleDTO> slaRuleDTOList) {
		this.slaRuleDTOList = slaRuleDTOList;
	}

	public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}

	public File getImportFile() {
		return importFile;
	}

	public void setImportFile(File importFile) {
		this.importFile = importFile;
	}

	public InputStream getExportFile() {
		return exportFile;
	}

	public void setExportFile(InputStream exportFile) {
		this.exportFile = exportFile;
	}

	public String getRuleJson() {
		return ruleJson;
	}

	public void setRuleJson(String ruleJson) {
		this.ruleJson = ruleJson;
	}

	public Long[] getRuleNos() {

        return ruleNos;
    }

    public void setRuleNos(Long[] ruleNos) {

        this.ruleNos = ruleNos;
    }

    public Long getRuleNo() {

        return ruleNo;
    }

    public void setRuleNo(Long ruleNo) {

        this.ruleNo = ruleNo;
    }

    public ISLARuleService getSlaRuleService() {

        return slaRuleService;
    }

    public void setSlaRuleService(ISLARuleService slaRuleService) {

        this.slaRuleService = slaRuleService;
    }

    public int getPage() {

        return page;
    }

    public SLARule getRule() {

        return rule;
    }

    public void setRule(SLARule rule) {

        this.rule = rule;
    }

    public void setPage(int page) {

        this.page = page;
    }

    public int getRows() {

        return rows;
    }

    public void setRows(int rows) {

        this.rows = rows;
    }

    public PageDTO getPageDTO() {

        return pageDTO;
    }

    public void setPageDTO(PageDTO pageDTO) {

        this.pageDTO = pageDTO;
    }

    public SLARuleDTO getSlaRuleDTO() {

        return slaRuleDTO;
    }

    public void setSlaRuleDTO(SLARuleDTO slaRuleDTO) {

        this.slaRuleDTO = slaRuleDTO;
    }

    public SLARuleQueryDTO getSlaRuleQueryDTO() {

        return slaRuleQueryDTO;
    }

    public void setSlaRuleQueryDTO(SLARuleQueryDTO slaRuleQueryDTO) {

        this.slaRuleQueryDTO = slaRuleQueryDTO;
    }

    public Long getContractNo() {

        return contractNo;
    }

    public void setContractNo(Long contractNo) {

        this.contractNo = contractNo;
    }
    
    
    
	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	/**
	 * SLA规则查询
	 * @return String sla查询DTO
	 */
    public String find() {
        slaRuleQueryDTO.setContractNo(contractNo);
        int start = (page - 1) * rows;
        pageDTO = slaRuleService.findPagerByContractNo(slaRuleQueryDTO, sidx,sord,start, rows);
        pageDTO.setPage(page);
        pageDTO.setRows(rows);
        return SUCCESS;
    }
    
    /**
     * 查询所有sla规则
     * */
    public String findAll(){
    	slaRuleQueryDTO.setContractNo(contractNo);
    	slaRuleDTOList = slaRuleService.findAll(slaRuleQueryDTO);
    	return "showSLARuleList";
    }

    /**
	 * SLA规则查询
	 * @return String sla规则实体
	 */
    public String findSLARuleb() {
    	 rule = slaRuleService.findSLARuleById(ruleNo);
    	 return "rulebb";

    }
    
    
    
    /**
	 * SLA规则查询
	 * @return String sla规则实体
	 */
    @SuppressWarnings("unchecked")
	public String findSLARule() {
        rule = slaRuleService.findSLARuleById(ruleNo);
        if(rule!=null){
	        StringBuffer sb=new StringBuffer();
	        sb.append("{");
	        //基本信息
	        sb.append("\"ruleNo\":\""+rule.getRuleNo()+"\"");
	        sb.append(",\"ruleName\":\""+rule.getRuleName()+"\"");
	        sb.append(",\"salience\":\""+rule.getSalience()+"\"");
	        sb.append(",\"rday\":\""+rule.getRday()+"\"");
	        sb.append(",\"rhour\":\""+rule.getRhour()+"\"");
	        sb.append(",\"rminute\":\""+rule.getRminute()+"\"");
	        sb.append(",\"fday\":\""+rule.getFday()+"\"");
	        sb.append(",\"fhour\":\""+rule.getFhour()+"\"");
	        sb.append(",\"fminute\":\""+rule.getFminute()+"\"");
	        sb.append(",\"responseRate\":\""+rule.getResponseRate()+"\"");
	        sb.append(",\"completeRate\":\""+rule.getCompleteRate()+"\"");
	        sb.append(",\"includeHoliday\":\""+rule.getIncludeHoliday()+"\"");
	        sb.append(",\"dataFlag\":\""+rule.getDataFlag()+"\"");
	        
	        //规则集
	        List<RuleConstraint> constraints=rule.getCondition().getConstraints();
	        
	        RuleConstraintComparator rcc = new RuleConstraintComparator();
	        Collections.sort(constraints,rcc);
	        
	        sb.append(",\"constraints\":[");
	        
	        for(RuleConstraint c:constraints){
	        	sb.append("{");
	        	sb.append("\"conNo\":\""+c.getConNo()+"\"");
	        	sb.append(",\"propertyName\":\""+c.getPropertyName()+"\"");
	        	sb.append(",\"propertyValue\":\""+c.getPropertyValue()+"\"");
	        	sb.append(",\"propertyValueName\":\""+c.getPropertyValueName()+"\"");
	        	sb.append(",\"dataType\":\""+c.getDataType()+"\"");
	        	sb.append(",\"andOr\":\""+c.getAndOr()+"\"");
	        	sb.append(",\"sequence\":\""+c.getSequence()+"\"");
	        	sb.append("},");
	        }
	        
	        sb.append("]");
	        
	        //动作集（1个）
	        //sb.append(",\"actionNo\":\""+rule.getActions().get(0).getActionNo()+"\"");
	        
	        //patten
	        sb.append(",\"rulePatternNo\":\""+rule.getCondition().getRulePatternNo()+"\"");
	        sb.append("}");
	        
	        ruleJson=sb.toString();
        
        }
        return "showslarule";
    }

    /**
     * 修改SLA规则 
     * @return SUCCESS 无
     */
    public String merge() {

        slaRuleService.mergeRuleEntity(rule);

        return SUCCESS;
    }

    /**
     * 保存SLA规则
     * @return SUCCESS 无
     */
    public String save() {

        slaRuleService.saveRuleEntity(rule);
        return SUCCESS;
    }

    /**
     * 删除SLA规则
     * @return SUCCESS 无
     */
    public String delete() {

    	
    	try{
    		slaRuleService.removeRules(ruleNos);
    	}
    	catch(Exception ex){
    		if("ERROR_SYSTEM_DATA_CAN_NOT_DELETE".indexOf(ex.getMessage())!=-1){
    			throw new ApplicationException("ERROR_SYSTEM_DATA_CAN_NOT_DELETE",ex);
    		}else{
    			throw new ApplicationException("ERROR_DATA_CAN_NOT_DELETE",ex);
    		}
    		
    	}
        return SUCCESS;
    }
    
    
	/**
	 * 导入SLA.
	 * @return 导入结果
	 */
	public String importSLARule(){
		
		try{
			effect=slaRuleService.importSLARule(importFile.getAbsolutePath());
		}
		catch(Exception ex){
			
			throw new ApplicationException("msg_dc_importFailure",ex);
		}
		
		return "effect";
	}
	
	/**
	 * 根据drl文件导入SLA.
	 * @return 导入结果
	 */
	public String importSLARule_drl(){
		
		try{
			effect=slaRuleService.importSLARule_drl(importFile.getAbsolutePath(),contractNo);
		}
		catch(Exception ex){
			
			throw new ApplicationException("msg_dc_importFailure",ex);
		}
		
		return "effect";
	}
	
	/**
	 * 导出sla
	 * @return 导出文件
	 */
	public String exportSLARule(){
		exportFile=slaRuleService.exportSLARule(slaRuleQueryDTO);
		return "exportFile";
	}
	/**
	 * 判断SLA规则名称是否存在
	 * @return 返回结果
	 */
	public String slaNameExist(){
		result = slaRuleService.slaNameExist(slaRuleQueryDTO.getRuleName());
		return "result";
	}
	
	/**
	 * 根据规则NO查询sla规则
	 * @return sla规则DTO
	 */
    public String findBySlaNo(){
    	slaRuleDTO=slaRuleService.findBySlaNo(slaRuleDTO.getRuleNo());
    	return "findByName";
    }
	
}