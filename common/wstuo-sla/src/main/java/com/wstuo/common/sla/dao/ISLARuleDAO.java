package com.wstuo.common.sla.dao;

import java.util.List;

import com.wstuo.common.sla.dto.SLARuleQueryDTO;
import com.wstuo.common.sla.entity.SLARule;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;


/**
 * SLA规则DAO类接口
 * @author QXY
 *
 */
public interface ISLARuleDAO extends IEntityDAO<SLARule> {

	/**
	 * 分页查找数据.
	 * @param qdto 查询DTO
	 * @param start 开始数据行
	 * @param limit 每页数据条数
	 * @return PageDTO 分页数据
	 */
    PageDTO findPager(SLARuleQueryDTO qdto, String sidx, String sord,int start, int limit);
    
    /**
     * 查询SLARule
     * @param qdto
     * @return SLARule List
     */
    List<SLARule> findSLARule(SLARuleQueryDTO qdto);
    
    /**
     * 查询SLARule
     * @param qdto
     * @return SLARule List
     */
   SLARule findUniqueSLARule(SLARuleQueryDTO qdto);
    
   
}