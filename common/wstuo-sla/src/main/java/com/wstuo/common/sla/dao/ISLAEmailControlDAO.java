package com.wstuo.common.sla.dao;


import java.util.List;

import com.wstuo.common.sla.entity.SLAEmailControl;
import com.wstuo.common.dao.IEntityDAO;

/**
 * sla协议发送邮件业务接口
 * @author QXY
 *
 */
public interface ISLAEmailControlDAO extends IEntityDAO<SLAEmailControl>{
	/**
	 * 查询根据sla协议发送的邮件
	 */
	public List<SLAEmailControl> findSLAEmailControl(SLAEmailControl slaEmailControl);
	
	
}
