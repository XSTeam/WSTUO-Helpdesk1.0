package com.wstuo.common.sla.comparator;

import java.util.Comparator;

import com.wstuo.common.rules.entity.RuleConstraint;
/**
 * 规则约束比较器
 * @author QXY
 *
 */
@SuppressWarnings("rawtypes")
public class RuleConstraintComparator implements Comparator{
	
	public int compare(Object arg0, Object arg1) {
	    int result = 0;
		RuleConstraint a0 = (RuleConstraint)arg0;
		RuleConstraint a1 = (RuleConstraint)arg1;
		//首先比较编号，如果编号相同，则比较名字
		Integer t1 = (Integer)a0.getSequence();
		Integer t2 = (Integer)a1.getSequence();
		int flag=t1.compareTo(t2);
		if(flag==0){
		    result = a0.getPropertyValueName().compareTo(a1.getPropertyValueName());
		}else{
		    result = flag;
		}
		return result;
	}
}
