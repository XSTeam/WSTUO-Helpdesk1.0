package com.wstuo.common.sla.dao;

import java.util.List;

import com.wstuo.common.sla.dto.PromoteRuleQueryDTO;
import com.wstuo.common.sla.entity.PromoteRule;
import com.wstuo.common.dao.IEntityDAO;
import com.wstuo.common.dto.PageDTO;

/**
 * 自动升级规则DAO类接口.
 * @author QXY
 *
 */
public interface IPromoteRuleDAO extends IEntityDAO<PromoteRule>{
	
	/**
	 * 分页查找自动升级列表.
	 * @param qdto 查询DTO
	 * @param start 开始数据行
	 * @param limit 每页数据条数
	 * @return PageDTO 分页数据
	 */
	PageDTO findPager(PromoteRuleQueryDTO qdto, int start, int limit,String sidx,String sord);

	 /**
     * find PromoteRule By ruleType
     * @param ruleType
     * @return List<PromoteRule>
     */
    public List<PromoteRule> findPromoteRuleByruleType(String ruleType);
}
